﻿Public Class frmcheckUp04Class
    Dim _mascheckup_project As mascheckup_project
    Dim condb As ConnecDBRYH
    Dim dtset As dtCheckUP

    Public Sub New(ByRef dtsetCheckup As dtCheckUP)
        dtset = dtsetCheckup
        condb = ConnecDBRYH.NewConnection
    End Sub
    Public Property setmascheckup_project As mascheckup_project
        Get
            Return _mascheckup_project
        End Get
        Set(value As mascheckup_project)
            _mascheckup_project = value
        End Set
    End Property

    Structure mascheckup_project
        Dim idmascheckup_project As String
        Dim project_name As String
        Dim txtEyeLR As String
        Dim txtBlindness As String
        Dim txtEye As String
        Dim txtTooth As String
        Dim GridControl1 As String
        Dim txtblood As String
        Dim txtUrineresult As String
        Dim txtVDRLresult As String
        Dim txtCEAResult As String
        Dim txtXrayResult As String
        Dim txtHivResult As String
        Dim txtVirusBResult As String
        Dim txtVirusAResult As String
        Dim CheckExamination As Boolean
        Dim CheckExamination2 As Boolean
        Dim checkBlood As Boolean
        Dim checkUrineAnalysis As Boolean
        Dim checkChemicalLab As Boolean
        Dim checkChemicallab2 As Boolean
        Dim status As Boolean
    End Structure

    Public Sub getMascheckup_project()
        Dim sql As String
        dtset.Tables("mascheckup_pro").Clear()
        sql = "SELECT * FROM mascheckup_project ;"
        condb.GetTable(sql, dtset.Tables("mascheckup_pro"))

    End Sub

    Public Sub getmascheckup_project_setting(ByVal idproject As String)
        Dim sql As String
        dtset.Tables("mascheckup_project").Clear()
        sql = "SELECT *  FROM mascheckup_project WHERE idmascheckup_project = '" & idproject & "'; "
        condb.GetTable(sql, dtset.Tables("mascheckup_project"))
    End Sub

    Public Sub Insertmascheckup_project()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        'Dim blood_result As String

        sql += "project_name , "
        sql1 += "'" & _mascheckup_project.project_name & "' ,"

        sql += "txtEyeLR , "
        sql1 += "" & If(_mascheckup_project.txtEyeLR Is Nothing, "NULL", _mascheckup_project.txtEyeLR) & " ,"

        sql += "txtEye , "
        sql1 += "" & If(_mascheckup_project.txtEyeLR Is Nothing, "NULL", _mascheckup_project.txtEye) & " ,"


        sql += "txtBlindness , "
        sql1 += "" & If(_mascheckup_project.txtBlindness Is Nothing, "NULL", _mascheckup_project.txtBlindness) & " ,"

        sql += "GridControl1 , "
        sql1 += "" & If(_mascheckup_project.GridControl1 Is Nothing, "NULL", _mascheckup_project.GridControl1) & " ,"


        sql += "txtblood , "
        sql1 += "" & If(_mascheckup_project.txtblood Is Nothing, "NULL", _mascheckup_project.txtblood) & " ,"

        sql += "txtUrineresult , "
        sql1 += "" & If(_mascheckup_project.txtUrineresult Is Nothing, "NULL", _mascheckup_project.txtUrineresult) & " ,"

        sql += "txtVDRLresult , "
        sql1 += "" & If(_mascheckup_project.txtVDRLresult Is Nothing, "NULL", _mascheckup_project.txtVDRLresult) & " ,"


        sql += "txtCEAResult , "
        sql1 += "" & If(_mascheckup_project.txtCEAResult Is Nothing, "NULL", _mascheckup_project.txtCEAResult) & " ,"

        sql += "txtXrayResult , "
        sql1 += "" & If(_mascheckup_project.txtXrayResult Is Nothing, "NULL", _mascheckup_project.txtXrayResult) & " ,"

        sql += "txtHivResult , "
        sql1 += "" & If(_mascheckup_project.txtHivResult Is Nothing, "NULL", _mascheckup_project.txtHivResult) & " ,"


        sql += "txtVirusBResult , "
        sql1 += "" & If(_mascheckup_project.txtVirusBResult Is Nothing, "NULL", _mascheckup_project.txtVirusBResult) & " ,"

        sql += "txtVirusAResult , "
        sql1 += "" & If(_mascheckup_project.txtVirusAResult Is Nothing, "NULL", _mascheckup_project.txtVirusAResult) & " ,"


        sql += "CheckExamination , "
        sql1 += "" & _mascheckup_project.CheckExamination & " ,"


        sql += "CheckExamination2 , "
        sql1 += "" & _mascheckup_project.CheckExamination2 & " ,"

        sql += "checkBlood , "
        sql1 += "" & _mascheckup_project.checkBlood & " ,"


        sql += "checkUrineAnalysis , "
        sql1 += "" & _mascheckup_project.checkUrineAnalysis & " ,"

        sql += "checkChemicalLab , "
        sql1 += "" & _mascheckup_project.checkChemicalLab & " ,"
        sql += "status , "
        sql1 += "" & _mascheckup_project.status & " ,"

        sql += "checkChemicallab2  "
        sql1 += "" & _mascheckup_project.checkChemicallab2 & " "


        sql2 = "INSERT mascheckup_project ( " & sql & ")  VALUES (" & sql1 & ");"
        condb.ExecuteNonQuery(sql2)
    End Sub
    Public Sub Updatemascheckup_project()

        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql1 = "idmascheckup_project = "
        sql1 += "'" & _mascheckup_project.idmascheckup_project & "' "





        sql = "project_name = "
        sql += "'" & If(_mascheckup_project.project_name Is Nothing, "NULL", _mascheckup_project.project_name) & "',"


        sql += "txtEyeLR = "
        sql += "" & If(_mascheckup_project.txtEyeLR Is Nothing, "NULL", _mascheckup_project.txtEyeLR) & " ,"

        sql += "txtEye = "
        sql += "" & If(_mascheckup_project.txtEye Is Nothing, "NULL", _mascheckup_project.txtEye) & " ,"


        sql += "txtBlindness = "
        sql += "" & If(_mascheckup_project.txtBlindness Is Nothing, "NULL", _mascheckup_project.txtBlindness) & " ,"

        sql += "GridControl1 = "
        sql += "" & If(_mascheckup_project.GridControl1 Is Nothing, "NULL", _mascheckup_project.GridControl1) & " ,"


        sql += "txtblood = "
        sql += "" & If(_mascheckup_project.txtblood Is Nothing, "NULL", _mascheckup_project.txtblood) & " ,"

        sql += "txtUrineresult = "
        sql += "" & If(_mascheckup_project.txtUrineresult Is Nothing, "NULL", _mascheckup_project.txtUrineresult) & " ,"

        sql += "txtVDRLresult = "
        sql += "" & If(_mascheckup_project.txtVDRLresult Is Nothing, "NULL", _mascheckup_project.txtVDRLresult) & " ,"


        sql += "txtCEAResult = "
        sql += "" & If(_mascheckup_project.txtCEAResult Is Nothing, "NULL", _mascheckup_project.txtCEAResult) & " ,"

        sql += "txtXrayResult = "
        sql += "" & If(_mascheckup_project.txtXrayResult Is Nothing, "NULL", _mascheckup_project.txtXrayResult) & " ,"

        sql += "txtHivResult = "
        sql += "" & If(_mascheckup_project.txtHivResult Is Nothing, "NULL", _mascheckup_project.txtHivResult) & " ,"


        sql += "txtVirusBResult = "
        sql += "" & If(_mascheckup_project.txtVirusBResult Is Nothing, "NULL", _mascheckup_project.txtVirusBResult) & " ,"

        sql += "txtVirusAResult = "
        sql += "" & If(_mascheckup_project.txtVirusAResult Is Nothing, "NULL", _mascheckup_project.txtVirusAResult) & " ,"


        sql += "CheckExamination = "
        sql += "" & _mascheckup_project.CheckExamination & " ,"


        sql += "CheckExamination = "
        sql += "" & _mascheckup_project.CheckExamination2 & " ,"

        sql += "checkBlood = "
        sql += "" & _mascheckup_project.checkBlood & " ,"

        sql += "checkUrineAnalysis = "
        sql += "" & _mascheckup_project.checkUrineAnalysis & " ,"

        sql += "checkChemicalLab = "
        sql += "" & _mascheckup_project.checkChemicalLab & " ,"
        sql += "status = "
        sql += "" & _mascheckup_project.status & " ,"



        sql += "checkChemicallab2 = "
        sql += "" & _mascheckup_project.checkChemicallab2 & " "


        sql2 = "UPDATE mascheckup_project SET " & sql & "  WHERE  " & sql1 & ";"
        condb.ExecuteNonQuery(sql2)
    End Sub
End Class
