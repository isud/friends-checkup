﻿Public Class frmCheckUp06Class
    Dim _mascheckup As mascheckup
    Dim condb As ConnecDBRYH
    Dim dtset As dtCheckUP

    Structure mascheckup

        Dim idmascheckup As String
        Dim name As String
        Dim name2 As String
        Dim description As String
        Dim form As String
        Dim formcode As String
        Dim formname As String
        Dim status As Boolean
        Dim name_dt As String
        Dim name2_dt As String
        Dim idmascheckupdt As String
        Dim f_result_all As Boolean


    End Structure
    Public Property setmascheckup As mascheckup
        Get
            Return _mascheckup
        End Get
        Set(value As mascheckup)
            _mascheckup = value
        End Set
    End Property

    Public Sub New(ByRef dtsetCheckup As dtCheckUP)
        dtset = dtsetCheckup
        condb = ConnecDBRYH.NewConnection
    End Sub
    Public Sub getMascheckup()
        dtset.Tables("mascheckup").Clear()
        Dim sql As String
        sql = "SELECT * FROM mascheckup  LEFT JOIN mascheckupdt ON mascheckupdt.idmascheckup = mascheckup.idmascheckup  WHERE det = 1 "
        condb.GetTable(sql, dtset.Tables("mascheckup"))

    End Sub
    Public Sub getMasSetForm()
        Dim sql As String
        sql = "SELECT form FROM  mascheckup WHERE det = 1 GROUP BY form;"
        condb.GetTable(sql, dtset.Tables("mastext"))
    End Sub
    Public Sub getMascheckupDt(ByVal idmascheckup As String)
        dtset.Tables("mascheck").Clear()
        Dim sql As String
        sql = "SELECT mascheckup.name , mascheckup.form ,mascheckup.name2, mascheckup.description , mascheckup.formcode, mascheckup.formname , mascheckup.idmascheckup, mascheckupdt.idmascheckupdt , mascheckupdt.name AS name_dt , mascheckupdt.name2 as name2_dt ,f_result_all ,mascheckupdt.status  FROM mascheckup LEFT JOIN mascheckupdt ON mascheckupdt.idmascheckup = mascheckup.idmascheckup WHERE mascheckup.idmascheckup = '" & idmascheckup & "';"
        condb.GetTable(sql, dtset.Tables("mascheck"))


    End Sub


    Public Sub Insertmascheckup()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql += "name , "
        sql1 += "'" & _mascheckup.name & "' ,"

        sql += "name2 , "
        sql1 += "'" & _mascheckup.name2 & "' ,"


        sql += "description , "
        sql1 += "'" & _mascheckup.description & "' ,"
        sql += "form , "
        sql1 += "'" & _mascheckup.form & "' ,"


        sql += "det , "
        sql1 += "1,"



        sql += "formcode , "
        sql1 += "" & If(_mascheckup.formcode Is Nothing, "NULL", _mascheckup.formcode) & " ,"


        sql += "formname  "
        sql1 += "'" & _mascheckup.formname & "' "




        sql2 = "INSERT INTO mascheckup ( " & sql & " ) VALUES ( " & sql1 & " );SELECT LAST_INSERT_ID();"

        _mascheckup.idmascheckup = condb.ExecuteScalar(sql2)


        InsertMascheckup_Dt()
    End Sub
    Public Sub InsertMascheckup_Dt()

        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql += "name , "
        sql1 += "'" & _mascheckup.name_dt & "' ,"

        sql += "name2 , "
        sql1 += "'" & _mascheckup.name2_dt & "' ,"


        sql += "idmascheckup , "
        sql1 += "'" & _mascheckup.idmascheckup & "' ,"




        sql += "f_result_all , "
        sql1 += "" & _mascheckup.f_result_all & " ,"



        sql += "f_sex , "
        sql1 += "0 ,"



        sql += "status "
        sql1 += "" & _mascheckup.status & " "


        sql2 = "INSERT INTO mascheckupdt ( " & sql & " ) VALUES ( " & sql1 & " );"

        condb.ExecuteNonQuery(sql2)



    End Sub



    Public Sub UpdateMascheckup()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String



        sql = "name = "
        sql += "'" & _mascheckup.name & "',"


        sql += "name2 = "
        sql += "'" & _mascheckup.name2 & "',"


        sql += "form = "
        sql += "'" & _mascheckup.form & "',"
        sql += "formcode = "
        sql += "'" & _mascheckup.formcode & "',"

        sql += "description = "
        sql += "'" & _mascheckup.description & "',"




        sql += "formname = "
        sql += "'" & _mascheckup.name2 & "' "






        sql2 = "UPDATE  mascheckup SET " & sql & " WHERE idmascheckup ='" & _mascheckup.idmascheckup & "';"
        condb.ExecuteNonQuery(sql2)

    End Sub


    Public Sub UpdateMascheckup_dt()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        sql = "name = "
        sql += "'" & _mascheckup.name_dt & "',"


        sql += "name2 = "
        sql += "'" & _mascheckup.name2_dt & "',"


        sql += "idmascheckup = "
        sql += "'" & _mascheckup.idmascheckup & "',"

        sql += "f_result_all = "
        sql += "" & _mascheckup.f_result_all & ","

        sql += "status = "
        Sql += "" & _mascheckup.status & " "
        sql2 = "UPDATE  mascheckup SET " & sql & " WHERE idmascheckup ='" & _mascheckup.idmascheckupdt & "';"
        condb.ExecuteNonQuery(sql2)
    End Sub

End Class
