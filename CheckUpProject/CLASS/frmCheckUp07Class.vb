﻿Imports MySql.Data.MySqlClient

Public Class frmCheckUp07Class
    Dim _mascheckup As mascheckup
    Dim condb As ConnecDBRYH
    Dim dtset As dtCheckUP

    Structure mascheckup

        Dim idmascheckup As String
        Dim name As String
        Dim name2 As String
        Dim description As String
        Dim form As String
        Dim formcode As String
        Dim formname As String
        Dim status As Boolean
        Dim name_dt As String
        Dim name2_dt As String
        Dim idmascheckupdt As String
        Dim condition1_1 As String
        Dim value1_1 As String
        Dim condition1_2 As String
        Dim value1_2 As String


        Dim condition2_1 As String
        Dim value2_1 As String
        Dim condition2_2 As String
        Dim value2_2 As String


        Dim f_sex As Boolean
        Dim f_result_all As Boolean


    End Structure
    Public Property setmascheckup As mascheckup
        Get
            Return _mascheckup
        End Get
        Set(value As mascheckup)
            _mascheckup = value
        End Set
    End Property


    Public Sub New(ByRef dtsetCheckup As dtCheckUP)
        dtset = dtsetCheckup
        condb = ConnecDBRYH.NewConnection
    End Sub


    Public Sub loadCondition()
        Dim R As DataRow = dtset.Tables("mascondition").NewRow
        R("condition") = "<="
        dtset.Tables("mascondition").Rows.Add(R)

        R = dtset.Tables("mascondition").NewRow
        R("condition") = "<"
        dtset.Tables("mascondition").Rows.Add(R)

        R = dtset.Tables("mascondition").NewRow
        R("condition") = ">="
        dtset.Tables("mascondition").Rows.Add(R)

        R = dtset.Tables("mascondition").NewRow
        R("condition") = ">"
        dtset.Tables("mascondition").Rows.Add(R)

        R = dtset.Tables("mascondition").NewRow
        R("condition") = "="
        dtset.Tables("mascondition").Rows.Add(R)

    End Sub
    Public Sub LoadMasterFormCode()
        Dim sql As String
        dtset.Tables("dgvOtherResult").Clear()
        sql = "SELECT formcode, formname,description FROM mascheckup WHERE form ='dgvOtherResult';"
        condb.GetTable(sql, dtset.Tables("dgvOtherResult"))
    End Sub

    Public Sub getMascheckup()
        dtset.Tables("mascheckup").Clear()
        Dim sql As String
        sql = "SELECT  mascheckupdt.name , mascheckup.form ,mascheckupdt.name2, mascheckup.description , mascheckup.formcode, mascheckup.formname , mascheckup.idmascheckup, mascheckupdt.idmascheckupdt , mascheckupdt.name AS name_dt , mascheckupdt.name2 as name2_dt ,f_result_all ,mascheckupdt.status ,condition1_1,condition1_2,condition2_1,condition2_2,value1_1,value1_2,value2_1,value2_2,f_sex  FROM mascheckup  LEFT JOIN mascheckupdt ON mascheckupdt.idmascheckup = mascheckup.idmascheckup  WHERE det = 2"
        condb.GetTable(sql, dtset.Tables("mascheckup"))

    End Sub
    Public Sub getMasSetForm()
        Dim sql As String
        sql = "SELECT form FROM  mascheckup WHERE det = 1 GROUP BY form;"
        condb.GetTable(sql, dtset.Tables("mastext"))
    End Sub
    Public Sub getMascheckupDt(ByVal idmascheckupdt As String)
        dtset.Tables("mascheck").Clear()
        Dim sql As String
        sql = "SELECT mascheckup.name , mascheckup.form ,mascheckup.name2, mascheckup.description , mascheckup.formcode, mascheckup.formname , mascheckup.idmascheckup, mascheckupdt.idmascheckupdt , mascheckupdt.name AS name_dt , mascheckupdt.name2 as name2_dt ,f_result_all ,mascheckupdt.status ,condition1_1,condition1_2,condition2_1,condition2_2,value1_1,value1_2,value2_1,value2_2,f_sex  FROM mascheckup LEFT JOIN mascheckupdt ON mascheckupdt.idmascheckup = mascheckup.idmascheckup WHERE mascheckupdt.idmascheckupdt = '" & idmascheckupdt & "';"
        condb.GetTable(sql, dtset.Tables("mascheck"))


    End Sub
    Public Sub getMaschekcupDt_nothing(ByVal idmascheckup As String)
        dtset.Tables("mascheck").Clear()
        Dim sql As String
        sql = "SELECT mascheckup.name , mascheckup.form ,mascheckup.name2, mascheckup.description , mascheckup.formcode, mascheckup.formname , mascheckup.idmascheckup FROM mascheckup LEFT JOIN mascheckupdt ON mascheckupdt.idmascheckup = mascheckup.idmascheckup WHERE mascheckup.idmascheckup = '" & idmascheckup & "';"
        condb.GetTable(sql, dtset.Tables("mascheck"))

    End Sub





    Public Sub InsertMascheckup_Dt()

        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql += "name , "
        sql1 += "'" & MySqlHelper.EscapeString(_mascheckup.name_dt) & "' , "

        sql += "name2 , "
        sql1 += "'" & MySqlHelper.EscapeString(_mascheckup.name2_dt) & "' , "


        sql += "idmascheckup , "
        sql1 += "'" & _mascheckup.idmascheckup & "' ,"



        sql += "condition1_1 , "
        sql1 += "" & If(_mascheckup.condition1_1 Is Nothing, "NULL", "'" & _mascheckup.condition1_1 & "'") & " ,"
        sql += "condition1_2 , "
        sql1 += "" & If(_mascheckup.condition1_2 Is Nothing, "NULL", "'" & _mascheckup.condition1_2 & "'") & " ,"
        sql += "condition2_1 , "
        sql1 += "" & If(_mascheckup.condition2_1 Is Nothing, "NULL", "'" & _mascheckup.condition2_1 & "'") & " ,"
        sql += "condition2_2 , "
        sql1 += "" & If(_mascheckup.condition2_2 Is Nothing, "NULL", "'" & _mascheckup.condition2_2 & "'") & " ,"



        sql += "value1_1 , "
        sql1 += "" & If(_mascheckup.value1_1 Is Nothing, "NULL", "'" & _mascheckup.value1_1 & "'") & " ,"
        sql += "value1_2 , "
        sql1 += "" & If(_mascheckup.value1_2 Is Nothing, "NULL", "'" & _mascheckup.value1_2 & "'") & " ,"
        sql += "value2_1 , "
        sql1 += "" & If(_mascheckup.value2_1 Is Nothing, "NULL", "'" & _mascheckup.value2_1 & "'") & " ,"
        sql += "value2_2 , "
        sql1 += "" & If(_mascheckup.value2_2 Is Nothing, "NULL", "'" & _mascheckup.value2_2 & "'") & " ,"





        sql += "f_result_all , "
        sql1 += "" & _mascheckup.f_result_all & " ,"



        sql += "f_sex , "
        sql1 += "" & _mascheckup.f_sex & " ,"



        sql += "status "
        sql1 += "" & _mascheckup.status & " "


        sql2 = "INSERT INTO mascheckupdt ( " & sql & " ) VALUES ( " & sql1 & " );"

        condb.ExecuteNonQuery(sql2)



    End Sub


    Public Sub UpdateMascheckup_dt()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        sql = "name = "
        sql += "'" & MySqlHelper.EscapeString(_mascheckup.name_dt) & "',"

        sql += "name2 = "
        sql += "'" & MySqlHelper.EscapeString(_mascheckup.name2_dt) & "',"

        sql += "idmascheckup = "
        sql += "'" & _mascheckup.idmascheckup & "',"

        sql += "condition1_1 = "
        sql += "" & If(_mascheckup.condition1_1 Is Nothing, "NULL", "'" & _mascheckup.condition1_1 & "'") & " ,"


        sql += "condition1_2 = "
        sql += "" & If(_mascheckup.condition1_2 Is Nothing, "NULL", "'" & _mascheckup.condition1_2 & "'") & " ,"

        sql += "condition2_1 = "
        sql += "" & If(_mascheckup.condition2_1 Is Nothing, "NULL", "'" & _mascheckup.condition2_1 & "'") & " ,"


        sql += "condition2_2 = "
        sql += "" & If(_mascheckup.condition2_2 Is Nothing, "NULL", "'" & _mascheckup.condition2_2 & "'") & " ,"

        sql += "value1_1 = "
        sql += "" & If(_mascheckup.value1_1 Is Nothing, "NULL", "'" & _mascheckup.value1_1 & "'") & " ,"

        sql += "value1_2 = "
        sql += "" & If(_mascheckup.value1_2 Is Nothing, "NULL", "'" & _mascheckup.value1_2 & "'") & " ,"

        sql += "value2_1 = "
        sql += "" & If(_mascheckup.value2_1 Is Nothing, "NULL", "'" & _mascheckup.value2_1 & "'") & " ,"

        sql += "value2_2 = "
        sql += "" & If(_mascheckup.value2_2 Is Nothing, "NULL", "'" & _mascheckup.value2_2 & "'") & " ,"

        sql += "f_result_all = "
        sql += "" & _mascheckup.f_result_all & ","


        sql += "f_sex = "
        sql += "" & _mascheckup.f_sex & " ,"


        sql += "status = "
        sql += "" & _mascheckup.status & " "



        sql2 = "UPDATE  mascheckupdt SET " & sql & " WHERE idmascheckupdt ='" & _mascheckup.idmascheckupdt & "';"




        condb.ExecuteNonQuery(sql2)
    End Sub
End Class
