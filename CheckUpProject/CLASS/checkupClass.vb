﻿Imports System
Imports System.Windows.Forms
Imports DevExpress.Data
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraReports.UI
Imports System.Runtime.InteropServices
Imports System.Threading
Imports MySql.Data.MySqlClient

Imports System.Globalization
Public Class checkupClass
    Dim dtset As dtCheckUP
    Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
    Dim _frnCheckUp As frnCheckup
    Dim _frnCheckUpBlood As frnCheckUpBlood
    Dim _frnCheckUpUrine As frnCheckUpUrine
    Public Property setDtcheckup As dtCheckUP
        Get
            Return dtset
        End Get
        Set(value As dtCheckUP)
            dtset = value
        End Set
    End Property

    Public Property setFrnCheckup As frnCheckup
        Get
            Return _frnCheckUp
        End Get
        Set(value As frnCheckup)
            _frnCheckUp = value
        End Set
    End Property

    Public Property setFrnCheckUprine As frnCheckUpUrine
        Get
            Return _frnCheckUpUrine
        End Get
        Set(value As frnCheckUpUrine)
            _frnCheckUpUrine = value
        End Set
    End Property

    Public Property setFrnCheckupBlood As frnCheckUpBlood
        Get
            Return _frnCheckUpBlood
        End Get
        Set(value As frnCheckUpBlood)
            _frnCheckUpBlood = value
        End Set
    End Property
    Structure frnCheckUpUrine
        Dim urine_color As String
        Dim urine_albumin As String
        Dim urine_spgr As String
        Dim urine_ph As String
        Dim urine_wbc As String
        Dim urine_appearance As String
        Dim urine_glucose As String
        Dim urine_blood As String
        Dim urine_rbc As String
        Dim urine_epi As String
        Dim urine_other As String
        Dim urine_amphetamine As String
        Dim urine_result As String
        Dim urine_result_remark As String
    End Structure
    Structure frnCheckUpBlood
        Dim bloodGroup As String
        Dim bloodRh As String
        Dim Hgb As String
        Dim Hct As String
        Dim wbc As String
        Dim Neutrophils As String
        Dim Lymphocytes As String
        Dim Eosionophils As String
        Dim Monocytes As String
        Dim Bashophils As String
        Dim Atypical_lymph As String
        Dim RBC_Morphology As String
        Dim PlateCount As String
        Dim blood As String
        Dim blood_result As String
    End Structure

    Structure frnCheckup
        Sub New(i As Integer)

        End Sub
        Dim idfrncheckup As String
        Dim idmascheckup_project As Integer
        Dim project_name As String
        Dim hn As String
        Dim vn As String
        Dim an As String
        Dim prename As String
        Dim stprename As String
        Dim name As String
        Dim lastname As String
        Dim age As String
        Dim idsex As Integer
        Dim doctor As String
        Dim sex As String
        Dim tellephone As String
        Dim email As String
        Dim birthdate As Date
        Dim weight As Double
        Dim height As Double
        Dim bmi As Double
        Dim pulseratebmi As Double
        Dim pulserate As String
        Dim bloodpressure As String
        Dim eyeresult As String
        Dim eyeL As String
        Dim eyeR As String
        Dim eyeLR As Double
        Dim eyeLR_result As String
        Dim blindness As Double
        Dim blindness_result As String
        Dim eye As Double
        Dim eye_result As String
        Dim tooth As Double
        Dim tooth_result As String
        Dim doccodename As String
        Dim address As String
        Dim datetest As Date
        Dim doctorname As String
        Dim fit_offshore As Boolean
        Dim fit_specific As Boolean
        Dim fit_aircrew As Boolean
        Dim fit_breathing As Boolean
        Dim fit_crane As Boolean
        Dim fit_emergency As Boolean
        Dim fit_food As Boolean
        Dim fit_unit As Boolean
        Dim fit_temporary As Boolean
        Dim doctor_license As String

        Dim glasses As Boolean
        Dim glasses_without As Boolean

        Dim vdrlresult As String
        Dim vdrlresult_remark As String
        Dim cearesult As String
        Dim cearesult_remark As String
        Dim xrayresult As String
        Dim xrayresult_remark As String
        Dim hivresult As String
        Dim hivresult_remark As String

        Dim ResultAll As String
        Dim Fitness_ResultAll As String


        Dim virusb_result As String

        Dim virusb_hbsag As String
        Dim virusb_hbsab As String
        Dim virusb_hbcab As String
        Dim virusa_result As String
        Dim virusa_havigg As String
        Dim virusa_havigm As String


        Dim result As String
        Dim fit_result As String
    End Structure
    Public Sub getVisited()
        dtset.Tables("frnservice").Clear()
        Dim sql As String
        sql = "SELECT vn,hn,date_serv,chiefcomp FROM frnservice WHERE hn ='" & _frnCheckUp.hn & "';"
        condb.GetTable(sql, dtset.Tables("frnservice"))
    End Sub
    Public Sub getOldCheckUp()
        dtset.Tables("frnservice").Clear()
        Dim sql As String
        sql = "SELECT vn,hn,datetest AS date_serv,idfrncheckup,project_name AS chieftcomp FROM  frncheckup WHERE hn = '" & _frnCheckUp.hn & "';"
        condb.GetTable(sql, dtset.Tables("frnservice"))
        getPerson()
    End Sub
    Public Sub getmascheckup_project_setting(ByVal idproject As String)
        Dim sql As String
        dtset.Tables("mascheckup_project").Clear()
        sql = "SELECT *  FROM mascheckup_project WHERE idmascheckup_project = '" & idproject & "'; "
        condb.GetTable(sql, dtset.Tables("mascheckup_project"))
    End Sub
    Public Function getLicenseCode(ByVal empid As String) As String
        Dim sql As String
        Dim dt As New DataTable
        sql = "SELECT registerno FROM hospemp WHERE empid = '" & empid & "';"
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)("registerno").ToString
        Else
            Return ""
        End If

    End Function
    Public Sub getmascheckup_project()
        Dim sql As String
        dtset.Tables("mascheckup_pro").Clear()
        sql = "SELECT idmascheckup_project , project_name,status FROM mascheckup_project WHERE status = 1; "
        condb.GetTable(sql, dtset.Tables("mascheckup_pro"))
    End Sub
    Public Sub getMassex()
        Dim sql As String
        sql = "SELECT * FROM massex WHERE status =1"
        condb.GetTable(sql, dtset.Tables("massex"))
    End Sub
    Public Function getCheckResult(idmascheckupdt As String) As Integer

        Dim sql As String
        Dim dt As New DataTable
        If IsNumeric(idmascheckupdt) Then
            dt = condb.GetTable("SELECT f_result_all FROM mascheckupdt WHERE idmascheckup = '" & idmascheckupdt & "';")
            If dt.Rows.Count > 0 Then
                Return If(IsDBNull(dt.Rows(0)(0)), 0, dt.Rows(0)(0))
            Else
                Return 0

            End If
        End If


    End Function
    Public Sub getMashospemp()
        Dim sql As String
        dtset.Tables("doctor").Clear()
        sql = "SELECT CAST(`empid` AS CHAR(50)) AS `PROVIDER`,CONCAT_WS('', masprename.stprename ,' ',`NAME`,' ',LNAME) AS `DRNAME` FROM (SELECT * FROM hospemp WHERE(F_DR = 1) ) AS hospemp LEFT JOIN masprename ON masprename.prename = hospemp.prename ;"
        condb.GetTable(sql, dtset.Tables("doctor"))
    End Sub
    Public Sub getPerson()

        Dim sql As String
        Dim sql1 As String
        Dim dtaddress As New DataTable
        Dim dtperson As New DataTable

        sql = "SELECT DISTINCT CAST(p.hn AS CHAR(15)) AS HN,p.cid,p.typep,p.prename,p.`name`,p.lname,p.ename,p.emname,p.elname,p.penname,mpre.stprename,mpre.seprename,mpre.ftprename,mpre.feprename,p.sex,msex.sexdesc,p.birth,p.race,mra.nationdesc AS RACEDESC,p.nation,ad.telephone,ad.mobile,p.cid ,mna.nationdesc,p.religion,mre.religiondesc,CASE WHEN(p.drugalert = 1) THEN 'แพ้ยา' ELSE 'ไม่แพ้ยา' END AS drugalert, p.drugalert as DRUGALERTSTAT,mabo.abogroupdesc,mrh.rhgroupdesc  FROM (SELECT * FROM  person WHERE(hn = " & _frnCheckUp.hn & " AND `status` = 1)) AS p LEFT JOIN (SELECT * FROM  masprename WHERE(`status` = 1)) AS mpre ON p.prename = mpre.prename LEFT JOIN (SELECT * FROM  massex WHERE(`status` = 1)) AS msex ON p.sex = msex.sex LEFT JOIN (SELECT * FROM  masnation WHERE(`status` = 1)) AS mra ON p.nation = mra.nation LEFT JOIN (SELECT * FROM  masnation WHERE(`status` = 1)) AS mna ON p.nation = mna.nation LEFT JOIN (SELECT * FROM  masreligion WHERE(`status` = 1)) AS mre ON p.religion = mre.religion LEFT JOIN (SELECT * FROM  masabogroup WHERE(`status` = 1)) AS mabo ON p.abogroup = mabo.abogroup LEFT JOIN (SELECT * FROM  masrhgroup WHERE(`status` = 1)) AS mrh ON p.rhgroup = mrh.rhgroup LEFT JOIN (SELECT * FROM  address WHERE(mobile IS NOT NULL AND mobile <> '') and hn =" & _frnCheckUp.hn & " ) AS ad ON p.hn = ad.hn  LEFT JOIN (SELECT * FROM  masoccupation WHERE `status` = 1) AS masoccupation ON p.occupation = masoccupation.occid "
        dtperson = condb.GetTable(sql)
        If dtperson.Rows.Count > 0 Then
            Dim datejaja As New FriendsDate

            datejaja.Date_Diff(Convert.ToDateTime(dtperson.Rows(0)("BIRTH")), Convert.ToDateTime(condb.GetTable("SELECT current_timestamp()").Rows(0)(0)))
            _frnCheckUp.age = datejaja._year
            _frnCheckUp.name = dtperson.Rows(0)("NAME").ToString
            _frnCheckUp.lastname = dtperson.Rows(0)("lname").ToString
            _frnCheckUp.tellephone = "Mobile" & dtperson.Rows(0)("mobile") & ", Telephone " & dtperson.Rows(0)("telephone")
            _frnCheckUp.idsex = dtperson.Rows(0)("sex")
            _frnCheckUp.sex = If(dtperson.Rows(0)("sex") = "1", "Male", "Female")
            _frnCheckUp.prename = dtperson.Rows(0)("prename")
            _frnCheckUp.stprename = dtperson.Rows(0)("stprename")
        Else
            MsgBox("ไม่พบเลข ผู้ป่วยที่ค้นหา")
            Exit Sub
        End If

        sql1 = "SELECT CONCAT(`addresdec1`,'  ',CASE WHEN(`tambon` IS null) THEN '' ELSE CONCAT('ต.',' ',`tambon`) END,'  ',CASE WHEN(`ampur` IS null) THEN '' ELSE CONCAT('อ.',' ',`ampur`) END,'  ',CASE WHEN(`changwat` IS null) THEN '' ELSE CONCAT('จ.',' ',`changwat`) end, '  ',CASE WHEN(mastambon.`postcode` IS null) THEN '' ELSE CONCAT(mastambon.`postcode`) END) AS 'address'  FROM address  LEFT JOIN maschangwat AS maschangwat ON address.`codechangwat` = maschangwat.`codechangwat` LEFT JOIN masampur AS masampur ON address.`ampid` = masampur.`ampid` LEFT JOIN mastambon AS mastambon ON address.`tmbid` = mastambon.`tmbid` WHERE `addresstype` = 1 AND hn = " & _frnCheckUp.hn & " AND address.status = 1 "
        dtaddress = condb.GetTable(sql1)
        If dtaddress.Rows.Count > 0 Then

            _frnCheckUp.address = dtaddress.Rows(0)("address").ToString

        End If




    End Sub
    Public Sub getReport()
        getFrnCheckUp()
        getfrncheckup_physical()
        getfrncheckupblood()
    End Sub


    Public Sub getfrncheckup_physical()
        Dim sql As String
        dtset.Tables("frncheckup_physical").Clear()
        sql = "SELECT idmascheckup_physical,frncheckup_physical.name ,frncheckup_physical.name2,mascheckup_physical_result.name AS namevalue FROM (SELECT * FROM frncheckup_physical WHERE idfrncheckup = '" & _frnCheckUp.idfrncheckup & "' )  AS  frncheckup_physical JOIN mascheckup_physical_result ON mascheckup_physical_result.idmascheckup_physical_result = frncheckup_physical.idvalue  ;"
        condb.GetTable(sql, dtset.Tables("frncheckup_physical"))
    End Sub
    Public Sub getfrncheckupblood()
        Dim sql As String
        dtset.Tables("frncheckupblood").Clear()
        sql = "SELECT * FROM frncheckupblood WHERE idfrncheckup = '" & _frnCheckUp.idfrncheckup & "';"
        condb.GetTable(sql, dtset.Tables("frncheckupblood"))
    End Sub
    Public Sub UpdateFrncheckUpBlood()
        Dim sql As String
        Dim sql1 As String
        sql = "bloodGroup  = "
        sql += "'" & _frnCheckUpBlood.bloodGroup & "' ,"
        sql += " bloodRh ="
        sql += "'" & _frnCheckUpBlood.bloodRh & "' ,"
        sql += " Hgb ="
        sql += "'" & _frnCheckUpBlood.Hgb & "' ,"
        sql += " Hct ="
        sql += "'" & _frnCheckUpBlood.Hct & "' ,"
        sql += " wbc ="
        sql += "'" & _frnCheckUpBlood.wbc & "' ,"
        sql += " Neutrophils ="
        sql += "'" & _frnCheckUpBlood.Neutrophils & "' ,"
        sql += " Lymphocytes ="
        sql += "'" & _frnCheckUpBlood.Lymphocytes & "' ,"
        sql += " Eosionophils ="
        sql += "'" & _frnCheckUpBlood.Eosionophils & "' ,"
        sql += " Monocytes="
        sql += "'" & _frnCheckUpBlood.Monocytes & "' ,"
        sql += " Bashophils ="
        sql += "'" & _frnCheckUpBlood.Bashophils & "' ,"
        sql += " Atypical_lymph ="
        sql += "'" & _frnCheckUpBlood.Atypical_lymph & "' ,"
        sql += " RBC_Morphology ="
        sql += "'" & _frnCheckUpBlood.RBC_Morphology & "' ,"
        sql += " PlateCount ="
        sql += "'" & _frnCheckUpBlood.PlateCount & "' ,"
        sql += " blood ="
        sql += "'" & _frnCheckUpBlood.blood & "' ,"
        sql += " blood_result ="
        sql += "'" & _frnCheckUpBlood.blood_result & "', "


        sql += " urine_albumin ="
        sql += "'" & _frnCheckUpUrine.urine_albumin & "', "
        sql += " urine_amphetamine ="
        sql += "'" & _frnCheckUpUrine.urine_amphetamine & "', "
        sql += " urine_appearance ="
        sql += "'" & _frnCheckUpUrine.urine_appearance & "', "
        sql += " urine_blood ="
        sql += "'" & _frnCheckUpUrine.urine_blood & "', "
        sql += " urine_color ="
        sql += "'" & _frnCheckUpUrine.urine_color & "', "
        sql += " urine_epi ="
        sql += "'" & _frnCheckUpUrine.urine_epi & "', "

        sql += " urine_glucose ="
        sql += "'" & _frnCheckUpUrine.urine_glucose & "', "
        sql += " urine_other ="
        sql += "'" & _frnCheckUpUrine.urine_other & "', "
        sql += " urine_ph ="
        sql += "'" & _frnCheckUpUrine.urine_ph & "', "
        sql += " urine_rbc ="
        sql += "'" & _frnCheckUpUrine.urine_rbc & "', "



        sql += " urine_result ="
        sql += "'" & _frnCheckUpUrine.urine_result & "', "




        sql += " urine_result_remark ="
        sql += "'" & _frnCheckUpUrine.urine_result_remark & "', "
        sql += " urine_spgr ="
        sql += "'" & _frnCheckUpUrine.urine_spgr & "', "
        sql += " urine_wbc ="
        sql += "'" & _frnCheckUpUrine.urine_wbc & "' "



        sql1 = "UPDATE  frncheckupblood  SET " & sql & " WHERE idfrncheckup = '" & _frnCheckUp.idfrncheckup & "';"
        condb.ExecuteNonQuery(sql1)
        UpdatefrnCheckup_physical()
    End Sub
    Public Sub InsertFrncheckUpBlood()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        'Dim blood_result As String
        sql = "bloodGroup , "
        sql1 += "'" & _frnCheckUpBlood.bloodGroup & "' ,"
        sql += " bloodRh ,"
        sql1 += "'" & _frnCheckUpBlood.bloodRh & "' ,"
        sql += " Hgb ,"
        sql1 += "'" & _frnCheckUpBlood.Hgb & "' ,"
        sql += " Hct ,"
        sql1 += "'" & _frnCheckUpBlood.Hct & "' ,"
        sql += " wbc ,"
        sql1 += "'" & _frnCheckUpBlood.wbc & "' ,"
        sql += " Neutrophils ,"
        sql1 += "'" & _frnCheckUpBlood.Neutrophils & "' ,"
        sql += " Lymphocytes ,"
        sql1 += "'" & _frnCheckUpBlood.Lymphocytes & "' ,"
        sql += " Eosionophils ,"
        sql1 += "'" & _frnCheckUpBlood.Eosionophils & "' ,"
        sql += " Monocytes ,"
        sql1 += "'" & _frnCheckUpBlood.Monocytes & "' ,"
        sql += " Bashophils ,"
        sql1 += "'" & _frnCheckUpBlood.Bashophils & "' ,"
        sql += " Atypical_lymph ,"
        sql1 += "'" & _frnCheckUpBlood.Atypical_lymph & "' ,"
        sql += " RBC_Morphology ,"
        sql1 += "'" & _frnCheckUpBlood.RBC_Morphology & "' ,"
        sql += " PlateCount ,"
        sql1 += "'" & _frnCheckUpBlood.PlateCount & "' ,"
        sql += " blood ,"
        sql1 += "'" & _frnCheckUpBlood.blood & "' ,"
        sql += " blood_result ,"
        sql1 += "'" & _frnCheckUpBlood.blood_result & "', "


        sql += " urine_albumin ,"
        sql1 += "'" & _frnCheckUpUrine.urine_albumin & "', "
        sql += " urine_amphetamine ,"
        sql1 += "'" & _frnCheckUpUrine.urine_amphetamine & "', "
        sql += " urine_appearance ,"
        sql1 += "'" & _frnCheckUpUrine.urine_appearance & "', "
        sql += " urine_blood ,"
        sql1 += "'" & _frnCheckUpUrine.urine_blood & "', "
        sql += " urine_color ,"
        sql1 += "'" & _frnCheckUpUrine.urine_color & "', "
        sql += " urine_epi ,"
        sql1 += "'" & _frnCheckUpUrine.urine_epi & "', "

        sql += " urine_glucose ,"
        sql1 += "'" & _frnCheckUpUrine.urine_glucose & "', "
        sql += " urine_other ,"
        sql1 += "'" & _frnCheckUpUrine.urine_other & "', "
        sql += " urine_ph ,"
        sql1 += "'" & _frnCheckUpUrine.urine_ph & "', "
        sql += " urine_rbc ,"
        sql1 += "'" & _frnCheckUpUrine.urine_rbc & "', "



        sql += " urine_result ,"
        sql1 += "'" & _frnCheckUpUrine.urine_result & "', "

        sql += " urine_result_remark ,"
        sql1 += "'" & _frnCheckUpUrine.urine_result_remark & "', "
        sql += " urine_spgr ,"
        sql1 += "'" & _frnCheckUpUrine.urine_spgr & "', "
        sql += " urine_wbc ,"
        sql1 += "'" & _frnCheckUpUrine.urine_wbc & "', "





        sql += "idfrncheckup "
        sql1 += "'" & _frnCheckUp.idfrncheckup & "' "

        sql2 = "INSERT INTO frncheckupblood ( " & sql & " ) VALUES ( " & sql1 & " )"
        condb.ExecuteNonQuery(sql2)

        Insertfrncheckup_physical()
    End Sub
    Public Sub UpdatefrnCheckup_physical()
        Dim sql As String
        Dim sql1 As String
        For i As Integer = 0 To dtset.Tables("mascheckup_physical").Rows.Count - 1


            sql = "idmascheckup_physical ="
            sql += " " & dtset.Tables("mascheckup_physical").Rows(i)("idmascheckup_physical").ToString & " , "
            sql += "name = "
            sql += " '" & dtset.Tables("mascheckup_physical").Rows(i)("name").ToString & "' , "
            sql += "name2 = "
            sql += " '" & dtset.Tables("mascheckup_physical").Rows(i)("name2").ToString & "' , "
            sql += "idvalue = "
            sql += " " & dtset.Tables("mascheckup_physical").Rows(i)("idvalue").ToString & " "

            sql1 = "UPDATE  frncheckup_physical SET  " & sql & "  WHERE idfrncheckup_physical = '" & dtset.Tables("mascheckup_physical").Rows(i)("idfrncheckup_physical").ToString & "'    "




            condb.ExecuteNonQuery(sql1)
        Next
        updatefrncheck_other()
    End Sub
    Public Sub Insertfrncheckup_physical()
        Dim sql As String = ""
        Dim sql1 As String = ""
        Dim sql2 As String = ""
        For i As Integer = 0 To dtset.Tables("mascheckup_physical").Rows.Count - 1


            sql = "idmascheckup_physical , "
            sql1 = " " & dtset.Tables("mascheckup_physical").Rows(i)("idmascheckup_physical").ToString & " , "
            sql += "name , "
            sql1 += " '" & dtset.Tables("mascheckup_physical").Rows(i)("name").ToString & "' , "
            sql += "name2 , "
            sql1 += " '" & dtset.Tables("mascheckup_physical").Rows(i)("name2").ToString & "' , "
            sql += "idvalue , "
            sql1 += " " & dtset.Tables("mascheckup_physical").Rows(i)("idvalue").ToString & " , "
            sql += "idfrncheckup  "
            sql1 += " " & _frnCheckUp.idfrncheckup & "  "
            sql2 = "INSERT INTO frncheckup_physical ( " & sql & " ) VALUES ( " & sql1 & " )"
            condb.ExecuteNonQuery(sql2)
        Next

        Insertfrncheck_other()
    End Sub
    Public Sub getFrnchekup_other()
        Dim sql As String
        dtset.Tables("mascheckup").Clear()
        dtset.Tables("frncheckupother").Clear()
        'sql = "SELECT * FROM mascheckup WHERE  form ='dgvOtherResult';"
        'condb.GetTable(sql, dtset.Tables("mascheckup"))

        'For i As Integer = 0 To dtset.Tables("mascheckup").Rows.Count - 1
        sql = "SELECT prdordelabresult.description,prdordelabresult.unit,prdordelabresult.normal,prdordelabresult.value,prdordelabresult.hn,prdordelabresult.an,prdordelabresult.vn, prdordelabresult.result,prdordelabresult.result2,prdordelabresult.idmascheckup,prdordelabresult.f_result AS f_result_all,prdordelabresult.idfrncheckupother FROM (SELECT * FROM frncheckupother WHERE idfrncheckup ='" & _frnCheckUp.idfrncheckup & "' ) AS prdordelabresult ;"
        condb.GetTable(sql, dtset.Tables("frncheckupother"))

        'Next
    End Sub

    Public Sub updatefrncheck_other()
        Dim sql As String
        Dim sql1 As String


        For i As Integer = 0 To dtset.Tables("frncheckupother").Rows.Count - 1
            sql = "description ="
            sql += " '" & dtset.Tables("frncheckupother").Rows(i)("description").ToString & "' ,  "

            sql += "unit ="
            sql += " '" & dtset.Tables("frncheckupother").Rows(i)("unit").ToString & "' ,  "

            sql += "normal ="
            sql += " '" & dtset.Tables("frncheckupother").Rows(i)("normal").ToString & "' ,  "
            sql += "idmascheckup ="
            sql += " '" & dtset.Tables("frncheckupother").Rows(i)("idmascheckup").ToString & "' ,  "


            sql += "value ="
            sql += " '" & dtset.Tables("frncheckupother").Rows(i)("value").ToString & "' ,  "
            sql += "result ="
            sql += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("result").ToString) & "' ,  "
            sql += "result2 ="
            sql += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("result2").ToString) & "' ,  "
            sql += "f_result ="
            sql += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("f_result_all").ToString) & "'  "


            sql1 = "UPDATE  frncheckupother SET " & sql & "  WHERE idfrncheckupother = '" & dtset.Tables("frncheckupother").Rows(i)("idfrncheckupother").ToString & "'"
            condb.ExecuteNonQuery(sql1)

        Next




    End Sub
    Public Sub Insertfrncheck_other()
        Dim sql As String = ""
        Dim sql1 As String = ""
        Dim sql2 As String = ""
        For i As Integer = 0 To dtset.Tables("frncheckupother").Rows.Count - 1
            sql = "description ,"
            sql1 = " '" & dtset.Tables("frncheckupother").Rows(i)("description").ToString & "' ,  "

            sql += "unit ,"
            sql1 += " '" & dtset.Tables("frncheckupother").Rows(i)("unit").ToString & "' ,  "

            sql += "normal ,"
            sql1 += " '" & dtset.Tables("frncheckupother").Rows(i)("normal").ToString & "' ,  "
            sql += "idmascheckup ,"
            sql1 += " '" & dtset.Tables("frncheckupother").Rows(i)("idmascheckup").ToString & "' ,  "


            sql += "value ,"
            sql1 += " '" & dtset.Tables("frncheckupother").Rows(i)("value").ToString & "' ,  "
            sql += "result ,"
            sql1 += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("result").ToString) & "' ,  "
            sql += "result2 ,"
            sql1 += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("result2").ToString) & "' ,  "

            sql += "f_result ,"
            sql1 += " '" & MySqlHelper.EscapeString(dtset.Tables("frncheckupother").Rows(i)("f_result_all").ToString) & "' ,  "

            sql += "idfrncheckup  "
            sql1 += " " & _frnCheckUp.idfrncheckup & "  "
            sql2 = "INSERT INTO frncheckupother ( " & sql & " ) VALUES ( " & sql1 & " )"
            condb.ExecuteNonQuery(sql2)

        Next
    End Sub
    Public Sub getFrnCheckup()
        Dim sql As String
        dtset.Tables("frncheckup").Clear()
        sql = "SELECT * FROM frncheckup WHERE idfrncheckup = '" & _frnCheckUp.idfrncheckup & "';"
        condb.GetTable(sql, dtset.Tables("frncheckup"))
    End Sub
    Public Sub UpdateFrncheckUp()
        Dim sql As String
        Dim sql1 As String


        sql += "hn = "
        sql += "'" & _frnCheckUp.hn & "' ,"

        sql += " vn = "
        sql += "" & If(_frnCheckUp.vn Is Nothing, "NULL", _frnCheckUp.vn) & " ,"


        sql += " idmascheckup_project = "
        sql += "'" & _frnCheckUp.idmascheckup_project & "' ,"

        sql += " project_name = "
        sql += "'" & _frnCheckUp.project_name & "' ,"

        sql += " an = "
        sql += "'" & _frnCheckUp.an & "' ,"

        sql += " sex = "
        sql += "'" & _frnCheckUp.sex & "' ,"
        sql += " idsex = "
        sql += "'" & _frnCheckUp.idsex & "' ,"

        sql += " stprename = "
        sql += "'" & _frnCheckUp.stprename & "' ,"
        sql += " prename = "
        sql += "'" & _frnCheckUp.prename & "' ,"

        sql += " name = "
        sql += "'" & _frnCheckUp.name & "' ,"

        sql += " lastname = "
        sql += "'" & _frnCheckUp.lastname & "' ,"

        sql += " age ="
        sql += "'" & _frnCheckUp.age & "' ,"

        sql += " tellephone = "
        sql += "'" & _frnCheckUp.tellephone & "' ,"

        sql += " email = "
        sql += "'" & _frnCheckUp.email & "' ,"

        sql += " birthdate = "
        sql += "'" & _frnCheckUp.birthdate.Year & _frnCheckUp.birthdate.ToString("-MM-dd hh:mm:ss") & "' ,"

        sql += " weight = "
        sql += "'" & _frnCheckUp.weight & "' ,"

        sql += " height = "
        sql += "'" & _frnCheckUp.height & "' ,"

        sql += " bmi = "
        sql += "'" & _frnCheckUp.bmi & "' ,"

        sql += " pulserate = "
        sql += "'" & _frnCheckUp.pulserate & "' ,"

        sql += " bloodpressure = "
        sql += "'" & _frnCheckUp.bloodpressure & "' ,"

        sql += " eye_result= "
        sql += "'" & _frnCheckUp.eyeresult & "' ,"

        sql += " eyeL  ="
        sql += "'" & _frnCheckUp.eyeL & "' ,"

        sql += " eyeR = "
        sql += "'" & _frnCheckUp.eyeR & "' ,"

        sql += " empdoctor_id  ="
        sql += "'" & _frnCheckUp.doctor & "', "

        sql += " doctorname  ="
        sql += "'" & _frnCheckUp.doctorname & "', "


        sql += " doctor_license  ="
        sql += "'" & _frnCheckUp.doctor_license & "', "

        sql += " blindness = "
        sql += "'" & _frnCheckUp.blindness & "' ,"

        sql += "blindness_result = "
        sql += "'" & _frnCheckUp.blindness_result & "' ,"

        sql += " eye ="
        sql += "'" & _frnCheckUp.eye & "' ,"



        sql += " eyeLR = "
        sql += "'" & _frnCheckUp.eyeLR & "' ,"

        sql += " eyeLR_result  ="
        sql += "'" & _frnCheckUp.eyeLR_result & "' ,"

        sql += " tooth = "
        sql += "'" & _frnCheckUp.tooth & "' ,"

        sql += " tooth_result = "
        sql += "'" & _frnCheckUp.tooth_result & "' ,"

        sql += " doccodename = "
        sql += "'" & _frnCheckUp.doccodename & "' ,"

        sql += " address = "
        sql += "'" & _frnCheckUp.address & "' ,"

        sql += " fit_offshore ="
        sql += "" & _frnCheckUp.fit_offshore & ", "
        sql += " fit_specific ="
        sql += "" & _frnCheckUp.fit_specific & ", "
        sql += " fit_aircrew ="
        sql += "" & _frnCheckUp.fit_aircrew & ", "
        sql += " fit_breathing ="
        sql += "" & _frnCheckUp.fit_breathing & ", "
        sql += " fit_crane ="
        sql += "" & _frnCheckUp.fit_crane & ", "
        sql += " fit_emergency ="
        sql += "" & _frnCheckUp.fit_emergency & ", "
        sql += " fit_food ="
        sql += "" & _frnCheckUp.fit_food & ", "
        sql += " fit_unit ="
        sql += "" & _frnCheckUp.fit_unit & ", "
        sql += " fit_temporary ="
        sql += "" & _frnCheckUp.fit_temporary & ", "


        sql += " glasses ="
        sql += "" & _frnCheckUp.glasses & ", "


        sql += " glasses_without = "
        sql += "" & _frnCheckUp.glasses_without & ", "




        sql += " vdrlresult ="
        sql += "'" & _frnCheckUp.vdrlresult & "', "
        sql += " vdrlresult_remark ="
        sql += "'" & _frnCheckUp.vdrlresult_remark & "', "
        sql += " cearesult ="
        sql += "'" & _frnCheckUp.cearesult & "', "
        sql += " cearesult_remark ="
        sql += "'" & _frnCheckUp.cearesult_remark & "', "
        sql += " xrayresult ="
        sql += "'" & _frnCheckUp.xrayresult & "', "
        sql += " xrayresult_remark ="
        sql += "'" & _frnCheckUp.xrayresult_remark & "', "
        sql += " hivresult ="
        sql += "'" & _frnCheckUp.hivresult & "', "
        sql += " hivresult_remark ="
        sql += "'" & _frnCheckUp.hivresult_remark & "', "

        sql += " ResultAll ="
        sql += "'" & _frnCheckUp.ResultAll & "', "

        sql += " Fitness_ResultAll ="
        sql += "'" & _frnCheckUp.Fitness_ResultAll & "', "



        sql += " virusb_result ="
        sql += "" & If(_frnCheckUp.virusb_result Is Nothing, "NULL", _frnCheckUp.virusb_result) & ", "

        sql += " virusa_result ="
        sql += "" & If(_frnCheckUp.virusa_result Is Nothing, "NULL", _frnCheckUp.virusa_result) & ", "



        sql += " virusb_hbsab ="
        sql += "'" & _frnCheckUp.virusb_hbsab & "', "
        sql += " virusb_hbcab ="
        sql += "'" & _frnCheckUp.virusb_hbcab & "', "
        sql += " virusa_havigg ="
        sql += "'" & _frnCheckUp.virusa_havigg & "', "
        sql += " virusa_havigm ="
        sql += "'" & _frnCheckUp.virusa_havigm & "', "





        sql += " datetest = "
        sql += "'" & _frnCheckUp.datetest.Year & _frnCheckUp.datetest.ToString("-MM-dd hh:mm:ss") & "' "


        sql1 = "UPDATE  frncheckup  SET " & sql & " WHERE idfrncheckup =  '" & _frnCheckUp.idfrncheckup & "';"
        condb.ExecuteNonQuery(sql1)
        UpdateFrncheckUpBlood()
    End Sub

    Public Sub InsertFrncheckUp()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        _frnCheckUp.idfrncheckup = generateIDYearAndRuning("frncheckup", "idfrncheckup")
        sql = "idfrncheckup "
        sql1 += "" & _frnCheckUp.idfrncheckup & " ,"
        sql += ",idmascheckup_project "
        sql1 += "'" & _frnCheckUp.idmascheckup_project & "' ,"
        sql += ",project_name "
        sql1 += "'" & _frnCheckUp.project_name & "' ,"

        sql += ",hn "
        sql1 += "'" & _frnCheckUp.hn & "' ,"

        sql += ", vn"
        sql1 += "'" & _frnCheckUp.vn & "' ,"

        sql += ", an"
        sql1 += "'" & _frnCheckUp.an & "' ,"

        sql += ", sex"
        sql1 += "'" & _frnCheckUp.sex & "' ,"
        sql += ", idsex"
        sql1 += "'" & _frnCheckUp.idsex & "' ,"

        sql += ", prename"
        sql1 += "'" & _frnCheckUp.prename & "' ,"
        sql += ", stprename"
        sql1 += "'" & _frnCheckUp.stprename & "' ,"

        sql += ", name"
        sql1 += "'" & _frnCheckUp.name & "' ,"

        sql += ", lastname"
        sql1 += "'" & _frnCheckUp.lastname & "' ,"

        sql += ", age"
        sql1 += "'" & _frnCheckUp.age & "' ,"

        sql += ", tellephone"
        sql1 += "'" & _frnCheckUp.tellephone & "' ,"

        sql += ", email "
        sql1 += "'" & _frnCheckUp.email & "' ,"

        sql += ", birthdate "
        sql1 += "'" & _frnCheckUp.birthdate.Year & _frnCheckUp.birthdate.ToString("-MM-dd hh:mm:ss") & "' ,"

        sql += ", weight "
        sql1 += "'" & _frnCheckUp.weight & "' ,"

        sql += ", height "
        sql1 += "'" & _frnCheckUp.height & "' ,"

        sql += ", bmi "
        sql1 += "'" & _frnCheckUp.bmi & "' ,"

        sql += ", pulserate "
        sql1 += "'" & _frnCheckUp.pulserate & "' ,"

        sql += ", bloodpressure "
        sql1 += "'" & _frnCheckUp.bloodpressure & "' ,"

        sql += ", eye_result "
        sql1 += "'" & _frnCheckUp.eyeresult & "' ,"

        sql += ", eyeL "
        sql1 += "'" & _frnCheckUp.eyeL & "' ,"

        sql += ", eyeR "
        sql1 += "'" & _frnCheckUp.eyeR & "' ,"

        sql += " , empdoctor_id "
        sql1 += "'" & _frnCheckUp.doctor & "', "

        sql += " , doctorname "
        sql1 += "'" & _frnCheckUp.doctorname & "', "

        sql += " , doctor_license "
        sql1 += "'" & _frnCheckUp.doctor_license & "', "

        sql += ", blindness "
        sql1 += "'" & _frnCheckUp.blindness & "' ,"

        sql += ", blindness_result "
        sql1 += "'" & _frnCheckUp.blindness_result & "' ,"

        sql += ", eye "
        sql1 += "'" & _frnCheckUp.eye & "' ,"



        sql += ", eyeLR "
        sql1 += "'" & _frnCheckUp.eyeLR & "' ,"

        sql += ", eyeLR_result "
        sql1 += "'" & _frnCheckUp.eyeLR_result & "' ,"

        sql += ", tooth "
        sql1 += "'" & _frnCheckUp.tooth & "' ,"

        sql += ", tooth_result "
        sql1 += "'" & _frnCheckUp.tooth_result & "' ,"

        sql += ", doccodename "
        sql1 += "'" & _frnCheckUp.doccodename & "' ,"

        sql += ", address , "
        sql1 += "'" & _frnCheckUp.address & "' ,"

        sql += " fit_offshore ,"
        sql1 += "" & _frnCheckUp.fit_offshore & ", "
        sql += " fit_specific ,"
        sql1 += "" & _frnCheckUp.fit_specific & ", "
        sql += " fit_aircrew ,"
        sql1 += "" & _frnCheckUp.fit_aircrew & ", "
        sql += " fit_breathing ,"
        sql1 += "" & _frnCheckUp.fit_breathing & ", "
        sql += " fit_crane ,"
        sql1 += "" & _frnCheckUp.fit_crane & ", "
        sql += " fit_emergency ,"
        sql1 += "" & _frnCheckUp.fit_emergency & ", "
        sql += " fit_food ,"
        sql1 += "" & _frnCheckUp.fit_food & ", "
        sql += " fit_unit ,"
        sql1 += "" & _frnCheckUp.fit_unit & ", "

        sql += " fit_temporary ,"
        sql1 += "" & _frnCheckUp.fit_temporary & ", "

        sql += " glasses ,"
        sql1 += "" & _frnCheckUp.glasses & ", "


        sql += " glasses_without ,"
        sql1 += "" & _frnCheckUp.glasses_without & ", "




        sql += " vdrlresult ,"
        sql1 += "'" & _frnCheckUp.vdrlresult & "', "
        sql += " vdrlresult_remark ,"
        sql1 += "'" & _frnCheckUp.vdrlresult_remark & "', "
        sql += " cearesult ,"
        sql1 += "'" & _frnCheckUp.cearesult & "', "
        sql += " cearesult_remark ,"
        sql1 += "'" & _frnCheckUp.cearesult_remark & "', "
        sql += " xrayresult ,"
        sql1 += "'" & _frnCheckUp.xrayresult & "', "
        sql += " xrayresult_remark ,"
        sql1 += "'" & _frnCheckUp.xrayresult_remark & "', "
        sql += " hivresult ,"
        sql1 += "'" & _frnCheckUp.hivresult & "', "
        sql += " hivresult_remark,  "
        sql1 += "'" & _frnCheckUp.hivresult_remark & "', "

        sql += " ResultAll , "
        sql1 += "'" & _frnCheckUp.ResultAll & "', "
        sql += " Fitness_ResultAll  ,"
        sql1 += "'" & _frnCheckUp.Fitness_ResultAll & "', "


        sql += " virusb_result , "
        sql1 += "" & If(_frnCheckUp.virusb_result Is Nothing, "NULL", _frnCheckUp.virusb_result) & ", "



        sql += " virusb_hbsab , "
        sql1 += "'" & _frnCheckUp.virusb_hbsab & "', "


        sql += " virusb_hbcab , "
        sql1 += "'" & _frnCheckUp.virusb_hbcab & "', "




        sql += " virusa_result , "
        sql1 += "" & If(_frnCheckUp.virusa_result Is Nothing, "NULL", _frnCheckUp.virusa_result) & ", "



        sql += " virusa_havigg , "
        sql1 += "'" & _frnCheckUp.virusa_havigg & "', "

        sql += " virusa_havigm , "
        sql1 += "'" & _frnCheckUp.virusa_havigm & "', "




        sql += " datetest "
        sql1 += "'" & _frnCheckUp.datetest.Year & _frnCheckUp.datetest.ToString("-MM-dd hh:mm:ss") & "' "





        sql2 = "INSERT INTO frncheckup ( " & sql & " ) VALUES ( " & sql1 & " );"
        condb.ExecuteNonQuery(sql2)
        InsertFrncheckUpBlood()
    End Sub
    Public Sub New(ByRef dtsetproduct As dtCheckUP)
        dtset = dtsetproduct
    End Sub
    Public Sub getData(formname As String)
        Dim sql As String
        sql = "SELECT * FROM mascheckup WHERE form = '" & formname & "';"
        condb.GetTable(sql, dtset.Tables(formname))
    End Sub
    Public Function getDataTotext(textSearch As DevExpress.XtraEditors.SearchLookUpEdit, textTarget As DevExpress.XtraEditors.TextEdit) As String
        Dim [property] As System.Reflection.PropertyInfo = textSearch.Properties.[GetType]().GetProperty("Controller", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
        Dim controller As BaseGridController = DirectCast([property].GetValue(textSearch.Properties, Nothing), BaseGridController)
        Dim index As Integer = controller.FindRowByValue(textSearch.Properties.ValueMember, textSearch.EditValue)
        Try
            If controller.GetRow(index)("det") = 0 Then
                textTarget.EditValue = controller.GetRow(index)("name")
            Else
                textTarget.EditValue = getDetailMascheckUp(controller.GetRow(index)("idmascheckup"))

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Function
    Public Function getDetailMascheckUp(ByVal id As String) As String
        Dim sql As String
        sql = "SELECT * from mascheckupdt WHERe idmascheckup = " & id & ""
        Dim dt As DataTable
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)("name")
        Else
            Return ""
        End If

    End Function
    Public Sub getmascheckup_physical_result()
        Dim sql As String
        dtset.Tables("mascheckup_physical_result").Clear()

        sql = "SELECT * FROM mascheckup_physical_result ;"
        condb.GetTable(sql, dtset.Tables("mascheckup_physical_result"))
    End Sub
    Public Sub getFrncheckup_physicalExam()
        Dim sql As String
        dtset.Tables("mascheckup_physical").Clear()

        sql = "SELECT * FROM frncheckup_physical WHERE idfrncheckup = '" & _frnCheckUp.idfrncheckup & "';"
        condb.GetTable(sql, dtset.Tables("mascheckup_physical"))
    End Sub
    Public Sub getMasPhysicalExam()
        Dim sql As String
        dtset.Tables("mascheckup_physical").Clear()

        sql = "SELECT * FROM mascheckup_physical WHERE status =1 ORDER BY order_number;"
        condb.GetTable(sql, dtset.Tables("mascheckup_physical"))
    End Sub

    Public Sub getOrtherLab()
        Dim sql As String
        dtset.Tables("mascheckup").Clear()
        dtset.Tables("frncheckupother").Clear()

        sql = "SELECT mascheckup.description,prdordelabresult.unit,prdordelabresult.normal,prdordelabresult.value,prdordelabresult.hn,prdordelabresult.an,prdordelabresult.vn,mascheckup.idmascheckup FROM (SELECT * FROM prdordelabresult WHERE vn ='" & _frnCheckUp.vn & "' ) AS prdordelabresult JOIN (  SELECT * FROM mascheckup WHERE  form ='dgvOtherResult') AS mascheckup  ON mascheckup.formcode = prdordelabresult.labcode  AND mascheckup.formname = prdordelabresult.name ;"
        condb.GetTable(sql, dtset.Tables("frncheckupother"))

        'Next
        setResultOtherChecmical()
    End Sub
    Public Function getMasCheckUpValue(ByVal Formname As String) As String
        Dim sql As String
        Dim dt As New DataTable
        Dim dt1 As New DataTable
        sql = "SELECT formcode , formname FROM mascheckup WHERE form = '" & Formname & "';"
        dt = condb.GetTable(sql)

        Dim sql1 As String
        sql1 = "SELECT value FROM prdordelabresult WHERE vn = '" & _frnCheckUp.vn & "' AND name = '" & dt.Rows(0)("formname") & "' AND labcode = '" & dt.Rows(0)("formcode") & "'"

        dt1 = condb.GetTable(sql1)
        If dt1.Rows.Count > 0 Then
            Return dt1.Rows(0)("value")
        Else

            Return ""
        End If
    End Function
    Public Sub getCBCCode(Optional opd As Boolean = True)
        Dim sql As String
        dtset.Tables("prdorderlabresult").Clear()

        If opd = True Then

            sql = "SELECT * FROM  (SELECT * from prdordelabresult  WHERE labcode = '2548' and  vn = '" & _frnCheckUp.vn & "' ) AS prdordelabresult JOIN ( SELECT * FROM myfriendsdb_real.mascheckup WHERE det = 0 ) AS mascheckup ON prdordelabresult.name = mascheckup.formname ;"

        Else
            sql = "SELECT labcode,name,unit,normal,value,hn,an,vn FROM prdorderlabresult WHERE labcode = '2548' AND an = '" & _frnCheckUp.vn & "'; "

        End If
        'sql = "SELECT * FROM  (SELECT * from myfriendsdb_chumphon.prdordelabresult  WHERE labcode = '2548' and vn = 5907110034 ) AS prdordelabresult JOIN ( SELECT * FROM myfriendsdb_real.mascheckup WHERE det = 0 ) AS mascheckup ON prdordelabresult.name = mascheckup.formname ;"
        condb.GetTable(sql, dtset.Tables("prdorderlabresult"))

    End Sub
    Public Sub getUrine(Optional opd As Boolean = True)
        dtset.Tables("prdorderlabresult").Clear()
        Dim sql As String
        If opd = True Then
            sql = "SELECT * FROM  (SELECT * from prdordelabresult  WHERE labcode = '2557' and vn = '" & _frnCheckUp.vn & "' ) AS prdordelabresult JOIN ( SELECT * FROM myfriendsdb_real.mascheckup WHERE det = 0 ) AS mascheckup ON prdordelabresult.name = mascheckup.formname"

        Else

            sql = "SELECT labcode,name,unit,normal,value,hn,an,vn FROM prdorderlabresult WHERE labcode = '2557' AND an = '" & _frnCheckUp.vn & "'; "

        End If
        'sql = "SELECT * FROM  (SELECT * from myfriendsdb_chumphon.prdordelabresult  WHERE labcode = '2557' and vn = 5907110034 ) AS prdordelabresult JOIN ( SELECT * FROM myfriendsdb_real.mascheckup WHERE det = 0 ) AS mascheckup ON prdordelabresult.name = mascheckup.formname ;"
        condb.GetTable(sql, dtset.Tables("prdorderlabresult"))
    End Sub





    Public Sub loadMascheckupDt()
        Dim sql As String
        dtset.Tables("mascheckupdt").Clear()
        sql = "SELECT * FROM mascheckupdt WHERE status = 1"
        condb.GetTable(sql, dtset.Tables("mascheckupdt"))

    End Sub
    Public Function lessthan(value1_1 As Double, value As Double) As Boolean
        If value < value1_1 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function morethan(value1_1 As Double, value As Double) As Boolean
        If value > value1_1 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function equal(value1_1 As Double, value As Double) As Boolean
        If value = value1_1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function conditionFormatNotnumeric(conditio1_1 As String, value1_1 As String, value As Double) As Boolean
        If value = value1_1 Then
            Return True
        Else
            Return False

        End If

    End Function

    Public Function conditionFormat(conditio1_1 As String, value1_1 As Double, value As Double) As Boolean
        If conditio1_1 = "<=" Then
            Return If(lessthan(value1_1, value) = True, True, equal(value1_1, value))
        ElseIf conditio1_1 = "<" Then
            Return If(lessthan(value1_1, value) = True, True, False)

        ElseIf conditio1_1 = ">" Then
            Return If(morethan(value1_1, value) = True, True, False)

        ElseIf conditio1_1 = ">=" Then
            Return If(morethan(value1_1, value) = True, True, equal(value1_1, value))

        ElseIf conditio1_1 = "=" Then
            Return If(equal(value1_1, value) = True, True, False)
        Else
            Return False

        End If


    End Function
    Public Function checkDBnull(fieldname As String, dtrow As DataRow) As Boolean
        Console.WriteLine(IsDBNull(dtrow(fieldname)))
        Return IsDBNull(dtrow(fieldname))
    End Function

    Public Function searchCondition(value As String, dtrow As DataRow) As Boolean

        If dtrow("f_sex") = 0 Then
            If If((checkDBnull("condition1_1", dtrow) Or checkDBnull("value1_1", dtrow)) = False, conditionFormat(dtrow("condition1_1"), dtrow("value1_1"), value), True) = True Then

                If If((checkDBnull("condition1_2", dtrow) Or checkDBnull("value1_2", dtrow)) = False, conditionFormat(dtrow("condition1_2"), dtrow("value1_2"), value), True) = True Then
                    Return True
                Else

                    Return False

                End If

            Else
                Return False
            End If

        ElseIf dtrow("f_sex") = 1 Then
            If _frnCheckUp.sex = "ชาย" Then
                If If((checkDBnull("condition1_1", dtrow) Or checkDBnull("value1_1", dtrow)) = False, conditionFormat(dtrow("condition1_1"), dtrow("value1_1"), value), True) = True Then

                    If If((checkDBnull("condition1_2", dtrow) Or checkDBnull("value1_2", dtrow)) = False, conditionFormat(dtrow("condition1_2"), dtrow("value1_2"), value), True) = True Then
                        Return True
                    Else

                        Return False

                    End If

                Else
                    Return False
                End If

            Else

                If If((checkDBnull("condition2_1", dtrow) Or checkDBnull("value2_1", dtrow)) = False, conditionFormat(dtrow("condition2_1"), dtrow("value2_1"), value), True) = True Then

                    If If((checkDBnull("condition2_2", dtrow) Or checkDBnull("value2_2", dtrow)) = False, conditionFormat(dtrow("condition2_2"), dtrow("value2_2"), value), True) = True Then
                        Return True
                    Else

                        Return False

                    End If

                Else
                    Return False
                End If
            End If
        End If





    End Function

    Public Function searchConditionNotnumeric(value As String, dtrow As DataRow) As Boolean
        If dtrow("f_sex") = 0 Then

            If If((checkDBnull("condition1_1", dtrow) Or checkDBnull("value1_1", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition1_1"), dtrow("value1_1"), value), True) = True Then

                If If((checkDBnull("condition1_2", dtrow) Or checkDBnull("value1_2", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition1_2"), dtrow("value1_2"), value), True) = True Then
                    Return True
                Else

                    Return False

                End If

            Else
                Return False
            End If
        ElseIf dtrow("f_sex") = 1 Then
            If _frnCheckUp.sex = "ชาย" Then
                If If((checkDBnull("condition1_1", dtrow) Or checkDBnull("value1_1", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition1_1"), dtrow("value1_1"), value), True) = True Then

                    If If((checkDBnull("condition1_2", dtrow) Or checkDBnull("value1_2", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition1_2"), dtrow("value1_2"), value), True) = True Then
                        Return True
                    Else

                        Return False

                    End If

                Else
                    Return False
                End If
            Else
                If If((checkDBnull("condition2_1", dtrow) Or checkDBnull("value2_1", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition2_1"), dtrow("value2_1"), value), True) = True Then

                    If If((checkDBnull("condition2_2", dtrow) Or checkDBnull("value2_2", dtrow)) = False, conditionFormatNotnumeric(dtrow("condition2_2"), dtrow("value2_2"), value), True) = True Then
                        Return True
                    Else

                        Return False

                    End If

                Else
                    Return False
                End If

            End If
        End If


    End Function

    Public Sub checkValueNotNumeric(value As String, idmascheckup As String, index As String)
        Dim dt As DataTable

        dt = searchResult(idmascheckup)

        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1

                If searchConditionNotnumeric(value, dt.Rows(i)) = True Then
                    dtset.Tables("frncheckupother").Rows(index)("result") = dt.Rows(i)("name")
                    dtset.Tables("frncheckupother").Rows(index)("result2") = dt.Rows(i)("name2")
                    dtset.Tables("frncheckupother").Rows(index)("f_result_all") = dt.Rows(i)("f_result_all")

                    Exit For
                Else

                End If
            Next

        Else

        End If
    End Sub
    Public Sub checkValue(value As Double, idmascheckup As String, index As String)
        Dim dt As DataTable

        dt = searchResult(idmascheckup)

        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                If searchCondition(value, dt.Rows(i)) = True Then
                    dtset.Tables("frncheckupother").Rows(index)("result") = dt.Rows(i)("name")
                    dtset.Tables("frncheckupother").Rows(index)("result2") = dt.Rows(i)("name2")
                    dtset.Tables("frncheckupother").Rows(index)("f_result_all") = dt.Rows(i)("f_result_all")

                    Exit For
                Else

                End If



            Next

        Else

        End If

    End Sub
    Public Sub setResultOtherChecmical()
        loadMascheckupDt()

        For i As Integer = 0 To dtset.Tables("frncheckupother").Rows.Count - 1
            If IsNumeric(dtset.Tables("frncheckupother").Rows(i)("value")) Then
                checkValue(dtset.Tables("frncheckupother").Rows(i)("value"), dtset.Tables("frncheckupother").Rows(i)("idmascheckup"), i)
            Else
                checkValueNotNumeric(dtset.Tables("frncheckupother").Rows(i)("value"), dtset.Tables("frncheckupother").Rows(i)("idmascheckup"), i)

            End If
        Next
    End Sub
    Public Function searchResult(ByVal idmascheckup) As DataTable
        Dim dvTest As New DataView
        Dim sqlFilter As String
        Dim dttable As New DataTable
        sqlFilter = "idmascheckup = '" & idmascheckup & "'"
        dvTest = dtset.Tables("mascheckupdt").DefaultView
        dvTest.RowFilter = sqlFilter

        Return dvTest.ToTable
    End Function

    Public Shared Function generateIDYearAndRuning(tableName As String, pk As String) As String
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim curdate As String = ""
        Dim dbHead As String = ""
        Dim dbRuning As String = ""
        Dim headLeight As Integer = 2
        Dim runLeight As Integer = 6

        db.BeginTrans()
        Dim id As String = ""
        Dim sql As String = "SELECT " & pk & ",SUBSTRING(" & pk & "," & headLeight + 1 & "," & runLeight & ") AS run,SUBSTRING(" & pk & ",1," & headLeight & ") AS head FROM  " & tableName & " ORDER BY " & pk & " DESC LIMIT 1;"
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            dbHead = dt.Rows(0)("head").ToString
            dbRuning = dt.Rows(0)("run").ToString
        End If
        sql = "SELECT DATE_FORMAT(CURRENT_TIMESTAMP(),'%y') cur"
        curdate = db.GetTable(sql).Rows(0)("cur").ToString
        If curdate = dbHead Then
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                numFormat &= "0"
            Next i
            id = dbHead & (Convert.ToInt32(dbRuning) + 1).ToString(numFormat)
        Else
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                If i = runLeight - 1 Then
                    numFormat &= "1"
                Else
                    numFormat &= "0"
                End If
            Next i
            id = curdate & numFormat
        End If
        db.CommitTrans()
        db.Dispose()
        Return id
    End Function
End Class

Public Class FriendsDate
    'Private USASettings As New List(Of RegionalSettings)
    'Private UKSettings As New List(Of RegionalSettings)
    'Private THSettings As New List(Of RegionalSettings)
    Private year As Integer
    Private month As Integer
    Private day As Integer

    Public ReadOnly Property _year
        Get
            Return year
        End Get
    End Property

    Public ReadOnly Property _month
        Get
            Return month
        End Get
    End Property

    Public ReadOnly Property _day
        Get
            Return day
        End Get
    End Property

    Public Shared ReadOnly Property Curdate As DateTime
        Get
            Dim db = ConnecDBRYH.NewConnection
            Dim dt As New DataTable
            dt = db.GetTable("SELECT current_timestamp()")
            db.Dispose()
            If dt.Rows.Count > 0 Then
                Return Convert.ToDateTime(dt.Rows(0)(0))
            Else
                Return Nothing
            End If
        End Get
    End Property

    Private Class RegionalSettings
        Public entry As String
        Public value As String
        Public Sub New(ByVal key As String, ByVal value As String)
            Me.entry = key : Me.value = value
        End Sub
    End Class
    ''' <summary>
    ''' เปลี่ยน Format วันที่ก่อน Save
    ''' </summary>
    ''' <remarks>- Output เปลี่ยน Format วันที่เป็น Eng</remarks>

    ''' <summary>
    ''' หาช่วงระยะห่างระหว่างวันที่
    ''' </summary>
    ''' <remarks>
    ''' - Input (date_birth = วันเกิด, date_now = วันปัจจุบัน)
    '''   - Output เก็บค่าระยะห่าง ปี,เดือน,วัน ลงใน Property year,mont,day
    ''' </remarks>
    Public Sub Date_Diff(ByVal date_birth As Date, ByVal date_now As Date)
        Try
            Dim datediff As TimeSpan
            'Dim date_birth_s() As String = Split(birth_datetimepicker.Text, "-") 'split ??????? Array ??????????
            'Dim date_now_s() As String = Split(Date.Today.ToString("dd-MM-yyyy", USCulture), "-")
            'Dim date_birth As Date
            'Dim date_now As Date
            'date_birth = New System.DateTime(CInt(date_birth_s(2)), CInt(date_birth_s(1)), CInt(date_birth_s(0)), 0, 0, 0)
            'date_now = New System.DateTime(date_now_s(2), date_now_s(1), date_now_s(0), 0, 0, 0)

            Dim minutes As Integer, seconds As Integer

            Dim ts As TimeSpan = DateTime.Now.Subtract(date_birth)


            month = 12 * (DateTime.Now.Year - date_birth.Year) + (DateTime.Now.Month - date_birth.Month)
            If DateTime.Now.Day < date_birth.Day Then
                month -= 1
                day = DateTime.DaysInMonth(date_birth.Year, date_birth.Month) - date_birth.Day + DateTime.Now.Day
            Else
                day = DateTime.Now.Day - date_birth.Day
            End If
            ' compute years & actual months
            year = Math.Floor(month / 12)
            month -= year * 12

            minutes = ts.Minutes
            seconds = ts.Seconds


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        'MsgBox(datediff.)
    End Sub
    ''' <summary>
    ''' ปรับ culTure เป็นไทย
    ''' </summary>
    ''' <remarks>- Output ปรับ culTure ของเครื่องเป็นไทย</remarks>
    Public Sub culTure_TH()
        Thread.CurrentThread.CurrentCulture = New CultureInfo("th-TH")
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("th-TH")
    End Sub

    ''' <summary>
    ''' ปรับ culTure เป็นอังกฤษ
    ''' </summary>
    ''' <remarks>- Output ปรับ culTure ของเครื่องเป็นอังกฤษ</remarks>
    Public Sub culTure_EN()
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")
    End Sub
    'Public Sub Date_Diff(ByVal date1 As Date, ByVal date2 As Date)
    '    Me.year = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Year, date1, date2))
    '    Me.month = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Month, date1, date2))
    '    Me.day = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Year, date1, date2))
    'End Sub

    ''' <summary>
    ''' ปรับ Registry ของเครื่องเป็น EN_US
    ''' </summary>
    ''' <remarks>- Output Registry ของเครื่องจะเปลี่ยนเป็น EN_US</remarks>
    ''' Public Sub Date_Diff(ByVal date1 As String, ByVal date2 As String)
    '''   Me.year = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Year, Convert.ToDateTime(date1), Convert.ToDateTime(date2)))
    '''   Me.month = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Month, Convert.ToDateTime(date1), Convert.ToDateTime(date2)))
    '''   Me.day = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Year, Convert.ToDateTime(date1), Convert.ToDateTime(date2)))
    ''' End Sub

    Public Sub TO_EN_US()
        Dim USASettings As New List(Of RegionalSettings)
        With USASettings
            .Add(New RegionalSettings("iCountry", "1"))
            .Add(New RegionalSettings("iCurrDigits", "2"))
            .Add(New RegionalSettings("iCurrency", "0"))
            .Add(New RegionalSettings("iDate", "1"))
            .Add(New RegionalSettings("iDigits", "2"))
            .Add(New RegionalSettings("iLZero", "1"))
            .Add(New RegionalSettings("iMeasure", "1"))
            .Add(New RegionalSettings("iNegCurr", "0"))
            .Add(New RegionalSettings("iTime", "0"))
            .Add(New RegionalSettings("iTLZero", "0"))
            .Add(New RegionalSettings("Locale", "00000409"))
            .Add(New RegionalSettings("s1159", "AM"))
            .Add(New RegionalSettings("s2359", "PM"))
            .Add(New RegionalSettings("sCountry", "United States"))
            .Add(New RegionalSettings("sCurrency", "$"))
            .Add(New RegionalSettings("sDate", "/"))
            .Add(New RegionalSettings("sDecimal", "."))
            .Add(New RegionalSettings("sLanguage", "ENU"))
            .Add(New RegionalSettings("sList", ","))
            .Add(New RegionalSettings("sLongDate", "dd/MM/yyyy"))
            .Add(New RegionalSettings("sShortDate", "dd/MM/yyyy"))
            .Add(New RegionalSettings("sThousand", ","))
            .Add(New RegionalSettings("sTime", ":"))
            .Add(New RegionalSettings("sTimeFormat", "h:mm:ss"))
            .Add(New RegionalSettings("iTimePrefix", "0"))
            .Add(New RegionalSettings("sMonDecimalSep", "."))
            .Add(New RegionalSettings("sMonThousandSep", ","))
            .Add(New RegionalSettings("iNegNumber", "1"))
            .Add(New RegionalSettings("sNativeDigits", "0123456789"))
            .Add(New RegionalSettings("NumShape", "1"))
            .Add(New RegionalSettings("iCalendarType", "1"))
            .Add(New RegionalSettings("iFirstDayOfWeek", "6"))
            .Add(New RegionalSettings("iFirstWeekOfYear", "0"))
            .Add(New RegionalSettings("sGrouping", "3;0"))
            .Add(New RegionalSettings("sMonGrouping", "3;0"))
            .Add(New RegionalSettings("sPositiveSign", ""))
            .Add(New RegionalSettings("sNegativeSign", "-"))
        End With

        For Each reg As RegionalSettings In USASettings
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", reg.entry, reg.value)
        Next
        NotifyInternationalChanges()
    End Sub

    ''' <summary>
    ''' ปรับ Registry ของเครื่องเป็น EN_UK
    ''' </summary>
    ''' <remarks>- Output  Registry ของเครื่องจะเปลี่ยนเป็น EN_UK</remarks>
    Public Sub TO_EN_UK()
        Dim UKSettings As New List(Of RegionalSettings)
        With UKSettings
            .Add(New RegionalSettings("iCountry", "44"))
            .Add(New RegionalSettings("iCurrDigits", "2"))
            .Add(New RegionalSettings("iCurrency", "0"))
            .Add(New RegionalSettings("iDate", "1"))
            .Add(New RegionalSettings("iDigits", "2"))
            .Add(New RegionalSettings("iLZero", "1"))
            .Add(New RegionalSettings("iMeasure", "0"))
            .Add(New RegionalSettings("iNegCurr", "1"))
            .Add(New RegionalSettings("iTime", "1"))
            .Add(New RegionalSettings("iTLZero", "1"))
            .Add(New RegionalSettings("Locale", "00000809"))
            .Add(New RegionalSettings("s1159", "AM"))
            .Add(New RegionalSettings("s2359", "PM"))
            .Add(New RegionalSettings("sCountry", "United Kingdom"))
            .Add(New RegionalSettings("sCurrency", "£"))
            .Add(New RegionalSettings("sDate", "/"))
            .Add(New RegionalSettings("sDecimal", "."))
            .Add(New RegionalSettings("sLanguage", "ENG"))
            .Add(New RegionalSettings("sList", ","))
            .Add(New RegionalSettings("sLongDate", "dd MMMM yyyy"))
            .Add(New RegionalSettings("sShortDate", "dd/MM/yyyy"))
            .Add(New RegionalSettings("sThousand", ","))
            .Add(New RegionalSettings("sTime", ":"))
            .Add(New RegionalSettings("sTimeFormat", "HH:mm:ss"))
            .Add(New RegionalSettings("iTimePrefix", "0"))
            .Add(New RegionalSettings("sMonDecimalSep", "."))
            .Add(New RegionalSettings("sMonThousandSep", ","))
            .Add(New RegionalSettings("iNegNumber", "1"))
            .Add(New RegionalSettings("sNativeDigits", "0123456789"))
            .Add(New RegionalSettings("NumShape", "1"))
            .Add(New RegionalSettings("iCalendarType", "1"))
            .Add(New RegionalSettings("iFirstDayOfWeek", "0"))
            .Add(New RegionalSettings("iFirstWeekOfYear", "0"))
            .Add(New RegionalSettings("sGrouping", "3;0"))
            .Add(New RegionalSettings("sMonGrouping", "3;0"))
            .Add(New RegionalSettings("sPositiveSign", ""))
            .Add(New RegionalSettings("sNegativeSign", "-"))
        End With
        For Each reg As RegionalSettings In UKSettings
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", reg.entry, reg.value)
        Next
        NotifyInternationalChanges()
    End Sub

    ''' <summary>
    ''' ปรับ Registry ของเครื่องเป็น TH
    ''' </summary>
    ''' <remarks>- Output ปรับ Registry ของเครื่องเป็น TH</remarks>
    Public Sub TO_TH()
        Dim THSettings As New List(Of RegionalSettings)
        With THSettings
            .Add(New RegionalSettings("iCountry", "66"))
            .Add(New RegionalSettings("iCurrDigits", "2"))
            .Add(New RegionalSettings("iCurrency", "0"))
            .Add(New RegionalSettings("iDate", "1"))
            .Add(New RegionalSettings("iDigits", "2"))
            .Add(New RegionalSettings("LocaleName", "th-TH"))
            .Add(New RegionalSettings("iLZero", "0"))
            .Add(New RegionalSettings("iMeasure", "0"))
            .Add(New RegionalSettings("iNegCurr", "1"))
            .Add(New RegionalSettings("iTime", "1"))
            .Add(New RegionalSettings("iTLZero", "1"))
            .Add(New RegionalSettings("Locale", "0000041E"))
            .Add(New RegionalSettings("s1159", "AM"))
            .Add(New RegionalSettings("s2359", "PM"))
            .Add(New RegionalSettings("sCountry", "Thailand"))
            .Add(New RegionalSettings("sCurrency", "฿"))
            .Add(New RegionalSettings("sDate", "/"))
            .Add(New RegionalSettings("sDecimal", "."))
            .Add(New RegionalSettings("sLanguage", "THA"))
            .Add(New RegionalSettings("sList", ","))
            .Add(New RegionalSettings("sLongDate", "dd/MMMM/yyyy"))
            .Add(New RegionalSettings("sShortDate", "dd/MM/yyyy"))
            .Add(New RegionalSettings("sThousand", ","))
            .Add(New RegionalSettings("sTime", ":"))
            .Add(New RegionalSettings("sTimeFormat", "HH:mm:ss"))
            .Add(New RegionalSettings("iTimePrefix", "0"))
            .Add(New RegionalSettings("sMonDecimalSep", "."))
            .Add(New RegionalSettings("sMonThousandSep", ","))
            .Add(New RegionalSettings("iNegNumber", "1"))
            .Add(New RegionalSettings("sNativeDigits", "๐๑๒๓๔๕๖๗๘๙"))
            .Add(New RegionalSettings("NumShape", "1"))
            .Add(New RegionalSettings("iCalendarType", "7"))
            .Add(New RegionalSettings("iFirstDayOfWeek", "0"))
            .Add(New RegionalSettings("iFirstWeekOfYear", "0"))
            .Add(New RegionalSettings("sGrouping", "3;0"))
            .Add(New RegionalSettings("sMonGrouping", "3;0"))
            .Add(New RegionalSettings("sPositiveSign", ""))
            .Add(New RegionalSettings("sNegativeSign", "-"))
        End With
        For Each reg As RegionalSettings In THSettings
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", reg.entry, reg.value)
        Next

        NotifyInternationalChanges()
    End Sub


    Private Sub NotifyInternationalChanges()
        'Ref: http://msdn.microsoft.com/en-us/library/windows/desktop/ms725497%28v=vs.85%29.aspx
        Dim HWND_BROADCAST As New IntPtr(&HFFFF) 'broadcast to entire system
        Dim Lparam As IntPtr = System.Runtime.InteropServices.Marshal.StringToBSTR("intl")
        SendNotifyMessage(HWND_BROADCAST, &H1A, UIntPtr.Zero, Lparam)
        System.Runtime.InteropServices.Marshal.FreeBSTR(Lparam)
    End Sub

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Public Shared Function SendNotifyMessage(ByVal hWnd As IntPtr,
                                            ByVal msg As UInt32,
                                            ByVal wParam As UIntPtr,
                                            ByVal lParam As IntPtr) As Boolean
    End Function
    ''' <summary>
    ''' หาวันเกิดจากอายุ
    ''' </summary>
    ''' <remarks>- Input (age  = อายุ)</remarks>
    ''' <returns>วันเกิดผู้ป่วย</returns>
    Public Function GENBIRTH(ByVal age As Integer) As DateTime
        Dim ConnectionDB As ConnecDBRYH = ConnecDBRYH.NewConnection()
        Dim sql As String
        Dim a As DateTime
        Dim y As Integer
        Try
            sql = "SELECT curdate()"
            a = Convert.ToDateTime(ConnectionDB.GetTable(sql).Rows(0)(0))
            y = Convert.ToInt32(a.ToString("yyyy")) - age
            a = Convert.ToDateTime(y.ToString & "/01/01")
            Return a
        Catch ex As Exception

        End Try

    End Function

    ''' <summary>
    ''' ดึงวันปัจจุบันจากฐานข้อมูล
    ''' </summary>
    ''' <returns>วันที่ปัจจุบัน</returns>
    Public Shared Function Today() As DateTime
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection()
        Dim sql As String
        sql = "SELECT curdate()"
        Dim dt As New DataTable()
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            Return Convert.ToDateTime(dt.Rows(0)(0))
        Else
            Return DateTime.Now
        End If
        db.Dispose()
    End Function
End Class
