﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckUp07
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtformcode = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP4 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.value2_2 = New DevExpress.XtraEditors.TextEdit()
        Me.value1_2 = New DevExpress.XtraEditors.TextEdit()
        Me.value2_1 = New DevExpress.XtraEditors.TextEdit()
        Me.value1_1 = New DevExpress.XtraEditors.TextEdit()
        Me.chkSex = New DevExpress.XtraEditors.CheckEdit()
        Me.condition2_2 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP3 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit4View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.condition2_1 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.condition1_1 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.condition1_2 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.chkstatus = New DevExpress.XtraEditors.CheckEdit()
        Me.txtname_dt = New DevExpress.XtraEditors.MemoEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.chk_result_all = New DevExpress.XtraEditors.CheckEdit()
        Me.txtname2_dt = New DevExpress.XtraEditors.MemoEdit()
        Me.txtformname = New DevExpress.XtraEditors.TextEdit()
        Me.txtdescriptions = New DevExpress.XtraEditors.TextEdit()
        Me.txtname2 = New DevExpress.XtraEditors.TextEdit()
        Me.txtname = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.DtCheckUP1 = New CheckUpProject.dtCheckUP()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.coldescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colunit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colnormal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvalue = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colhn = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colan = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvn = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformcode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidmascheckup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colf_sex = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP2 = New CheckUpProject.dtCheckUP()
        Me.MasconditionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtformcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.value2_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.value1_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.value2_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.value1_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSex.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.condition2_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.condition2_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.condition1_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.condition1_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkstatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtname_dt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_result_all.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtname2_dt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtformname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtdescriptions.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtname2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasconditionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtformcode)
        Me.LayoutControl1.Controls.Add(Me.value2_2)
        Me.LayoutControl1.Controls.Add(Me.value1_2)
        Me.LayoutControl1.Controls.Add(Me.value2_1)
        Me.LayoutControl1.Controls.Add(Me.value1_1)
        Me.LayoutControl1.Controls.Add(Me.chkSex)
        Me.LayoutControl1.Controls.Add(Me.condition2_2)
        Me.LayoutControl1.Controls.Add(Me.condition2_1)
        Me.LayoutControl1.Controls.Add(Me.condition1_1)
        Me.LayoutControl1.Controls.Add(Me.condition1_2)
        Me.LayoutControl1.Controls.Add(Me.chkstatus)
        Me.LayoutControl1.Controls.Add(Me.txtname_dt)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.btnSave)
        Me.LayoutControl1.Controls.Add(Me.chk_result_all)
        Me.LayoutControl1.Controls.Add(Me.txtname2_dt)
        Me.LayoutControl1.Controls.Add(Me.txtformname)
        Me.LayoutControl1.Controls.Add(Me.txtdescriptions)
        Me.LayoutControl1.Controls.Add(Me.txtname2)
        Me.LayoutControl1.Controls.Add(Me.txtname)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(823, 500)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txtformcode
        '
        Me.txtformcode.Location = New System.Drawing.Point(117, 84)
        Me.txtformcode.Name = "txtformcode"
        Me.txtformcode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtformcode.Properties.DataSource = Me.BindingSource3
        Me.txtformcode.Properties.DisplayMember = "formname"
        Me.txtformcode.Properties.ReadOnly = True
        Me.txtformcode.Properties.ValueMember = "formcode"
        Me.txtformcode.Properties.View = Me.SearchLookUpEdit1View
        Me.txtformcode.Size = New System.Drawing.Size(215, 20)
        Me.txtformcode.StyleController = Me.LayoutControl1
        Me.txtformcode.TabIndex = 34
        '
        'BindingSource3
        '
        Me.BindingSource3.DataMember = "dgvOtherResult"
        Me.BindingSource3.DataSource = Me.DtCheckUP4
        Me.BindingSource3.Sort = ""
        '
        'DtCheckUP4
        '
        Me.DtCheckUP4.DataSetName = "dtCheckUP"
        Me.DtCheckUP4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'value2_2
        '
        Me.value2_2.Location = New System.Drawing.Point(180, 204)
        Me.value2_2.Name = "value2_2"
        Me.value2_2.Size = New System.Drawing.Size(152, 20)
        Me.value2_2.StyleController = Me.LayoutControl1
        Me.value2_2.TabIndex = 33
        '
        'value1_2
        '
        Me.value1_2.Location = New System.Drawing.Point(180, 156)
        Me.value1_2.Name = "value1_2"
        Me.value1_2.Size = New System.Drawing.Size(152, 20)
        Me.value1_2.StyleController = Me.LayoutControl1
        Me.value1_2.TabIndex = 32
        '
        'value2_1
        '
        Me.value2_1.Location = New System.Drawing.Point(180, 180)
        Me.value2_1.Name = "value2_1"
        Me.value2_1.Size = New System.Drawing.Size(152, 20)
        Me.value2_1.StyleController = Me.LayoutControl1
        Me.value2_1.TabIndex = 31
        '
        'value1_1
        '
        Me.value1_1.Location = New System.Drawing.Point(180, 132)
        Me.value1_1.Name = "value1_1"
        Me.value1_1.Size = New System.Drawing.Size(152, 20)
        Me.value1_1.StyleController = Me.LayoutControl1
        Me.value1_1.TabIndex = 30
        '
        'chkSex
        '
        Me.chkSex.Location = New System.Drawing.Point(12, 397)
        Me.chkSex.Name = "chkSex"
        Me.chkSex.Properties.Caption = "แบ่งเพศ(ติ้กถูกCon1 จะเป็น ชาย , Con2 เป็นหญิง)"
        Me.chkSex.Size = New System.Drawing.Size(320, 19)
        Me.chkSex.StyleController = Me.LayoutControl1
        Me.chkSex.TabIndex = 28
        '
        'condition2_2
        '
        Me.condition2_2.Location = New System.Drawing.Point(117, 204)
        Me.condition2_2.Name = "condition2_2"
        Me.condition2_2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.condition2_2.Properties.DataSource = Me.BindingSource2
        Me.condition2_2.Properties.DisplayMember = "condition"
        Me.condition2_2.Properties.ValueMember = "condition"
        Me.condition2_2.Properties.View = Me.SearchLookUpEdit4View
        Me.condition2_2.Size = New System.Drawing.Size(59, 20)
        Me.condition2_2.StyleController = Me.LayoutControl1
        Me.condition2_2.TabIndex = 25
        '
        'BindingSource2
        '
        Me.BindingSource2.DataMember = "mascondition"
        Me.BindingSource2.DataSource = Me.DtCheckUP3
        Me.BindingSource2.Sort = ""
        '
        'DtCheckUP3
        '
        Me.DtCheckUP3.DataSetName = "dtCheckUP"
        Me.DtCheckUP3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit4View
        '
        Me.SearchLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit4View.Name = "SearchLookUpEdit4View"
        Me.SearchLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit4View.OptionsView.ShowGroupPanel = False
        '
        'condition2_1
        '
        Me.condition2_1.Location = New System.Drawing.Point(117, 180)
        Me.condition2_1.Name = "condition2_1"
        Me.condition2_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.condition2_1.Properties.DataSource = Me.BindingSource2
        Me.condition2_1.Properties.DisplayMember = "condition"
        Me.condition2_1.Properties.ValueMember = "condition"
        Me.condition2_1.Properties.View = Me.SearchLookUpEdit3View
        Me.condition2_1.Size = New System.Drawing.Size(59, 20)
        Me.condition2_1.StyleController = Me.LayoutControl1
        Me.condition2_1.TabIndex = 24
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'condition1_1
        '
        Me.condition1_1.Location = New System.Drawing.Point(117, 132)
        Me.condition1_1.Name = "condition1_1"
        Me.condition1_1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.condition1_1.Properties.DataSource = Me.BindingSource2
        Me.condition1_1.Properties.DisplayMember = "condition"
        Me.condition1_1.Properties.ValueMember = "condition"
        Me.condition1_1.Properties.View = Me.SearchLookUpEdit2View
        Me.condition1_1.Size = New System.Drawing.Size(59, 20)
        Me.condition1_1.StyleController = Me.LayoutControl1
        Me.condition1_1.TabIndex = 23
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'condition1_2
        '
        Me.condition1_2.Location = New System.Drawing.Point(117, 156)
        Me.condition1_2.Name = "condition1_2"
        Me.condition1_2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.condition1_2.Properties.DataSource = Me.BindingSource2
        Me.condition1_2.Properties.DisplayMember = "condition"
        Me.condition1_2.Properties.ValueMember = "condition"
        Me.condition1_2.Properties.View = Me.GridView2
        Me.condition1_2.Size = New System.Drawing.Size(59, 20)
        Me.condition1_2.StyleController = Me.LayoutControl1
        Me.condition1_2.TabIndex = 22
        '
        'GridView2
        '
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'chkstatus
        '
        Me.chkstatus.Location = New System.Drawing.Point(12, 420)
        Me.chkstatus.Name = "chkstatus"
        Me.chkstatus.Properties.Caption = "Status"
        Me.chkstatus.Size = New System.Drawing.Size(320, 19)
        Me.chkstatus.StyleController = Me.LayoutControl1
        Me.chkstatus.TabIndex = 16
        '
        'txtname_dt
        '
        Me.txtname_dt.Location = New System.Drawing.Point(117, 228)
        Me.txtname_dt.Name = "txtname_dt"
        Me.txtname_dt.Size = New System.Drawing.Size(215, 82)
        Me.txtname_dt.StyleController = Me.LayoutControl1
        Me.txtname_dt.TabIndex = 11
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(186, 466)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(146, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 15
        Me.SimpleButton2.Text = "Clear"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(12, 466)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(170, 22)
        Me.btnSave.StyleController = Me.LayoutControl1
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "Save"
        '
        'chk_result_all
        '
        Me.chk_result_all.Location = New System.Drawing.Point(12, 443)
        Me.chk_result_all.Name = "chk_result_all"
        Me.chk_result_all.Properties.Caption = "f_result_all"
        Me.chk_result_all.Size = New System.Drawing.Size(320, 19)
        Me.chk_result_all.StyleController = Me.LayoutControl1
        Me.chk_result_all.TabIndex = 13
        '
        'txtname2_dt
        '
        Me.txtname2_dt.Location = New System.Drawing.Point(117, 314)
        Me.txtname2_dt.Name = "txtname2_dt"
        Me.txtname2_dt.Size = New System.Drawing.Size(215, 79)
        Me.txtname2_dt.StyleController = Me.LayoutControl1
        Me.txtname2_dt.TabIndex = 12
        '
        'txtformname
        '
        Me.txtformname.Location = New System.Drawing.Point(117, 108)
        Me.txtformname.Name = "txtformname"
        Me.txtformname.Properties.ReadOnly = True
        Me.txtformname.Size = New System.Drawing.Size(215, 20)
        Me.txtformname.StyleController = Me.LayoutControl1
        Me.txtformname.TabIndex = 9
        '
        'txtdescriptions
        '
        Me.txtdescriptions.Location = New System.Drawing.Point(117, 60)
        Me.txtdescriptions.Name = "txtdescriptions"
        Me.txtdescriptions.Properties.ReadOnly = True
        Me.txtdescriptions.Size = New System.Drawing.Size(215, 20)
        Me.txtdescriptions.StyleController = Me.LayoutControl1
        Me.txtdescriptions.TabIndex = 7
        '
        'txtname2
        '
        Me.txtname2.Location = New System.Drawing.Point(117, 36)
        Me.txtname2.Name = "txtname2"
        Me.txtname2.Properties.ReadOnly = True
        Me.txtname2.Size = New System.Drawing.Size(215, 20)
        Me.txtname2.StyleController = Me.LayoutControl1
        Me.txtname2.TabIndex = 6
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(117, 12)
        Me.txtname.Name = "txtname"
        Me.txtname.Properties.ReadOnly = True
        Me.txtname.Size = New System.Drawing.Size(215, 20)
        Me.txtname.StyleController = Me.LayoutControl1
        Me.txtname.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "mascheckup"
        Me.GridControl1.DataSource = Me.DtCheckUP1
        Me.GridControl1.Location = New System.Drawing.Point(336, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(475, 476)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtCheckUP1
        '
        Me.DtCheckUP1.DataSetName = "dtCheckUP"
        Me.DtCheckUP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.coldescription, Me.colunit, Me.colnormal, Me.colvalue, Me.colhn, Me.colan, Me.colvn, Me.colformname, Me.colformcode, Me.colname, Me.colname2, Me.colidmascheckup, Me.colstatus, Me.colf_sex})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'coldescription
        '
        Me.coldescription.FieldName = "description"
        Me.coldescription.Name = "coldescription"
        Me.coldescription.OptionsColumn.AllowEdit = False
        Me.coldescription.OptionsColumn.ReadOnly = True
        Me.coldescription.Visible = True
        Me.coldescription.VisibleIndex = 1
        Me.coldescription.Width = 60
        '
        'colunit
        '
        Me.colunit.FieldName = "unit"
        Me.colunit.Name = "colunit"
        Me.colunit.OptionsColumn.AllowEdit = False
        Me.colunit.OptionsColumn.ReadOnly = True
        '
        'colnormal
        '
        Me.colnormal.FieldName = "normal"
        Me.colnormal.Name = "colnormal"
        Me.colnormal.OptionsColumn.AllowEdit = False
        Me.colnormal.OptionsColumn.ReadOnly = True
        '
        'colvalue
        '
        Me.colvalue.FieldName = "value"
        Me.colvalue.Name = "colvalue"
        Me.colvalue.OptionsColumn.AllowEdit = False
        Me.colvalue.OptionsColumn.ReadOnly = True
        '
        'colhn
        '
        Me.colhn.FieldName = "hn"
        Me.colhn.Name = "colhn"
        Me.colhn.OptionsColumn.AllowEdit = False
        Me.colhn.OptionsColumn.ReadOnly = True
        '
        'colan
        '
        Me.colan.FieldName = "an"
        Me.colan.Name = "colan"
        Me.colan.OptionsColumn.AllowEdit = False
        Me.colan.OptionsColumn.ReadOnly = True
        '
        'colvn
        '
        Me.colvn.FieldName = "vn"
        Me.colvn.Name = "colvn"
        Me.colvn.OptionsColumn.AllowEdit = False
        Me.colvn.OptionsColumn.ReadOnly = True
        '
        'colformname
        '
        Me.colformname.FieldName = "formname"
        Me.colformname.Name = "colformname"
        Me.colformname.OptionsColumn.AllowEdit = False
        Me.colformname.OptionsColumn.ReadOnly = True
        Me.colformname.Visible = True
        Me.colformname.VisibleIndex = 2
        Me.colformname.Width = 60
        '
        'colformcode
        '
        Me.colformcode.FieldName = "formcode"
        Me.colformcode.Name = "colformcode"
        Me.colformcode.OptionsColumn.AllowEdit = False
        Me.colformcode.OptionsColumn.ReadOnly = True
        Me.colformcode.Visible = True
        Me.colformcode.VisibleIndex = 3
        Me.colformcode.Width = 60
        '
        'colname
        '
        Me.colname.FieldName = "name"
        Me.colname.Name = "colname"
        Me.colname.OptionsColumn.AllowEdit = False
        Me.colname.OptionsColumn.ReadOnly = True
        Me.colname.Visible = True
        Me.colname.VisibleIndex = 4
        Me.colname.Width = 60
        '
        'colname2
        '
        Me.colname2.FieldName = "name2"
        Me.colname2.Name = "colname2"
        Me.colname2.OptionsColumn.AllowEdit = False
        Me.colname2.OptionsColumn.ReadOnly = True
        Me.colname2.Visible = True
        Me.colname2.VisibleIndex = 5
        Me.colname2.Width = 60
        '
        'colidmascheckup
        '
        Me.colidmascheckup.FieldName = "idmascheckup"
        Me.colidmascheckup.Name = "colidmascheckup"
        Me.colidmascheckup.OptionsColumn.AllowEdit = False
        Me.colidmascheckup.OptionsColumn.ReadOnly = True
        Me.colidmascheckup.Visible = True
        Me.colidmascheckup.VisibleIndex = 0
        Me.colidmascheckup.Width = 20
        '
        'colstatus
        '
        Me.colstatus.FieldName = "status"
        Me.colstatus.Name = "colstatus"
        Me.colstatus.OptionsColumn.AllowEdit = False
        Me.colstatus.OptionsColumn.ReadOnly = True
        Me.colstatus.Visible = True
        Me.colstatus.VisibleIndex = 7
        Me.colstatus.Width = 45
        '
        'colf_sex
        '
        Me.colf_sex.FieldName = "f_sex"
        Me.colf_sex.Name = "colf_sex"
        Me.colf_sex.Visible = True
        Me.colf_sex.VisibleIndex = 6
        Me.colf_sex.Width = 35
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem7, Me.LayoutControlItem15, Me.LayoutControlItem19, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem25, Me.LayoutControlItem24, Me.LayoutControlItem16, Me.LayoutControlItem18, Me.LayoutControlItem14})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(823, 500)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(324, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(479, 480)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtname
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem2.Text = "Name"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtname2
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem3.Text = "Name2"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtdescriptions
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem4.Text = "Description"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtformname
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem6.Text = "formname"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txtname_dt
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 216)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(324, 86)
        Me.LayoutControlItem8.Text = "Result"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.txtname2_dt
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 302)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(324, 83)
        Me.LayoutControlItem9.Text = "Result2"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.chk_result_all
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 431)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(324, 23)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.btnSave
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 454)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(174, 26)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.SimpleButton2
        Me.LayoutControlItem12.Location = New System.Drawing.Point(174, 454)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(150, 26)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.chkstatus
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 408)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(324, 23)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.condition1_2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(168, 24)
        Me.LayoutControlItem7.Text = "Con1_2"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.condition1_1
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(168, 24)
        Me.LayoutControlItem15.Text = "Con1_1"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.condition2_2
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 192)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(168, 24)
        Me.LayoutControlItem19.Text = "Con2_2"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.chkSex
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 385)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(324, 23)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.value1_1
        Me.LayoutControlItem23.Location = New System.Drawing.Point(168, 120)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(156, 24)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.value2_2
        Me.LayoutControlItem25.Location = New System.Drawing.Point(168, 192)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(156, 24)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.value1_2
        Me.LayoutControlItem24.Location = New System.Drawing.Point(168, 144)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(156, 24)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.value2_1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(168, 168)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(156, 24)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.condition2_1
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(168, 24)
        Me.LayoutControlItem18.Text = "Con2_1"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.txtformcode
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(102, 13)
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "MasText"
        Me.BindingSource1.DataSource = Me.DtCheckUP2
        Me.BindingSource1.Sort = ""
        '
        'DtCheckUP2
        '
        Me.DtCheckUP2.DataSetName = "dtCheckUP"
        Me.DtCheckUP2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MasconditionBindingSource
        '
        Me.MasconditionBindingSource.DataMember = "mascondition"
        Me.MasconditionBindingSource.DataSource = Me.DtCheckUP1
        '
        'frmCheckUp07
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(823, 500)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmCheckUp07"
        Me.Text = "frmCheckUp06"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtformcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.value2_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.value1_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.value2_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.value1_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSex.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.condition2_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.condition2_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.condition1_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.condition1_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkstatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtname_dt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_result_all.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtname2_dt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtformname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtdescriptions.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtname2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasconditionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chk_result_all As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtname2_dt As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtname_dt As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtformname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtdescriptions As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtname2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DtCheckUP1 As dtCheckUP
    Friend WithEvents chkstatus As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents DtCheckUP2 As dtCheckUP
    Friend WithEvents coldescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colunit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colnormal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvalue As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colhn As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colan As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvn As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformcode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidmascheckup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSex As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents condition2_2 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit4View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents condition2_1 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents condition1_1 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents condition1_2 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource2 As BindingSource
    Friend WithEvents DtCheckUP3 As dtCheckUP
    Friend WithEvents MasconditionBindingSource As BindingSource
    Friend WithEvents colf_sex As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents value2_2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents value1_2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents value2_1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents value1_1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtformcode As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource3 As BindingSource
    Friend WithEvents DtCheckUP4 As dtCheckUP
End Class
