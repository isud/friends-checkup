﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCheckUp04
    Dim dtset As New dtCheckUP
    Dim checkupClass As checkupClass
    Dim frmcheckUp04 As frmcheckUp04Class
    Dim mascheckup_project As frmcheckUp04Class.mascheckup_project
    Private Sub frmCheckUp04_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadDefault()
    End Sub
    Public Sub LoadDefault()
        checkupClass = New checkupClass(dtset)
        checkupClass.getData(txtEye.Name)
        txtEye.Properties.DataSource = dtset.Tables(txtEye.Name)

        checkupClass.getData(txtTooth.Name)
        txtTooth.Properties.DataSource = dtset.Tables(txtTooth.Name)

        checkupClass.getData(txtBlindness.Name)
        txtBlindness.Properties.DataSource = dtset.Tables(txtBlindness.Name)


        checkupClass.getData(txtEyeLR.Name)
        txtEyeLR.Properties.DataSource = dtset.Tables(txtEyeLR.Name)
        checkupClass.getData(txtblood.Name)
        txtblood.Properties.DataSource = dtset.Tables(txtblood.Name)
        checkupClass.getData(txtUrineresult.Name)
        txtUrineresult.Properties.DataSource = dtset.Tables(txtUrineresult.Name)

        checkupClass.getData(txtVDRLresult.Name)
        txtVDRLresult.Properties.DataSource = dtset.Tables(txtVDRLresult.Name)

        checkupClass.getData(txtCEAResult.Name)
        txtCEAResult.Properties.DataSource = dtset.Tables(txtCEAResult.Name)


        checkupClass.getData(txtXrayResult.Name)
        txtXrayResult.Properties.DataSource = dtset.Tables(txtXrayResult.Name)

        checkupClass.getData(txtHivResult.Name)
        txtHivResult.Properties.DataSource = dtset.Tables(txtHivResult.Name)

        checkupClass.getData(txtVirusBResult.Name)
        txtVirusBResult.Properties.DataSource = dtset.Tables(txtVirusBResult.Name)


        checkupClass.getData(txtVirusAResult.Name)
        txtVirusAResult.Properties.DataSource = dtset.Tables(txtVirusAResult.Name)

        checkupClass.getmascheckup_physical_result()
        checkUpPhysical_result.Properties.DataSource = dtset.Tables("mascheckup_physical_result")



        loadMasterToGrid()

    End Sub
    Public Sub loadMasterToGrid()
        frmcheckUp04 = New frmcheckUp04Class(dtset)
        frmcheckUp04.getMascheckup_project()
        GridControl1.DataSource = dtset
    End Sub
    Public Sub setmascheckup_project()

        mascheckup_project.project_name = txtProjectName.EditValue
        mascheckup_project.txtEyeLR = txtEyeLR.EditValue
        mascheckup_project.txtBlindness = txtBlindness.EditValue
        mascheckup_project.txtEye = txtEye.EditValue
        mascheckup_project.txtTooth = txtTooth.EditValue
        mascheckup_project.txtblood = txtblood.EditValue
        mascheckup_project.txtUrineresult = txtUrineresult.EditValue
        mascheckup_project.txtVDRLresult = txtVDRLresult.EditValue
        mascheckup_project.txtCEAResult = txtCEAResult.EditValue
        mascheckup_project.txtXrayResult = txtXrayResult.EditValue
        mascheckup_project.txtHivResult = txtHivResult.EditValue
        mascheckup_project.txtVirusBResult = txtVirusBResult.EditValue
        mascheckup_project.txtVirusAResult = txtVirusAResult.EditValue
        mascheckup_project.CheckExamination = CheckExamination.EditValue
        mascheckup_project.CheckExamination2 = CheckExamination2.EditValue
        mascheckup_project.checkBlood = checkBlood.EditValue
        mascheckup_project.checkUrineAnalysis = checkUrineAnalysis.EditValue
        mascheckup_project.checkChemicalLab = checkChemicalLab.EditValue
        mascheckup_project.checkChemicallab2 = checkChemicallab2.EditValue
        mascheckup_project.status = status.EditValue


    End Sub

    Public Sub getDefaultCompany()
        mascheckup_project.idmascheckup_project = dtset.Tables("mascheckup_project").Rows(0)("idmascheckup_project")

        txtProjectName.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)("project_name")), Nothing, dtset.Tables("mascheckup_project").Rows(0)("project_name"))
        txtEyeLR.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtEyeLR.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtEyeLR.Name))

        txtBlindness.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtBlindness.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtBlindness.Name))


        txtEye.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtEye.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtEye.Name))

        txtTooth.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name))



        txtblood.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtblood.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtblood.Name))
        ' MsgBox(dtset.Tables("mascheckup_project").Rows(0)("txtUrineresult"))

        txtUrineresult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtUrineresult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtUrineresult.Name))


        txtVDRLresult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVDRLresult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVDRLresult.Name))

        txtCEAResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtCEAResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtCEAResult.Name))



        txtXrayResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtXrayResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtXrayResult.Name))


        txtHivResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtHivResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtHivResult.Name))

        txtVirusBResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVirusBResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVirusBResult.Name))


        txtVirusAResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVirusAResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVirusAResult.Name))

        CheckExamination.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(CheckExamination.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(CheckExamination.Name))

        CheckExamination2.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(CheckExamination2.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(CheckExamination2.Name))

        checkBlood.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkBlood.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkBlood.Name))

        checkUrineAnalysis.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkUrineAnalysis.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkUrineAnalysis.Name))

        checkChemicalLab.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkChemicalLab.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkChemicalLab.Name))

        checkChemicallab2.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkChemicallab2.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkChemicallab2.Name))

        status.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkChemicallab2.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(status.Name))


    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        'If fnc.getAn(GridView6.GetRowCellValue(GridView6.FocusedRowHandle, "an")) <> "" Then
        ClearStruct()
        frmcheckUp04.getmascheckup_project_setting(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckup_project"))
        getDefaultCompany()
        'End If

    End Sub
    Public Sub ClearDesign()
        mascheckup_project.idmascheckup_project = Nothing

        txtProjectName.EditValue = Nothing
        txtEyeLR.EditValue = Nothing

        txtBlindness.EditValue = Nothing


        txtEye.EditValue = Nothing

        txtTooth.EditValue = Nothing



        txtblood.EditValue = Nothing
        ' MsgBox(dtset.Tables("mascheckup_project").Rows(0)("txtUrineresult"))

        txtUrineresult.EditValue = Nothing


        txtVDRLresult.EditValue = Nothing

        txtCEAResult.EditValue = Nothing



        txtXrayResult.EditValue = Nothing


        txtHivResult.EditValue = Nothing

        txtVirusBResult.EditValue = Nothing


        txtVirusAResult.EditValue = Nothing

        CheckExamination.EditValue = False

        CheckExamination2.EditValue = False

        checkBlood.EditValue = False

        checkUrineAnalysis.EditValue = False

        checkChemicalLab.EditValue = False

        checkChemicallab2.EditValue = False
        status.EditValue = True
    End Sub
    Public Sub ClearStruct()
        mascheckup_project = New frmcheckUp04Class.mascheckup_project

    End Sub

    Private Sub Save_Click(sender As Object, e As EventArgs) Handles Save.Click

        setmascheckup_project()
        frmcheckUp04.setmascheckup_project = mascheckup_project

        If mascheckup_project.idmascheckup_project Is Nothing Then
            frmcheckUp04.Insertmascheckup_project()
        Else
            frmcheckUp04.Updatemascheckup_project()
        End If
        loadMasterToGrid()
        ClearDesign()
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        ClearDesign()
    End Sub
End Class