﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckUp04
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.status = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Save = New DevExpress.XtraEditors.SimpleButton()
        Me.txtXrayResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtXrayResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP = New CheckUpProject.dtCheckUP()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVirusAResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtVirusAResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit12View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn49 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn50 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn51 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn52 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn53 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn54 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVirusBResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtVirusBResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit11View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn55 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn56 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn57 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn58 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn59 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn60 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtHivResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtHivResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit10View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn43 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn44 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn45 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn46 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn47 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn48 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.checkChemicallab2 = New DevExpress.XtraEditors.CheckEdit()
        Me.checkBlood = New DevExpress.XtraEditors.CheckEdit()
        Me.checkChemicalLab = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckExamination2 = New DevExpress.XtraEditors.CheckEdit()
        Me.checkUrineAnalysis = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckExamination = New DevExpress.XtraEditors.CheckEdit()
        Me.txtCEAResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtCEAResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit9View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn61 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn62 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn63 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn64 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn65 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn66 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVDRLresult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtVDRLresultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit8View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtUrineresult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtUrineresultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit7View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtblood = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtbloodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit6View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.checkUpPhysical_result = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.MascheckupphysicalresultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit5View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtTooth = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtToothBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit4View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtEye = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtEyeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtBlindness = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtBlindnessBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtEyeLR = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtEyeLRBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascheckup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldet = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colform = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtProjectName = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.DtCheckUP2 = New CheckUpProject.dtCheckUP()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascheckup_project = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtEyeLR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtBlindness = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtEye = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtTooth = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGridControl1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtblood = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtUrineresult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtVDRLresult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtCEAResult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtXrayResult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtHivResult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtVirusBResult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltxtVirusAResult = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCheckExamination = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCheckExamination2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheckBlood = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheckChemicalLab = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheckUrineAnalysis = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheckChemicallab2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colproject_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DtCheckUP1 = New CheckUpProject.dtCheckUP()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtXrayResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtXrayResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusAResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtVirusAResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit12View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusBResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtVirusBResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit11View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHivResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtHivResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit10View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkChemicallab2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkBlood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkChemicalLab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExamination2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkUrineAnalysis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExamination.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCEAResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtCEAResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit9View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVDRLresult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtVDRLresultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit8View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineresult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUrineresultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit7View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtblood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtbloodBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkUpPhysical_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MascheckupphysicalresultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTooth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtToothBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEye.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEyeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlindness.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBlindnessBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEyeLR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEyeLRBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProjectName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.status)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.Save)
        Me.LayoutControl1.Controls.Add(Me.txtXrayResult)
        Me.LayoutControl1.Controls.Add(Me.txtVirusAResult)
        Me.LayoutControl1.Controls.Add(Me.txtVirusBResult)
        Me.LayoutControl1.Controls.Add(Me.txtHivResult)
        Me.LayoutControl1.Controls.Add(Me.checkChemicallab2)
        Me.LayoutControl1.Controls.Add(Me.checkBlood)
        Me.LayoutControl1.Controls.Add(Me.checkChemicalLab)
        Me.LayoutControl1.Controls.Add(Me.CheckExamination2)
        Me.LayoutControl1.Controls.Add(Me.checkUrineAnalysis)
        Me.LayoutControl1.Controls.Add(Me.CheckExamination)
        Me.LayoutControl1.Controls.Add(Me.txtCEAResult)
        Me.LayoutControl1.Controls.Add(Me.txtVDRLresult)
        Me.LayoutControl1.Controls.Add(Me.txtUrineresult)
        Me.LayoutControl1.Controls.Add(Me.txtblood)
        Me.LayoutControl1.Controls.Add(Me.checkUpPhysical_result)
        Me.LayoutControl1.Controls.Add(Me.txtTooth)
        Me.LayoutControl1.Controls.Add(Me.txtEye)
        Me.LayoutControl1.Controls.Add(Me.txtBlindness)
        Me.LayoutControl1.Controls.Add(Me.txtEyeLR)
        Me.LayoutControl1.Controls.Add(Me.txtProjectName)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1042, 565)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'status
        '
        Me.status.Location = New System.Drawing.Point(218, 417)
        Me.status.Name = "status"
        Me.status.Properties.Caption = "Status"
        Me.status.Size = New System.Drawing.Size(248, 19)
        Me.status.StyleController = Me.LayoutControl1
        Me.status.TabIndex = 27
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(344, 440)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(122, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 26
        Me.SimpleButton2.Text = "Clear"
        '
        'Save
        '
        Me.Save.Location = New System.Drawing.Point(218, 440)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(122, 22)
        Me.Save.StyleController = Me.LayoutControl1
        Me.Save.TabIndex = 25
        Me.Save.Text = "Save"
        '
        'txtXrayResult
        '
        Me.txtXrayResult.Location = New System.Drawing.Point(180, 252)
        Me.txtXrayResult.Name = "txtXrayResult"
        Me.txtXrayResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtXrayResult.Properties.DataSource = Me.TxtXrayResultBindingSource
        Me.txtXrayResult.Properties.DisplayMember = "name"
        Me.txtXrayResult.Properties.ValueMember = "idmascheckup"
        Me.txtXrayResult.Properties.View = Me.GridView2
        Me.txtXrayResult.Size = New System.Drawing.Size(286, 20)
        Me.txtXrayResult.StyleController = Me.LayoutControl1
        Me.txtXrayResult.TabIndex = 24
        '
        'TxtXrayResultBindingSource
        '
        Me.TxtXrayResultBindingSource.DataMember = "txtXrayResult"
        Me.TxtXrayResultBindingSource.DataSource = Me.DtCheckUP
        '
        'DtCheckUP
        '
        Me.DtCheckUP.DataSetName = "dtCheckUP"
        Me.DtCheckUP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38, Me.GridColumn39, Me.GridColumn40, Me.GridColumn41, Me.GridColumn42})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn37
        '
        Me.GridColumn37.FieldName = "idmascheckup"
        Me.GridColumn37.Name = "GridColumn37"
        '
        'GridColumn38
        '
        Me.GridColumn38.FieldName = "name"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = True
        Me.GridColumn38.VisibleIndex = 0
        '
        'GridColumn39
        '
        Me.GridColumn39.FieldName = "name2"
        Me.GridColumn39.Name = "GridColumn39"
        '
        'GridColumn40
        '
        Me.GridColumn40.FieldName = "det"
        Me.GridColumn40.Name = "GridColumn40"
        '
        'GridColumn41
        '
        Me.GridColumn41.FieldName = "description"
        Me.GridColumn41.Name = "GridColumn41"
        '
        'GridColumn42
        '
        Me.GridColumn42.FieldName = "form"
        Me.GridColumn42.Name = "GridColumn42"
        '
        'txtVirusAResult
        '
        Me.txtVirusAResult.Location = New System.Drawing.Point(180, 324)
        Me.txtVirusAResult.Name = "txtVirusAResult"
        Me.txtVirusAResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVirusAResult.Properties.DataSource = Me.TxtVirusAResultBindingSource
        Me.txtVirusAResult.Properties.DisplayMember = "name"
        Me.txtVirusAResult.Properties.ValueMember = "idmascheckup"
        Me.txtVirusAResult.Properties.View = Me.SearchLookUpEdit12View
        Me.txtVirusAResult.Size = New System.Drawing.Size(286, 20)
        Me.txtVirusAResult.StyleController = Me.LayoutControl1
        Me.txtVirusAResult.TabIndex = 23
        '
        'TxtVirusAResultBindingSource
        '
        Me.TxtVirusAResultBindingSource.DataMember = "txtVirusAResult"
        Me.TxtVirusAResultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit12View
        '
        Me.SearchLookUpEdit12View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn49, Me.GridColumn50, Me.GridColumn51, Me.GridColumn52, Me.GridColumn53, Me.GridColumn54})
        Me.SearchLookUpEdit12View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit12View.Name = "SearchLookUpEdit12View"
        Me.SearchLookUpEdit12View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit12View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn49
        '
        Me.GridColumn49.FieldName = "idmascheckup"
        Me.GridColumn49.Name = "GridColumn49"
        '
        'GridColumn50
        '
        Me.GridColumn50.FieldName = "name"
        Me.GridColumn50.Name = "GridColumn50"
        Me.GridColumn50.Visible = True
        Me.GridColumn50.VisibleIndex = 0
        '
        'GridColumn51
        '
        Me.GridColumn51.FieldName = "name2"
        Me.GridColumn51.Name = "GridColumn51"
        '
        'GridColumn52
        '
        Me.GridColumn52.FieldName = "det"
        Me.GridColumn52.Name = "GridColumn52"
        '
        'GridColumn53
        '
        Me.GridColumn53.FieldName = "description"
        Me.GridColumn53.Name = "GridColumn53"
        '
        'GridColumn54
        '
        Me.GridColumn54.FieldName = "form"
        Me.GridColumn54.Name = "GridColumn54"
        '
        'txtVirusBResult
        '
        Me.txtVirusBResult.Location = New System.Drawing.Point(180, 300)
        Me.txtVirusBResult.Name = "txtVirusBResult"
        Me.txtVirusBResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVirusBResult.Properties.DataSource = Me.TxtVirusBResultBindingSource
        Me.txtVirusBResult.Properties.DisplayMember = "name"
        Me.txtVirusBResult.Properties.ValueMember = "idmascheckup"
        Me.txtVirusBResult.Properties.View = Me.SearchLookUpEdit11View
        Me.txtVirusBResult.Size = New System.Drawing.Size(286, 20)
        Me.txtVirusBResult.StyleController = Me.LayoutControl1
        Me.txtVirusBResult.TabIndex = 22
        '
        'TxtVirusBResultBindingSource
        '
        Me.TxtVirusBResultBindingSource.DataMember = "txtVirusBResult"
        Me.TxtVirusBResultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit11View
        '
        Me.SearchLookUpEdit11View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn55, Me.GridColumn56, Me.GridColumn57, Me.GridColumn58, Me.GridColumn59, Me.GridColumn60})
        Me.SearchLookUpEdit11View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit11View.Name = "SearchLookUpEdit11View"
        Me.SearchLookUpEdit11View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit11View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn55
        '
        Me.GridColumn55.FieldName = "idmascheckup"
        Me.GridColumn55.Name = "GridColumn55"
        '
        'GridColumn56
        '
        Me.GridColumn56.FieldName = "name"
        Me.GridColumn56.Name = "GridColumn56"
        Me.GridColumn56.Visible = True
        Me.GridColumn56.VisibleIndex = 0
        '
        'GridColumn57
        '
        Me.GridColumn57.FieldName = "name2"
        Me.GridColumn57.Name = "GridColumn57"
        '
        'GridColumn58
        '
        Me.GridColumn58.FieldName = "det"
        Me.GridColumn58.Name = "GridColumn58"
        '
        'GridColumn59
        '
        Me.GridColumn59.FieldName = "description"
        Me.GridColumn59.Name = "GridColumn59"
        '
        'GridColumn60
        '
        Me.GridColumn60.FieldName = "form"
        Me.GridColumn60.Name = "GridColumn60"
        '
        'txtHivResult
        '
        Me.txtHivResult.Location = New System.Drawing.Point(180, 276)
        Me.txtHivResult.Name = "txtHivResult"
        Me.txtHivResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtHivResult.Properties.DataSource = Me.TxtHivResultBindingSource
        Me.txtHivResult.Properties.DisplayMember = "name"
        Me.txtHivResult.Properties.ValueMember = "idmascheckup"
        Me.txtHivResult.Properties.View = Me.SearchLookUpEdit10View
        Me.txtHivResult.Size = New System.Drawing.Size(286, 20)
        Me.txtHivResult.StyleController = Me.LayoutControl1
        Me.txtHivResult.TabIndex = 21
        '
        'TxtHivResultBindingSource
        '
        Me.TxtHivResultBindingSource.DataMember = "txtHivResult"
        Me.TxtHivResultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit10View
        '
        Me.SearchLookUpEdit10View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn43, Me.GridColumn44, Me.GridColumn45, Me.GridColumn46, Me.GridColumn47, Me.GridColumn48})
        Me.SearchLookUpEdit10View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit10View.Name = "SearchLookUpEdit10View"
        Me.SearchLookUpEdit10View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit10View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn43
        '
        Me.GridColumn43.FieldName = "idmascheckup"
        Me.GridColumn43.Name = "GridColumn43"
        '
        'GridColumn44
        '
        Me.GridColumn44.FieldName = "name"
        Me.GridColumn44.Name = "GridColumn44"
        Me.GridColumn44.Visible = True
        Me.GridColumn44.VisibleIndex = 0
        '
        'GridColumn45
        '
        Me.GridColumn45.FieldName = "name2"
        Me.GridColumn45.Name = "GridColumn45"
        '
        'GridColumn46
        '
        Me.GridColumn46.FieldName = "det"
        Me.GridColumn46.Name = "GridColumn46"
        '
        'GridColumn47
        '
        Me.GridColumn47.FieldName = "description"
        Me.GridColumn47.Name = "GridColumn47"
        '
        'GridColumn48
        '
        Me.GridColumn48.FieldName = "form"
        Me.GridColumn48.Name = "GridColumn48"
        '
        'checkChemicallab2
        '
        Me.checkChemicallab2.Location = New System.Drawing.Point(218, 394)
        Me.checkChemicallab2.Name = "checkChemicallab2"
        Me.checkChemicallab2.Properties.Caption = "Chemical Lab2"
        Me.checkChemicallab2.Size = New System.Drawing.Size(248, 19)
        Me.checkChemicallab2.StyleController = Me.LayoutControl1
        Me.checkChemicallab2.TabIndex = 20
        '
        'checkBlood
        '
        Me.checkBlood.Location = New System.Drawing.Point(12, 394)
        Me.checkBlood.Name = "checkBlood"
        Me.checkBlood.Properties.Caption = "โลหิตวิทยา"
        Me.checkBlood.Size = New System.Drawing.Size(202, 19)
        Me.checkBlood.StyleController = Me.LayoutControl1
        Me.checkBlood.TabIndex = 19
        '
        'checkChemicalLab
        '
        Me.checkChemicalLab.Location = New System.Drawing.Point(218, 371)
        Me.checkChemicalLab.Name = "checkChemicalLab"
        Me.checkChemicalLab.Properties.Caption = "Chemical Lab"
        Me.checkChemicalLab.Size = New System.Drawing.Size(248, 19)
        Me.checkChemicalLab.StyleController = Me.LayoutControl1
        Me.checkChemicalLab.TabIndex = 18
        '
        'CheckExamination2
        '
        Me.CheckExamination2.Location = New System.Drawing.Point(12, 371)
        Me.CheckExamination2.Name = "CheckExamination2"
        Me.CheckExamination2.Properties.Caption = "ผลตรวจสุขภาพร่างกาย 2"
        Me.CheckExamination2.Size = New System.Drawing.Size(202, 19)
        Me.CheckExamination2.StyleController = Me.LayoutControl1
        Me.CheckExamination2.TabIndex = 17
        '
        'checkUrineAnalysis
        '
        Me.checkUrineAnalysis.Location = New System.Drawing.Point(218, 348)
        Me.checkUrineAnalysis.Name = "checkUrineAnalysis"
        Me.checkUrineAnalysis.Properties.Caption = "UrineAnalysis"
        Me.checkUrineAnalysis.Size = New System.Drawing.Size(248, 19)
        Me.checkUrineAnalysis.StyleController = Me.LayoutControl1
        Me.checkUrineAnalysis.TabIndex = 16
        '
        'CheckExamination
        '
        Me.CheckExamination.Location = New System.Drawing.Point(12, 348)
        Me.CheckExamination.Name = "CheckExamination"
        Me.CheckExamination.Properties.Caption = "ผลตรวจสุขภาพร่างกาย"
        Me.CheckExamination.Size = New System.Drawing.Size(202, 19)
        Me.CheckExamination.StyleController = Me.LayoutControl1
        Me.CheckExamination.TabIndex = 15
        '
        'txtCEAResult
        '
        Me.txtCEAResult.Location = New System.Drawing.Point(180, 228)
        Me.txtCEAResult.Name = "txtCEAResult"
        Me.txtCEAResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtCEAResult.Properties.DataSource = Me.TxtCEAResultBindingSource
        Me.txtCEAResult.Properties.DisplayMember = "name"
        Me.txtCEAResult.Properties.ValueMember = "idmascheckup"
        Me.txtCEAResult.Properties.View = Me.SearchLookUpEdit9View
        Me.txtCEAResult.Size = New System.Drawing.Size(286, 20)
        Me.txtCEAResult.StyleController = Me.LayoutControl1
        Me.txtCEAResult.TabIndex = 14
        '
        'TxtCEAResultBindingSource
        '
        Me.TxtCEAResultBindingSource.DataMember = "txtCEAResult"
        Me.TxtCEAResultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit9View
        '
        Me.SearchLookUpEdit9View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn61, Me.GridColumn62, Me.GridColumn63, Me.GridColumn64, Me.GridColumn65, Me.GridColumn66})
        Me.SearchLookUpEdit9View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit9View.Name = "SearchLookUpEdit9View"
        Me.SearchLookUpEdit9View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit9View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn61
        '
        Me.GridColumn61.FieldName = "idmascheckup"
        Me.GridColumn61.Name = "GridColumn61"
        '
        'GridColumn62
        '
        Me.GridColumn62.FieldName = "name"
        Me.GridColumn62.Name = "GridColumn62"
        Me.GridColumn62.Visible = True
        Me.GridColumn62.VisibleIndex = 0
        '
        'GridColumn63
        '
        Me.GridColumn63.FieldName = "name2"
        Me.GridColumn63.Name = "GridColumn63"
        '
        'GridColumn64
        '
        Me.GridColumn64.FieldName = "det"
        Me.GridColumn64.Name = "GridColumn64"
        '
        'GridColumn65
        '
        Me.GridColumn65.FieldName = "description"
        Me.GridColumn65.Name = "GridColumn65"
        '
        'GridColumn66
        '
        Me.GridColumn66.FieldName = "form"
        Me.GridColumn66.Name = "GridColumn66"
        '
        'txtVDRLresult
        '
        Me.txtVDRLresult.Location = New System.Drawing.Point(180, 204)
        Me.txtVDRLresult.Name = "txtVDRLresult"
        Me.txtVDRLresult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVDRLresult.Properties.DataSource = Me.TxtVDRLresultBindingSource
        Me.txtVDRLresult.Properties.DisplayMember = "name"
        Me.txtVDRLresult.Properties.ValueMember = "idmascheckup"
        Me.txtVDRLresult.Properties.View = Me.SearchLookUpEdit8View
        Me.txtVDRLresult.Size = New System.Drawing.Size(286, 20)
        Me.txtVDRLresult.StyleController = Me.LayoutControl1
        Me.txtVDRLresult.TabIndex = 13
        '
        'TxtVDRLresultBindingSource
        '
        Me.TxtVDRLresultBindingSource.DataMember = "txtVDRLresult"
        Me.TxtVDRLresultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit8View
        '
        Me.SearchLookUpEdit8View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn25, Me.GridColumn26, Me.GridColumn27, Me.GridColumn28, Me.GridColumn29, Me.GridColumn30})
        Me.SearchLookUpEdit8View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit8View.Name = "SearchLookUpEdit8View"
        Me.SearchLookUpEdit8View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit8View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn25
        '
        Me.GridColumn25.FieldName = "idmascheckup"
        Me.GridColumn25.Name = "GridColumn25"
        '
        'GridColumn26
        '
        Me.GridColumn26.FieldName = "name"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 0
        '
        'GridColumn27
        '
        Me.GridColumn27.FieldName = "name2"
        Me.GridColumn27.Name = "GridColumn27"
        '
        'GridColumn28
        '
        Me.GridColumn28.FieldName = "det"
        Me.GridColumn28.Name = "GridColumn28"
        '
        'GridColumn29
        '
        Me.GridColumn29.FieldName = "description"
        Me.GridColumn29.Name = "GridColumn29"
        '
        'GridColumn30
        '
        Me.GridColumn30.FieldName = "form"
        Me.GridColumn30.Name = "GridColumn30"
        '
        'txtUrineresult
        '
        Me.txtUrineresult.Location = New System.Drawing.Point(180, 180)
        Me.txtUrineresult.Name = "txtUrineresult"
        Me.txtUrineresult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtUrineresult.Properties.DataSource = Me.TxtUrineresultBindingSource
        Me.txtUrineresult.Properties.DisplayMember = "name"
        Me.txtUrineresult.Properties.ValueMember = "idmascheckup"
        Me.txtUrineresult.Properties.View = Me.SearchLookUpEdit7View
        Me.txtUrineresult.Size = New System.Drawing.Size(286, 20)
        Me.txtUrineresult.StyleController = Me.LayoutControl1
        Me.txtUrineresult.TabIndex = 12
        '
        'TxtUrineresultBindingSource
        '
        Me.TxtUrineresultBindingSource.DataMember = "txtUrineresult"
        Me.TxtUrineresultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit7View
        '
        Me.SearchLookUpEdit7View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn31, Me.GridColumn32, Me.GridColumn33, Me.GridColumn34, Me.GridColumn35, Me.GridColumn36})
        Me.SearchLookUpEdit7View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit7View.Name = "SearchLookUpEdit7View"
        Me.SearchLookUpEdit7View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit7View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn31
        '
        Me.GridColumn31.FieldName = "idmascheckup"
        Me.GridColumn31.Name = "GridColumn31"
        '
        'GridColumn32
        '
        Me.GridColumn32.FieldName = "name"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 0
        '
        'GridColumn33
        '
        Me.GridColumn33.FieldName = "name2"
        Me.GridColumn33.Name = "GridColumn33"
        '
        'GridColumn34
        '
        Me.GridColumn34.FieldName = "det"
        Me.GridColumn34.Name = "GridColumn34"
        '
        'GridColumn35
        '
        Me.GridColumn35.FieldName = "description"
        Me.GridColumn35.Name = "GridColumn35"
        '
        'GridColumn36
        '
        Me.GridColumn36.FieldName = "form"
        Me.GridColumn36.Name = "GridColumn36"
        '
        'txtblood
        '
        Me.txtblood.Location = New System.Drawing.Point(180, 156)
        Me.txtblood.Name = "txtblood"
        Me.txtblood.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtblood.Properties.DataSource = Me.TxtbloodBindingSource
        Me.txtblood.Properties.DisplayMember = "name"
        Me.txtblood.Properties.ValueMember = "idmascheckup"
        Me.txtblood.Properties.View = Me.SearchLookUpEdit6View
        Me.txtblood.Size = New System.Drawing.Size(286, 20)
        Me.txtblood.StyleController = Me.LayoutControl1
        Me.txtblood.TabIndex = 11
        '
        'TxtbloodBindingSource
        '
        Me.TxtbloodBindingSource.DataMember = "txtblood"
        Me.TxtbloodBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit6View
        '
        Me.SearchLookUpEdit6View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn19, Me.GridColumn20, Me.GridColumn21, Me.GridColumn22, Me.GridColumn23, Me.GridColumn24})
        Me.SearchLookUpEdit6View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit6View.Name = "SearchLookUpEdit6View"
        Me.SearchLookUpEdit6View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit6View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn19
        '
        Me.GridColumn19.FieldName = "idmascheckup"
        Me.GridColumn19.Name = "GridColumn19"
        '
        'GridColumn20
        '
        Me.GridColumn20.FieldName = "name"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 0
        '
        'GridColumn21
        '
        Me.GridColumn21.FieldName = "name2"
        Me.GridColumn21.Name = "GridColumn21"
        '
        'GridColumn22
        '
        Me.GridColumn22.FieldName = "det"
        Me.GridColumn22.Name = "GridColumn22"
        '
        'GridColumn23
        '
        Me.GridColumn23.FieldName = "description"
        Me.GridColumn23.Name = "GridColumn23"
        '
        'GridColumn24
        '
        Me.GridColumn24.FieldName = "form"
        Me.GridColumn24.Name = "GridColumn24"
        '
        'checkUpPhysical_result
        '
        Me.checkUpPhysical_result.Location = New System.Drawing.Point(180, 132)
        Me.checkUpPhysical_result.Name = "checkUpPhysical_result"
        Me.checkUpPhysical_result.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.checkUpPhysical_result.Properties.DataSource = Me.MascheckupphysicalresultBindingSource
        Me.checkUpPhysical_result.Properties.DisplayMember = "name"
        Me.checkUpPhysical_result.Properties.ValueMember = "idmascheckup_physical_result"
        Me.checkUpPhysical_result.Properties.View = Me.SearchLookUpEdit5View
        Me.checkUpPhysical_result.Size = New System.Drawing.Size(286, 20)
        Me.checkUpPhysical_result.StyleController = Me.LayoutControl1
        Me.checkUpPhysical_result.TabIndex = 10
        '
        'MascheckupphysicalresultBindingSource
        '
        Me.MascheckupphysicalresultBindingSource.DataMember = "mascheckup_physical_result"
        Me.MascheckupphysicalresultBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit5View
        '
        Me.SearchLookUpEdit5View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit5View.Name = "SearchLookUpEdit5View"
        Me.SearchLookUpEdit5View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit5View.OptionsView.ShowGroupPanel = False
        '
        'txtTooth
        '
        Me.txtTooth.Location = New System.Drawing.Point(180, 108)
        Me.txtTooth.Name = "txtTooth"
        Me.txtTooth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtTooth.Properties.DataSource = Me.TxtToothBindingSource
        Me.txtTooth.Properties.DisplayMember = "name"
        Me.txtTooth.Properties.ValueMember = "idmascheckup"
        Me.txtTooth.Properties.View = Me.SearchLookUpEdit4View
        Me.txtTooth.Size = New System.Drawing.Size(286, 20)
        Me.txtTooth.StyleController = Me.LayoutControl1
        Me.txtTooth.TabIndex = 9
        '
        'TxtToothBindingSource
        '
        Me.TxtToothBindingSource.DataMember = "txtTooth"
        Me.TxtToothBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit4View
        '
        Me.SearchLookUpEdit4View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn17, Me.GridColumn18})
        Me.SearchLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit4View.Name = "SearchLookUpEdit4View"
        Me.SearchLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit4View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn13
        '
        Me.GridColumn13.FieldName = "idmascheckup"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'GridColumn14
        '
        Me.GridColumn14.FieldName = "name"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.FieldName = "name2"
        Me.GridColumn15.Name = "GridColumn15"
        '
        'GridColumn16
        '
        Me.GridColumn16.FieldName = "det"
        Me.GridColumn16.Name = "GridColumn16"
        '
        'GridColumn17
        '
        Me.GridColumn17.FieldName = "description"
        Me.GridColumn17.Name = "GridColumn17"
        '
        'GridColumn18
        '
        Me.GridColumn18.FieldName = "form"
        Me.GridColumn18.Name = "GridColumn18"
        '
        'txtEye
        '
        Me.txtEye.Location = New System.Drawing.Point(180, 84)
        Me.txtEye.Name = "txtEye"
        Me.txtEye.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEye.Properties.DataSource = Me.TxtEyeBindingSource
        Me.txtEye.Properties.DisplayMember = "name"
        Me.txtEye.Properties.ValueMember = "idmascheckup"
        Me.txtEye.Properties.View = Me.SearchLookUpEdit3View
        Me.txtEye.Size = New System.Drawing.Size(286, 20)
        Me.txtEye.StyleController = Me.LayoutControl1
        Me.txtEye.TabIndex = 8
        '
        'TxtEyeBindingSource
        '
        Me.TxtEyeBindingSource.DataMember = "txtEye"
        Me.TxtEyeBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12})
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.FieldName = "idmascheckup"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "name"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        '
        'GridColumn9
        '
        Me.GridColumn9.FieldName = "name2"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'GridColumn10
        '
        Me.GridColumn10.FieldName = "det"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'GridColumn11
        '
        Me.GridColumn11.FieldName = "description"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.FieldName = "form"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'txtBlindness
        '
        Me.txtBlindness.Location = New System.Drawing.Point(180, 60)
        Me.txtBlindness.Name = "txtBlindness"
        Me.txtBlindness.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtBlindness.Properties.DataSource = Me.TxtBlindnessBindingSource
        Me.txtBlindness.Properties.DisplayMember = "name"
        Me.txtBlindness.Properties.ValueMember = "idmascheckup"
        Me.txtBlindness.Properties.View = Me.SearchLookUpEdit2View
        Me.txtBlindness.Size = New System.Drawing.Size(286, 20)
        Me.txtBlindness.StyleController = Me.LayoutControl1
        Me.txtBlindness.TabIndex = 7
        '
        'TxtBlindnessBindingSource
        '
        Me.TxtBlindnessBindingSource.DataMember = "txtBlindness"
        Me.TxtBlindnessBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "idmascheckup"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "name"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "name2"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "det"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "description"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "form"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'txtEyeLR
        '
        Me.txtEyeLR.Location = New System.Drawing.Point(180, 36)
        Me.txtEyeLR.Name = "txtEyeLR"
        Me.txtEyeLR.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEyeLR.Properties.DataSource = Me.TxtEyeLRBindingSource
        Me.txtEyeLR.Properties.DisplayMember = "name"
        Me.txtEyeLR.Properties.ValueMember = "idmascheckup"
        Me.txtEyeLR.Properties.View = Me.SearchLookUpEdit1View
        Me.txtEyeLR.Size = New System.Drawing.Size(286, 20)
        Me.txtEyeLR.StyleController = Me.LayoutControl1
        Me.txtEyeLR.TabIndex = 6
        '
        'TxtEyeLRBindingSource
        '
        Me.TxtEyeLRBindingSource.DataMember = "txtEyeLR"
        Me.TxtEyeLRBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascheckup, Me.colname, Me.colname2, Me.coldet, Me.coldescription, Me.colform})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmascheckup
        '
        Me.colidmascheckup.FieldName = "idmascheckup"
        Me.colidmascheckup.Name = "colidmascheckup"
        '
        'colname
        '
        Me.colname.FieldName = "name"
        Me.colname.Name = "colname"
        Me.colname.Visible = True
        Me.colname.VisibleIndex = 0
        '
        'colname2
        '
        Me.colname2.FieldName = "name2"
        Me.colname2.Name = "colname2"
        '
        'coldet
        '
        Me.coldet.FieldName = "det"
        Me.coldet.Name = "coldet"
        '
        'coldescription
        '
        Me.coldescription.FieldName = "description"
        Me.coldescription.Name = "coldescription"
        '
        'colform
        '
        Me.colform.FieldName = "form"
        Me.colform.Name = "colform"
        '
        'txtProjectName
        '
        Me.txtProjectName.Location = New System.Drawing.Point(180, 12)
        Me.txtProjectName.Name = "txtProjectName"
        Me.txtProjectName.Size = New System.Drawing.Size(286, 20)
        Me.txtProjectName.StyleController = Me.LayoutControl1
        Me.txtProjectName.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "mascheckup_pro"
        Me.GridControl1.DataSource = Me.DtCheckUP2
        Me.GridControl1.Location = New System.Drawing.Point(470, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(560, 541)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtCheckUP2
        '
        Me.DtCheckUP2.DataSetName = "dtCheckUP"
        Me.DtCheckUP2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascheckup_project, Me.coltxtEyeLR, Me.coltxtBlindness, Me.coltxtEye, Me.coltxtTooth, Me.colGridControl1, Me.coltxtblood, Me.coltxtUrineresult, Me.coltxtVDRLresult, Me.coltxtCEAResult, Me.coltxtXrayResult, Me.coltxtHivResult, Me.coltxtVirusBResult, Me.coltxtVirusAResult, Me.colCheckExamination, Me.colCheckExamination2, Me.colcheckBlood, Me.colcheckChemicalLab, Me.colcheckUrineAnalysis, Me.colcheckChemicallab2, Me.colproject_name, Me.colstatus})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'colidmascheckup_project
        '
        Me.colidmascheckup_project.FieldName = "idmascheckup_project"
        Me.colidmascheckup_project.Name = "colidmascheckup_project"
        Me.colidmascheckup_project.OptionsColumn.AllowEdit = False
        Me.colidmascheckup_project.OptionsColumn.ReadOnly = True
        Me.colidmascheckup_project.Visible = True
        Me.colidmascheckup_project.VisibleIndex = 0
        Me.colidmascheckup_project.Width = 143
        '
        'coltxtEyeLR
        '
        Me.coltxtEyeLR.FieldName = "txtEyeLR"
        Me.coltxtEyeLR.Name = "coltxtEyeLR"
        Me.coltxtEyeLR.OptionsColumn.AllowEdit = False
        Me.coltxtEyeLR.OptionsColumn.ReadOnly = True
        '
        'coltxtBlindness
        '
        Me.coltxtBlindness.FieldName = "txtBlindness"
        Me.coltxtBlindness.Name = "coltxtBlindness"
        Me.coltxtBlindness.OptionsColumn.AllowEdit = False
        Me.coltxtBlindness.OptionsColumn.ReadOnly = True
        Me.coltxtBlindness.Width = 98
        '
        'coltxtEye
        '
        Me.coltxtEye.FieldName = "txtEye"
        Me.coltxtEye.Name = "coltxtEye"
        Me.coltxtEye.OptionsColumn.AllowEdit = False
        Me.coltxtEye.OptionsColumn.ReadOnly = True
        '
        'coltxtTooth
        '
        Me.coltxtTooth.FieldName = "txtTooth"
        Me.coltxtTooth.Name = "coltxtTooth"
        Me.coltxtTooth.OptionsColumn.AllowEdit = False
        Me.coltxtTooth.OptionsColumn.ReadOnly = True
        '
        'colGridControl1
        '
        Me.colGridControl1.FieldName = "GridControl1"
        Me.colGridControl1.Name = "colGridControl1"
        Me.colGridControl1.OptionsColumn.AllowEdit = False
        Me.colGridControl1.OptionsColumn.ReadOnly = True
        '
        'coltxtblood
        '
        Me.coltxtblood.FieldName = "txtblood"
        Me.coltxtblood.Name = "coltxtblood"
        Me.coltxtblood.OptionsColumn.AllowEdit = False
        Me.coltxtblood.OptionsColumn.ReadOnly = True
        '
        'coltxtUrineresult
        '
        Me.coltxtUrineresult.FieldName = "txtUrineresult"
        Me.coltxtUrineresult.Name = "coltxtUrineresult"
        Me.coltxtUrineresult.OptionsColumn.AllowEdit = False
        Me.coltxtUrineresult.OptionsColumn.ReadOnly = True
        '
        'coltxtVDRLresult
        '
        Me.coltxtVDRLresult.FieldName = "txtVDRLresult"
        Me.coltxtVDRLresult.Name = "coltxtVDRLresult"
        Me.coltxtVDRLresult.OptionsColumn.AllowEdit = False
        Me.coltxtVDRLresult.OptionsColumn.ReadOnly = True
        Me.coltxtVDRLresult.Width = 100
        '
        'coltxtCEAResult
        '
        Me.coltxtCEAResult.FieldName = "txtCEAResult"
        Me.coltxtCEAResult.Name = "coltxtCEAResult"
        Me.coltxtCEAResult.OptionsColumn.AllowEdit = False
        Me.coltxtCEAResult.OptionsColumn.ReadOnly = True
        Me.coltxtCEAResult.Width = 105
        '
        'coltxtXrayResult
        '
        Me.coltxtXrayResult.FieldName = "txtXrayResult"
        Me.coltxtXrayResult.Name = "coltxtXrayResult"
        Me.coltxtXrayResult.OptionsColumn.AllowEdit = False
        Me.coltxtXrayResult.OptionsColumn.ReadOnly = True
        Me.coltxtXrayResult.Width = 105
        '
        'coltxtHivResult
        '
        Me.coltxtHivResult.FieldName = "txtHivResult"
        Me.coltxtHivResult.Name = "coltxtHivResult"
        Me.coltxtHivResult.OptionsColumn.AllowEdit = False
        Me.coltxtHivResult.OptionsColumn.ReadOnly = True
        Me.coltxtHivResult.Width = 105
        '
        'coltxtVirusBResult
        '
        Me.coltxtVirusBResult.FieldName = "txtVirusBResult"
        Me.coltxtVirusBResult.Name = "coltxtVirusBResult"
        Me.coltxtVirusBResult.OptionsColumn.AllowEdit = False
        Me.coltxtVirusBResult.OptionsColumn.ReadOnly = True
        Me.coltxtVirusBResult.Width = 105
        '
        'coltxtVirusAResult
        '
        Me.coltxtVirusAResult.FieldName = "txtVirusAResult"
        Me.coltxtVirusAResult.Name = "coltxtVirusAResult"
        Me.coltxtVirusAResult.OptionsColumn.AllowEdit = False
        Me.coltxtVirusAResult.OptionsColumn.ReadOnly = True
        Me.coltxtVirusAResult.Width = 105
        '
        'colCheckExamination
        '
        Me.colCheckExamination.FieldName = "CheckExamination"
        Me.colCheckExamination.Name = "colCheckExamination"
        Me.colCheckExamination.OptionsColumn.AllowEdit = False
        Me.colCheckExamination.OptionsColumn.ReadOnly = True
        Me.colCheckExamination.Width = 105
        '
        'colCheckExamination2
        '
        Me.colCheckExamination2.FieldName = "CheckExamination2"
        Me.colCheckExamination2.Name = "colCheckExamination2"
        Me.colCheckExamination2.OptionsColumn.AllowEdit = False
        Me.colCheckExamination2.OptionsColumn.ReadOnly = True
        Me.colCheckExamination2.Width = 105
        '
        'colcheckBlood
        '
        Me.colcheckBlood.FieldName = "checkBlood"
        Me.colcheckBlood.Name = "colcheckBlood"
        Me.colcheckBlood.OptionsColumn.AllowEdit = False
        Me.colcheckBlood.OptionsColumn.ReadOnly = True
        Me.colcheckBlood.Width = 105
        '
        'colcheckChemicalLab
        '
        Me.colcheckChemicalLab.FieldName = "checkChemicalLab"
        Me.colcheckChemicalLab.Name = "colcheckChemicalLab"
        Me.colcheckChemicalLab.OptionsColumn.AllowEdit = False
        Me.colcheckChemicalLab.OptionsColumn.ReadOnly = True
        Me.colcheckChemicalLab.Width = 105
        '
        'colcheckUrineAnalysis
        '
        Me.colcheckUrineAnalysis.FieldName = "checkUrineAnalysis"
        Me.colcheckUrineAnalysis.Name = "colcheckUrineAnalysis"
        Me.colcheckUrineAnalysis.OptionsColumn.AllowEdit = False
        Me.colcheckUrineAnalysis.OptionsColumn.ReadOnly = True
        Me.colcheckUrineAnalysis.Width = 105
        '
        'colcheckChemicallab2
        '
        Me.colcheckChemicallab2.FieldName = "checkChemicallab2"
        Me.colcheckChemicallab2.Name = "colcheckChemicallab2"
        Me.colcheckChemicallab2.OptionsColumn.AllowEdit = False
        Me.colcheckChemicallab2.OptionsColumn.ReadOnly = True
        Me.colcheckChemicallab2.Width = 105
        '
        'colproject_name
        '
        Me.colproject_name.FieldName = "project_name"
        Me.colproject_name.Name = "colproject_name"
        Me.colproject_name.OptionsColumn.AllowEdit = False
        Me.colproject_name.OptionsColumn.ReadOnly = True
        Me.colproject_name.Visible = True
        Me.colproject_name.VisibleIndex = 1
        Me.colproject_name.Width = 1320
        '
        'colstatus
        '
        Me.colstatus.FieldName = "status"
        Me.colstatus.Name = "colstatus"
        Me.colstatus.OptionsColumn.AllowEdit = False
        Me.colstatus.OptionsColumn.ReadOnly = True
        Me.colstatus.Visible = True
        Me.colstatus.VisibleIndex = 2
        Me.colstatus.Width = 169
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1042, 565)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(458, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(564, 545)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtProjectName
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem2.Text = "ชื่อProject"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtEyeLR
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem3.Text = "ตรวจวัดสายตา"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtBlindness
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem4.Text = "ColorBlindness"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtEye
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem5.Text = "ตรวจตา"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtTooth
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem6.Text = "ตรวจฟัน"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.checkUpPhysical_result
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem7.Text = "ตรวจสุขภาพ 2"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txtblood
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem8.Text = "โลหิตวิทยา"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.txtUrineresult
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem9.Text = "UrineAnalysis"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtVDRLresult
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 192)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem10.Text = "VDRL(ตรวจหาเชื้อซิฟิลิส)"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.txtCEAResult
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 216)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem11.Text = "CEA ตรวจหาสารบ่งชี้มะเร็งลำใส้ใหญ่"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.CheckExamination
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 336)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(206, 23)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.checkUrineAnalysis
        Me.LayoutControlItem13.Location = New System.Drawing.Point(206, 336)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(252, 23)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.CheckExamination2
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 359)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(206, 23)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.checkChemicalLab
        Me.LayoutControlItem15.Location = New System.Drawing.Point(206, 359)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(252, 23)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.checkBlood
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 382)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(206, 163)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.checkChemicallab2
        Me.LayoutControlItem17.Location = New System.Drawing.Point(206, 382)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(252, 23)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.txtHivResult
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 264)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem18.Text = "HIV Antibody"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.txtVirusBResult
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 288)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem19.Text = "Virus B"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.txtVirusAResult
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 312)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem20.Text = "Virus A"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.txtXrayResult
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(458, 24)
        Me.LayoutControlItem21.Text = "Chest X-Ray"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(165, 13)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.Save
        Me.LayoutControlItem22.Location = New System.Drawing.Point(206, 428)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(126, 117)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.SimpleButton2
        Me.LayoutControlItem23.Location = New System.Drawing.Point(332, 428)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(126, 117)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.status
        Me.LayoutControlItem24.Location = New System.Drawing.Point(206, 405)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(252, 23)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'DtCheckUP1
        '
        Me.DtCheckUP1.DataSetName = "dtCheckUP"
        Me.DtCheckUP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmCheckUp04
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1042, 565)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmCheckUp04"
        Me.Text = "frmCheckUp04"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtXrayResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtXrayResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusAResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtVirusAResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit12View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusBResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtVirusBResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit11View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHivResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtHivResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit10View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkChemicallab2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkBlood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkChemicalLab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExamination2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkUrineAnalysis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExamination.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCEAResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtCEAResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit9View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVDRLresult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtVDRLresultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit8View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineresult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUrineresultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit7View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtblood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtbloodBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkUpPhysical_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MascheckupphysicalresultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTooth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtToothBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEye.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEyeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlindness.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBlindnessBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEyeLR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEyeLRBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProjectName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtHivResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit10View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents checkChemicallab2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkBlood As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkChemicalLab As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckExamination2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkUrineAnalysis As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckExamination As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtCEAResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit9View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtVDRLresult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit8View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtUrineresult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit7View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtblood As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit6View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents checkUpPhysical_result As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit5View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtTooth As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit4View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtEye As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtBlindness As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtEyeLR As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtProjectName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtVirusAResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit12View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtVirusBResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit11View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtXrayResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DtCheckUP As dtCheckUP
    Friend WithEvents TxtEyeLRBindingSource As BindingSource
    Friend WithEvents colidmascheckup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldet As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colform As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtToothBindingSource As BindingSource
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtEyeBindingSource As BindingSource
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtBlindnessBindingSource As BindingSource
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtVDRLresultBindingSource As BindingSource
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtUrineresultBindingSource As BindingSource
    Friend WithEvents TxtbloodBindingSource As BindingSource
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtCEAResultBindingSource As BindingSource
    Friend WithEvents TxtXrayResultBindingSource As BindingSource
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtHivResultBindingSource As BindingSource
    Friend WithEvents GridColumn43 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn44 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn45 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn46 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn47 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn48 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtVirusAResultBindingSource As BindingSource
    Friend WithEvents GridColumn49 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn50 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn51 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn52 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn53 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn54 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtVirusBResultBindingSource As BindingSource
    Friend WithEvents GridColumn55 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn56 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn57 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn58 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn59 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn60 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn61 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn62 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn63 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn64 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn65 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn66 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MascheckupphysicalresultBindingSource As BindingSource
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Save As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DtCheckUP1 As dtCheckUP
    Friend WithEvents colidmascheckup_project As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtEyeLR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtBlindness As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtEye As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtTooth As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGridControl1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtblood As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtUrineresult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtVDRLresult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtCEAResult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtXrayResult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtHivResult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtVirusBResult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltxtVirusAResult As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCheckExamination As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCheckExamination2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheckBlood As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheckChemicalLab As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheckUrineAnalysis As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheckChemicallab2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colproject_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DtCheckUP2 As dtCheckUP
    Friend WithEvents status As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
End Class
