﻿Imports DevExpress.XtraReports.UI

Public Class frmCheckUp08
    Dim dtset As dtCheckUP
    Dim checkUpClass As checkupClass
    Public Sub New(ByVal dtsetReport As dtCheckUP, ByRef chkclass As checkupClass)

        ' This call is required by the designer.
        InitializeComponent()
        dtset = dtsetReport
        checkUpClass = chkclass
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub frmCheckUp08_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim report1 As rptCheckUpA5 = New rptCheckUpA5
        checkUpClass.getReport()
        report1.DataSource = dtset
        report1.CreateDocument()
        Dim report2 As rptCheckUpA5_1 = New rptCheckUpA5_1
        Dim report3 As rptCheckUpA5_2 = New rptCheckUpA5_2
        Dim report4 As rptCheckUpA5_3 = New rptCheckUpA5_3
        Dim report5 As rptCheckUpA5_4 = New rptCheckUpA5_4
        Dim report6 As rptCheckUpA5_5 = New rptCheckUpA5_5

        ' Create the 2nd report and generate its document. 
        If CheckEdit1.EditValue = True Then
            report2.DataSource = dtset
            report2.CreateDocument()
            report1.Pages.AddRange(report2.Pages)

        End If
        If CheckEdit2.EditValue = True Then
            report3.DataSource = dtset
            report3.CreateDocument()
            report1.Pages.AddRange(report3.Pages)

        End If

        If CheckEdit3.EditValue = True Then
            report4.DataSource = dtset
            report4.CreateDocument()
            report1.Pages.AddRange(report4.Pages)

        End If


        If CheckEdit4.EditValue = True Then
            report5.DataSource = dtset
            report5.CreateDocument()
            report1.Pages.AddRange(report5.Pages)

        End If


        If CheckEdit5.EditValue = True Then
            report6.DataSource = dtset
            report6.CreateDocument()
            report1.Pages.AddRange(report6.Pages)


        End If



        ' Add all pages of the 2nd report to the end of the 1st report. 

        ' Reset all page numbers in the resulting document. 
        report1.PrintingSystem.ContinuousPageNumbering = True

        ' Create a Print Tool and show the Print Preview form. 
        Dim printTool As New ReportPrintTool(report1)
        printTool.ShowPreviewDialog()
    End Sub
End Class