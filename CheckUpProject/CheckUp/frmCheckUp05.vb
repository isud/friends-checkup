﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCheckUp05
    Dim dtset As New dtCheckUP
    Dim checkupClass As checkupClass
    Dim frmcheckUp05 As frmCheckUp05Class
    Dim mascheckup_physical As frmCheckUp05Class.mascheckup_physical
    Private Sub frmCheckUp05_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadMasterToGrid()
    End Sub
    Public Sub loadMasterToGrid()
        frmcheckUp05 = New frmCheckUp05Class(dtset)
        frmcheckUp05.getmascheckup_physical()
        GridControl1.DataSource = dtset
    End Sub
    Public Sub setmascheckup_physical()
        mascheckup_physical.name = txtname.Text
        mascheckup_physical.name2 = txtname2.Text
        mascheckup_physical.order_number = order_number.EditValue
        mascheckup_physical.status = status.EditValue
    End Sub
    Public Sub ClearDesign()
        ClearStruct()
        txtname.Text = Nothing
        txtname2.Text = Nothing
        order_number.EditValue = 0
        status.EditValue = True
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        ClearDesign()
    End Sub
    Public Sub ClearStruct()
        mascheckup_physical = New frmCheckUp05Class.mascheckup_physical

    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        ClearStruct()
        frmcheckUp05.getmascheckup_physical_setting(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckup_physical"))
        getDefaultCompany()
    End Sub
    Public Sub getDefaultCompany()
        mascheckup_physical.idmascheck_physical = If(IsDBNull(dtset.Tables("mascheckup_physical").Rows(0)("idmascheckup_physical")), Nothing, dtset.Tables("mascheckup_physical").Rows(0)("idmascheckup_physical"))

        txtname.Text = If(IsDBNull(dtset.Tables("mascheckup_physical").Rows(0)("name")), Nothing, dtset.Tables("mascheckup_physical").Rows(0)("name"))
        txtname2.Text = If(IsDBNull(dtset.Tables("mascheckup_physical").Rows(0)("name2")), Nothing, dtset.Tables("mascheckup_physical").Rows(0)("name2"))
        order_number.EditValue = If(IsDBNull(dtset.Tables("mascheckup_physical").Rows(0)("order_number")), Nothing, dtset.Tables("mascheckup_physical").Rows(0)("order_number"))
        status.EditValue = dtset.Tables("mascheckup_physical").Rows(0)("status")

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        setmascheckup_physical()
        frmcheckUp05.setmascheckup_project = mascheckup_physical

        If mascheckup_physical.idmascheck_physical Is Nothing Then
            frmcheckUp05.Insertmascheckup_physical()
        Else
            frmcheckUp05.Updatemascheckup_physical()
        End If
        loadMasterToGrid()
        ClearDesign()


    End Sub
End Class