﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCheckUp01
    Inherits System.Windows.Forms.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer


    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txtLicense = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.chkglasses_without = New DevExpress.XtraEditors.CheckEdit()
        Me.chkglasses = New DevExpress.XtraEditors.CheckEdit()
        Me.checkEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.btnResult = New DevExpress.XtraEditors.SimpleButton()
        Me.checkBlood = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckExamination2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckExamination = New DevExpress.XtraEditors.CheckEdit()
        Me.checkChemicallab2 = New DevExpress.XtraEditors.CheckEdit()
        Me.checkChemicalLab = New DevExpress.XtraEditors.CheckEdit()
        Me.checkUrineAnalysis = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfrnCheckUpFitness_Result = New DevExpress.XtraEditors.MemoEdit()
        Me.txtFrncheckupResult_all = New DevExpress.XtraEditors.MemoEdit()
        Me.txtfit_food = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_breathing = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_crane = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_emergency = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_aircrew = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_temporary = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_unit = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_specific = New DevExpress.XtraEditors.CheckEdit()
        Me.txtfit_offshore = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtAge = New DevExpress.XtraEditors.TextEdit()
        Me.txtVn = New DevExpress.XtraEditors.TextEdit()
        Me.txtAddress = New DevExpress.XtraEditors.TextEdit()
        Me.txtDoctor = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource8 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP11 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit9View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtTelephone = New DevExpress.XtraEditors.TextEdit()
        Me.txtMassex = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP4 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit8View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colsex = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsexdesc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtDateTest = New DevExpress.XtraEditors.DateEdit()
        Me.txtCompanyName = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource9 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP12 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit7View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtName = New DevExpress.XtraEditors.TextEdit()
        Me.txtHn = New DevExpress.XtraEditors.TextEdit()
        Me.txtCEAResult_remark = New DevExpress.XtraEditors.MemoEdit()
        Me.txtCEAResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP6 = New CheckUpProject.dtCheckUP()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtvirusa_havigm = New DevExpress.XtraEditors.TextEdit()
        Me.txtvirusa_havigg = New DevExpress.XtraEditors.TextEdit()
        Me.txtVirusAResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource7 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP10 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit6View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn55 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn56 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn57 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn58 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn59 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn60 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVirusb_hbsab = New DevExpress.XtraEditors.TextEdit()
        Me.txtVirusb_hbcab = New DevExpress.XtraEditors.TextEdit()
        Me.txtVirusb_hbsag = New DevExpress.XtraEditors.TextEdit()
        Me.txtVirusBResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource6 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP9 = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit5View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn49 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn50 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn51 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn52 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn53 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn54 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVDRLresult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP5 = New CheckUpProject.dtCheckUP()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtVDRLResult_remark = New DevExpress.XtraEditors.MemoEdit()
        Me.txtHivResult_remark = New DevExpress.XtraEditors.MemoEdit()
        Me.txtHivResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP8 = New CheckUpProject.dtCheckUP()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn43 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn44 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn45 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn46 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn47 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn48 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtXrayResult_remark = New DevExpress.XtraEditors.MemoEdit()
        Me.txtXrayResult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.BindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP7 = New CheckUpProject.dtCheckUP()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn61 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn62 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn63 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn64 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn65 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn66 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.DtCheckUP3 = New CheckUpProject.dtCheckUP()
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.coldescription1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colvalue = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colunit = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colnormal = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colhn = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colan = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colvn = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colresult = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.colresult2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.DtCheckUP1 = New CheckUpProject.dtCheckUP()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascheckup_physical = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidvalue = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.checkUpPhysical_result = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.MascheckupphysicalresultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP2 = New CheckUpProject.dtCheckUP()
        Me.RepositoryItemSearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtUrineResult_remark = New DevExpress.XtraEditors.MemoEdit()
        Me.txtUrineresult = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtUrineresultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtUrineAmphetamine = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineOther = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineEpi = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineRBC = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineBlood = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineGlucose = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineAppearance = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineWBC = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrinepH = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineSPGR = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineAlbumin = New DevExpress.XtraEditors.TextEdit()
        Me.txtUrineColor = New DevExpress.XtraEditors.TextEdit()
        Me.txtblood_result = New DevExpress.XtraEditors.MemoEdit()
        Me.txtblood = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtBloodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPlateCount = New DevExpress.XtraEditors.TextEdit()
        Me.txtRBC_Morphology = New DevExpress.XtraEditors.TextEdit()
        Me.txtAtypical_lymph = New DevExpress.XtraEditors.TextEdit()
        Me.txtBashophils = New DevExpress.XtraEditors.TextEdit()
        Me.txtMonocytes = New DevExpress.XtraEditors.TextEdit()
        Me.txtEosionophils = New DevExpress.XtraEditors.TextEdit()
        Me.txtLymphocytes = New DevExpress.XtraEditors.TextEdit()
        Me.txtNeutrophils = New DevExpress.XtraEditors.TextEdit()
        Me.txtWBC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtHct = New DevExpress.XtraEditors.TextEdit()
        Me.txtHgb = New DevExpress.XtraEditors.TextEdit()
        Me.txtBloodRh = New DevExpress.XtraEditors.TextEdit()
        Me.txtBloodgroup = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtEyeLR_result = New DevExpress.XtraEditors.TextEdit()
        Me.txteyeL = New DevExpress.XtraEditors.TextEdit()
        Me.txtTooth = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtToothBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtCheckUP = New CheckUpProject.dtCheckUP()
        Me.SearchLookUpEdit4View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtTooth_result = New DevExpress.XtraEditors.TextEdit()
        Me.txteye_result = New DevExpress.XtraEditors.TextEdit()
        Me.txtBlindness_result = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtEyeLR = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtEyeLRBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtEyeR = New DevExpress.XtraEditors.TextEdit()
        Me.txtEye = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtEyeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascheckup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldet = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colform = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtBlindness = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.TxtBlindnessBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtEyeResult = New DevExpress.XtraEditors.TextEdit()
        Me.txtBloodPressure1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtBloodPressure = New DevExpress.XtraEditors.TextEdit()
        Me.txtHeight = New DevExpress.XtraEditors.TextEdit()
        Me.txtBmi = New DevExpress.XtraEditors.TextEdit()
        Me.txtWeight = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem109 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem108 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup14 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem76 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem77 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem89 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem90 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem91 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem92 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem93 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem94 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem95 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem96 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem97 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup16 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem98 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem99 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem101 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem102 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem103 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem100 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem104 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem105 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem106 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem78 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem79 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem80 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem81 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem82 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem83 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem84 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem85 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem86 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem87 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem88 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem107 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem110 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem111 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtLicense.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkglasses_without.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkglasses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkBlood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExamination2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExamination.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkChemicallab2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkChemicalLab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkUrineAnalysis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfrnCheckUpFitness_Result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFrncheckupResult_all.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_food.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_breathing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_crane.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_emergency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_aircrew.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_temporary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_unit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_specific.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtfit_offshore.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDoctor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit9View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelephone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMassex.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit8View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDateTest.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDateTest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCompanyName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit7View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCEAResult_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCEAResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtvirusa_havigm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtvirusa_havigg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusAResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusb_hbsab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusb_hbcab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusb_hbsag.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVirusBResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVDRLresult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVDRLResult_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHivResult_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHivResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtXrayResult_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtXrayResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.checkUpPhysical_result, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MascheckupphysicalresultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineResult_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineresult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtUrineresultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineAmphetamine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineOther.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineEpi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineRBC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineBlood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineGlucose.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineAppearance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineWBC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrinepH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineSPGR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineAlbumin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUrineColor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtblood_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtblood.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBloodBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlateCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRBC_Morphology.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAtypical_lymph.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBashophils.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonocytes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEosionophils.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLymphocytes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNeutrophils.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWBC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHgb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBloodRh.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBloodgroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEyeLR_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txteyeL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTooth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtToothBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTooth_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txteye_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlindness_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEyeLR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEyeLRBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEyeR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEye.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEyeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBlindness.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBlindnessBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEyeResult.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBloodPressure1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBloodPressure.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHeight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBmi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(768, 24)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(160, 418)
        '
        'TextEdit8
        '
        Me.TextEdit8.EditValue = "TextEdit8"
        Me.TextEdit8.Location = New System.Drawing.Point(436, 2)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Size = New System.Drawing.Size(490, 20)
        Me.TextEdit8.TabIndex = 22
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Location = New System.Drawing.Point(6, 6)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Size = New System.Drawing.Size(149, 130)
        Me.PictureEdit1.StyleController = Me.LayoutControl1
        Me.PictureEdit1.TabIndex = 6
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtLicense)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton5)
        Me.LayoutControl1.Controls.Add(Me.chkglasses_without)
        Me.LayoutControl1.Controls.Add(Me.chkglasses)
        Me.LayoutControl1.Controls.Add(Me.checkEdit)
        Me.LayoutControl1.Controls.Add(Me.btnResult)
        Me.LayoutControl1.Controls.Add(Me.checkBlood)
        Me.LayoutControl1.Controls.Add(Me.CheckExamination2)
        Me.LayoutControl1.Controls.Add(Me.CheckExamination)
        Me.LayoutControl1.Controls.Add(Me.checkChemicallab2)
        Me.LayoutControl1.Controls.Add(Me.checkChemicalLab)
        Me.LayoutControl1.Controls.Add(Me.checkUrineAnalysis)
        Me.LayoutControl1.Controls.Add(Me.txtfrnCheckUpFitness_Result)
        Me.LayoutControl1.Controls.Add(Me.txtFrncheckupResult_all)
        Me.LayoutControl1.Controls.Add(Me.txtfit_food)
        Me.LayoutControl1.Controls.Add(Me.txtfit_breathing)
        Me.LayoutControl1.Controls.Add(Me.txtfit_crane)
        Me.LayoutControl1.Controls.Add(Me.txtfit_emergency)
        Me.LayoutControl1.Controls.Add(Me.txtfit_aircrew)
        Me.LayoutControl1.Controls.Add(Me.txtfit_temporary)
        Me.LayoutControl1.Controls.Add(Me.txtfit_unit)
        Me.LayoutControl1.Controls.Add(Me.txtfit_specific)
        Me.LayoutControl1.Controls.Add(Me.txtfit_offshore)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Controls.Add(Me.txtAge)
        Me.LayoutControl1.Controls.Add(Me.txtVn)
        Me.LayoutControl1.Controls.Add(Me.txtAddress)
        Me.LayoutControl1.Controls.Add(Me.txtDoctor)
        Me.LayoutControl1.Controls.Add(Me.txtTelephone)
        Me.LayoutControl1.Controls.Add(Me.txtMassex)
        Me.LayoutControl1.Controls.Add(Me.txtDateTest)
        Me.LayoutControl1.Controls.Add(Me.txtCompanyName)
        Me.LayoutControl1.Controls.Add(Me.txtName)
        Me.LayoutControl1.Controls.Add(Me.txtHn)
        Me.LayoutControl1.Controls.Add(Me.txtCEAResult_remark)
        Me.LayoutControl1.Controls.Add(Me.txtCEAResult)
        Me.LayoutControl1.Controls.Add(Me.txtvirusa_havigm)
        Me.LayoutControl1.Controls.Add(Me.txtvirusa_havigg)
        Me.LayoutControl1.Controls.Add(Me.txtVirusAResult)
        Me.LayoutControl1.Controls.Add(Me.txtVirusb_hbsab)
        Me.LayoutControl1.Controls.Add(Me.txtVirusb_hbcab)
        Me.LayoutControl1.Controls.Add(Me.txtVirusb_hbsag)
        Me.LayoutControl1.Controls.Add(Me.txtVirusBResult)
        Me.LayoutControl1.Controls.Add(Me.txtVDRLresult)
        Me.LayoutControl1.Controls.Add(Me.txtVDRLResult_remark)
        Me.LayoutControl1.Controls.Add(Me.txtHivResult_remark)
        Me.LayoutControl1.Controls.Add(Me.txtHivResult)
        Me.LayoutControl1.Controls.Add(Me.txtXrayResult_remark)
        Me.LayoutControl1.Controls.Add(Me.txtXrayResult)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.LabelControl13)
        Me.LayoutControl1.Controls.Add(Me.LabelControl12)
        Me.LayoutControl1.Controls.Add(Me.LabelControl11)
        Me.LayoutControl1.Controls.Add(Me.LabelControl10)
        Me.LayoutControl1.Controls.Add(Me.LabelControl9)
        Me.LayoutControl1.Controls.Add(Me.LabelControl8)
        Me.LayoutControl1.Controls.Add(Me.LabelControl7)
        Me.LayoutControl1.Controls.Add(Me.LabelControl6)
        Me.LayoutControl1.Controls.Add(Me.LabelControl5)
        Me.LayoutControl1.Controls.Add(Me.txtUrineResult_remark)
        Me.LayoutControl1.Controls.Add(Me.txtUrineresult)
        Me.LayoutControl1.Controls.Add(Me.txtUrineAmphetamine)
        Me.LayoutControl1.Controls.Add(Me.txtUrineOther)
        Me.LayoutControl1.Controls.Add(Me.txtUrineEpi)
        Me.LayoutControl1.Controls.Add(Me.txtUrineRBC)
        Me.LayoutControl1.Controls.Add(Me.txtUrineBlood)
        Me.LayoutControl1.Controls.Add(Me.txtUrineGlucose)
        Me.LayoutControl1.Controls.Add(Me.txtUrineAppearance)
        Me.LayoutControl1.Controls.Add(Me.txtUrineWBC)
        Me.LayoutControl1.Controls.Add(Me.txtUrinepH)
        Me.LayoutControl1.Controls.Add(Me.txtUrineSPGR)
        Me.LayoutControl1.Controls.Add(Me.txtUrineAlbumin)
        Me.LayoutControl1.Controls.Add(Me.txtUrineColor)
        Me.LayoutControl1.Controls.Add(Me.txtblood_result)
        Me.LayoutControl1.Controls.Add(Me.txtblood)
        Me.LayoutControl1.Controls.Add(Me.txtPlateCount)
        Me.LayoutControl1.Controls.Add(Me.txtRBC_Morphology)
        Me.LayoutControl1.Controls.Add(Me.txtAtypical_lymph)
        Me.LayoutControl1.Controls.Add(Me.txtBashophils)
        Me.LayoutControl1.Controls.Add(Me.txtMonocytes)
        Me.LayoutControl1.Controls.Add(Me.txtEosionophils)
        Me.LayoutControl1.Controls.Add(Me.txtLymphocytes)
        Me.LayoutControl1.Controls.Add(Me.txtNeutrophils)
        Me.LayoutControl1.Controls.Add(Me.txtWBC)
        Me.LayoutControl1.Controls.Add(Me.LabelControl4)
        Me.LayoutControl1.Controls.Add(Me.txtHct)
        Me.LayoutControl1.Controls.Add(Me.txtHgb)
        Me.LayoutControl1.Controls.Add(Me.txtBloodRh)
        Me.LayoutControl1.Controls.Add(Me.txtBloodgroup)
        Me.LayoutControl1.Controls.Add(Me.LabelControl3)
        Me.LayoutControl1.Controls.Add(Me.LabelControl2)
        Me.LayoutControl1.Controls.Add(Me.txtEyeLR_result)
        Me.LayoutControl1.Controls.Add(Me.txteyeL)
        Me.LayoutControl1.Controls.Add(Me.txtTooth)
        Me.LayoutControl1.Controls.Add(Me.txtTooth_result)
        Me.LayoutControl1.Controls.Add(Me.txteye_result)
        Me.LayoutControl1.Controls.Add(Me.txtBlindness_result)
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.txtEyeLR)
        Me.LayoutControl1.Controls.Add(Me.txtEyeR)
        Me.LayoutControl1.Controls.Add(Me.txtEye)
        Me.LayoutControl1.Controls.Add(Me.txtBlindness)
        Me.LayoutControl1.Controls.Add(Me.txtEyeResult)
        Me.LayoutControl1.Controls.Add(Me.txtBloodPressure1)
        Me.LayoutControl1.Controls.Add(Me.txtBloodPressure)
        Me.LayoutControl1.Controls.Add(Me.txtHeight)
        Me.LayoutControl1.Controls.Add(Me.txtBmi)
        Me.LayoutControl1.Controls.Add(Me.txtWeight)
        Me.LayoutControl1.Controls.Add(Me.PictureEdit1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1080, 564)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txtLicense
        '
        Me.txtLicense.Location = New System.Drawing.Point(675, 6)
        Me.txtLicense.Name = "txtLicense"
        Me.txtLicense.Size = New System.Drawing.Size(150, 20)
        Me.txtLicense.StyleController = Me.LayoutControl1
        Me.txtLicense.TabIndex = 164
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Location = New System.Drawing.Point(995, 114)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(79, 22)
        Me.SimpleButton5.StyleController = Me.LayoutControl1
        Me.SimpleButton5.TabIndex = 163
        Me.SimpleButton5.Text = "Cert"
        '
        'chkglasses_without
        '
        Me.chkglasses_without.Location = New System.Drawing.Point(592, 169)
        Me.chkglasses_without.Name = "chkglasses_without"
        Me.chkglasses_without.Properties.Caption = "Without Glasses (ไม่สวมแว่น)"
        Me.chkglasses_without.Size = New System.Drawing.Size(161, 19)
        Me.chkglasses_without.StyleController = Me.LayoutControl1
        Me.chkglasses_without.TabIndex = 162
        '
        'chkglasses
        '
        Me.chkglasses.Location = New System.Drawing.Point(757, 169)
        Me.chkglasses.Name = "chkglasses"
        Me.chkglasses.Properties.Caption = "With glasses or contact lens ( สวมแว่นหรือใส่คอนแทคเลนส์ )"
        Me.chkglasses.Size = New System.Drawing.Size(310, 19)
        Me.chkglasses.StyleController = Me.LayoutControl1
        Me.chkglasses.TabIndex = 161
        '
        'checkEdit
        '
        Me.checkEdit.Location = New System.Drawing.Point(159, 114)
        Me.checkEdit.Name = "checkEdit"
        Me.checkEdit.Properties.Caption = "Mode Edit"
        Me.checkEdit.Size = New System.Drawing.Size(161, 19)
        Me.checkEdit.StyleController = Me.LayoutControl1
        Me.checkEdit.TabIndex = 160
        '
        'btnResult
        '
        Me.btnResult.Location = New System.Drawing.Point(1010, 215)
        Me.btnResult.Name = "btnResult"
        Me.btnResult.Size = New System.Drawing.Size(57, 22)
        Me.btnResult.StyleController = Me.LayoutControl1
        Me.btnResult.TabIndex = 159
        Me.btnResult.Text = "ดึงสรุปผล"
        '
        'checkBlood
        '
        Me.checkBlood.Location = New System.Drawing.Point(13, 215)
        Me.checkBlood.Name = "checkBlood"
        Me.checkBlood.Properties.Caption = "โลหิตวิทยา"
        Me.checkBlood.Size = New System.Drawing.Size(661, 19)
        Me.checkBlood.StyleController = Me.LayoutControl1
        Me.checkBlood.TabIndex = 158
        '
        'CheckExamination2
        '
        Me.CheckExamination2.Location = New System.Drawing.Point(13, 192)
        Me.CheckExamination2.Name = "CheckExamination2"
        Me.CheckExamination2.Properties.Caption = "ผลตรวจสุขภาพร่างกาย2"
        Me.CheckExamination2.Size = New System.Drawing.Size(661, 19)
        Me.CheckExamination2.StyleController = Me.LayoutControl1
        Me.CheckExamination2.TabIndex = 157
        '
        'CheckExamination
        '
        Me.CheckExamination.Location = New System.Drawing.Point(13, 169)
        Me.CheckExamination.Name = "CheckExamination"
        Me.CheckExamination.Properties.Caption = "ผลครวจสุขภาพร่างกาย"
        Me.CheckExamination.Size = New System.Drawing.Size(661, 19)
        Me.CheckExamination.StyleController = Me.LayoutControl1
        Me.CheckExamination.TabIndex = 156
        '
        'checkChemicallab2
        '
        Me.checkChemicallab2.Location = New System.Drawing.Point(678, 215)
        Me.checkChemicallab2.Name = "checkChemicallab2"
        Me.checkChemicallab2.Properties.Caption = "Chemical Lab2"
        Me.checkChemicallab2.Size = New System.Drawing.Size(328, 19)
        Me.checkChemicallab2.StyleController = Me.LayoutControl1
        Me.checkChemicallab2.TabIndex = 155
        '
        'checkChemicalLab
        '
        Me.checkChemicalLab.Location = New System.Drawing.Point(678, 192)
        Me.checkChemicalLab.Name = "checkChemicalLab"
        Me.checkChemicalLab.Properties.Caption = "Chemical Lab"
        Me.checkChemicalLab.Size = New System.Drawing.Size(389, 19)
        Me.checkChemicalLab.StyleController = Me.LayoutControl1
        Me.checkChemicalLab.TabIndex = 154
        '
        'checkUrineAnalysis
        '
        Me.checkUrineAnalysis.Location = New System.Drawing.Point(678, 169)
        Me.checkUrineAnalysis.Name = "checkUrineAnalysis"
        Me.checkUrineAnalysis.Properties.Caption = "UrineAnalysis"
        Me.checkUrineAnalysis.Size = New System.Drawing.Size(389, 19)
        Me.checkUrineAnalysis.StyleController = Me.LayoutControl1
        Me.checkUrineAnalysis.TabIndex = 153
        '
        'txtfrnCheckUpFitness_Result
        '
        Me.txtfrnCheckUpFitness_Result.Location = New System.Drawing.Point(233, 388)
        Me.txtfrnCheckUpFitness_Result.Name = "txtfrnCheckUpFitness_Result"
        Me.txtfrnCheckUpFitness_Result.Size = New System.Drawing.Size(834, 163)
        Me.txtfrnCheckUpFitness_Result.StyleController = Me.LayoutControl1
        Me.txtfrnCheckUpFitness_Result.TabIndex = 151
        '
        'txtFrncheckupResult_all
        '
        Me.txtFrncheckupResult_all.Location = New System.Drawing.Point(233, 241)
        Me.txtFrncheckupResult_all.Name = "txtFrncheckupResult_all"
        Me.txtFrncheckupResult_all.Size = New System.Drawing.Size(834, 143)
        Me.txtFrncheckupResult_all.StyleController = Me.LayoutControl1
        Me.txtFrncheckupResult_all.TabIndex = 150
        '
        'txtfit_food
        '
        Me.txtfit_food.Location = New System.Drawing.Point(83, 313)
        Me.txtfit_food.Name = "txtfit_food"
        Me.txtfit_food.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_food.Properties.Appearance.Options.UseFont = True
        Me.txtfit_food.Properties.Caption = "Food and Catering"
        Me.txtfit_food.Size = New System.Drawing.Size(984, 20)
        Me.txtfit_food.StyleController = Me.LayoutControl1
        Me.txtfit_food.TabIndex = 149
        '
        'txtfit_breathing
        '
        Me.txtfit_breathing.Location = New System.Drawing.Point(83, 241)
        Me.txtfit_breathing.Name = "txtfit_breathing"
        Me.txtfit_breathing.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_breathing.Properties.Appearance.Options.UseFont = True
        Me.txtfit_breathing.Properties.Caption = "Breathing Apparatus Work"
        Me.txtfit_breathing.Size = New System.Drawing.Size(984, 20)
        Me.txtfit_breathing.StyleController = Me.LayoutControl1
        Me.txtfit_breathing.TabIndex = 148
        '
        'txtfit_crane
        '
        Me.txtfit_crane.Location = New System.Drawing.Point(83, 265)
        Me.txtfit_crane.Name = "txtfit_crane"
        Me.txtfit_crane.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_crane.Properties.Appearance.Options.UseFont = True
        Me.txtfit_crane.Properties.Caption = "Crane Operator "
        Me.txtfit_crane.Size = New System.Drawing.Size(984, 20)
        Me.txtfit_crane.StyleController = Me.LayoutControl1
        Me.txtfit_crane.TabIndex = 147
        '
        'txtfit_emergency
        '
        Me.txtfit_emergency.Location = New System.Drawing.Point(83, 289)
        Me.txtfit_emergency.Name = "txtfit_emergency"
        Me.txtfit_emergency.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_emergency.Properties.Appearance.Options.UseFont = True
        Me.txtfit_emergency.Properties.Caption = "Emergency Response Teams (ERT)"
        Me.txtfit_emergency.Size = New System.Drawing.Size(984, 20)
        Me.txtfit_emergency.StyleController = Me.LayoutControl1
        Me.txtfit_emergency.TabIndex = 146
        '
        'txtfit_aircrew
        '
        Me.txtfit_aircrew.Location = New System.Drawing.Point(83, 217)
        Me.txtfit_aircrew.Name = "txtfit_aircrew"
        Me.txtfit_aircrew.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_aircrew.Properties.Appearance.Options.UseFont = True
        Me.txtfit_aircrew.Properties.Caption = "Air crew and commercial Driver"
        Me.txtfit_aircrew.Size = New System.Drawing.Size(984, 20)
        Me.txtfit_aircrew.StyleController = Me.LayoutControl1
        Me.txtfit_aircrew.TabIndex = 145
        '
        'txtfit_temporary
        '
        Me.txtfit_temporary.Location = New System.Drawing.Point(13, 361)
        Me.txtfit_temporary.Name = "txtfit_temporary"
        Me.txtfit_temporary.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_temporary.Properties.Appearance.Options.UseFont = True
        Me.txtfit_temporary.Properties.Caption = "Temporary unfit for offshore employment in his / her position so please contact t" &
    "he doctor as soon as possible"
        Me.txtfit_temporary.Size = New System.Drawing.Size(1054, 20)
        Me.txtfit_temporary.StyleController = Me.LayoutControl1
        Me.txtfit_temporary.TabIndex = 144
        '
        'txtfit_unit
        '
        Me.txtfit_unit.Location = New System.Drawing.Point(13, 337)
        Me.txtfit_unit.Name = "txtfit_unit"
        Me.txtfit_unit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_unit.Properties.Appearance.Options.UseFont = True
        Me.txtfit_unit.Properties.Caption = "Unfit for offshore employment in his / her position"
        Me.txtfit_unit.Size = New System.Drawing.Size(1054, 20)
        Me.txtfit_unit.StyleController = Me.LayoutControl1
        Me.txtfit_unit.TabIndex = 143
        '
        'txtfit_specific
        '
        Me.txtfit_specific.Location = New System.Drawing.Point(13, 193)
        Me.txtfit_specific.Name = "txtfit_specific"
        Me.txtfit_specific.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_specific.Properties.Appearance.Options.UseFont = True
        Me.txtfit_specific.Properties.Caption = "Fit for specific Task:"
        Me.txtfit_specific.Size = New System.Drawing.Size(1054, 20)
        Me.txtfit_specific.StyleController = Me.LayoutControl1
        Me.txtfit_specific.TabIndex = 142
        '
        'txtfit_offshore
        '
        Me.txtfit_offshore.Location = New System.Drawing.Point(13, 169)
        Me.txtfit_offshore.Name = "txtfit_offshore"
        Me.txtfit_offshore.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfit_offshore.Properties.Appearance.Options.UseFont = True
        Me.txtfit_offshore.Properties.Caption = "Fit for offshore employment in his / her position. This person is free from commu" &
    "nicable disease and is considered mecially fit for the work intended."
        Me.txtfit_offshore.Size = New System.Drawing.Size(1054, 20)
        Me.txtfit_offshore.StyleController = Me.LayoutControl1
        Me.txtfit_offshore.TabIndex = 141
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Location = New System.Drawing.Point(746, 114)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(84, 22)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 140
        Me.SimpleButton4.Text = "Load New Data"
        '
        'txtAge
        '
        Me.txtAge.Location = New System.Drawing.Point(587, 66)
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Size = New System.Drawing.Size(201, 20)
        Me.txtAge.StyleController = Me.LayoutControl1
        Me.txtAge.TabIndex = 139
        '
        'txtVn
        '
        Me.txtVn.Location = New System.Drawing.Point(534, 36)
        Me.txtVn.Name = "txtVn"
        Me.txtVn.Size = New System.Drawing.Size(254, 20)
        Me.txtVn.StyleController = Me.LayoutControl1
        Me.txtVn.TabIndex = 138
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(234, 90)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(840, 20)
        Me.txtAddress.StyleController = Me.LayoutControl1
        Me.txtAddress.TabIndex = 137
        '
        'txtDoctor
        '
        Me.txtDoctor.Location = New System.Drawing.Point(534, 6)
        Me.txtDoctor.Name = "txtDoctor"
        Me.txtDoctor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDoctor.Properties.DataSource = Me.BindingSource8
        Me.txtDoctor.Properties.DisplayMember = "DRNAME"
        Me.txtDoctor.Properties.ValueMember = "PROVIDER"
        Me.txtDoctor.Properties.View = Me.SearchLookUpEdit9View
        Me.txtDoctor.Size = New System.Drawing.Size(137, 20)
        Me.txtDoctor.StyleController = Me.LayoutControl1
        Me.txtDoctor.TabIndex = 136
        '
        'BindingSource8
        '
        Me.BindingSource8.DataMember = "doctor"
        Me.BindingSource8.DataSource = Me.DtCheckUP11
        Me.BindingSource8.Sort = ""
        '
        'DtCheckUP11
        '
        Me.DtCheckUP11.DataSetName = "dtCheckUP"
        Me.DtCheckUP11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit9View
        '
        Me.SearchLookUpEdit9View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit9View.Name = "SearchLookUpEdit9View"
        Me.SearchLookUpEdit9View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit9View.OptionsView.ShowGroupPanel = False
        '
        'txtTelephone
        '
        Me.txtTelephone.Location = New System.Drawing.Point(867, 66)
        Me.txtTelephone.Name = "txtTelephone"
        Me.txtTelephone.Size = New System.Drawing.Size(207, 20)
        Me.txtTelephone.StyleController = Me.LayoutControl1
        Me.txtTelephone.TabIndex = 135
        '
        'txtMassex
        '
        Me.txtMassex.Location = New System.Drawing.Point(847, 36)
        Me.txtMassex.Name = "txtMassex"
        Me.txtMassex.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMassex.Properties.DataSource = Me.BindingSource1
        Me.txtMassex.Properties.DisplayMember = "sexdesc"
        Me.txtMassex.Properties.ValueMember = "sex"
        Me.txtMassex.Properties.View = Me.SearchLookUpEdit8View
        Me.txtMassex.Size = New System.Drawing.Size(227, 20)
        Me.txtMassex.StyleController = Me.LayoutControl1
        Me.txtMassex.TabIndex = 134
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "massex"
        Me.BindingSource1.DataSource = Me.DtCheckUP4
        Me.BindingSource1.Sort = ""
        '
        'DtCheckUP4
        '
        Me.DtCheckUP4.DataSetName = "dtCheckUP"
        Me.DtCheckUP4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit8View
        '
        Me.SearchLookUpEdit8View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colsex, Me.colsexdesc})
        Me.SearchLookUpEdit8View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit8View.Name = "SearchLookUpEdit8View"
        Me.SearchLookUpEdit8View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit8View.OptionsView.ShowGroupPanel = False
        '
        'colsex
        '
        Me.colsex.FieldName = "sex"
        Me.colsex.Name = "colsex"
        Me.colsex.Visible = True
        Me.colsex.VisibleIndex = 0
        Me.colsex.Width = 298
        '
        'colsexdesc
        '
        Me.colsexdesc.FieldName = "sexdesc"
        Me.colsexdesc.Name = "colsexdesc"
        Me.colsexdesc.Visible = True
        Me.colsexdesc.VisibleIndex = 1
        Me.colsexdesc.Width = 1334
        '
        'txtDateTest
        '
        Me.txtDateTest.EditValue = Nothing
        Me.txtDateTest.Location = New System.Drawing.Point(904, 6)
        Me.txtDateTest.Name = "txtDateTest"
        Me.txtDateTest.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDateTest.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDateTest.Size = New System.Drawing.Size(170, 20)
        Me.txtDateTest.StyleController = Me.LayoutControl1
        Me.txtDateTest.TabIndex = 133
        '
        'txtCompanyName
        '
        Me.txtCompanyName.Location = New System.Drawing.Point(234, 6)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.Properties.Appearance.Options.UseFont = True
        Me.txtCompanyName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtCompanyName.Properties.DataSource = Me.BindingSource9
        Me.txtCompanyName.Properties.DisplayMember = "project_name"
        Me.txtCompanyName.Properties.ValueMember = "idmascheckup_project"
        Me.txtCompanyName.Properties.View = Me.SearchLookUpEdit7View
        Me.txtCompanyName.Size = New System.Drawing.Size(221, 26)
        Me.txtCompanyName.StyleController = Me.LayoutControl1
        Me.txtCompanyName.TabIndex = 132
        '
        'BindingSource9
        '
        Me.BindingSource9.DataMember = "mascheckup_pro"
        Me.BindingSource9.DataSource = Me.DtCheckUP12
        Me.BindingSource9.Sort = ""
        '
        'DtCheckUP12
        '
        Me.DtCheckUP12.DataSetName = "dtCheckUP"
        Me.DtCheckUP12.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit7View
        '
        Me.SearchLookUpEdit7View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit7View.Name = "SearchLookUpEdit7View"
        Me.SearchLookUpEdit7View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit7View.OptionsView.ShowGroupPanel = False
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(234, 66)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(294, 20)
        Me.txtName.StyleController = Me.LayoutControl1
        Me.txtName.TabIndex = 131
        '
        'txtHn
        '
        Me.txtHn.Location = New System.Drawing.Point(234, 36)
        Me.txtHn.Name = "txtHn"
        Me.txtHn.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHn.Properties.Appearance.Options.UseFont = True
        Me.txtHn.Size = New System.Drawing.Size(221, 26)
        Me.txtHn.StyleController = Me.LayoutControl1
        Me.txtHn.TabIndex = 130
        '
        'txtCEAResult_remark
        '
        Me.txtCEAResult_remark.Location = New System.Drawing.Point(553, 223)
        Me.txtCEAResult_remark.Name = "txtCEAResult_remark"
        Me.txtCEAResult_remark.Size = New System.Drawing.Size(502, 38)
        Me.txtCEAResult_remark.StyleController = Me.LayoutControl1
        Me.txtCEAResult_remark.TabIndex = 129
        '
        'txtCEAResult
        '
        Me.txtCEAResult.Location = New System.Drawing.Point(608, 199)
        Me.txtCEAResult.Name = "txtCEAResult"
        Me.txtCEAResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtCEAResult.Properties.DataSource = Me.BindingSource3
        Me.txtCEAResult.Properties.DisplayMember = "name"
        Me.txtCEAResult.Properties.ValueMember = "idmascheckup"
        Me.txtCEAResult.Properties.View = Me.GridView9
        Me.txtCEAResult.Size = New System.Drawing.Size(447, 20)
        Me.txtCEAResult.StyleController = Me.LayoutControl1
        Me.txtCEAResult.TabIndex = 128
        '
        'BindingSource3
        '
        Me.BindingSource3.DataMember = "txtCEAResult"
        Me.BindingSource3.DataSource = Me.DtCheckUP6
        Me.BindingSource3.Sort = ""
        '
        'DtCheckUP6
        '
        Me.DtCheckUP6.DataSetName = "dtCheckUP"
        Me.DtCheckUP6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView9
        '
        Me.GridView9.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38, Me.GridColumn39, Me.GridColumn40, Me.GridColumn41, Me.GridColumn42})
        Me.GridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView9.Name = "GridView9"
        Me.GridView9.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView9.OptionsView.ShowGroupPanel = False
        '
        'GridColumn37
        '
        Me.GridColumn37.FieldName = "idmascheckup"
        Me.GridColumn37.Name = "GridColumn37"
        '
        'GridColumn38
        '
        Me.GridColumn38.FieldName = "name"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = True
        Me.GridColumn38.VisibleIndex = 0
        '
        'GridColumn39
        '
        Me.GridColumn39.FieldName = "name2"
        Me.GridColumn39.Name = "GridColumn39"
        '
        'GridColumn40
        '
        Me.GridColumn40.FieldName = "det"
        Me.GridColumn40.Name = "GridColumn40"
        '
        'GridColumn41
        '
        Me.GridColumn41.FieldName = "description"
        Me.GridColumn41.Name = "GridColumn41"
        '
        'GridColumn42
        '
        Me.GridColumn42.FieldName = "form"
        Me.GridColumn42.Name = "GridColumn42"
        '
        'txtvirusa_havigm
        '
        Me.txtvirusa_havigm.Location = New System.Drawing.Point(773, 495)
        Me.txtvirusa_havigm.Name = "txtvirusa_havigm"
        Me.txtvirusa_havigm.Size = New System.Drawing.Size(282, 20)
        Me.txtvirusa_havigm.StyleController = Me.LayoutControl1
        Me.txtvirusa_havigm.TabIndex = 127
        '
        'txtvirusa_havigg
        '
        Me.txtvirusa_havigg.Location = New System.Drawing.Point(773, 471)
        Me.txtvirusa_havigg.Name = "txtvirusa_havigg"
        Me.txtvirusa_havigg.Size = New System.Drawing.Size(282, 20)
        Me.txtvirusa_havigg.StyleController = Me.LayoutControl1
        Me.txtvirusa_havigg.TabIndex = 126
        '
        'txtVirusAResult
        '
        Me.txtVirusAResult.Location = New System.Drawing.Point(773, 447)
        Me.txtVirusAResult.Name = "txtVirusAResult"
        Me.txtVirusAResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVirusAResult.Properties.DataSource = Me.BindingSource7
        Me.txtVirusAResult.Properties.DisplayMember = "name"
        Me.txtVirusAResult.Properties.ValueMember = "idmascheckup"
        Me.txtVirusAResult.Properties.View = Me.SearchLookUpEdit6View
        Me.txtVirusAResult.Size = New System.Drawing.Size(282, 20)
        Me.txtVirusAResult.StyleController = Me.LayoutControl1
        Me.txtVirusAResult.TabIndex = 125
        '
        'BindingSource7
        '
        Me.BindingSource7.DataMember = "txtVirusAResult"
        Me.BindingSource7.DataSource = Me.DtCheckUP10
        Me.BindingSource7.Sort = ""
        '
        'DtCheckUP10
        '
        Me.DtCheckUP10.DataSetName = "dtCheckUP"
        Me.DtCheckUP10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit6View
        '
        Me.SearchLookUpEdit6View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn55, Me.GridColumn56, Me.GridColumn57, Me.GridColumn58, Me.GridColumn59, Me.GridColumn60})
        Me.SearchLookUpEdit6View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit6View.Name = "SearchLookUpEdit6View"
        Me.SearchLookUpEdit6View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit6View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn55
        '
        Me.GridColumn55.FieldName = "idmascheckup"
        Me.GridColumn55.Name = "GridColumn55"
        '
        'GridColumn56
        '
        Me.GridColumn56.FieldName = "name"
        Me.GridColumn56.Name = "GridColumn56"
        Me.GridColumn56.Visible = True
        Me.GridColumn56.VisibleIndex = 0
        '
        'GridColumn57
        '
        Me.GridColumn57.FieldName = "name2"
        Me.GridColumn57.Name = "GridColumn57"
        '
        'GridColumn58
        '
        Me.GridColumn58.FieldName = "det"
        Me.GridColumn58.Name = "GridColumn58"
        '
        'GridColumn59
        '
        Me.GridColumn59.FieldName = "description"
        Me.GridColumn59.Name = "GridColumn59"
        '
        'GridColumn60
        '
        Me.GridColumn60.FieldName = "form"
        Me.GridColumn60.Name = "GridColumn60"
        '
        'txtVirusb_hbsab
        '
        Me.txtVirusb_hbsab.Location = New System.Drawing.Point(245, 495)
        Me.txtVirusb_hbsab.Name = "txtVirusb_hbsab"
        Me.txtVirusb_hbsab.Size = New System.Drawing.Size(280, 20)
        Me.txtVirusb_hbsab.StyleController = Me.LayoutControl1
        Me.txtVirusb_hbsab.TabIndex = 124
        '
        'txtVirusb_hbcab
        '
        Me.txtVirusb_hbcab.Location = New System.Drawing.Point(245, 519)
        Me.txtVirusb_hbcab.Name = "txtVirusb_hbcab"
        Me.txtVirusb_hbcab.Size = New System.Drawing.Size(280, 20)
        Me.txtVirusb_hbcab.StyleController = Me.LayoutControl1
        Me.txtVirusb_hbcab.TabIndex = 123
        '
        'txtVirusb_hbsag
        '
        Me.txtVirusb_hbsag.Location = New System.Drawing.Point(245, 471)
        Me.txtVirusb_hbsag.Name = "txtVirusb_hbsag"
        Me.txtVirusb_hbsag.Size = New System.Drawing.Size(280, 20)
        Me.txtVirusb_hbsag.StyleController = Me.LayoutControl1
        Me.txtVirusb_hbsag.TabIndex = 122
        '
        'txtVirusBResult
        '
        Me.txtVirusBResult.Location = New System.Drawing.Point(245, 447)
        Me.txtVirusBResult.Name = "txtVirusBResult"
        Me.txtVirusBResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVirusBResult.Properties.DataSource = Me.BindingSource6
        Me.txtVirusBResult.Properties.DisplayMember = "name"
        Me.txtVirusBResult.Properties.ValueMember = "idmascheckup"
        Me.txtVirusBResult.Properties.View = Me.SearchLookUpEdit5View
        Me.txtVirusBResult.Size = New System.Drawing.Size(280, 20)
        Me.txtVirusBResult.StyleController = Me.LayoutControl1
        Me.txtVirusBResult.TabIndex = 121
        '
        'BindingSource6
        '
        Me.BindingSource6.DataMember = "txtVirusBResult"
        Me.BindingSource6.DataSource = Me.DtCheckUP9
        Me.BindingSource6.Sort = ""
        '
        'DtCheckUP9
        '
        Me.DtCheckUP9.DataSetName = "dtCheckUP"
        Me.DtCheckUP9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit5View
        '
        Me.SearchLookUpEdit5View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn49, Me.GridColumn50, Me.GridColumn51, Me.GridColumn52, Me.GridColumn53, Me.GridColumn54})
        Me.SearchLookUpEdit5View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit5View.Name = "SearchLookUpEdit5View"
        Me.SearchLookUpEdit5View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit5View.OptionsView.ShowGroupPanel = False
        Me.SearchLookUpEdit5View.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn52, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn49
        '
        Me.GridColumn49.FieldName = "idmascheckup"
        Me.GridColumn49.Name = "GridColumn49"
        '
        'GridColumn50
        '
        Me.GridColumn50.FieldName = "name"
        Me.GridColumn50.Name = "GridColumn50"
        Me.GridColumn50.Visible = True
        Me.GridColumn50.VisibleIndex = 0
        '
        'GridColumn51
        '
        Me.GridColumn51.FieldName = "name2"
        Me.GridColumn51.Name = "GridColumn51"
        '
        'GridColumn52
        '
        Me.GridColumn52.FieldName = "det"
        Me.GridColumn52.Name = "GridColumn52"
        '
        'GridColumn53
        '
        Me.GridColumn53.FieldName = "description"
        Me.GridColumn53.Name = "GridColumn53"
        '
        'GridColumn54
        '
        Me.GridColumn54.FieldName = "form"
        Me.GridColumn54.Name = "GridColumn54"
        '
        'txtVDRLresult
        '
        Me.txtVDRLresult.Location = New System.Drawing.Point(80, 199)
        Me.txtVDRLresult.Name = "txtVDRLresult"
        Me.txtVDRLresult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtVDRLresult.Properties.DataSource = Me.BindingSource2
        Me.txtVDRLresult.Properties.DisplayMember = "name"
        Me.txtVDRLresult.Properties.ValueMember = "idmascheckup"
        Me.txtVDRLresult.Properties.View = Me.GridView8
        Me.txtVDRLresult.Size = New System.Drawing.Size(445, 20)
        Me.txtVDRLresult.StyleController = Me.LayoutControl1
        Me.txtVDRLresult.TabIndex = 119
        '
        'BindingSource2
        '
        Me.BindingSource2.DataMember = "txtVDRLresult"
        Me.BindingSource2.DataSource = Me.DtCheckUP5
        Me.BindingSource2.Sort = ""
        '
        'DtCheckUP5
        '
        Me.DtCheckUP5.DataSetName = "dtCheckUP"
        Me.DtCheckUP5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView8
        '
        Me.GridView8.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn31, Me.GridColumn32, Me.GridColumn33, Me.GridColumn34, Me.GridColumn35, Me.GridColumn36})
        Me.GridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView8.Name = "GridView8"
        Me.GridView8.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView8.OptionsView.ShowGroupPanel = False
        '
        'GridColumn31
        '
        Me.GridColumn31.FieldName = "idmascheckup"
        Me.GridColumn31.Name = "GridColumn31"
        '
        'GridColumn32
        '
        Me.GridColumn32.FieldName = "name"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 0
        '
        'GridColumn33
        '
        Me.GridColumn33.FieldName = "name2"
        Me.GridColumn33.Name = "GridColumn33"
        '
        'GridColumn34
        '
        Me.GridColumn34.FieldName = "det"
        Me.GridColumn34.Name = "GridColumn34"
        '
        'GridColumn35
        '
        Me.GridColumn35.FieldName = "description"
        Me.GridColumn35.Name = "GridColumn35"
        '
        'GridColumn36
        '
        Me.GridColumn36.FieldName = "form"
        Me.GridColumn36.Name = "GridColumn36"
        '
        'txtVDRLResult_remark
        '
        Me.txtVDRLResult_remark.Location = New System.Drawing.Point(25, 223)
        Me.txtVDRLResult_remark.Name = "txtVDRLResult_remark"
        Me.txtVDRLResult_remark.Size = New System.Drawing.Size(500, 38)
        Me.txtVDRLResult_remark.StyleController = Me.LayoutControl1
        Me.txtVDRLResult_remark.TabIndex = 117
        '
        'txtHivResult_remark
        '
        Me.txtHivResult_remark.Location = New System.Drawing.Point(553, 331)
        Me.txtHivResult_remark.Name = "txtHivResult_remark"
        Me.txtHivResult_remark.Size = New System.Drawing.Size(502, 70)
        Me.txtHivResult_remark.StyleController = Me.LayoutControl1
        Me.txtHivResult_remark.TabIndex = 116
        '
        'txtHivResult
        '
        Me.txtHivResult.Location = New System.Drawing.Point(608, 307)
        Me.txtHivResult.Name = "txtHivResult"
        Me.txtHivResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtHivResult.Properties.DataSource = Me.BindingSource5
        Me.txtHivResult.Properties.DisplayMember = "name"
        Me.txtHivResult.Properties.ValueMember = "idmascheckup"
        Me.txtHivResult.Properties.View = Me.GridView7
        Me.txtHivResult.Size = New System.Drawing.Size(447, 20)
        Me.txtHivResult.StyleController = Me.LayoutControl1
        Me.txtHivResult.TabIndex = 115
        '
        'BindingSource5
        '
        Me.BindingSource5.DataMember = "txtHivResult"
        Me.BindingSource5.DataSource = Me.DtCheckUP8
        Me.BindingSource5.Sort = ""
        '
        'DtCheckUP8
        '
        Me.DtCheckUP8.DataSetName = "dtCheckUP"
        Me.DtCheckUP8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn43, Me.GridColumn44, Me.GridColumn45, Me.GridColumn46, Me.GridColumn47, Me.GridColumn48})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn43
        '
        Me.GridColumn43.FieldName = "idmascheckup"
        Me.GridColumn43.Name = "GridColumn43"
        '
        'GridColumn44
        '
        Me.GridColumn44.FieldName = "name"
        Me.GridColumn44.Name = "GridColumn44"
        Me.GridColumn44.Visible = True
        Me.GridColumn44.VisibleIndex = 0
        '
        'GridColumn45
        '
        Me.GridColumn45.FieldName = "name2"
        Me.GridColumn45.Name = "GridColumn45"
        '
        'GridColumn46
        '
        Me.GridColumn46.FieldName = "det"
        Me.GridColumn46.Name = "GridColumn46"
        '
        'GridColumn47
        '
        Me.GridColumn47.FieldName = "description"
        Me.GridColumn47.Name = "GridColumn47"
        '
        'GridColumn48
        '
        Me.GridColumn48.FieldName = "form"
        Me.GridColumn48.Name = "GridColumn48"
        '
        'txtXrayResult_remark
        '
        Me.txtXrayResult_remark.Location = New System.Drawing.Point(25, 331)
        Me.txtXrayResult_remark.Name = "txtXrayResult_remark"
        Me.txtXrayResult_remark.Size = New System.Drawing.Size(500, 70)
        Me.txtXrayResult_remark.StyleController = Me.LayoutControl1
        Me.txtXrayResult_remark.TabIndex = 114
        '
        'txtXrayResult
        '
        Me.txtXrayResult.Location = New System.Drawing.Point(80, 307)
        Me.txtXrayResult.Name = "txtXrayResult"
        Me.txtXrayResult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtXrayResult.Properties.DataSource = Me.BindingSource4
        Me.txtXrayResult.Properties.DisplayMember = "name"
        Me.txtXrayResult.Properties.ValueMember = "idmascheckup"
        Me.txtXrayResult.Properties.View = Me.GridView6
        Me.txtXrayResult.Size = New System.Drawing.Size(445, 20)
        Me.txtXrayResult.StyleController = Me.LayoutControl1
        Me.txtXrayResult.TabIndex = 113
        '
        'BindingSource4
        '
        Me.BindingSource4.DataMember = "txtXrayResult"
        Me.BindingSource4.DataSource = Me.DtCheckUP7
        Me.BindingSource4.Sort = ""
        '
        'DtCheckUP7
        '
        Me.DtCheckUP7.DataSetName = "dtCheckUP"
        Me.DtCheckUP7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn61, Me.GridColumn62, Me.GridColumn63, Me.GridColumn64, Me.GridColumn65, Me.GridColumn66})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsView.ShowGroupPanel = False
        '
        'GridColumn61
        '
        Me.GridColumn61.FieldName = "idmascheckup"
        Me.GridColumn61.Name = "GridColumn61"
        '
        'GridColumn62
        '
        Me.GridColumn62.FieldName = "name"
        Me.GridColumn62.Name = "GridColumn62"
        Me.GridColumn62.Visible = True
        Me.GridColumn62.VisibleIndex = 0
        '
        'GridColumn63
        '
        Me.GridColumn63.FieldName = "name2"
        Me.GridColumn63.Name = "GridColumn63"
        '
        'GridColumn64
        '
        Me.GridColumn64.FieldName = "det"
        Me.GridColumn64.Name = "GridColumn64"
        '
        'GridColumn65
        '
        Me.GridColumn65.FieldName = "description"
        Me.GridColumn65.Name = "GridColumn65"
        '
        'GridColumn66
        '
        Me.GridColumn66.FieldName = "form"
        Me.GridColumn66.Name = "GridColumn66"
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "frncheckupother"
        Me.GridControl2.DataSource = Me.DtCheckUP3
        Me.GridControl2.Location = New System.Drawing.Point(13, 169)
        Me.GridControl2.MainView = Me.AdvBandedGridView1
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(1054, 382)
        Me.GridControl2.TabIndex = 112
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.AdvBandedGridView1})
        '
        'DtCheckUP3
        '
        Me.DtCheckUP3.DataSetName = "dtCheckUP"
        Me.DtCheckUP3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.coldescription1, Me.colunit, Me.colnormal, Me.colvalue, Me.colhn, Me.colan, Me.colvn, Me.colresult, Me.colresult2})
        Me.AdvBandedGridView1.GridControl = Me.GridControl2
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ColumnAutoWidth = True
        Me.AdvBandedGridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand1
        '
        Me.GridBand1.Columns.Add(Me.coldescription1)
        Me.GridBand1.Columns.Add(Me.colvalue)
        Me.GridBand1.Columns.Add(Me.colunit)
        Me.GridBand1.Columns.Add(Me.colnormal)
        Me.GridBand1.Columns.Add(Me.colhn)
        Me.GridBand1.Columns.Add(Me.colan)
        Me.GridBand1.Columns.Add(Me.colvn)
        Me.GridBand1.Columns.Add(Me.colresult)
        Me.GridBand1.Columns.Add(Me.colresult2)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 1632
        '
        'coldescription1
        '
        Me.coldescription1.FieldName = "description"
        Me.coldescription1.Name = "coldescription1"
        Me.coldescription1.Visible = True
        Me.coldescription1.Width = 841
        '
        'colvalue
        '
        Me.colvalue.FieldName = "value"
        Me.colvalue.Name = "colvalue"
        Me.colvalue.Visible = True
        Me.colvalue.Width = 211
        '
        'colunit
        '
        Me.colunit.FieldName = "unit"
        Me.colunit.Name = "colunit"
        Me.colunit.Visible = True
        Me.colunit.Width = 271
        '
        'colnormal
        '
        Me.colnormal.FieldName = "normal"
        Me.colnormal.Name = "colnormal"
        Me.colnormal.Visible = True
        Me.colnormal.Width = 309
        '
        'colhn
        '
        Me.colhn.FieldName = "hn"
        Me.colhn.Name = "colhn"
        '
        'colan
        '
        Me.colan.FieldName = "an"
        Me.colan.Name = "colan"
        '
        'colvn
        '
        Me.colvn.FieldName = "vn"
        Me.colvn.Name = "colvn"
        '
        'colresult
        '
        Me.colresult.FieldName = "result"
        Me.colresult.Name = "colresult"
        Me.colresult.RowIndex = 1
        Me.colresult.Visible = True
        Me.colresult.Width = 1632
        '
        'colresult2
        '
        Me.colresult2.FieldName = "result2"
        Me.colresult2.Name = "colresult2"
        Me.colresult2.RowIndex = 2
        Me.colresult2.Visible = True
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(922, 114)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(69, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 111
        Me.SimpleButton3.Text = "Print Report"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(883, 114)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(35, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 110
        Me.SimpleButton2.Text = "Save"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "mascheckup_physical"
        Me.GridControl1.DataSource = Me.DtCheckUP1
        Me.GridControl1.Location = New System.Drawing.Point(13, 169)
        Me.GridControl1.MainView = Me.GridView4
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.checkUpPhysical_result})
        Me.GridControl1.Size = New System.Drawing.Size(1054, 382)
        Me.GridControl1.TabIndex = 109
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'DtCheckUP1
        '
        Me.DtCheckUP1.DataSetName = "dtCheckUP"
        Me.DtCheckUP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascheckup_physical, Me.colname1, Me.colname21, Me.colidvalue})
        Me.GridView4.GridControl = Me.GridControl1
        Me.GridView4.Name = "GridView4"
        '
        'colidmascheckup_physical
        '
        Me.colidmascheckup_physical.FieldName = "idmascheckup_physical"
        Me.colidmascheckup_physical.Name = "colidmascheckup_physical"
        '
        'colname1
        '
        Me.colname1.FieldName = "name"
        Me.colname1.Name = "colname1"
        Me.colname1.Visible = True
        Me.colname1.VisibleIndex = 0
        Me.colname1.Width = 1250
        '
        'colname21
        '
        Me.colname21.FieldName = "name2"
        Me.colname21.Name = "colname21"
        '
        'colidvalue
        '
        Me.colidvalue.ColumnEdit = Me.checkUpPhysical_result
        Me.colidvalue.FieldName = "idvalue"
        Me.colidvalue.Name = "colidvalue"
        Me.colidvalue.Visible = True
        Me.colidvalue.VisibleIndex = 1
        Me.colidvalue.Width = 382
        '
        'checkUpPhysical_result
        '
        Me.checkUpPhysical_result.AutoHeight = False
        Me.checkUpPhysical_result.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.checkUpPhysical_result.DataSource = Me.MascheckupphysicalresultBindingSource
        Me.checkUpPhysical_result.DisplayMember = "name"
        Me.checkUpPhysical_result.Name = "checkUpPhysical_result"
        Me.checkUpPhysical_result.ValueMember = "idmascheckup_physical_result"
        Me.checkUpPhysical_result.View = Me.RepositoryItemSearchLookUpEdit1View
        '
        'MascheckupphysicalresultBindingSource
        '
        Me.MascheckupphysicalresultBindingSource.DataMember = "mascheckup_physical_result"
        Me.MascheckupphysicalresultBindingSource.DataSource = Me.DtCheckUP2
        '
        'DtCheckUP2
        '
        Me.DtCheckUP2.DataSetName = "dtCheckUP"
        Me.DtCheckUP2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RepositoryItemSearchLookUpEdit1View
        '
        Me.RepositoryItemSearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.RepositoryItemSearchLookUpEdit1View.Name = "RepositoryItemSearchLookUpEdit1View"
        Me.RepositoryItemSearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.RepositoryItemSearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(1051, 329)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl13.StyleController = Me.LayoutControl1
        Me.LabelControl13.TabIndex = 108
        Me.LabelControl13.Text = "%"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(1051, 305)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl12.StyleController = Me.LayoutControl1
        Me.LabelControl12.TabIndex = 107
        Me.LabelControl12.Text = "%"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(1051, 281)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl11.StyleController = Me.LayoutControl1
        Me.LabelControl11.TabIndex = 106
        Me.LabelControl11.Text = "%"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(451, 329)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl10.StyleController = Me.LayoutControl1
        Me.LabelControl10.TabIndex = 105
        Me.LabelControl10.Text = "%"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(451, 305)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl9.StyleController = Me.LayoutControl1
        Me.LabelControl9.TabIndex = 104
        Me.LabelControl9.Text = "%"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(451, 281)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl8.StyleController = Me.LayoutControl1
        Me.LabelControl8.TabIndex = 103
        Me.LabelControl8.Text = "%"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(466, 257)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl7.StyleController = Me.LayoutControl1
        Me.LabelControl7.TabIndex = 102
        Me.LabelControl7.Text = "*10 cells^3/mm^3"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(1051, 216)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl6.StyleController = Me.LayoutControl1
        Me.LabelControl6.TabIndex = 101
        Me.LabelControl6.Text = "%"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(466, 216)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl5.StyleController = Me.LayoutControl1
        Me.LabelControl5.TabIndex = 100
        Me.LabelControl5.Text = "gm%"
        '
        'txtUrineResult_remark
        '
        Me.txtUrineResult_remark.Location = New System.Drawing.Point(13, 481)
        Me.txtUrineResult_remark.Name = "txtUrineResult_remark"
        Me.txtUrineResult_remark.Size = New System.Drawing.Size(750, 70)
        Me.txtUrineResult_remark.StyleController = Me.LayoutControl1
        Me.txtUrineResult_remark.TabIndex = 99
        Me.txtUrineResult_remark.Tag = "urine_result_remark"
        '
        'txtUrineresult
        '
        Me.txtUrineresult.Location = New System.Drawing.Point(233, 457)
        Me.txtUrineresult.Name = "txtUrineresult"
        Me.txtUrineresult.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtUrineresult.Properties.DataSource = Me.TxtUrineresultBindingSource
        Me.txtUrineresult.Properties.DisplayMember = "name"
        Me.txtUrineresult.Properties.ValueMember = "idmascheckup"
        Me.txtUrineresult.Properties.View = Me.GridView3
        Me.txtUrineresult.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineresult.StyleController = Me.LayoutControl1
        Me.txtUrineresult.TabIndex = 98
        Me.txtUrineresult.Tag = "urine_result"
        '
        'TxtUrineresultBindingSource
        '
        Me.TxtUrineresultBindingSource.DataMember = "txtUrineresult"
        Me.TxtUrineresultBindingSource.DataSource = Me.DtCheckUP3
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn25, Me.GridColumn26, Me.GridColumn27, Me.GridColumn28, Me.GridColumn29, Me.GridColumn30})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn25
        '
        Me.GridColumn25.FieldName = "idmascheckup"
        Me.GridColumn25.Name = "GridColumn25"
        '
        'GridColumn26
        '
        Me.GridColumn26.FieldName = "name"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 0
        '
        'GridColumn27
        '
        Me.GridColumn27.FieldName = "name2"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 1
        '
        'GridColumn28
        '
        Me.GridColumn28.FieldName = "det"
        Me.GridColumn28.Name = "GridColumn28"
        '
        'GridColumn29
        '
        Me.GridColumn29.FieldName = "description"
        Me.GridColumn29.Name = "GridColumn29"
        '
        'GridColumn30
        '
        Me.GridColumn30.FieldName = "form"
        Me.GridColumn30.Name = "GridColumn30"
        '
        'txtUrineAmphetamine
        '
        Me.txtUrineAmphetamine.Location = New System.Drawing.Point(233, 433)
        Me.txtUrineAmphetamine.Name = "txtUrineAmphetamine"
        Me.txtUrineAmphetamine.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineAmphetamine.StyleController = Me.LayoutControl1
        Me.txtUrineAmphetamine.TabIndex = 97
        Me.txtUrineAmphetamine.Tag = "urine_amphetamine"
        '
        'txtUrineOther
        '
        Me.txtUrineOther.Location = New System.Drawing.Point(233, 409)
        Me.txtUrineOther.Name = "txtUrineOther"
        Me.txtUrineOther.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineOther.StyleController = Me.LayoutControl1
        Me.txtUrineOther.TabIndex = 96
        Me.txtUrineOther.Tag = "urine_other"
        '
        'txtUrineEpi
        '
        Me.txtUrineEpi.Location = New System.Drawing.Point(233, 385)
        Me.txtUrineEpi.Name = "txtUrineEpi"
        Me.txtUrineEpi.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineEpi.StyleController = Me.LayoutControl1
        Me.txtUrineEpi.TabIndex = 95
        Me.txtUrineEpi.Tag = "urine_epi"
        '
        'txtUrineRBC
        '
        Me.txtUrineRBC.Location = New System.Drawing.Point(233, 361)
        Me.txtUrineRBC.Name = "txtUrineRBC"
        Me.txtUrineRBC.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineRBC.StyleController = Me.LayoutControl1
        Me.txtUrineRBC.TabIndex = 94
        Me.txtUrineRBC.Tag = "urine_rbc"
        '
        'txtUrineBlood
        '
        Me.txtUrineBlood.Location = New System.Drawing.Point(233, 337)
        Me.txtUrineBlood.Name = "txtUrineBlood"
        Me.txtUrineBlood.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineBlood.StyleController = Me.LayoutControl1
        Me.txtUrineBlood.TabIndex = 93
        Me.txtUrineBlood.Tag = "urine_blood"
        '
        'txtUrineGlucose
        '
        Me.txtUrineGlucose.Location = New System.Drawing.Point(233, 313)
        Me.txtUrineGlucose.Name = "txtUrineGlucose"
        Me.txtUrineGlucose.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineGlucose.StyleController = Me.LayoutControl1
        Me.txtUrineGlucose.TabIndex = 92
        Me.txtUrineGlucose.Tag = "urine_glucose"
        '
        'txtUrineAppearance
        '
        Me.txtUrineAppearance.Location = New System.Drawing.Point(233, 289)
        Me.txtUrineAppearance.Name = "txtUrineAppearance"
        Me.txtUrineAppearance.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineAppearance.StyleController = Me.LayoutControl1
        Me.txtUrineAppearance.TabIndex = 91
        Me.txtUrineAppearance.Tag = "urine_appearance"
        '
        'txtUrineWBC
        '
        Me.txtUrineWBC.Location = New System.Drawing.Point(233, 265)
        Me.txtUrineWBC.Name = "txtUrineWBC"
        Me.txtUrineWBC.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineWBC.StyleController = Me.LayoutControl1
        Me.txtUrineWBC.TabIndex = 90
        Me.txtUrineWBC.Tag = "urine_wbc"
        '
        'txtUrinepH
        '
        Me.txtUrinepH.Location = New System.Drawing.Point(233, 241)
        Me.txtUrinepH.Name = "txtUrinepH"
        Me.txtUrinepH.Size = New System.Drawing.Size(530, 20)
        Me.txtUrinepH.StyleController = Me.LayoutControl1
        Me.txtUrinepH.TabIndex = 89
        Me.txtUrinepH.Tag = "urine_ph"
        '
        'txtUrineSPGR
        '
        Me.txtUrineSPGR.Location = New System.Drawing.Point(233, 217)
        Me.txtUrineSPGR.Name = "txtUrineSPGR"
        Me.txtUrineSPGR.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineSPGR.StyleController = Me.LayoutControl1
        Me.txtUrineSPGR.TabIndex = 88
        Me.txtUrineSPGR.Tag = "urine_spgr"
        '
        'txtUrineAlbumin
        '
        Me.txtUrineAlbumin.Location = New System.Drawing.Point(233, 193)
        Me.txtUrineAlbumin.Name = "txtUrineAlbumin"
        Me.txtUrineAlbumin.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineAlbumin.StyleController = Me.LayoutControl1
        Me.txtUrineAlbumin.TabIndex = 87
        Me.txtUrineAlbumin.Tag = "urine_albumin"
        '
        'txtUrineColor
        '
        Me.txtUrineColor.Location = New System.Drawing.Point(233, 169)
        Me.txtUrineColor.Name = "txtUrineColor"
        Me.txtUrineColor.Size = New System.Drawing.Size(530, 20)
        Me.txtUrineColor.StyleController = Me.LayoutControl1
        Me.txtUrineColor.TabIndex = 86
        Me.txtUrineColor.Tag = "urine_color"
        '
        'txtblood_result
        '
        Me.txtblood_result.Location = New System.Drawing.Point(18, 401)
        Me.txtblood_result.Name = "txtblood_result"
        Me.txtblood_result.Size = New System.Drawing.Size(444, 145)
        Me.txtblood_result.StyleController = Me.LayoutControl1
        Me.txtblood_result.TabIndex = 85
        Me.txtblood_result.Tag = "blood_result"
        '
        'txtblood
        '
        Me.txtblood.Location = New System.Drawing.Point(238, 377)
        Me.txtblood.Name = "txtblood"
        Me.txtblood.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtblood.Properties.DataSource = Me.TxtBloodBindingSource
        Me.txtblood.Properties.DisplayMember = "name"
        Me.txtblood.Properties.ValueMember = "idmascheckup"
        Me.txtblood.Properties.View = Me.GridView2
        Me.txtblood.Size = New System.Drawing.Size(224, 20)
        Me.txtblood.StyleController = Me.LayoutControl1
        Me.txtblood.TabIndex = 84
        Me.txtblood.Tag = "blood"
        '
        'TxtBloodBindingSource
        '
        Me.TxtBloodBindingSource.DataMember = "txtBlood"
        Me.TxtBloodBindingSource.DataSource = Me.DtCheckUP3
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn19, Me.GridColumn20, Me.GridColumn21, Me.GridColumn22, Me.GridColumn23, Me.GridColumn24})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn19
        '
        Me.GridColumn19.FieldName = "idmascheckup"
        Me.GridColumn19.Name = "GridColumn19"
        '
        'GridColumn20
        '
        Me.GridColumn20.FieldName = "name"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 0
        '
        'GridColumn21
        '
        Me.GridColumn21.FieldName = "name2"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = True
        Me.GridColumn21.VisibleIndex = 1
        '
        'GridColumn22
        '
        Me.GridColumn22.FieldName = "det"
        Me.GridColumn22.Name = "GridColumn22"
        '
        'GridColumn23
        '
        Me.GridColumn23.FieldName = "description"
        Me.GridColumn23.Name = "GridColumn23"
        '
        'GridColumn24
        '
        Me.GridColumn24.FieldName = "form"
        Me.GridColumn24.Name = "GridColumn24"
        '
        'txtPlateCount
        '
        Me.txtPlateCount.Location = New System.Drawing.Point(686, 353)
        Me.txtPlateCount.Name = "txtPlateCount"
        Me.txtPlateCount.Size = New System.Drawing.Size(376, 20)
        Me.txtPlateCount.StyleController = Me.LayoutControl1
        Me.txtPlateCount.TabIndex = 83
        Me.txtPlateCount.Tag = "PlateCount"
        '
        'txtRBC_Morphology
        '
        Me.txtRBC_Morphology.Location = New System.Drawing.Point(238, 353)
        Me.txtRBC_Morphology.Name = "txtRBC_Morphology"
        Me.txtRBC_Morphology.Size = New System.Drawing.Size(224, 20)
        Me.txtRBC_Morphology.StyleController = Me.LayoutControl1
        Me.txtRBC_Morphology.TabIndex = 82
        Me.txtRBC_Morphology.Tag = "RBC_Morphology"
        '
        'txtAtypical_lymph
        '
        Me.txtAtypical_lymph.Location = New System.Drawing.Point(686, 329)
        Me.txtAtypical_lymph.Name = "txtAtypical_lymph"
        Me.txtAtypical_lymph.Size = New System.Drawing.Size(361, 20)
        Me.txtAtypical_lymph.StyleController = Me.LayoutControl1
        Me.txtAtypical_lymph.TabIndex = 81
        Me.txtAtypical_lymph.Tag = "Atypical_lymph"
        '
        'txtBashophils
        '
        Me.txtBashophils.Location = New System.Drawing.Point(238, 329)
        Me.txtBashophils.Name = "txtBashophils"
        Me.txtBashophils.Size = New System.Drawing.Size(209, 20)
        Me.txtBashophils.StyleController = Me.LayoutControl1
        Me.txtBashophils.TabIndex = 80
        Me.txtBashophils.Tag = "Bashophils"
        '
        'txtMonocytes
        '
        Me.txtMonocytes.Location = New System.Drawing.Point(686, 305)
        Me.txtMonocytes.Name = "txtMonocytes"
        Me.txtMonocytes.Size = New System.Drawing.Size(361, 20)
        Me.txtMonocytes.StyleController = Me.LayoutControl1
        Me.txtMonocytes.TabIndex = 79
        Me.txtMonocytes.Tag = "Monocytes"
        '
        'txtEosionophils
        '
        Me.txtEosionophils.Location = New System.Drawing.Point(238, 305)
        Me.txtEosionophils.Name = "txtEosionophils"
        Me.txtEosionophils.Size = New System.Drawing.Size(209, 20)
        Me.txtEosionophils.StyleController = Me.LayoutControl1
        Me.txtEosionophils.TabIndex = 78
        Me.txtEosionophils.Tag = "Eosionophils"
        '
        'txtLymphocytes
        '
        Me.txtLymphocytes.Location = New System.Drawing.Point(686, 281)
        Me.txtLymphocytes.Name = "txtLymphocytes"
        Me.txtLymphocytes.Size = New System.Drawing.Size(361, 20)
        Me.txtLymphocytes.StyleController = Me.LayoutControl1
        Me.txtLymphocytes.TabIndex = 77
        Me.txtLymphocytes.Tag = "Lymphocytes"
        '
        'txtNeutrophils
        '
        Me.txtNeutrophils.Location = New System.Drawing.Point(238, 281)
        Me.txtNeutrophils.Name = "txtNeutrophils"
        Me.txtNeutrophils.Size = New System.Drawing.Size(209, 20)
        Me.txtNeutrophils.StyleController = Me.LayoutControl1
        Me.txtNeutrophils.TabIndex = 76
        Me.txtNeutrophils.Tag = "Neutrophils"
        '
        'txtWBC
        '
        Me.txtWBC.Location = New System.Drawing.Point(238, 257)
        Me.txtWBC.Name = "txtWBC"
        Me.txtWBC.Size = New System.Drawing.Size(224, 20)
        Me.txtWBC.StyleController = Me.LayoutControl1
        Me.txtWBC.TabIndex = 75
        Me.txtWBC.Tag = "wbc"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(18, 240)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(1044, 13)
        Me.LabelControl4.StyleController = Me.LayoutControl1
        Me.LabelControl4.TabIndex = 74
        Me.LabelControl4.Text = "เม็ดเลือดขาและแยกประเภท ( White blood count & differentiation ) :"
        '
        'txtHct
        '
        Me.txtHct.Location = New System.Drawing.Point(715, 216)
        Me.txtHct.Name = "txtHct"
        Me.txtHct.Size = New System.Drawing.Size(332, 20)
        Me.txtHct.StyleController = Me.LayoutControl1
        Me.txtHct.TabIndex = 73
        Me.txtHct.Tag = "Hct"
        '
        'txtHgb
        '
        Me.txtHgb.Location = New System.Drawing.Point(238, 216)
        Me.txtHgb.Name = "txtHgb"
        Me.txtHgb.Size = New System.Drawing.Size(224, 20)
        Me.txtHgb.StyleController = Me.LayoutControl1
        Me.txtHgb.TabIndex = 72
        Me.txtHgb.Tag = "Hgb"
        '
        'txtBloodRh
        '
        Me.txtBloodRh.EditValue = ""
        Me.txtBloodRh.Location = New System.Drawing.Point(693, 192)
        Me.txtBloodRh.Name = "txtBloodRh"
        Me.txtBloodRh.Size = New System.Drawing.Size(369, 20)
        Me.txtBloodRh.StyleController = Me.LayoutControl1
        Me.txtBloodRh.TabIndex = 71
        Me.txtBloodRh.Tag = "bloodRh"
        '
        'txtBloodgroup
        '
        Me.txtBloodgroup.Location = New System.Drawing.Point(238, 192)
        Me.txtBloodgroup.Name = "txtBloodgroup"
        Me.txtBloodgroup.Size = New System.Drawing.Size(231, 20)
        Me.txtBloodgroup.StyleController = Me.LayoutControl1
        Me.txtBloodgroup.TabIndex = 70
        Me.txtBloodgroup.Tag = "bloodGroup"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(1142, 132)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(0, 13)
        Me.LabelControl3.StyleController = Me.LayoutControl1
        Me.LabelControl3.TabIndex = 69
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(507, 132)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(0, 13)
        Me.LabelControl2.StyleController = Me.LayoutControl1
        Me.LabelControl2.TabIndex = 68
        '
        'txtEyeLR_result
        '
        Me.txtEyeLR_result.Location = New System.Drawing.Point(696, 336)
        Me.txtEyeLR_result.Name = "txtEyeLR_result"
        Me.txtEyeLR_result.Size = New System.Drawing.Size(371, 20)
        Me.txtEyeLR_result.StyleController = Me.LayoutControl1
        Me.txtEyeLR_result.TabIndex = 58
        '
        'txteyeL
        '
        Me.txteyeL.Location = New System.Drawing.Point(328, 336)
        Me.txteyeL.Name = "txteyeL"
        Me.txteyeL.Size = New System.Drawing.Size(203, 20)
        Me.txteyeL.StyleController = Me.LayoutControl1
        Me.txteyeL.TabIndex = 57
        '
        'txtTooth
        '
        Me.txtTooth.Location = New System.Drawing.Point(168, 408)
        Me.txtTooth.Name = "txtTooth"
        Me.txtTooth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtTooth.Properties.DataSource = Me.TxtToothBindingSource
        Me.txtTooth.Properties.DisplayMember = "name"
        Me.txtTooth.Properties.ValueMember = "idmascheckup"
        Me.txtTooth.Properties.View = Me.SearchLookUpEdit4View
        Me.txtTooth.Size = New System.Drawing.Size(363, 20)
        Me.txtTooth.StyleController = Me.LayoutControl1
        Me.txtTooth.TabIndex = 55
        '
        'TxtToothBindingSource
        '
        Me.TxtToothBindingSource.DataMember = "txtTooth"
        Me.TxtToothBindingSource.DataSource = Me.DtCheckUP
        '
        'DtCheckUP
        '
        Me.DtCheckUP.DataSetName = "dtCheckUP"
        Me.DtCheckUP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit4View
        '
        Me.SearchLookUpEdit4View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn17, Me.GridColumn18})
        Me.SearchLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit4View.Name = "SearchLookUpEdit4View"
        Me.SearchLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit4View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn13
        '
        Me.GridColumn13.FieldName = "idmascheckup"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'GridColumn14
        '
        Me.GridColumn14.FieldName = "name"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.FieldName = "name2"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 1
        '
        'GridColumn16
        '
        Me.GridColumn16.FieldName = "det"
        Me.GridColumn16.Name = "GridColumn16"
        '
        'GridColumn17
        '
        Me.GridColumn17.FieldName = "description"
        Me.GridColumn17.Name = "GridColumn17"
        '
        'GridColumn18
        '
        Me.GridColumn18.FieldName = "form"
        Me.GridColumn18.Name = "GridColumn18"
        '
        'txtTooth_result
        '
        Me.txtTooth_result.Location = New System.Drawing.Point(535, 408)
        Me.txtTooth_result.Name = "txtTooth_result"
        Me.txtTooth_result.Size = New System.Drawing.Size(532, 20)
        Me.txtTooth_result.StyleController = Me.LayoutControl1
        Me.txtTooth_result.TabIndex = 54
        '
        'txteye_result
        '
        Me.txteye_result.Location = New System.Drawing.Point(535, 384)
        Me.txteye_result.Name = "txteye_result"
        Me.txteye_result.Size = New System.Drawing.Size(532, 20)
        Me.txteye_result.StyleController = Me.LayoutControl1
        Me.txteye_result.TabIndex = 53
        '
        'txtBlindness_result
        '
        Me.txtBlindness_result.Location = New System.Drawing.Point(535, 360)
        Me.txtBlindness_result.Name = "txtBlindness_result"
        Me.txtBlindness_result.Size = New System.Drawing.Size(532, 20)
        Me.txtBlindness_result.StyleController = Me.LayoutControl1
        Me.txtBlindness_result.TabIndex = 52
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(13, 289)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(541, 19)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 51
        Me.LabelControl1.Text = "ตรวจต้อหินและตรวจตาโดยจักษุแพทย์ (Eyes examination by Ophthalamologist)"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(834, 114)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(45, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 50
        Me.SimpleButton1.Text = "History"
        '
        'txtEyeLR
        '
        Me.txtEyeLR.Location = New System.Drawing.Point(535, 336)
        Me.txtEyeLR.Name = "txtEyeLR"
        Me.txtEyeLR.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEyeLR.Properties.DataSource = Me.TxtEyeLRBindingSource
        Me.txtEyeLR.Properties.DisplayMember = "name"
        Me.txtEyeLR.Properties.ValueMember = "idmascheckup"
        Me.txtEyeLR.Properties.View = Me.SearchLookUpEdit3View
        Me.txtEyeLR.Size = New System.Drawing.Size(157, 20)
        Me.txtEyeLR.StyleController = Me.LayoutControl1
        Me.txtEyeLR.TabIndex = 47
        '
        'TxtEyeLRBindingSource
        '
        Me.TxtEyeLRBindingSource.DataMember = "txtEyeLR"
        Me.TxtEyeLRBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12})
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.FieldName = "idmascheckup"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "name"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        '
        'GridColumn9
        '
        Me.GridColumn9.FieldName = "name2"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        '
        'GridColumn10
        '
        Me.GridColumn10.FieldName = "det"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'GridColumn11
        '
        Me.GridColumn11.FieldName = "description"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.FieldName = "form"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'txtEyeR
        '
        Me.txtEyeR.Location = New System.Drawing.Point(66, 336)
        Me.txtEyeR.Name = "txtEyeR"
        Me.txtEyeR.Size = New System.Drawing.Size(202, 20)
        Me.txtEyeR.StyleController = Me.LayoutControl1
        Me.txtEyeR.TabIndex = 46
        '
        'txtEye
        '
        Me.txtEye.Location = New System.Drawing.Point(168, 384)
        Me.txtEye.Name = "txtEye"
        Me.txtEye.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEye.Properties.DataSource = Me.TxtEyeBindingSource
        Me.txtEye.Properties.DisplayMember = "name"
        Me.txtEye.Properties.ValueMember = "idmascheckup"
        Me.txtEye.Properties.View = Me.SearchLookUpEdit2View
        Me.txtEye.Size = New System.Drawing.Size(363, 20)
        Me.txtEye.StyleController = Me.LayoutControl1
        Me.txtEye.TabIndex = 45
        '
        'TxtEyeBindingSource
        '
        Me.TxtEyeBindingSource.DataMember = "txtEye"
        Me.TxtEyeBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascheckup, Me.colname, Me.colname2, Me.coldet, Me.coldescription, Me.colform})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'colidmascheckup
        '
        Me.colidmascheckup.FieldName = "idmascheckup"
        Me.colidmascheckup.Name = "colidmascheckup"
        '
        'colname
        '
        Me.colname.FieldName = "name"
        Me.colname.Name = "colname"
        Me.colname.Visible = True
        Me.colname.VisibleIndex = 0
        '
        'colname2
        '
        Me.colname2.FieldName = "name2"
        Me.colname2.Name = "colname2"
        Me.colname2.Visible = True
        Me.colname2.VisibleIndex = 1
        '
        'coldet
        '
        Me.coldet.FieldName = "det"
        Me.coldet.Name = "coldet"
        '
        'coldescription
        '
        Me.coldescription.FieldName = "description"
        Me.coldescription.Name = "coldescription"
        '
        'colform
        '
        Me.colform.FieldName = "form"
        Me.colform.Name = "colform"
        '
        'txtBlindness
        '
        Me.txtBlindness.Location = New System.Drawing.Point(168, 360)
        Me.txtBlindness.Name = "txtBlindness"
        Me.txtBlindness.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtBlindness.Properties.DataSource = Me.TxtBlindnessBindingSource
        Me.txtBlindness.Properties.DisplayMember = "name"
        Me.txtBlindness.Properties.ValueMember = "idmascheckup"
        Me.txtBlindness.Properties.View = Me.SearchLookUpEdit1View
        Me.txtBlindness.Size = New System.Drawing.Size(363, 20)
        Me.txtBlindness.StyleController = Me.LayoutControl1
        Me.txtBlindness.TabIndex = 43
        '
        'TxtBlindnessBindingSource
        '
        Me.TxtBlindnessBindingSource.DataMember = "txtBlindness"
        Me.TxtBlindnessBindingSource.DataSource = Me.DtCheckUP
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "idmascheckup"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Width = 253
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "name"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 689
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "name2"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 690
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "det"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "description"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "form"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'txtEyeResult
        '
        Me.txtEyeResult.Location = New System.Drawing.Point(13, 312)
        Me.txtEyeResult.Name = "txtEyeResult"
        Me.txtEyeResult.Size = New System.Drawing.Size(1054, 20)
        Me.txtEyeResult.StyleController = Me.LayoutControl1
        Me.txtEyeResult.TabIndex = 42
        '
        'txtBloodPressure1
        '
        Me.txtBloodPressure1.Location = New System.Drawing.Point(233, 265)
        Me.txtBloodPressure1.Name = "txtBloodPressure1"
        Me.txtBloodPressure1.Size = New System.Drawing.Size(834, 20)
        Me.txtBloodPressure1.StyleController = Me.LayoutControl1
        Me.txtBloodPressure1.TabIndex = 40
        '
        'txtBloodPressure
        '
        Me.txtBloodPressure.Location = New System.Drawing.Point(233, 241)
        Me.txtBloodPressure.Name = "txtBloodPressure"
        Me.txtBloodPressure.Size = New System.Drawing.Size(355, 20)
        Me.txtBloodPressure.StyleController = Me.LayoutControl1
        Me.txtBloodPressure.TabIndex = 39
        '
        'txtHeight
        '
        Me.txtHeight.Location = New System.Drawing.Point(233, 169)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(355, 20)
        Me.txtHeight.StyleController = Me.LayoutControl1
        Me.txtHeight.TabIndex = 38
        '
        'txtBmi
        '
        Me.txtBmi.Location = New System.Drawing.Point(233, 217)
        Me.txtBmi.Name = "txtBmi"
        Me.txtBmi.Properties.ReadOnly = True
        Me.txtBmi.Size = New System.Drawing.Size(355, 20)
        Me.txtBmi.StyleController = Me.LayoutControl1
        Me.txtBmi.TabIndex = 15
        '
        'txtWeight
        '
        Me.txtWeight.Location = New System.Drawing.Point(233, 193)
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Size = New System.Drawing.Size(355, 20)
        Me.txtWeight.StyleController = Me.LayoutControl1
        Me.txtWeight.TabIndex = 13
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1, Me.LayoutControlItem13, Me.LayoutControlItem60, Me.LayoutControlItem61, Me.LayoutControlItem78, Me.LayoutControlItem3, Me.LayoutControlItem79, Me.LayoutControlItem80, Me.LayoutControlItem81, Me.LayoutControlItem82, Me.LayoutControlItem83, Me.LayoutControlItem84, Me.LayoutControlItem85, Me.LayoutControlItem86, Me.LayoutControlItem87, Me.LayoutControlItem88, Me.LayoutControlItem107, Me.EmptySpaceItem5, Me.LayoutControlItem110, Me.LayoutControlItem111})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1080, 564)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'TabbedControlGroup1
        '
        Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 134)
        Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4)
        Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup2
        Me.TabbedControlGroup1.SelectedTabPageIndex = 0
        Me.TabbedControlGroup1.Size = New System.Drawing.Size(1072, 422)
        Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup6, Me.LayoutControlGroup3, Me.LayoutControlGroup4, Me.LayoutControlGroup7, Me.LayoutControlGroup8, Me.LayoutControlGroup15, Me.LayoutControlGroup16})
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem39, Me.LayoutControlItem40, Me.LayoutControlItem14, Me.LayoutControlItem11, Me.LayoutControlItem15, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem41, Me.LayoutControlItem21, Me.LayoutControlItem20, Me.EmptySpaceItem1, Me.LayoutControlItem12, Me.LayoutControlItem10, Me.LayoutControlItem25, Me.LayoutControlItem34, Me.LayoutControlItem109, Me.LayoutControlItem108})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup2.Text = "ตรวจสุขภาพร่างกาย"
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.txtBloodPressure
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(579, 24)
        Me.LayoutControlItem33.Text = "ชีพจร (Pulse rate) :"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.txtEyeResult
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 143)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(1058, 24)
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem36.TextVisible = False
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.txtBlindness
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 191)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(522, 24)
        Me.LayoutControlItem37.Text = "ColorBlindness :"
        Me.LayoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(150, 13)
        Me.LayoutControlItem37.TextToControlDistance = 5
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.txtEye
        Me.LayoutControlItem39.Location = New System.Drawing.Point(0, 215)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(522, 24)
        Me.LayoutControlItem39.Text = "ตรวจตา :"
        Me.LayoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(150, 13)
        Me.LayoutControlItem39.TextToControlDistance = 5
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.txtEyeR
        Me.LayoutControlItem40.Location = New System.Drawing.Point(0, 167)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(259, 24)
        Me.LayoutControlItem40.Text = "ขวา ( R ) :"
        Me.LayoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(48, 13)
        Me.LayoutControlItem40.TextToControlDistance = 5
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.LabelControl1
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(1058, 23)
        Me.LayoutControlItem14.Text = "ตรวจต้อหินและตรวจตาโดยจักษุแพทย์ (Eyes examination by Ophthalamologist)"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.txtBlindness_result
        Me.LayoutControlItem11.Location = New System.Drawing.Point(522, 191)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(536, 24)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.txteye_result
        Me.LayoutControlItem15.Location = New System.Drawing.Point(522, 215)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(536, 24)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.txtTooth_result
        Me.LayoutControlItem17.Location = New System.Drawing.Point(522, 239)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(536, 147)
        Me.LayoutControlItem17.Text = "ตรวจฟัน :"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.txtTooth
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 239)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(522, 147)
        Me.LayoutControlItem18.Text = "ตรวจฟัน :"
        Me.LayoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(150, 13)
        Me.LayoutControlItem18.TextToControlDistance = 5
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.txtEyeLR
        Me.LayoutControlItem41.Location = New System.Drawing.Point(522, 167)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(161, 24)
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem41.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.txtEyeLR_result
        Me.LayoutControlItem21.Location = New System.Drawing.Point(683, 167)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(375, 24)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.txteyeL
        Me.LayoutControlItem20.Location = New System.Drawing.Point(259, 167)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(263, 24)
        Me.LayoutControlItem20.Text = "ซ้าย ( L ) : "
        Me.LayoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(51, 13)
        Me.LayoutControlItem20.TextToControlDistance = 5
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(579, 48)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(479, 48)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.txtBmi
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(579, 24)
        Me.LayoutControlItem12.Text = "BMI"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtWeight
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(579, 24)
        Me.LayoutControlItem10.Text = "น้ำหนัก ( KG. )"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.txtHeight
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(579, 24)
        Me.LayoutControlItem25.Text = "ส่วนสูง ( CM. )"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.txtBloodPressure1
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(1058, 24)
        Me.LayoutControlItem34.Text = "ความดันโลหิต (Blood Pressure) :"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem109
        '
        Me.LayoutControlItem109.Control = Me.chkglasses_without
        Me.LayoutControlItem109.Location = New System.Drawing.Point(579, 0)
        Me.LayoutControlItem109.Name = "LayoutControlItem109"
        Me.LayoutControlItem109.Size = New System.Drawing.Size(165, 48)
        Me.LayoutControlItem109.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem109.TextVisible = False
        '
        'LayoutControlItem108
        '
        Me.LayoutControlItem108.Control = Me.chkglasses
        Me.LayoutControlItem108.Location = New System.Drawing.Point(744, 0)
        Me.LayoutControlItem108.Name = "LayoutControlItem108"
        Me.LayoutControlItem108.Size = New System.Drawing.Size(314, 48)
        Me.LayoutControlItem108.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem108.TextVisible = False
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem59})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup6.Text = "ตรวจสุขภาพร่างกาย2"
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.Control = Me.GridControl1
        Me.LayoutControlItem59.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem59.Name = "LayoutControlItem59"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem59.TextVisible = False
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup5})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup3.Text = "โลหิตวิทยา"
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem16, Me.LayoutControlItem19, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem5, Me.LayoutControlItem50, Me.LayoutControlItem51, Me.LayoutControlItem52, Me.EmptySpaceItem3, Me.LayoutControlItem53, Me.LayoutControlItem54, Me.LayoutControlItem55, Me.LayoutControlItem56, Me.LayoutControlItem57, Me.LayoutControlItem58})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup5.Text = "โลหิตวิทยา"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.txtBloodgroup
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(455, 24)
        Me.LayoutControlItem1.Text = "หมู่เลือด"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtBloodRh
        Me.LayoutControlItem2.Location = New System.Drawing.Point(455, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(593, 24)
        Me.LayoutControlItem2.Text = "หมู่เลือด ( Rh )"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtHgb
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(448, 24)
        Me.LayoutControlItem4.Text = "Hemoglobin (Hgb)"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.LabelControl4
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(1048, 17)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtWBC
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 65)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(448, 24)
        Me.LayoutControlItem7.Text = "WBC :"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txtNeutrophils
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 89)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(433, 24)
        Me.LayoutControlItem8.Text = "Neutrophils :"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.txtLymphocytes
        Me.LayoutControlItem9.Location = New System.Drawing.Point(448, 89)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(585, 24)
        Me.LayoutControlItem9.Text = "Lymphocytes :"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.txtEosionophils
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 113)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(433, 24)
        Me.LayoutControlItem16.Text = "Eosinophils :"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.txtMonocytes
        Me.LayoutControlItem19.Location = New System.Drawing.Point(448, 113)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(585, 24)
        Me.LayoutControlItem19.Text = "Monocytes :"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.txtBashophils
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 137)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(433, 24)
        Me.LayoutControlItem22.Text = "Bashophils :"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.txtAtypical_lymph
        Me.LayoutControlItem23.Location = New System.Drawing.Point(448, 137)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(585, 24)
        Me.LayoutControlItem23.Text = "Atypical lymph :"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.txtRBC_Morphology
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 161)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(448, 24)
        Me.LayoutControlItem24.Text = "ลักษณะเม็ดเลือดแดง (Red Cell Morphology ) :"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.txtPlateCount
        Me.LayoutControlItem26.Location = New System.Drawing.Point(448, 161)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(600, 197)
        Me.LayoutControlItem26.Text = "เกล็ดเลือด (Platelet count) :"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.txtblood
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 185)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(448, 24)
        Me.LayoutControlItem27.Text = "สรุปผล"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.txtblood_result
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 209)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(448, 149)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtHct
        Me.LayoutControlItem5.Location = New System.Drawing.Point(477, 24)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(556, 24)
        Me.LayoutControlItem5.Text = "Hematocrit (HCT) "
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.Control = Me.LabelControl5
        Me.LayoutControlItem50.Location = New System.Drawing.Point(448, 24)
        Me.LayoutControlItem50.Name = "LayoutControlItem50"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(29, 24)
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem50.TextVisible = False
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.LabelControl6
        Me.LayoutControlItem51.Location = New System.Drawing.Point(1033, 24)
        Me.LayoutControlItem51.Name = "LayoutControlItem51"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem51.TextVisible = False
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.Control = Me.LabelControl7
        Me.LayoutControlItem52.Location = New System.Drawing.Point(448, 65)
        Me.LayoutControlItem52.Name = "LayoutControlItem52"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(93, 24)
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem52.TextVisible = False
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(541, 65)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(507, 24)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.Control = Me.LabelControl8
        Me.LayoutControlItem53.Location = New System.Drawing.Point(433, 89)
        Me.LayoutControlItem53.Name = "LayoutControlItem53"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem53.TextVisible = False
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.Control = Me.LabelControl9
        Me.LayoutControlItem54.Location = New System.Drawing.Point(433, 113)
        Me.LayoutControlItem54.Name = "LayoutControlItem54"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem54.TextVisible = False
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.Control = Me.LabelControl10
        Me.LayoutControlItem55.Location = New System.Drawing.Point(433, 137)
        Me.LayoutControlItem55.Name = "LayoutControlItem55"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem55.TextVisible = False
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.Control = Me.LabelControl11
        Me.LayoutControlItem56.Location = New System.Drawing.Point(1033, 89)
        Me.LayoutControlItem56.Name = "LayoutControlItem56"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem56.TextVisible = False
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.Control = Me.LabelControl12
        Me.LayoutControlItem57.Location = New System.Drawing.Point(1033, 113)
        Me.LayoutControlItem57.Name = "LayoutControlItem57"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem57.TextVisible = False
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.Control = Me.LabelControl13
        Me.LayoutControlItem58.Location = New System.Drawing.Point(1033, 137)
        Me.LayoutControlItem58.Name = "LayoutControlItem58"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(15, 24)
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem58.TextVisible = False
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem29, Me.EmptySpaceItem2, Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem32, Me.LayoutControlItem35, Me.LayoutControlItem38, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem44, Me.LayoutControlItem45, Me.LayoutControlItem46, Me.LayoutControlItem47, Me.LayoutControlItem48, Me.LayoutControlItem49})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup4.Text = "Urine Analysis"
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.txtUrineColor
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem29.Text = "Color :"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(217, 13)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(754, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(304, 386)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.txtUrineAlbumin
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem30.Text = "Albumin :"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.txtUrineSPGR
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem31.Text = "Sp. Gr. :"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.txtUrinepH
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem32.Text = "pH :"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.txtUrineWBC
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem35.Text = "WBC :"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.txtUrineAppearance
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem38.Text = "Appearance :"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.txtUrineGlucose
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem42.Text = "Glucose :"
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.txtUrineBlood
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem43.Text = "Blood :"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.txtUrineRBC
        Me.LayoutControlItem44.Location = New System.Drawing.Point(0, 192)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem44.Text = "RBC :"
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.txtUrineEpi
        Me.LayoutControlItem45.Location = New System.Drawing.Point(0, 216)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem45.Text = "Epi :"
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.txtUrineOther
        Me.LayoutControlItem46.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem46.Name = "LayoutControlItem46"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem46.Text = "Other :"
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.txtUrineAmphetamine
        Me.LayoutControlItem47.Location = New System.Drawing.Point(0, 264)
        Me.LayoutControlItem47.Name = "LayoutControlItem47"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem47.Text = "Amphetamine :"
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.txtUrineresult
        Me.LayoutControlItem48.Location = New System.Drawing.Point(0, 288)
        Me.LayoutControlItem48.Name = "LayoutControlItem48"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(754, 24)
        Me.LayoutControlItem48.Text = "สรุปผล"
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.Control = Me.txtUrineResult_remark
        Me.LayoutControlItem49.Location = New System.Drawing.Point(0, 312)
        Me.LayoutControlItem49.Name = "LayoutControlItem49"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(754, 74)
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem49.TextVisible = False
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem62})
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup7.Name = "LayoutControlGroup7"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup7.Text = "Chemical Lab"
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.Control = Me.GridControl2
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem62.Name = "LayoutControlItem62"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem62.TextVisible = False
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup9, Me.LayoutControlGroup10, Me.LayoutControlGroup14, Me.LayoutControlGroup13, Me.LayoutControlGroup11, Me.LayoutControlGroup12})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup8.Text = "Chemical Lab2"
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem65, Me.LayoutControlItem66})
        Me.LayoutControlGroup9.Location = New System.Drawing.Point(528, 108)
        Me.LayoutControlGroup9.Name = "LayoutControlGroup9"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(530, 140)
        Me.LayoutControlGroup9.Text = "HIV Antibody ตรวจหาเชื้อไวรัสเอดส์"
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.Control = Me.txtHivResult
        Me.LayoutControlItem65.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem65.Name = "LayoutControlItem65"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(506, 24)
        Me.LayoutControlItem65.Text = "ผล"
        Me.LayoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem65.TextToControlDistance = 5
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.Control = Me.txtHivResult_remark
        Me.LayoutControlItem66.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem66.Name = "LayoutControlItem66"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(506, 74)
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem66.TextVisible = False
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem63, Me.LayoutControlItem64})
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 108)
        Me.LayoutControlGroup10.Name = "LayoutControlGroup10"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(528, 140)
        Me.LayoutControlGroup10.Text = "Chest X-Ray เอกซเรย์ปอด"
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.Control = Me.txtXrayResult
        Me.LayoutControlItem63.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem63.Name = "LayoutControlItem63"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem63.Text = "ผล"
        Me.LayoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem63.TextToControlDistance = 5
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.Control = Me.txtXrayResult_remark
        Me.LayoutControlItem64.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem64.Name = "LayoutControlItem64"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(504, 74)
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem64.TextVisible = False
        '
        'LayoutControlGroup14
        '
        Me.LayoutControlGroup14.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem75, Me.LayoutControlItem76, Me.LayoutControlItem77})
        Me.LayoutControlGroup14.Location = New System.Drawing.Point(528, 248)
        Me.LayoutControlGroup14.Name = "LayoutControlGroup14"
        Me.LayoutControlGroup14.Size = New System.Drawing.Size(530, 138)
        Me.LayoutControlGroup14.Text = "Virus A ตรวจหาไวรัสตับอักเสบเอ"
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.Control = Me.txtVirusAResult
        Me.LayoutControlItem75.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem75.Name = "LayoutControlItem75"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(506, 24)
        Me.LayoutControlItem75.Text = "ผล"
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem76
        '
        Me.LayoutControlItem76.Control = Me.txtvirusa_havigg
        Me.LayoutControlItem76.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem76.Name = "LayoutControlItem76"
        Me.LayoutControlItem76.Size = New System.Drawing.Size(506, 24)
        Me.LayoutControlItem76.Text = "Anti HAV IgG"
        Me.LayoutControlItem76.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem77
        '
        Me.LayoutControlItem77.Control = Me.txtvirusa_havigm
        Me.LayoutControlItem77.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem77.Name = "LayoutControlItem77"
        Me.LayoutControlItem77.Size = New System.Drawing.Size(506, 48)
        Me.LayoutControlItem77.Text = "Anti HAV IgM"
        Me.LayoutControlItem77.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem73, Me.LayoutControlItem74, Me.LayoutControlItem72, Me.LayoutControlItem71})
        Me.LayoutControlGroup13.Location = New System.Drawing.Point(0, 248)
        Me.LayoutControlGroup13.Name = "LayoutControlGroup13"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(528, 138)
        Me.LayoutControlGroup13.Text = "Virus B ตรวจหาไวรัสตับอักเสบบี"
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.txtVirusb_hbcab
        Me.LayoutControlItem73.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem73.Text = "HbcAb"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.Control = Me.txtVirusb_hbsab
        Me.LayoutControlItem74.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem74.Name = "LayoutControlItem74"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem74.Text = "HbsAb"
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.Control = Me.txtVirusb_hbsag
        Me.LayoutControlItem72.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem72.Name = "LayoutControlItem72"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem72.Text = "HbsAg"
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.Control = Me.txtVirusBResult
        Me.LayoutControlItem71.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem71.Name = "LayoutControlItem71"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem71.Text = "ผล"
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem67, Me.LayoutControlItem69})
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "LayoutControlGroup11"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(528, 108)
        Me.LayoutControlGroup11.Text = "VDRL (ตรวจหาเชื้อซิฟิลิส)"
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.Control = Me.txtVDRLResult_remark
        Me.LayoutControlItem67.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem67.Name = "LayoutControlItem67"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(504, 42)
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem67.TextVisible = False
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.Control = Me.txtVDRLresult
        Me.LayoutControlItem69.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem69.Name = "LayoutControlItem69"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(504, 24)
        Me.LayoutControlItem69.Text = "ผล"
        Me.LayoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem69.TextToControlDistance = 5
        '
        'LayoutControlGroup12
        '
        Me.LayoutControlGroup12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem70, Me.LayoutControlItem68})
        Me.LayoutControlGroup12.Location = New System.Drawing.Point(528, 0)
        Me.LayoutControlGroup12.Name = "LayoutControlGroup12"
        Me.LayoutControlGroup12.Size = New System.Drawing.Size(530, 108)
        Me.LayoutControlGroup12.Text = "CEA ตรงหาสารบ่งชี้มะเร็งลำไส้ใหญ่"
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.Control = Me.txtCEAResult_remark
        Me.LayoutControlItem70.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem70.Name = "LayoutControlItem70"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(506, 42)
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem70.TextVisible = False
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.Control = Me.txtCEAResult
        Me.LayoutControlItem68.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem68.Name = "LayoutControlItem68"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(506, 24)
        Me.LayoutControlItem68.Text = "ผล"
        Me.LayoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem68.TextToControlDistance = 5
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem89, Me.LayoutControlItem90, Me.LayoutControlItem91, Me.LayoutControlItem92, Me.EmptySpaceItem4, Me.LayoutControlItem93, Me.LayoutControlItem94, Me.LayoutControlItem95, Me.LayoutControlItem96, Me.LayoutControlItem97})
        Me.LayoutControlGroup15.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup15.Name = "LayoutControlGroup15"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup15.Text = "Fitness"
        '
        'LayoutControlItem89
        '
        Me.LayoutControlItem89.Control = Me.txtfit_offshore
        Me.LayoutControlItem89.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem89.Name = "LayoutControlItem89"
        Me.LayoutControlItem89.Size = New System.Drawing.Size(1058, 24)
        Me.LayoutControlItem89.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem89.TextVisible = False
        '
        'LayoutControlItem90
        '
        Me.LayoutControlItem90.Control = Me.txtfit_specific
        Me.LayoutControlItem90.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem90.Name = "LayoutControlItem90"
        Me.LayoutControlItem90.Size = New System.Drawing.Size(1058, 24)
        Me.LayoutControlItem90.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem90.TextVisible = False
        '
        'LayoutControlItem91
        '
        Me.LayoutControlItem91.Control = Me.txtfit_unit
        Me.LayoutControlItem91.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem91.Name = "LayoutControlItem91"
        Me.LayoutControlItem91.Size = New System.Drawing.Size(1058, 24)
        Me.LayoutControlItem91.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem91.TextVisible = False
        '
        'LayoutControlItem92
        '
        Me.LayoutControlItem92.Control = Me.txtfit_temporary
        Me.LayoutControlItem92.Location = New System.Drawing.Point(0, 192)
        Me.LayoutControlItem92.Name = "LayoutControlItem92"
        Me.LayoutControlItem92.Size = New System.Drawing.Size(1058, 194)
        Me.LayoutControlItem92.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem92.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 48)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(70, 120)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem93
        '
        Me.LayoutControlItem93.Control = Me.txtfit_aircrew
        Me.LayoutControlItem93.Location = New System.Drawing.Point(70, 48)
        Me.LayoutControlItem93.Name = "LayoutControlItem93"
        Me.LayoutControlItem93.Size = New System.Drawing.Size(988, 24)
        Me.LayoutControlItem93.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem93.TextVisible = False
        '
        'LayoutControlItem94
        '
        Me.LayoutControlItem94.Control = Me.txtfit_emergency
        Me.LayoutControlItem94.Location = New System.Drawing.Point(70, 120)
        Me.LayoutControlItem94.Name = "LayoutControlItem94"
        Me.LayoutControlItem94.Size = New System.Drawing.Size(988, 24)
        Me.LayoutControlItem94.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem94.TextVisible = False
        '
        'LayoutControlItem95
        '
        Me.LayoutControlItem95.Control = Me.txtfit_crane
        Me.LayoutControlItem95.Location = New System.Drawing.Point(70, 96)
        Me.LayoutControlItem95.Name = "LayoutControlItem95"
        Me.LayoutControlItem95.Size = New System.Drawing.Size(988, 24)
        Me.LayoutControlItem95.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem95.TextVisible = False
        '
        'LayoutControlItem96
        '
        Me.LayoutControlItem96.Control = Me.txtfit_breathing
        Me.LayoutControlItem96.Location = New System.Drawing.Point(70, 72)
        Me.LayoutControlItem96.Name = "LayoutControlItem96"
        Me.LayoutControlItem96.Size = New System.Drawing.Size(988, 24)
        Me.LayoutControlItem96.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem96.TextVisible = False
        '
        'LayoutControlItem97
        '
        Me.LayoutControlItem97.Control = Me.txtfit_food
        Me.LayoutControlItem97.Location = New System.Drawing.Point(70, 144)
        Me.LayoutControlItem97.Name = "LayoutControlItem97"
        Me.LayoutControlItem97.Size = New System.Drawing.Size(988, 24)
        Me.LayoutControlItem97.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem97.TextVisible = False
        '
        'LayoutControlGroup16
        '
        Me.LayoutControlGroup16.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem98, Me.LayoutControlItem99, Me.LayoutControlItem101, Me.LayoutControlItem102, Me.LayoutControlItem103, Me.LayoutControlItem100, Me.LayoutControlItem104, Me.LayoutControlItem105, Me.LayoutControlItem106})
        Me.LayoutControlGroup16.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup16.Name = "LayoutControlGroup16"
        Me.LayoutControlGroup16.Size = New System.Drawing.Size(1058, 386)
        Me.LayoutControlGroup16.Text = "Medical Certificate"
        '
        'LayoutControlItem98
        '
        Me.LayoutControlItem98.Control = Me.txtFrncheckupResult_all
        Me.LayoutControlItem98.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem98.Name = "LayoutControlItem98"
        Me.LayoutControlItem98.Size = New System.Drawing.Size(1058, 147)
        Me.LayoutControlItem98.Text = "สรุปผล"
        Me.LayoutControlItem98.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem99
        '
        Me.LayoutControlItem99.Control = Me.txtfrnCheckUpFitness_Result
        Me.LayoutControlItem99.Location = New System.Drawing.Point(0, 219)
        Me.LayoutControlItem99.Name = "LayoutControlItem99"
        Me.LayoutControlItem99.Size = New System.Drawing.Size(1058, 167)
        Me.LayoutControlItem99.Text = "สรุปผล Fitness Other"
        Me.LayoutControlItem99.TextSize = New System.Drawing.Size(217, 13)
        '
        'LayoutControlItem101
        '
        Me.LayoutControlItem101.Control = Me.checkUrineAnalysis
        Me.LayoutControlItem101.Location = New System.Drawing.Point(665, 0)
        Me.LayoutControlItem101.Name = "LayoutControlItem101"
        Me.LayoutControlItem101.Size = New System.Drawing.Size(393, 23)
        Me.LayoutControlItem101.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem101.TextVisible = False
        '
        'LayoutControlItem102
        '
        Me.LayoutControlItem102.Control = Me.checkChemicalLab
        Me.LayoutControlItem102.Location = New System.Drawing.Point(665, 23)
        Me.LayoutControlItem102.Name = "LayoutControlItem102"
        Me.LayoutControlItem102.Size = New System.Drawing.Size(393, 23)
        Me.LayoutControlItem102.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem102.TextVisible = False
        '
        'LayoutControlItem103
        '
        Me.LayoutControlItem103.Control = Me.checkChemicallab2
        Me.LayoutControlItem103.Location = New System.Drawing.Point(665, 46)
        Me.LayoutControlItem103.Name = "LayoutControlItem103"
        Me.LayoutControlItem103.Size = New System.Drawing.Size(332, 26)
        Me.LayoutControlItem103.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem103.TextVisible = False
        '
        'LayoutControlItem100
        '
        Me.LayoutControlItem100.Control = Me.CheckExamination
        Me.LayoutControlItem100.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem100.Name = "LayoutControlItem100"
        Me.LayoutControlItem100.Size = New System.Drawing.Size(665, 23)
        Me.LayoutControlItem100.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem100.TextVisible = False
        '
        'LayoutControlItem104
        '
        Me.LayoutControlItem104.Control = Me.CheckExamination2
        Me.LayoutControlItem104.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem104.Name = "LayoutControlItem104"
        Me.LayoutControlItem104.Size = New System.Drawing.Size(665, 23)
        Me.LayoutControlItem104.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem104.TextVisible = False
        '
        'LayoutControlItem105
        '
        Me.LayoutControlItem105.Control = Me.checkBlood
        Me.LayoutControlItem105.Location = New System.Drawing.Point(0, 46)
        Me.LayoutControlItem105.Name = "LayoutControlItem105"
        Me.LayoutControlItem105.Size = New System.Drawing.Size(665, 26)
        Me.LayoutControlItem105.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem105.TextVisible = False
        '
        'LayoutControlItem106
        '
        Me.LayoutControlItem106.Control = Me.btnResult
        Me.LayoutControlItem106.Location = New System.Drawing.Point(997, 46)
        Me.LayoutControlItem106.Name = "LayoutControlItem106"
        Me.LayoutControlItem106.Size = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem106.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem106.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.SimpleButton1
        Me.LayoutControlItem13.Location = New System.Drawing.Point(828, 108)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.Control = Me.SimpleButton2
        Me.LayoutControlItem60.Location = New System.Drawing.Point(877, 108)
        Me.LayoutControlItem60.Name = "LayoutControlItem60"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(39, 26)
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem60.TextVisible = False
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.Control = Me.SimpleButton3
        Me.LayoutControlItem61.Location = New System.Drawing.Point(916, 108)
        Me.LayoutControlItem61.Name = "LayoutControlItem61"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(73, 26)
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem61.TextVisible = False
        '
        'LayoutControlItem78
        '
        Me.LayoutControlItem78.Control = Me.txtHn
        Me.LayoutControlItem78.Location = New System.Drawing.Point(153, 30)
        Me.LayoutControlItem78.Name = "LayoutControlItem78"
        Me.LayoutControlItem78.Size = New System.Drawing.Size(300, 30)
        Me.LayoutControlItem78.Text = "HN"
        Me.LayoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem78.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem78.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.PictureEdit1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(153, 134)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem79
        '
        Me.LayoutControlItem79.Control = Me.txtName
        Me.LayoutControlItem79.Location = New System.Drawing.Point(153, 60)
        Me.LayoutControlItem79.Name = "LayoutControlItem79"
        Me.LayoutControlItem79.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem79.Text = "ชื่อ - นามสกุล"
        Me.LayoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem79.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem79.TextToControlDistance = 5
        '
        'LayoutControlItem80
        '
        Me.LayoutControlItem80.Control = Me.txtCompanyName
        Me.LayoutControlItem80.Location = New System.Drawing.Point(153, 0)
        Me.LayoutControlItem80.Name = "LayoutControlItem80"
        Me.LayoutControlItem80.Size = New System.Drawing.Size(300, 30)
        Me.LayoutControlItem80.Text = "หัวข้อ/บริษัท"
        Me.LayoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem80.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem80.TextToControlDistance = 5
        '
        'LayoutControlItem81
        '
        Me.LayoutControlItem81.Control = Me.txtDateTest
        Me.LayoutControlItem81.Location = New System.Drawing.Point(823, 0)
        Me.LayoutControlItem81.Name = "LayoutControlItem81"
        Me.LayoutControlItem81.Size = New System.Drawing.Size(249, 30)
        Me.LayoutControlItem81.Text = "วันที่ตรวจ"
        Me.LayoutControlItem81.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem81.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem81.TextToControlDistance = 5
        '
        'LayoutControlItem82
        '
        Me.LayoutControlItem82.Control = Me.txtMassex
        Me.LayoutControlItem82.Location = New System.Drawing.Point(786, 30)
        Me.LayoutControlItem82.Name = "LayoutControlItem82"
        Me.LayoutControlItem82.Size = New System.Drawing.Size(286, 30)
        Me.LayoutControlItem82.Text = "เพศ"
        Me.LayoutControlItem82.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem82.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem82.TextToControlDistance = 5
        '
        'LayoutControlItem83
        '
        Me.LayoutControlItem83.Control = Me.txtTelephone
        Me.LayoutControlItem83.Location = New System.Drawing.Point(786, 60)
        Me.LayoutControlItem83.Name = "LayoutControlItem83"
        Me.LayoutControlItem83.Size = New System.Drawing.Size(286, 24)
        Me.LayoutControlItem83.Text = "เบอร์โทรศัพท์"
        Me.LayoutControlItem83.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem83.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem83.TextToControlDistance = 5
        '
        'LayoutControlItem84
        '
        Me.LayoutControlItem84.Control = Me.txtDoctor
        Me.LayoutControlItem84.Location = New System.Drawing.Point(453, 0)
        Me.LayoutControlItem84.Name = "LayoutControlItem84"
        Me.LayoutControlItem84.Size = New System.Drawing.Size(216, 30)
        Me.LayoutControlItem84.Text = "แพทย์ผู้ตรวจ"
        Me.LayoutControlItem84.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem84.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem84.TextToControlDistance = 5
        '
        'LayoutControlItem85
        '
        Me.LayoutControlItem85.Control = Me.txtAddress
        Me.LayoutControlItem85.Location = New System.Drawing.Point(153, 84)
        Me.LayoutControlItem85.Name = "LayoutControlItem85"
        Me.LayoutControlItem85.Size = New System.Drawing.Size(919, 24)
        Me.LayoutControlItem85.Text = "ที่อยูุ่ ( Address )"
        Me.LayoutControlItem85.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem85.TextSize = New System.Drawing.Size(70, 13)
        Me.LayoutControlItem85.TextToControlDistance = 5
        '
        'LayoutControlItem86
        '
        Me.LayoutControlItem86.Control = Me.txtVn
        Me.LayoutControlItem86.Location = New System.Drawing.Point(453, 30)
        Me.LayoutControlItem86.Name = "LayoutControlItem86"
        Me.LayoutControlItem86.Size = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem86.Text = "VisitID"
        Me.LayoutControlItem86.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem86.TextSize = New System.Drawing.Size(70, 20)
        Me.LayoutControlItem86.TextToControlDistance = 5
        '
        'LayoutControlItem87
        '
        Me.LayoutControlItem87.Control = Me.txtAge
        Me.LayoutControlItem87.Location = New System.Drawing.Point(526, 60)
        Me.LayoutControlItem87.Name = "LayoutControlItem87"
        Me.LayoutControlItem87.Size = New System.Drawing.Size(260, 24)
        Me.LayoutControlItem87.Text = "อายุ"
        Me.LayoutControlItem87.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem87.TextSize = New System.Drawing.Size(50, 20)
        Me.LayoutControlItem87.TextToControlDistance = 5
        '
        'LayoutControlItem88
        '
        Me.LayoutControlItem88.Control = Me.SimpleButton4
        Me.LayoutControlItem88.Location = New System.Drawing.Point(740, 108)
        Me.LayoutControlItem88.Name = "LayoutControlItem88"
        Me.LayoutControlItem88.Size = New System.Drawing.Size(88, 26)
        Me.LayoutControlItem88.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem88.TextVisible = False
        '
        'LayoutControlItem107
        '
        Me.LayoutControlItem107.Control = Me.checkEdit
        Me.LayoutControlItem107.Location = New System.Drawing.Point(153, 108)
        Me.LayoutControlItem107.Name = "LayoutControlItem107"
        Me.LayoutControlItem107.Size = New System.Drawing.Size(165, 26)
        Me.LayoutControlItem107.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem107.TextVisible = False
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(318, 108)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(422, 26)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem110
        '
        Me.LayoutControlItem110.Control = Me.SimpleButton5
        Me.LayoutControlItem110.Location = New System.Drawing.Point(989, 108)
        Me.LayoutControlItem110.Name = "LayoutControlItem110"
        Me.LayoutControlItem110.Size = New System.Drawing.Size(83, 26)
        Me.LayoutControlItem110.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem110.TextVisible = False
        '
        'LayoutControlItem111
        '
        Me.LayoutControlItem111.Control = Me.txtLicense
        Me.LayoutControlItem111.Location = New System.Drawing.Point(669, 0)
        Me.LayoutControlItem111.Name = "LayoutControlItem111"
        Me.LayoutControlItem111.Size = New System.Drawing.Size(154, 30)
        Me.LayoutControlItem111.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem111.TextVisible = False
        '
        'GridView1
        '
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'frmCheckUp01
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1080, 564)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmCheckUp01"
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        Me.LayoutControl1.PerformLayout()
        CType(Me.txtLicense.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkglasses_without.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkglasses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkBlood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExamination2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExamination.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkChemicallab2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkChemicalLab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkUrineAnalysis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfrnCheckUpFitness_Result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFrncheckupResult_all.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_food.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_breathing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_crane.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_emergency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_aircrew.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_temporary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_unit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_specific.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtfit_offshore.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDoctor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit9View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelephone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMassex.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit8View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDateTest.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDateTest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCompanyName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit7View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCEAResult_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCEAResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtvirusa_havigm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtvirusa_havigg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusAResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusb_hbsab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusb_hbcab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusb_hbsag.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVirusBResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVDRLresult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVDRLResult_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHivResult_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHivResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtXrayResult_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtXrayResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.checkUpPhysical_result, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MascheckupphysicalresultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineResult_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineresult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtUrineresultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineAmphetamine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineOther.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineEpi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineRBC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineBlood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineGlucose.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineAppearance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineWBC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrinepH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineSPGR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineAlbumin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUrineColor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtblood_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtblood.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBloodBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlateCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRBC_Morphology.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAtypical_lymph.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBashophils.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonocytes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEosionophils.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLymphocytes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNeutrophils.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWBC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHgb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBloodRh.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBloodgroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEyeLR_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txteyeL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTooth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtToothBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTooth_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txteye_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlindness_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEyeLR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEyeLRBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEyeR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEye.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEyeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBlindness.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBlindnessBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEyeResult.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBloodPressure1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBloodPressure.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHeight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBmi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DtCheckUP As dtCheckUP
    Friend WithEvents TxtEyeBindingSource As BindingSource
    Friend WithEvents TxtToothBindingSource As BindingSource
    Friend WithEvents TxtBlindnessBindingSource As BindingSource
    Friend WithEvents TxtEyeLRBindingSource As BindingSource
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtEyeLR_result As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txteyeL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTooth As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit4View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtTooth_result As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txteye_result As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBlindness_result As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtEyeLR As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtEyeR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEye As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colidmascheckup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldet As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colform As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtBlindness As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtEyeResult As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBloodPressure1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBloodPressure As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtHeight As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBmi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWeight As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtBloodgroup As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtHct As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtHgb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBloodRh As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtLymphocytes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNeutrophils As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWBC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtAtypical_lymph As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBashophils As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMonocytes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEosionophils As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtblood_result As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtblood As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtPlateCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRBC_Morphology As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtUrineResult_remark As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtUrineresult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtUrineAmphetamine As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineOther As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineEpi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineRBC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineBlood As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineGlucose As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineAppearance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineWBC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrinepH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineSPGR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineAlbumin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUrineColor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DtCheckUP1 As dtCheckUP
    Friend WithEvents colidmascheckup_physical As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidvalue As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents checkUpPhysical_result As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents RepositoryItemSearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents MascheckupphysicalresultBindingSource As BindingSource
    Friend WithEvents DtCheckUP2 As dtCheckUP
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DtCheckUP3 As dtCheckUP
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtHivResult_remark As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtHivResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtXrayResult_remark As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtXrayResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtVDRLresult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtVDRLResult_remark As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtVirusb_hbsab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtVirusb_hbcab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtVirusb_hbsag As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtVirusBResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit5View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtvirusa_havigm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtvirusa_havigg As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtVirusAResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit6View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup14 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem76 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem77 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtCEAResult_remark As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtCEAResult As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup16 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtBloodBindingSource As BindingSource
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TxtUrineresultBindingSource As BindingSource
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents coldescription1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colvalue As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colunit As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colnormal As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colhn As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colan As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colvn As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colresult As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colresult2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents txtCompanyName As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit7View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtHn As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem78 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem79 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem80 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDoctor As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit9View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtTelephone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMassex As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit8View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtDateTest As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem81 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem82 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem83 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem84 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem85 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtVn As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem86 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtAge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem87 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem88 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents DtCheckUP4 As dtCheckUP
    Friend WithEvents colsex As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsexdesc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtfit_breathing As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_crane As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_emergency As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_aircrew As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_temporary As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_unit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_specific As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfit_offshore As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem89 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem90 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem91 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem92 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem93 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem94 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem95 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem96 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtfit_food As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem97 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents checkBlood As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckExamination2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckExamination As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkChemicallab2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkChemicalLab As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents checkUrineAnalysis As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtfrnCheckUpFitness_Result As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtFrncheckupResult_all As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem98 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem99 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem101 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem102 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem103 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem100 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem104 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem105 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource2 As BindingSource
    Friend WithEvents DtCheckUP5 As dtCheckUP
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BindingSource3 As BindingSource
    Friend WithEvents DtCheckUP6 As dtCheckUP
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BindingSource4 As BindingSource
    Friend WithEvents DtCheckUP7 As dtCheckUP
    Friend WithEvents BindingSource5 As BindingSource
    Friend WithEvents DtCheckUP8 As dtCheckUP
    Friend WithEvents BindingSource6 As BindingSource
    Friend WithEvents DtCheckUP9 As dtCheckUP
    Friend WithEvents BindingSource7 As BindingSource
    Friend WithEvents DtCheckUP10 As dtCheckUP
    Friend WithEvents GridColumn49 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn50 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn51 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn52 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn53 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn54 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn43 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn44 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn45 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn46 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn47 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn48 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn55 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn56 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn57 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn58 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn59 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn60 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn61 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn62 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn63 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn64 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn65 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn66 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BindingSource8 As BindingSource
    Friend WithEvents DtCheckUP11 As dtCheckUP
    Friend WithEvents btnResult As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem106 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents checkEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem107 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BindingSource9 As BindingSource
    Friend WithEvents DtCheckUP12 As dtCheckUP
    Friend WithEvents chkglasses_without As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkglasses As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem109 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem108 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem110 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtLicense As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem111 As DevExpress.XtraLayout.LayoutControlItem
End Class
