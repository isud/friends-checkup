﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCheckUp06

    Dim dtset As New dtCheckUP
    Dim checkupClass As checkupClass
    Dim frmcheckUp06 As frmCheckUp06Class
    Dim mascheckup As frmCheckUp06Class.mascheckup
    Private Sub frmCheckUp05_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadMasterToGrid()
    End Sub
    Public Sub loadMasterToGrid()
        frmcheckUp06 = New frmCheckUp06Class(dtset)
        frmcheckUp06.getMascheckup()
        GridControl1.DataSource = dtset


        frmcheckUp06.getMasSetForm()
        chkForm.Properties.DataSource = dtset.Tables("mastext")


    End Sub
    Public Sub ClearStruct()
        mascheckup = New frmCheckUp06Class.mascheckup

    End Sub
    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        ClearStruct()
        frmcheckUp06.getMascheckupDt(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckup"))
        getDefaultCompany()
    End Sub

    Public Sub getDefaultCompany()
        If dtset.Tables("mascheck").Rows.Count > 0 Then


            mascheckup.idmascheckup = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckup")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckup"))

            mascheckup.idmascheckupdt = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckupdt")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckupdt"))


            txtname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name")), Nothing, dtset.Tables("mascheck").Rows(0)("name"))
            txtname2.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name2")), Nothing, dtset.Tables("mascheck").Rows(0)("name2"))
            txtdescriptions.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("description")), Nothing, dtset.Tables("mascheck").Rows(0)("description"))
            txtformcode.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formcode")), Nothing, dtset.Tables("mascheck").Rows(0)("formcode"))
            txtformname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formname")), Nothing, dtset.Tables("mascheck").Rows(0)("formname"))
            chkForm.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("form")), Nothing, dtset.Tables("mascheck").Rows(0)("form"))

            txtname_dt.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name_dt")), Nothing, dtset.Tables("mascheck").Rows(0)("name_dt"))
            txtname2_dt.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name2_dt")), Nothing, dtset.Tables("mascheck").Rows(0)("name2_dt"))
            chk_result_all.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("f_result_all")), False, dtset.Tables("mascheck").Rows(0)("f_result_all"))
            chkstatus.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("status")), False, dtset.Tables("mascheck").Rows(0)("status"))

        End If

    End Sub
    Public Sub ClearDesign()
        ClearStruct()
        txtname.Text = Nothing
        txtname2.Text = Nothing
        txtdescriptions.Text = Nothing
        txtformcode.Text = Nothing
        txtformname.Text = Nothing
        chkForm.EditValue = Nothing
        txtname_dt.Text = Nothing
        txtname2_dt.Text = Nothing
        chkstatus.EditValue = True
        chk_result_all.EditValue = False

    End Sub
    Public Sub setmascheckup()

        mascheckup.name = txtname.EditValue
        mascheckup.name2 = txtname2.EditValue
        mascheckup.description = txtdescriptions.EditValue
        mascheckup.formcode = txtformcode.EditValue
        mascheckup.formname = txtformname.EditValue
        mascheckup.form = chkForm.EditValue
        mascheckup.name_dt = txtname_dt.Text
        mascheckup.name2_dt = txtname2_dt.Text
        mascheckup.status = chkstatus.EditValue
        mascheckup.f_result_all = chk_result_all.EditValue
    End Sub
    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        ClearDesign()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If chkForm.EditValue Is Nothing Then
            MsgBox("กรุณา ระบุ Form")
            Exit Sub
        End If
        setmascheckup()
        frmcheckUp06.setmascheckup = mascheckup

        If mascheckup.idmascheckup Is Nothing Then
            frmcheckUp06.Insertmascheckup()
        Else
            ' frmcheckUp06.Updatemascheckup_physical()
        End If
        loadMasterToGrid()
        ClearDesign()



    End Sub
End Class