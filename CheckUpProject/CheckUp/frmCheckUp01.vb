﻿Imports System
Imports System.Windows.Forms
Imports DevExpress.Data
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraLayout
Public Class frmCheckUp01
    Dim rptCheckUpA5_1 As New rptCheckUpA5_1
    Dim dtset As New dtCheckUP
    Dim checkUpClass As checkupClass
    Public Shared frncheckUp As checkupClass.frnCheckup
    Dim frncheckUpBlood As checkupClass.frnCheckUpBlood
    Dim frncheckUpUrine As checkupClass.frnCheckUpUrine

    Public Sub getValueByvn()
        checkUpClass.getCBCCode()

        setToDesignFormBlood()
        checkUpClass.getUrine()

        setToDesingFormUrine()
        checkUpClass.getmascheckup_physical_result()
        checkUpPhysical_result.DataSource = dtset.Tables("mascheckup_physical_result")

        txtDateTest.EditValue = frncheckUp.datetest
        checkUpClass.getMasPhysicalExam()
        GridControl1.DataSource = dtset
        checkUpClass.getOrtherLab()
        GridControl2.DataSource = dtset
        setValueOther()
    End Sub
    Public Sub getValueByOld()
        checkUpClass.getmascheckup_physical_result()
        checkUpPhysical_result.DataSource = dtset.Tables("mascheckup_physical_result")

        checkUpClass.getFrnchekup_other()
        GridControl2.DataSource = dtset
        checkUpClass.getFrnCheckup()
        setToDesign()

        checkUpClass.getFrncheckup_physicalExam()
        GridControl1.DataSource = dtset

        checkUpClass.getfrncheckupblood()
        setToDesingFromBloodOld()
        setToDesingFromUrine()
    End Sub
    Public Sub setValueOther()
        txtVirusb_hbsag.EditValue = checkUpClass.getMasCheckUpValue(txtVirusb_hbsag.Name)
        txtVirusb_hbsab.EditValue = checkUpClass.getMasCheckUpValue(txtVirusb_hbsab.Name)
        txtVirusb_hbcab.EditValue = checkUpClass.getMasCheckUpValue(txtVirusb_hbcab.Name)

        txtvirusa_havigg.EditValue = checkUpClass.getMasCheckUpValue(txtvirusa_havigg.Name)
        txtvirusa_havigm.EditValue = checkUpClass.getMasCheckUpValue(txtvirusa_havigm.Name)
    End Sub
    Public Sub SetValue()
        rptCheckUpA5_1.Parameters("txtHeight").Value = txtHeight.EditValue
        rptCheckUpA5_1.Parameters("txtWeight").Value = txtWeight.EditValue

        rptCheckUpA5_1.CreateDocument()
    End Sub
    Public Sub loadDataSetup()
        checkUpClass = New checkupClass(dtset)
        checkUpClass.getData(txtEye.Name)
        txtEye.Properties.DataSource = dtset.Tables(txtEye.Name)

        checkUpClass.getData(txtTooth.Name)
        txtTooth.Properties.DataSource = dtset.Tables(txtTooth.Name)

        checkUpClass.getData(txtBlindness.Name)
        txtBlindness.Properties.DataSource = dtset.Tables(txtBlindness.Name)


        checkUpClass.getData(txtEyeLR.Name)
        txtEyeLR.Properties.DataSource = dtset.Tables(txtEyeLR.Name)
        checkUpClass.getData(txtblood.Name)
        txtblood.Properties.DataSource = dtset.Tables(txtblood.Name)
        checkUpClass.getData(txtUrineresult.Name)
        txtUrineresult.Properties.DataSource = dtset.Tables(txtUrineresult.Name)

        checkUpClass.getData(txtVDRLresult.Name)
        txtVDRLresult.Properties.DataSource = dtset.Tables(txtVDRLresult.Name)

        checkUpClass.getData(txtCEAResult.Name)
        txtCEAResult.Properties.DataSource = dtset.Tables(txtCEAResult.Name)


        checkUpClass.getData(txtXrayResult.Name)
        txtXrayResult.Properties.DataSource = dtset.Tables(txtXrayResult.Name)

        checkUpClass.getData(txtHivResult.Name)
        txtHivResult.Properties.DataSource = dtset.Tables(txtHivResult.Name)

        checkUpClass.getData(txtVirusBResult.Name)
        txtVirusBResult.Properties.DataSource = dtset.Tables(txtVirusBResult.Name)


        checkUpClass.getData(txtVirusAResult.Name)
        txtVirusAResult.Properties.DataSource = dtset.Tables(txtVirusAResult.Name)




        checkUpClass.getMassex()
        txtMassex.Properties.DataSource = dtset.Tables("massex")

        checkUpClass.getMashospemp()
        txtDoctor.Properties.DataSource = dtset.Tables("doctor")

        checkUpClass.getmascheckup_project()
        txtCompanyName.Properties.DataSource = dtset.Tables("mascheckup_pro")


    End Sub
    Public Sub bmi_cal()
        Dim height As Double
        Dim weight As Double
        Dim result As Double



        If txtHeight.EditValue Is Nothing Or txtWeight.EditValue Is Nothing Then

        Else

            height = txtHeight.Text.Trim
            weight = txtWeight.Text.Trim
            height = height / 100
            weight = weight / height
            weight = weight / height

            txtBmi.EditValue = Format(weight, "##.##")
        End If
    End Sub

    Private Sub txtHeight_KeyDown(sender As Object, e As KeyEventArgs) Handles txtHeight.KeyDown, txtWeight.KeyDown
        If e.KeyCode = Keys.Enter Then
            bmi_cal()
        End If
    End Sub

    Private Sub frmCheckUp01_Load(sender As Object, e As EventArgs) Handles Me.Load
        loadDataSetup()
    End Sub
    Private Sub txtUrineresult_EditValueChanged(sender As Object, e As EventArgs) Handles txtUrineresult.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtUrineresult.EditValue Is Nothing Then

            Else
                checkUpClass.getDataTotext(txtUrineresult, txtUrineResult_remark)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtUrineresult.EditValue Is Nothing Then

                Else
                    checkUpClass.getDataTotext(txtUrineresult, txtUrineResult_remark)

                End If
            End If

        End If

    End Sub

    Private Sub txtVDRLresult_EditValueChanged(sender As Object, e As EventArgs) Handles txtVDRLresult.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtVDRLresult.EditValue Is Nothing Then
            Else
                checkUpClass.getDataTotext(txtVDRLresult, txtVDRLResult_remark)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtVDRLresult.EditValue Is Nothing Then
                Else
                    checkUpClass.getDataTotext(txtVDRLresult, txtVDRLResult_remark)

                End If
            End If
        End If

    End Sub
    Private Sub txtCEAResult_EditValueChanged(sender As Object, e As EventArgs) Handles txtCEAResult.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtCEAResult.EditValue Is Nothing Then
            Else
                checkUpClass.getDataTotext(txtCEAResult, txtCEAResult_remark)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtCEAResult.EditValue Is Nothing Then
                Else
                    checkUpClass.getDataTotext(txtCEAResult, txtCEAResult_remark)

                End If
            End If
        End If
    End Sub

    Private Sub txtXrayResult_EditValueChanged(sender As Object, e As EventArgs) Handles txtXrayResult.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtXrayResult.EditValue Is Nothing Then

            Else
                checkUpClass.getDataTotext(txtXrayResult, txtXrayResult_remark)


            End If
        Else
            If checkEdit.EditValue = True Then
                If txtXrayResult.EditValue Is Nothing Then

                Else
                    checkUpClass.getDataTotext(txtXrayResult, txtXrayResult_remark)


                End If
            End If
        End If
    End Sub


    Private Sub txtHivResult_EditValueChanged(sender As Object, e As EventArgs) Handles txtHivResult.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtHivResult.EditValue Is Nothing Then
            Else

                checkUpClass.getDataTotext(txtHivResult, txtHivResult_remark)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtHivResult.EditValue Is Nothing Then
                Else

                    checkUpClass.getDataTotext(txtHivResult, txtHivResult_remark)

                End If
            End If
        End If
    End Sub


    Private Sub txtblood_EditValueChanged(sender As Object, e As EventArgs) Handles txtblood.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtblood.EditValue Is Nothing Then

            Else

                checkUpClass.getDataTotext(txtblood, txtblood_result)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtblood.EditValue Is Nothing Then

                Else

                    checkUpClass.getDataTotext(txtblood, txtblood_result)

                End If
            End If
        End If


    End Sub

    Private Sub txtEye_Properties_EditValueChanged(sender As Object, e As EventArgs) Handles txtEye.Properties.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtEye.EditValue Is Nothing Then

            Else

                checkUpClass.getDataTotext(txtEye, txteye_result)

            End If
        Else
            If checkEdit.EditValue = True Then

                If txtEye.EditValue Is Nothing Then

                Else

                    checkUpClass.getDataTotext(txtEye, txteye_result)

                End If
            End If
        End If


    End Sub

    Private Sub txtBlindness_EditValueChanged(sender As Object, e As EventArgs) Handles txtBlindness.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtBlindness.EditValue Is Nothing Then

            Else
                checkUpClass.getDataTotext(txtBlindness, txtBlindness_result)


            End If
        Else
            If checkEdit.EditValue = True Then
                If txtBlindness.EditValue Is Nothing Then

                Else
                    checkUpClass.getDataTotext(txtBlindness, txtBlindness_result)


                End If
            End If
        End If


    End Sub

    Private Sub txtTooth_EditValueChanged(sender As Object, e As EventArgs) Handles txtTooth.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtTooth.EditValue Is Nothing Then

            Else
                checkUpClass.getDataTotext(txtTooth, txtTooth_result)

            End If

        Else
            If checkEdit.EditValue = True Then
                If txtTooth.EditValue Is Nothing Then

                Else
                    checkUpClass.getDataTotext(txtTooth, txtTooth_result)

                End If
            End If
        End If



    End Sub

    Private Sub txtEyeLR_EditValueChanged(sender As Object, e As EventArgs) Handles txtEyeLR.EditValueChanged
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then
            If txtEyeLR.EditValue Is Nothing Then

            Else
                checkUpClass.getDataTotext(txtEyeLR, txtEyeLR_result)

            End If
        Else
            If checkEdit.EditValue = True Then
                If txtEyeLR.EditValue Is Nothing Then

                Else
                    checkUpClass.getDataTotext(txtEyeLR, txtEyeLR_result)

                End If



            End If
            End If


    End Sub
    Public Sub ClearDesign()

        txtVn.EditValue = Nothing
        txtWeight.EditValue = Nothing
        txtHeight.EditValue = Nothing
        txtBmi.EditValue = Nothing
        txtBloodPressure.EditValue = Nothing
        txtBloodPressure1.EditValue = Nothing
        txtEyeResult.EditValue = Nothing
        txteyeL.EditValue = Nothing
        txtEyeR.EditValue = Nothing
        txtEyeLR.EditValue = Nothing
        txtEyeLR_result.EditValue = Nothing
        txtBlindness.EditValue = Nothing
        txtBlindness_result.EditValue = Nothing
        txtEye.EditValue = Nothing
        txteye_result.EditValue = Nothing
        txtTooth.EditValue = Nothing
        txtTooth_result.EditValue = Nothing
        txtDoctor.EditValue = Nothing

        txtfit_offshore.EditValue = Nothing
        txtfit_specific.EditValue = Nothing
        txtfit_aircrew.EditValue = Nothing
        txtfit_breathing.EditValue = Nothing
        txtfit_crane.EditValue = Nothing
        txtfit_emergency.EditValue = Nothing
        txtfit_food.EditValue = Nothing
        txtfit_unit.EditValue = Nothing
        txtfit_temporary.EditValue = Nothing





        txtVDRLresult.EditValue = Nothing
        txtVDRLResult_remark.EditValue = Nothing
        txtCEAResult.EditValue = Nothing
        txtCEAResult_remark.EditValue = Nothing
        txtXrayResult.EditValue = Nothing
        txtXrayResult_remark.EditValue = Nothing
        txtHivResult.EditValue = Nothing
        txtHivResult_remark.EditValue = Nothing

    End Sub
    Public Sub setToDesign()


        txtCompanyName.EditValue = dtset.Tables("frncheckup").Rows(0)("idmascheckup_project").ToString

       txtDoctor.EditValue = dtset.Tables("frncheckup").Rows(0)("empdoctor_id").ToString

        txtVn.EditValue = dtset.Tables("frncheckup").Rows(0)("vn").ToString
        txtWeight.EditValue = dtset.Tables("frncheckup").Rows(0)("weight").ToString
        txtHeight.EditValue = dtset.Tables("frncheckup").Rows(0)("height").ToString
        txtBmi.EditValue = dtset.Tables("frncheckup").Rows(0)("bmi").ToString
        txtBloodPressure.EditValue = dtset.Tables("frncheckup").Rows(0)("pulserate").ToString
        txtBloodPressure1.EditValue = dtset.Tables("frncheckup").Rows(0)("bloodpressure").ToString
        txtEyeResult.EditValue = dtset.Tables("frncheckup").Rows(0)("eyeresult").ToString
        txteyeL.EditValue = dtset.Tables("frncheckup").Rows(0)("eyeL").ToString
        txtEyeR.EditValue = dtset.Tables("frncheckup").Rows(0)("eyeR").ToString
        txtEyeLR.EditValue = If(dtset.Tables("frncheckup").Rows(0)("eyeLR") = 0, Nothing, dtset.Tables("frncheckup").Rows(0)("eyeLR").ToString)
        txtEyeLR_result.EditValue = dtset.Tables("frncheckup").Rows(0)("eyeLR_result").ToString
        txtBlindness.EditValue = If(dtset.Tables("frncheckup").Rows(0)("blindness") = 0, Nothing, dtset.Tables("frncheckup").Rows(0)("blindness").ToString)
        txtBlindness_result.EditValue = dtset.Tables("frncheckup").Rows(0)("blindness_result").ToString
        txtEye.EditValue = If(dtset.Tables("frncheckup").Rows(0)("eye") = 0, Nothing, dtset.Tables("frncheckup").Rows(0)("eye").ToString)
        txteye_result.EditValue = dtset.Tables("frncheckup").Rows(0)("eye_result").ToString
        txtTooth.EditValue = If(dtset.Tables("frncheckup").Rows(0)("tooth") = 0, Nothing, dtset.Tables("frncheckup").Rows(0)("tooth").ToString)
        txtTooth_result.EditValue = dtset.Tables("frncheckup").Rows(0)("tooth_result").ToString

        txtLicense.EditValue = dtset.Tables("frncheckup").Rows(0)("doctor_license")

        txtfit_offshore.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_offshore")
        txtfit_specific.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_specific")
        txtfit_aircrew.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_aircrew")
        txtfit_breathing.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_breathing")
        txtfit_crane.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_crane")
        txtfit_emergency.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_emergency")
        txtfit_food.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_food")
        txtfit_unit.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_unit")
        txtfit_temporary.EditValue = dtset.Tables("frncheckup").Rows(0)("fit_temporary")





        txtVDRLresult.EditValue = dtset.Tables("frncheckup").Rows(0)("vdrlresult").ToString
        txtVDRLResult_remark.EditValue = dtset.Tables("frncheckup").Rows(0)("vdrlresult_remark").ToString
        txtCEAResult.EditValue = dtset.Tables("frncheckup").Rows(0)("cearesult").ToString
        txtCEAResult_remark.EditValue = dtset.Tables("frncheckup").Rows(0)("cearesult_remark").ToString
        txtXrayResult.EditValue = dtset.Tables("frncheckup").Rows(0)("xrayresult").ToString
        txtXrayResult_remark.EditValue = dtset.Tables("frncheckup").Rows(0)("xrayresult_remark").ToString
        txtHivResult.EditValue = dtset.Tables("frncheckup").Rows(0)("hivresult").ToString
        txtHivResult_remark.EditValue = dtset.Tables("frncheckup").Rows(0)("hivresult_remark").ToString
        txtFrncheckupResult_all.EditValue = dtset.Tables("frncheckup").Rows(0)("ResultAll").ToString
        txtfrnCheckUpFitness_Result.EditValue = dtset.Tables("frncheckup").Rows(0)("Fitness_ResultAll").ToString



    End Sub

    Public Sub setToDesignBloodRh()
        If dtset.Tables("prdorderlabresult").Rows.Count - 1 Then
            If txtBloodRh.Name = dtset.Tables("prdorderlabresult").Rows(0)("form").ToString Then
                txtBloodRh.EditValue = dtset.Tables("prdorderlabresult").Rows(0)("value").ToString
            End If
        End If

    End Sub
    Public Sub setToDesignBlood()
        If dtset.Tables("prdorderlabresult").Rows.Count - 1 Then
            If txtBloodgroup.Name = dtset.Tables("prdorderlabresult").Rows(0)("form").ToString Then
                txtBloodgroup.EditValue = dtset.Tables("prdorderlabresult").Rows(0)("value").ToString
            End If
        End If


    End Sub
    Public Sub setToDesingFormUrine()

        For i As Integer = 0 To dtset.Tables("prdorderlabresult").Rows.Count - 1
            If txtUrineColor.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then

                txtUrineColor.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString
                Continue For
            End If
            If txtUrineAlbumin.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineAlbumin.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineSPGR.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineSPGR.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrinepH.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrinepH.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If


            If txtUrineWBC.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineWBC.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineAppearance.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineAppearance.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineGlucose.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineGlucose.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineBlood.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineBlood.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineRBC.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineRBC.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineEpi.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineEpi.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineOther.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineOther.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtUrineAmphetamine.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtUrineAmphetamine.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
        Next

    End Sub
    Public Sub cleartDesingFromBloodOld()
        txtBloodgroup.EditValue = Nothing
        txtBloodRh.EditValue = Nothing
        txtHgb.EditValue = Nothing
        txtHct.EditValue = Nothing
        txtWBC.EditValue = Nothing
        txtNeutrophils.EditValue = Nothing
        txtLymphocytes.EditValue = Nothing
        txtEosionophils.EditValue = Nothing
        txtMonocytes.EditValue = Nothing
        txtBashophils.EditValue = Nothing
        txtAtypical_lymph.EditValue = Nothing
        txtRBC_Morphology.EditValue = Nothing
        txtblood.EditValue = Nothing
        txtblood_result.EditValue = Nothing
    End Sub
    Public Sub setToDesingFromBloodOld()


        txtBloodgroup.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtBloodgroup.Tag).ToString
        txtBloodRh.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtBloodRh.Tag).ToString
        txtHgb.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtHgb.Tag).ToString
        txtHct.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtHct.Tag).ToString
        txtWBC.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtWBC.Tag).ToString
        txtNeutrophils.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtNeutrophils.Tag).ToString
        txtLymphocytes.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtLymphocytes.Tag).ToString
        txtEosionophils.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtEosionophils.Tag).ToString
        txtMonocytes.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtMonocytes.Tag).ToString
        txtBashophils.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtBashophils.Tag).ToString
        txtAtypical_lymph.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtAtypical_lymph.Tag).ToString
        txtRBC_Morphology.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtRBC_Morphology.Tag).ToString


        txtblood.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtblood.Tag).ToString


        txtblood_result.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtblood_result.Tag).ToString
    End Sub


    Public Sub ClearDesingFromUrine()
        txtUrineAlbumin.EditValue = Nothing
        txtUrineAmphetamine.EditValue = Nothing
        txtUrineAppearance.EditValue = Nothing
        txtUrineBlood.EditValue = Nothing
        txtUrineColor.EditValue = Nothing
        txtUrineEpi.EditValue = Nothing

        txtUrineGlucose.EditValue = Nothing

        txtUrineOther.EditValue = Nothing


        txtUrinepH.EditValue = Nothing



        txtUrineRBC.EditValue = Nothing



        txtUrineresult.EditValue = Nothing


        txtUrineResult_remark.EditValue = Nothing
    End Sub
    Public Sub setToDesingFromUrine()
        txtUrineAlbumin.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineAlbumin.Tag).ToString
        txtUrineAmphetamine.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineAmphetamine.Tag).ToString
        txtUrineAppearance.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineAppearance.Tag).ToString
        txtUrineBlood.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineBlood.Tag).ToString
        txtUrineColor.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineColor.Tag).ToString
        txtUrineEpi.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineEpi.Tag).ToString

        txtUrineGlucose.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineGlucose.Tag).ToString

        txtUrineOther.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineOther.Tag).ToString


        txtUrinepH.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrinepH.Tag).ToString



        txtUrineRBC.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineRBC.Tag).ToString



        txtUrineresult.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineresult.Tag).ToString


        txtUrineResult_remark.EditValue = dtset.Tables("frncheckupblood").Rows(0)(txtUrineResult_remark.Tag).ToString
    End Sub



    Public Sub ClearDesignFormBlood()

        txtHgb.EditValue = Nothing
        txtHct.EditValue = Nothing

        txtWBC.EditValue = Nothing


        txtNeutrophils.EditValue = Nothing

        txtEosionophils.EditValue = Nothing


        txtBashophils.EditValue = Nothing


        txtLymphocytes.EditValue = Nothing


        txtMonocytes.EditValue = Nothing

        txtRBC_Morphology.EditValue = Nothing


        txtPlateCount.EditValue = Nothing


    End Sub



    Public Sub setToDesignFormBlood()
        For i As Integer = 0 To dtset.Tables("prdorderlabresult").Rows.Count - 1
            If txtHgb.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then

                txtHgb.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString
                Continue For
            End If
            If txtHct.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtHct.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtWBC.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtWBC.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtNeutrophils.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtNeutrophils.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtEosionophils.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtEosionophils.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtBashophils.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtBashophils.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtLymphocytes.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtLymphocytes.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtMonocytes.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtMonocytes.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtRBC_Morphology.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtRBC_Morphology.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
            If txtPlateCount.Name = dtset.Tables("prdorderlabresult").Rows(i)("form").ToString Then
                txtPlateCount.EditValue = dtset.Tables("prdorderlabresult").Rows(i)("value").ToString

                Continue For

            End If
        Next
    End Sub
    Public Sub setClearDesign()

        dtset = New dtCheckUP
        checkUpClass = New checkupClass(dtset)

        frncheckUp = New checkupClass.frnCheckup
        frncheckUpBlood = New checkupClass.frnCheckUpBlood
        frncheckUpUrine = New checkupClass.frnCheckUpUrine
        cleartDesingFromBloodOld()
        ClearDesign()
        ClearDesignFormBlood()
        ClearDesingFromUrine()
        If txtCompanyName.EditValue Is Nothing Then

        Else
            checkUpClass.getmascheckup_project_setting(txtCompanyName.EditValue)
            getDefaultCompany()

        End If
    End Sub
    Public Sub getFrnCheckUp()
        txtName.EditValue = frncheckUp.stprename + " " + frncheckUp.name & "  " & frncheckUp.lastname
        txtAddress.EditValue = frncheckUp.address
        txtAge.EditValue = frncheckUp.age
        txtTelephone.EditValue = frncheckUp.tellephone
        txtMassex.EditValue = frncheckUp.idsex
    End Sub
    Public Sub setFrnCheckUp()

        frncheckUp.hn = txtHn.EditValue


        frncheckUp.birthdate = Date.Now
        frncheckUp.weight = txtWeight.EditValue
        frncheckUp.height = txtHeight.EditValue
        frncheckUp.bmi = txtBmi.EditValue
        frncheckUp.pulserate = txtBloodPressure.EditValue
        frncheckUp.bloodpressure = txtBloodPressure1.EditValue
        frncheckUp.idsex = txtMassex.EditValue
        frncheckUp.sex = txtMassex.Text




        frncheckUp.eyeresult = txtEyeResult.EditValue
        frncheckUp.eyeL = txteyeL.EditValue
        frncheckUp.eyeR = txtEyeR.EditValue
        frncheckUp.eyeLR = txtEyeLR.EditValue

        frncheckUp.doctor = txtDoctor.EditValue
        frncheckUp.doctorname = txtDoctor.Text

        frncheckUp.eyeLR_result = txtEyeLR_result.EditValue
        frncheckUp.blindness = txtBlindness.EditValue
        frncheckUp.blindness_result = txtBlindness_result.EditValue
        frncheckUp.eye = txtEye.EditValue
        frncheckUp.eye_result = txteye_result.EditValue
        frncheckUp.tooth = txtTooth.EditValue
        frncheckUp.tooth_result = txtTooth_result.EditValue
        frncheckUp.doccodename = "dasdas"
        frncheckUp.address = txtAddress.EditValue
        frncheckUp.datetest = txtDateTest.EditValue

        frncheckUp.glasses = chkglasses.EditValue
        frncheckUp.glasses_without = chkglasses_without.EditValue



        frncheckUp.fit_offshore = txtfit_offshore.EditValue
        frncheckUp.fit_specific = txtfit_specific.EditValue
        frncheckUp.fit_aircrew = txtfit_aircrew.EditValue
        frncheckUp.fit_breathing = txtfit_breathing.EditValue
        frncheckUp.fit_crane = txtfit_crane.EditValue
        frncheckUp.fit_emergency = txtfit_emergency.EditValue
        frncheckUp.fit_food = txtfit_food.EditValue
        frncheckUp.fit_unit = txtfit_unit.EditValue
        frncheckUp.fit_temporary = txtfit_temporary.EditValue



        frncheckUp.vdrlresult = txtVDRLresult.EditValue
        frncheckUp.vdrlresult_remark = txtVDRLResult_remark.EditValue
        frncheckUp.cearesult = txtCEAResult.EditValue
        frncheckUp.cearesult_remark = txtCEAResult_remark.EditValue
        frncheckUp.xrayresult = txtXrayResult.EditValue
        frncheckUp.xrayresult_remark = txtXrayResult_remark.EditValue
        frncheckUp.hivresult = txtHivResult.EditValue
        frncheckUp.hivresult_remark = txtHivResult.EditValue
        frncheckUp.ResultAll = txtFrncheckupResult_all.EditValue
        frncheckUp.Fitness_ResultAll = txtfrnCheckUpFitness_Result.EditValue

        frncheckUp.doctor_license = txtLicense.EditValue
        frncheckUp.idmascheckup_project = txtCompanyName.EditValue
        frncheckUp.project_name = txtCompanyName.Text

        frncheckUp.virusb_result = txtVirusBResult.EditValue
        frncheckUp.virusb_hbsab = txtVirusb_hbsab.Text
        frncheckUp.virusa_result = txtVirusAResult.EditValue
        frncheckUp.virusb_hbcab = txtVirusb_hbcab.Text
        frncheckUp.virusa_havigg = txtvirusa_havigg.Text
        frncheckUp.virusa_havigm = txtvirusa_havigm.Text


        checkUpClass.setFrnCheckup = frncheckUp
    End Sub
    Public Sub setFrnCheckUpUrine()


        frncheckUpUrine.urine_albumin = txtUrineAlbumin.EditValue
        frncheckUpUrine.urine_amphetamine = txtUrineAmphetamine.EditValue
        frncheckUpUrine.urine_appearance = txtUrineAppearance.EditValue
        frncheckUpUrine.urine_blood = txtUrineBlood.EditValue
        frncheckUpUrine.urine_color = txtUrineColor.EditValue
        frncheckUpUrine.urine_epi = txtUrineEpi.EditValue
        frncheckUpUrine.urine_glucose = txtUrineGlucose.EditValue
        frncheckUpUrine.urine_other = txtUrineOther.EditValue
        frncheckUpUrine.urine_ph = txtUrinepH.EditValue
        frncheckUpUrine.urine_rbc = txtUrineRBC.EditValue
        frncheckUpUrine.urine_spgr = txtUrineSPGR.EditValue
        frncheckUpUrine.urine_wbc = txtUrineWBC.EditValue
        frncheckUpUrine.urine_result = If(IsDBNull(txtUrineresult.EditValue), "", txtUrineresult.EditValue)
        frncheckUpUrine.urine_result_remark = txtUrineResult_remark.EditValue

        checkUpClass.setFrnCheckUprine = frncheckUpUrine

    End Sub

    Public Sub setFrnCheckUpBlood()
        ''Dim blood_result As String
        frncheckUpBlood.bloodGroup = txtBloodgroup.EditValue
        frncheckUpBlood.bloodRh = txtBloodRh.EditValue
        frncheckUpBlood.Hgb = txtHgb.EditValue
        frncheckUpBlood.Hct = txtHct.EditValue
        frncheckUpBlood.wbc = txtWBC.EditValue
        frncheckUpBlood.Neutrophils = txtNeutrophils.EditValue
        frncheckUpBlood.Lymphocytes = txtLymphocytes.EditValue

        frncheckUpBlood.Eosionophils = txtEosionophils.EditValue

        frncheckUpBlood.Monocytes = txtMonocytes.EditValue
        frncheckUpBlood.Bashophils = txtBashophils.EditValue
        frncheckUpBlood.Atypical_lymph = txtAtypical_lymph.EditValue

        frncheckUpBlood.RBC_Morphology = txtRBC_Morphology.EditValue
        frncheckUpBlood.PlateCount = txtPlateCount.EditValue
        frncheckUpBlood.blood = txtblood.EditValue


        frncheckUpBlood.blood_result = txtblood_result.EditValue

        checkUpClass.setFrnCheckupBlood = frncheckUpBlood
    End Sub

    Private Sub txtHeight_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtHeight.KeyPress, txtWeight.KeyPress
        If e.KeyChar <> ChrW(Keys.Back) Then
            If (e.KeyChar.ToString >= "A" And e.KeyChar.ToString <= "Z") Or (e.KeyChar.ToString >= "a" And e.KeyChar.ToString <= "z") Then
                e.Handled = True
            Else
                If e.KeyChar = ChrW(Keys.Enter) Then
                    bmi_cal()
                End If
            End If
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        If checkUpClass.setFrnCheckup.idfrncheckup Is Nothing Then

            setFrnCheckUp()
            setFrnCheckUpBlood()
            setFrnCheckUpUrine()
            If checkUpClass.setFrnCheckup.idmascheckup_project = 0 Then
                MsgBox("กรุณาระบุุหัวข้อ")
                Exit Sub
            End If
            If checkUpClass.setFrnCheckup.doctor = 0 Then
                MsgBox("กรุณาระบแพทย์ผู้ตรวจ")
                Exit Sub
            End If
            checkUpClass.InsertFrncheckUp()



            MsgBox("ได้ทำการบันทึกไปแล้ว")

        Else
            frncheckUp.idfrncheckup = checkUpClass.setFrnCheckup.idfrncheckup
            setFrnCheckUp()
            setFrnCheckUpBlood()
            setFrnCheckUpUrine()
            Dim ANS As DialogResult = MessageBox.Show("ต้องการอัพเดทข้อมูลใช่หรือไม่", "Update ข้อมูล", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If ANS = DialogResult.Yes Then
                checkUpClass.UpdateFrncheckUp()
            End If

        End If
        getValueByOld()

    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Dim nextForm As frmCheckUp08 = New frmCheckUp08(dtset, checkUpClass)
        nextForm.ShowDialog()
    End Sub



    Private Sub txtHn_KeyDown(sender As Object, e As KeyEventArgs) Handles txtHn.KeyDown
        If e.KeyCode = Keys.Enter Then
            setClearDesign()
            frncheckUp.hn = txtHn.EditValue
            checkUpClass.setFrnCheckup = frncheckUp
            checkUpClass.getPerson()
            frncheckUp = checkUpClass.setFrnCheckup
            getFrnCheckUp()
        End If
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        Dim nextform As New frmCheckUp02(checkUpClass)
        If nextform.ShowDialog = DialogResult.OK Then
            checkUpClass.setFrnCheckup = frncheckUp

            getValueByvn()
        End If


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        If checkUpClass.setFrnCheckup.hn Is Nothing Then
            MsgBox("กรุณา ระบุ HN")
        Else
            Dim nextform As New frmCheckUp03(checkUpClass)
            If nextform.ShowDialog = DialogResult.OK Then
                checkUpClass.setFrnCheckup = frncheckUp

                getValueByOld()
            End If
        End If

    End Sub

    Private Sub txtfit_specific_CheckedChanged(sender As Object, e As EventArgs) Handles txtfit_specific.CheckedChanged

    End Sub

    Private Sub btnResult_Click(sender As Object, e As EventArgs) Handles btnResult.Click
        checkGetResult()
    End Sub
    Public Sub checkGetResult()
        frncheckUp.result = Nothing
        If CheckExamination.EditValue = True Then
            getResultExamination()
        End If
        If CheckExamination2.EditValue = True Then

        End If
        If checkBlood.EditValue = True Then
            getResultBlood()
        End If
        If checkUrineAnalysis.EditValue = True Then
            getResultUrine()
        End If
        If checkChemicalLab.EditValue = True Then
            getResultChemicalLab()
        End If
        If checkChemicallab2.EditValue = True Then
            getResultChemicalLab2()

        End If
        txtFrncheckupResult_all.EditValue = frncheckUp.result
    End Sub
    Public Sub getResultBlood()
        If txtblood_result.EditValue Is Nothing Then
        Else
            frncheckUp.result += " " & txtblood_result.EditValue & Environment.NewLine
        End If

    End Sub
    Public Sub getResultChemicalLab()
        For i As Integer = 0 To dtset.Tables("frncheckupother").Rows.Count - 1
            If dtset.Tables("frncheckupother").Rows(i)("f_result_all") = 1 Then
                frncheckUp.result += "" & dtset.Tables("frncheckupother").Rows(i)("result") & Environment.NewLine

            End If


        Next
    End Sub
    Public Sub getResultChemicalLab2()
        'If checkUpClass.getCheckResult(txtEyeLR.EditValue) = 0 Then
        'Else
        '    frncheckUp.result += "ตรวจสายตา :" & txtEyeLR_result.EditValue & Environment.NewLine

        'End If
    End Sub
    Public Sub getResultUrine()
        If checkUpClass.getCheckResult(txtVDRLresult.EditValue) = 0 Then
        Else
            frncheckUp.result += " " & txtVDRLResult_remark.EditValue & Environment.NewLine
        End If



        If checkUpClass.getCheckResult(txtCEAResult.EditValue) = 0 Then
        Else
            frncheckUp.result += " " & txtCEAResult_remark.EditValue & Environment.NewLine
        End If


        If checkUpClass.getCheckResult(txtXrayResult.EditValue) = 0 Then
        Else
            frncheckUp.result += " " & txtXrayResult_remark.EditValue & Environment.NewLine
        End If


        If checkUpClass.getCheckResult(txtHivResult.EditValue) = 0 Then
        Else
            frncheckUp.result += " " & txtHivResult_remark.EditValue & Environment.NewLine
        End If



    End Sub
    Public Sub getResultExamination()

        If checkUpClass.getCheckResult(txtEyeLR.EditValue) = 0 Then
        Else
            frncheckUp.result += "ตรวจสายตา :" & txtEyeLR_result.EditValue & Environment.NewLine

        End If
        If checkUpClass.getCheckResult(txtBlindness.EditValue) = 0 Then

        Else
            frncheckUp.result += "ตรวจตาบอดสี :" & txtBlindness_result.EditValue & Environment.NewLine

        End If


        If checkUpClass.getCheckResult(txtEye.EditValue) = 0 Then

        Else
            frncheckUp.result += "ตรวจตา :" & txteye_result.EditValue & Environment.NewLine

        End If


        If checkUpClass.getCheckResult(txtTooth.EditValue) = 0 Then

        Else
            frncheckUp.result += "ตรวจฟัน :" & txtTooth_result.EditValue & Environment.NewLine

        End If


    End Sub

    Private Sub SearchLookUpEdit7_EditValueChanged(sender As Object, e As EventArgs) Handles txtCompanyName.EditValueChanged
        If txtCompanyName.EditValue Is Nothing Then

        Else
            checkUpClass.getmascheckup_project_setting(txtCompanyName.EditValue)

            getDefaultCompany()

        End If
    End Sub
    Public Sub getDefaultCompany()
        If dtset.Tables("mascheckup_project").Rows.Count > 0 Then
            txtEyeLR.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtEyeLR.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtEyeLR.Name))

            txtBlindness.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtBlindness.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtBlindness.Name))


            txtEye.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtEye.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtEye.Name))

            If IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name)) Then
                txtTooth.EditValue = Nothing
            Else
                txtTooth.EditValue = dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name)
            End If
            '  txtTooth.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtTooth.Name))



            txtblood.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtblood.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtblood.Name))
            ' MsgBox(dtset.Tables("mascheckup_project").Rows(0)("txtUrineresult"))

            txtUrineresult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtUrineresult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtUrineresult.Name))


            txtVDRLresult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVDRLresult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVDRLresult.Name))

            txtCEAResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtCEAResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtCEAResult.Name))



            txtXrayResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtXrayResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtXrayResult.Name))


            txtHivResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtHivResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtHivResult.Name))

            txtVirusBResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVirusBResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVirusBResult.Name))


            txtVirusAResult.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(txtVirusAResult.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(txtVirusAResult.Name))

            CheckExamination.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(CheckExamination.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(CheckExamination.Name))

            CheckExamination2.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(CheckExamination2.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(CheckExamination2.Name))

            checkBlood.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkBlood.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkBlood.Name))

            checkUrineAnalysis.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkUrineAnalysis.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkUrineAnalysis.Name))

            checkChemicalLab.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkChemicalLab.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkChemicalLab.Name))

            checkChemicallab2.EditValue = If(IsDBNull(dtset.Tables("mascheckup_project").Rows(0)(checkChemicallab2.Name)), Nothing, dtset.Tables("mascheckup_project").Rows(0)(checkChemicallab2.Name))
        End If





    End Sub

    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click

        Dim report6 As rptCheckUpA5_6 = New rptCheckUpA5_6
        report6.DataSource = dtset

        report6.CreateDocument()

        ' Add all pages of the 2nd report to the end of the 1st report. 

        ' Reset all page numbers in the resulting document. 
        report6.PrintingSystem.ContinuousPageNumbering = True

        ' Create a Print Tool and show the Print Preview form. 
        Dim printTool As New ReportPrintTool(report6)
        printTool.ShowPreviewDialog()
    End Sub

    Private Sub txtDoctor_EditValueChanged(sender As Object, e As EventArgs) Handles txtDoctor.EditValueChanged
        If txtDoctor.EditValue Is Nothing Then
        Else
            txtLicense.EditValue = checkUpClass.getLicenseCode(txtDoctor.EditValue)
        End If
    End Sub

    Private Sub TextEdit5_EditValueChanged(sender As Object, e As EventArgs) Handles txtvirusa_havigm.EditValueChanged

    End Sub
End Class
