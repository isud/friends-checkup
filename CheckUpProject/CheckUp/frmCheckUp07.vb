﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmCheckUp07

    Dim dtset As New dtCheckUP
    Dim checkupClass As checkupClass
    Dim frmcheckUp07 As frmCheckUp07Class
    Dim mascheckup As frmCheckUp07Class.mascheckup
    Private Sub frmCheckUp05_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadMasterToGrid()
    End Sub
    Public Sub loadMasterToGrid()
        frmcheckUp07 = New frmCheckUp07Class(dtset)
        frmcheckUp07.loadCondition()
        condition1_1.Properties.DataSource = dtset.Tables("mascondition")
        condition1_2.Properties.DataSource = dtset.Tables("mascondition")
        condition2_1.Properties.DataSource = dtset.Tables("mascondition")
        condition2_2.Properties.DataSource = dtset.Tables("mascondition")

        frmcheckUp07.LoadMasterFormCode()

        txtformcode.Properties.DataSource = dtset.Tables("dgvOtherResult")

        frmcheckUp07.getMascheckup()
        GridControl1.DataSource = dtset


    End Sub
    Public Sub getDefaultCompany2()
        mascheckup.idmascheckup = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckup")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckup"))

        mascheckup.idmascheckupdt = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckupdt")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckupdt"))


        txtname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name")), Nothing, dtset.Tables("mascheck").Rows(0)("name"))
        txtname2.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name2")), Nothing, dtset.Tables("mascheck").Rows(0)("name2"))
        txtdescriptions.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("description")), Nothing, dtset.Tables("mascheck").Rows(0)("description"))
        txtformcode.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formcode")), Nothing, dtset.Tables("mascheck").Rows(0)("formcode"))
        txtformname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formname")), Nothing, dtset.Tables("mascheck").Rows(0)("formname"))
    End Sub

    Public Sub getDefaultCompany()
        If dtset.Tables("mascheck").Rows.Count > 0 Then


            mascheckup.idmascheckup = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckup")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckup"))

            mascheckup.idmascheckupdt = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("idmascheckupdt")), Nothing, dtset.Tables("mascheck").Rows(0)("idmascheckupdt"))


            txtname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name")), Nothing, dtset.Tables("mascheck").Rows(0)("name"))
            txtname2.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name2")), Nothing, dtset.Tables("mascheck").Rows(0)("name2"))
            txtdescriptions.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("description")), Nothing, dtset.Tables("mascheck").Rows(0)("description"))
            txtformcode.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formcode")), Nothing, dtset.Tables("mascheck").Rows(0)("formcode"))
            txtformname.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("formname")), Nothing, dtset.Tables("mascheck").Rows(0)("formname"))
            '   chkForm.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("form")), Nothing, dtset.Tables("mascheck").Rows(0)("form"))
            condition1_1.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("condition1_1")), Nothing, dtset.Tables("mascheck").Rows(0)("condition1_1"))
            condition1_2.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("condition1_2")), Nothing, dtset.Tables("mascheck").Rows(0)("condition1_2"))

            condition2_2.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("condition2_2")), Nothing, dtset.Tables("mascheck").Rows(0)("condition2_2"))

            condition2_1.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("condition2_1")), Nothing, dtset.Tables("mascheck").Rows(0)("condition2_1"))



            value1_1.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("value1_1")), Nothing, dtset.Tables("mascheck").Rows(0)("value1_1"))
            value1_2.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("value1_2")), Nothing, dtset.Tables("mascheck").Rows(0)("value1_2"))
            value2_1.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("value2_1")), Nothing, dtset.Tables("mascheck").Rows(0)("value2_1"))
            value2_2.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("value2_2")), Nothing, dtset.Tables("mascheck").Rows(0)("value2_2"))



            txtname_dt.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name_dt")), Nothing, dtset.Tables("mascheck").Rows(0)("name_dt"))
            txtname2_dt.Text = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("name2_dt")), Nothing, dtset.Tables("mascheck").Rows(0)("name2_dt"))
            chk_result_all.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("f_result_all")), False, dtset.Tables("mascheck").Rows(0)("f_result_all"))
            chkstatus.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("status")), False, dtset.Tables("mascheck").Rows(0)("status"))
            chkSex.EditValue = If(IsDBNull(dtset.Tables("mascheck").Rows(0)("f_sex")), False, dtset.Tables("mascheck").Rows(0)("f_sex"))
        End If

    End Sub
    Public Sub ClearDesign()
        ClearStruct()
        txtname.Text = Nothing
        txtname2.Text = Nothing
        txtdescriptions.Text = Nothing
        txtformcode.Text = Nothing
        txtformname.Text = Nothing
        ' chkForm.EditValue = Nothing
        txtname_dt.Text = Nothing
        txtname2_dt.Text = Nothing
        chkstatus.EditValue = True
        chk_result_all.EditValue = False



        condition1_1.EditValue = Nothing
        condition1_2.EditValue = Nothing

        condition2_2.EditValue = Nothing

        condition2_1.EditValue = Nothing

        value1_1.EditValue = Nothing
        value1_2.EditValue = Nothing
        value2_1.EditValue = Nothing
        value2_2.EditValue = Nothing
        chkSex.EditValue = Nothing


    End Sub
    Public Sub setmascheckup()

        mascheckup.name = txtname.EditValue
        mascheckup.name2 = txtname2.EditValue
        mascheckup.description = txtdescriptions.EditValue
        mascheckup.formcode = txtformcode.EditValue
        mascheckup.formname = txtformname.EditValue
        ' mascheckup.form = chkForm.EditValue
        mascheckup.name_dt = txtname_dt.Text
        mascheckup.name2_dt = txtname2_dt.Text
        mascheckup.status = chkstatus.EditValue
        mascheckup.f_result_all = chk_result_all.EditValue



        mascheckup.condition1_1 = condition1_1.EditValue
        mascheckup.condition1_2 = condition1_2.EditValue
        mascheckup.condition2_1 = condition2_1.EditValue
        mascheckup.condition2_2 = condition2_2.EditValue

        mascheckup.value1_1 = value1_1.EditValue
        mascheckup.value1_2 = value1_2.EditValue
        mascheckup.value2_1 = value2_1.EditValue
        mascheckup.value2_2 = value2_2.EditValue
        mascheckup.f_sex = chkSex.EditValue




    End Sub
    Public Sub setReadOnlyFalse()
        txtformcode.ReadOnly = False
    End Sub
    Public Sub setReadOnlyTrue()
        txtformcode.ReadOnly = True

    End Sub
    Public Sub ClearStruct()
        mascheckup = New frmCheckUp07Class.mascheckup
    End Sub
    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        ClearStruct()
        ClearDesign()
        setReadOnlyTrue()

        Try
            If IsDBNull(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckupdt")) Then

                frmcheckUp07.getMaschekcupDt_nothing(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckup"))
                getDefaultCompany2()
            Else
                frmcheckUp07.getMascheckupDt(GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmascheckupdt"))
                getDefaultCompany()
            End If


        Catch ex As Exception

        End Try

    End Sub



    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        setReadOnlyFalse()
        ClearDesign()

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        setmascheckup()
        frmcheckUp07.setmascheckup = mascheckup

        If mascheckup.idmascheckupdt Is Nothing Then
            frmcheckUp07.InsertMascheckup_Dt()
        Else
            frmcheckUp07.UpdateMascheckup_dt()
        End If
        loadMasterToGrid()
        ClearDesign()
        setReadOnlyFalse()
    End Sub
End Class