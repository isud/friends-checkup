﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class setting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(setting))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnEditSetting = New DevExpress.XtraEditors.SimpleButton()
        Me.txtDBSCHEMA = New DevExpress.XtraEditors.TextEdit()
        Me.txtDBPORT = New DevExpress.XtraEditors.TextEdit()
        Me.txtDBPASSWORD = New DevExpress.XtraEditors.TextEdit()
        Me.txtDBUSER = New DevExpress.XtraEditors.TextEdit()
        Me.txtIPDB = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtDBSCHEMA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDBPORT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDBPASSWORD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDBUSER.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIPDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.btnEditSetting)
        Me.LayoutControl1.Controls.Add(Me.txtDBSCHEMA)
        Me.LayoutControl1.Controls.Add(Me.txtDBPORT)
        Me.LayoutControl1.Controls.Add(Me.txtDBPASSWORD)
        Me.LayoutControl1.Controls.Add(Me.txtDBUSER)
        Me.LayoutControl1.Controls.Add(Me.txtIPDB)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(296, 183)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'btnEditSetting
        '
        Me.btnEditSetting.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditSetting.Appearance.Options.UseFont = True
        Me.btnEditSetting.Image = CType(resources.GetObject("btnEditSetting.Image"), System.Drawing.Image)
        Me.btnEditSetting.Location = New System.Drawing.Point(12, 142)
        Me.btnEditSetting.Name = "btnEditSetting"
        Me.btnEditSetting.Size = New System.Drawing.Size(272, 23)
        Me.btnEditSetting.StyleController = Me.LayoutControl1
        Me.btnEditSetting.TabIndex = 9
        Me.btnEditSetting.Text = "Submit"
        '
        'txtDBSCHEMA
        '
        Me.txtDBSCHEMA.Location = New System.Drawing.Point(98, 116)
        Me.txtDBSCHEMA.Name = "txtDBSCHEMA"
        Me.txtDBSCHEMA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBSCHEMA.Properties.Appearance.Options.UseFont = True
        Me.txtDBSCHEMA.Size = New System.Drawing.Size(186, 22)
        Me.txtDBSCHEMA.StyleController = Me.LayoutControl1
        Me.txtDBSCHEMA.TabIndex = 8
        '
        'txtDBPORT
        '
        Me.txtDBPORT.Location = New System.Drawing.Point(98, 90)
        Me.txtDBPORT.Name = "txtDBPORT"
        Me.txtDBPORT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPORT.Properties.Appearance.Options.UseFont = True
        Me.txtDBPORT.Size = New System.Drawing.Size(186, 22)
        Me.txtDBPORT.StyleController = Me.LayoutControl1
        Me.txtDBPORT.TabIndex = 7
        '
        'txtDBPASSWORD
        '
        Me.txtDBPASSWORD.Location = New System.Drawing.Point(98, 64)
        Me.txtDBPASSWORD.Name = "txtDBPASSWORD"
        Me.txtDBPASSWORD.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPASSWORD.Properties.Appearance.Options.UseFont = True
        Me.txtDBPASSWORD.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDBPASSWORD.Size = New System.Drawing.Size(186, 22)
        Me.txtDBPASSWORD.StyleController = Me.LayoutControl1
        Me.txtDBPASSWORD.TabIndex = 6
        '
        'txtDBUSER
        '
        Me.txtDBUSER.Location = New System.Drawing.Point(98, 38)
        Me.txtDBUSER.Name = "txtDBUSER"
        Me.txtDBUSER.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBUSER.Properties.Appearance.Options.UseFont = True
        Me.txtDBUSER.Size = New System.Drawing.Size(186, 22)
        Me.txtDBUSER.StyleController = Me.LayoutControl1
        Me.txtDBUSER.TabIndex = 5
        '
        'txtIPDB
        '
        Me.txtIPDB.Location = New System.Drawing.Point(98, 12)
        Me.txtIPDB.Name = "txtIPDB"
        Me.txtIPDB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIPDB.Properties.Appearance.Options.UseFont = True
        Me.txtIPDB.Size = New System.Drawing.Size(186, 22)
        Me.txtIPDB.StyleController = Me.LayoutControl1
        Me.txtIPDB.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(296, 183)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.txtIPDB
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(276, 26)
        Me.LayoutControlItem1.Text = "IPDB"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(83, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.txtDBUSER
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(276, 26)
        Me.LayoutControlItem2.Text = "DBUSER"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(83, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.txtDBPASSWORD
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(276, 26)
        Me.LayoutControlItem3.Text = "DBPASSWORD"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(83, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.txtDBPORT
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(276, 26)
        Me.LayoutControlItem4.Text = "DBPORT"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(83, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem5.Control = Me.txtDBSCHEMA
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 104)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(276, 26)
        Me.LayoutControlItem5.Text = "DBSCHEMA"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(83, 16)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.btnEditSetting
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 130)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(276, 33)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'setting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(296, 183)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "setting"
        Me.Text = "Setting CheckUp"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtDBSCHEMA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDBPORT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDBPASSWORD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDBUSER.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIPDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtIPDB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnEditSetting As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtDBSCHEMA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDBPORT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDBPASSWORD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDBUSER As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
End Class
