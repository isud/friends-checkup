﻿Imports System.ComponentModel
Imports System.Text
Imports System.Deployment.Application


Partial Public Class frmMain
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm
    Shared Sub New()
        DevExpress.UserSkins.BonusSkins.Register()
        DevExpress.Skins.SkinManager.EnableFormSkins()
    End Sub
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load

        DevExpress.Utils.AppearanceObject.DefaultFont = New Font("Tahoma", 12)

        If ApplicationDeployment.IsNetworkDeployed Then
            Me.Text = "FRIENDS CheckUp Version: " & ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString
        End If
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Dim nextform As New frmCheckUp04
        nextform.MdiParent = Me
        nextform.Dock = DockStyle.Fill
        nextform.WindowState = FormWindowState.Maximized
        nextform.Show()
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        Dim nextform As New frmCheckUp05
        nextform.MdiParent = Me
        nextform.Dock = DockStyle.Fill
        nextform.WindowState = FormWindowState.Maximized
        nextform.Show()
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Dim nextform As New frmCheckUp06
        nextform.MdiParent = Me
        nextform.Dock = DockStyle.Fill
        nextform.WindowState = FormWindowState.Maximized
        nextform.Show()
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        Dim nextform As New frmCheckUp07
        nextform.MdiParent = Me
        nextform.Dock = DockStyle.Fill
        nextform.WindowState = FormWindowState.Maximized
        nextform.Show()
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        Dim drugExtPanel As New frmCheckUp01
        'ReportviewerPanel.Controls.Add(extRaportCtrl)
        drugExtPanel.MdiParent = Me
        drugExtPanel.Dock = DockStyle.Fill
        drugExtPanel.WindowState = FormWindowState.Maximized
        drugExtPanel.Show()
    End Sub

    Private Sub btnSetting_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSetting.ItemClick
        Dim settingForm As New setting
        settingForm.ShowDialog()
    End Sub
End Class
