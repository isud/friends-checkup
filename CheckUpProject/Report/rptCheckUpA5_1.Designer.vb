﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rptCheckUpA5_1
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrCheckBox2 = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell52 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell46 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell41 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell45 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell53 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrCheckBox1 = New DevExpress.XtraReports.UI.XRCheckBox()
        Me.tableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell36 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell39 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.txtBloodPressure = New DevExpress.XtraReports.Parameters.Parameter()
        Me.tableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell56 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell57 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell58 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell61 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell60 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell62 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell50 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell59 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell40 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell37 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.txtHeight = New DevExpress.XtraReports.Parameters.Parameter()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrTableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell51 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.txtEyeL = New DevExpress.XtraReports.Parameters.Parameter()
        Me.tableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.txtWeight = New DevExpress.XtraReports.Parameters.Parameter()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.tableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell54 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell55 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.txtHeartRate = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.txtEyeR = New DevExpress.XtraReports.Parameters.Parameter()
        Me.txtBmi = New DevExpress.XtraReports.Parameters.Parameter()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.txtColorBlind = New DevExpress.XtraReports.Parameters.Parameter()
        Me.DtCheckUP1 = New CheckUpProject.dtCheckUP()
        Me.CalculatedField1 = New DevExpress.XtraReports.UI.CalculatedField()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'tableCell26
        '
        Me.tableCell26.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrCheckBox2})
        Me.tableCell26.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell26.Multiline = True
        Me.tableCell26.Name = "tableCell26"
        Me.tableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell26.StylePriority.UseFont = False
        Me.tableCell26.StylePriority.UsePadding = False
        Me.tableCell26.StylePriority.UseTextAlignment = False
        Me.tableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell26.Weight = 2.4323854759990731R
        '
        'XrCheckBox2
        '
        Me.XrCheckBox2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("CheckState", Nothing, "frncheckup.glasses_without")})
        Me.XrCheckBox2.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 0!)
        Me.XrCheckBox2.Name = "XrCheckBox2"
        Me.XrCheckBox2.SizeF = New System.Drawing.SizeF(236.4583!, 22.99997!)
        Me.XrCheckBox2.Text = "Without Glasses (ไม่สวมแว่น)"
        '
        'XrTableCell13
        '
        Me.XrTableCell13.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell13.ForeColor = System.Drawing.Color.DimGray
        Me.XrTableCell13.Name = "XrTableCell13"
        Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.XrTableCell13.StylePriority.UseFont = False
        Me.XrTableCell13.StylePriority.UseForeColor = False
        Me.XrTableCell13.StylePriority.UsePadding = False
        Me.XrTableCell13.Text = "HEALTHY RANGE"
        Me.XrTableCell13.Weight = 0.6982405515433342R
        '
        'tableCell12
        '
        Me.tableCell12.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell12.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell12.Name = "tableCell12"
        Me.tableCell12.StylePriority.UseFont = False
        Me.tableCell12.StylePriority.UseForeColor = False
        Me.tableCell12.StylePriority.UseTextAlignment = False
        Me.tableCell12.Text = "bpm"
        Me.tableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.tableCell12.Weight = 0.33010619794445845R
        '
        'tableCell31
        '
        Me.tableCell31.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell31.Name = "tableCell31"
        Me.tableCell31.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell31.StylePriority.UseFont = False
        Me.tableCell31.StylePriority.UsePadding = False
        Me.tableCell31.StylePriority.UseTextAlignment = False
        Me.tableCell31.Text = "( Tooth )"
        Me.tableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell31.Weight = 0.50819623792493673R
        '
        'tableCell44
        '
        Me.tableCell44.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell44.ForeColor = System.Drawing.Color.Gray
        Me.tableCell44.Name = "tableCell44"
        Me.tableCell44.StylePriority.UseFont = False
        Me.tableCell44.StylePriority.UseForeColor = False
        Me.tableCell44.Weight = 0.33010635594966115R
        '
        'tableRow13
        '
        Me.tableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell25, Me.tableCell30, Me.tableCell52})
        Me.tableRow13.Name = "tableRow13"
        Me.tableRow13.Weight = 1.0R
        '
        'tableCell25
        '
        Me.tableCell25.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell25.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell25.Name = "tableCell25"
        Me.tableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell25.StylePriority.UseBackColor = False
        Me.tableCell25.StylePriority.UseFont = False
        Me.tableCell25.StylePriority.UsePadding = False
        Me.tableCell25.StylePriority.UseTextAlignment = False
        Me.tableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell25.Weight = 0.059418343208931468R
        '
        'tableCell30
        '
        Me.tableCell30.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell30.Name = "tableCell30"
        Me.tableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell30.StylePriority.UseFont = False
        Me.tableCell30.StylePriority.UsePadding = False
        Me.tableCell30.StylePriority.UseTextAlignment = False
        Me.tableCell30.Text = "ตรวจตา"
        Me.tableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell30.Weight = 0.50819623792493673R
        '
        'tableCell52
        '
        Me.tableCell52.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.eye_result")})
        Me.tableCell52.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell52.Name = "tableCell52"
        Me.tableCell52.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell52.StylePriority.UseFont = False
        Me.tableCell52.StylePriority.UsePadding = False
        Me.tableCell52.StylePriority.UseTextAlignment = False
        Me.tableCell52.Text = "tableCell52"
        Me.tableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell52.Weight = 2.432385418866132R
        '
        'tableCell46
        '
        Me.tableCell46.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell46.ForeColor = System.Drawing.Color.Gray
        Me.tableCell46.Name = "tableCell46"
        Me.tableCell46.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell46.StylePriority.UseFont = False
        Me.tableCell46.StylePriority.UseForeColor = False
        Me.tableCell46.StylePriority.UsePadding = False
        Me.tableCell46.Weight = 0.6982405515433342R
        '
        'tableRow11
        '
        Me.tableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell41, Me.tableCell42, Me.tableCell43, Me.tableCell44, Me.tableCell45, Me.tableCell46, Me.tableCell47})
        Me.tableRow11.Name = "tableRow11"
        Me.tableRow11.Weight = 1.0R
        '
        'tableCell41
        '
        Me.tableCell41.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell41.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell41.Name = "tableCell41"
        Me.tableCell41.StylePriority.UseBackColor = False
        Me.tableCell41.StylePriority.UseBorderColor = False
        Me.tableCell41.Weight = 0.059418317314414729R
        '
        'tableCell42
        '
        Me.tableCell42.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell42.ForeColor = System.Drawing.Color.Gray
        Me.tableCell42.Name = "tableCell42"
        Me.tableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell42.StylePriority.UseFont = False
        Me.tableCell42.StylePriority.UseForeColor = False
        Me.tableCell42.StylePriority.UsePadding = False
        Me.tableCell42.Weight = 0.81939594126368787R
        '
        'tableCell43
        '
        Me.tableCell43.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell43.Name = "tableCell43"
        Me.tableCell43.StylePriority.UseFont = False
        Me.tableCell43.StylePriority.UseTextAlignment = False
        Me.tableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell43.Weight = 0.31792359661606318R
        '
        'tableCell45
        '
        Me.tableCell45.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell45.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell45.ForeColor = System.Drawing.Color.Gray
        Me.tableCell45.Name = "tableCell45"
        Me.tableCell45.StylePriority.UseBackColor = False
        Me.tableCell45.StylePriority.UseFont = False
        Me.tableCell45.StylePriority.UseForeColor = False
        Me.tableCell45.Weight = 0.065755268329852568R
        '
        'tableCell47
        '
        Me.tableCell47.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell47.Name = "tableCell47"
        Me.tableCell47.StylePriority.UseFont = False
        Me.tableCell47.StylePriority.UseTextAlignment = False
        Me.tableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell47.Weight = 0.70915996898298617R
        '
        'tableCell53
        '
        Me.tableCell53.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell53.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell53.Name = "tableCell53"
        Me.tableCell53.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell53.StylePriority.UseBackColor = False
        Me.tableCell53.StylePriority.UseFont = False
        Me.tableCell53.StylePriority.UsePadding = False
        Me.tableCell53.StylePriority.UseTextAlignment = False
        Me.tableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell53.Weight = 0.059418343208931468R
        '
        'tableRow8
        '
        Me.tableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell27, Me.XrTableCell19, Me.tableCell24})
        Me.tableRow8.Name = "tableRow8"
        Me.tableRow8.Weight = 1.0R
        '
        'tableCell27
        '
        Me.tableCell27.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell27.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell27.Name = "tableCell27"
        Me.tableCell27.StylePriority.UseBackColor = False
        Me.tableCell27.StylePriority.UseBorderColor = False
        Me.tableCell27.Weight = 0.059418317314414729R
        '
        'XrTableCell19
        '
        Me.XrTableCell19.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell19.Name = "XrTableCell19"
        Me.XrTableCell19.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.XrTableCell19.StylePriority.UseFont = False
        Me.XrTableCell19.StylePriority.UsePadding = False
        Me.XrTableCell19.StylePriority.UseTextAlignment = False
        Me.XrTableCell19.Text = "( สายตา )"
        Me.XrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrTableCell19.Weight = 0.50819620668651244R
        '
        'tableCell24
        '
        Me.tableCell24.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrCheckBox1})
        Me.tableCell24.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell24.Name = "tableCell24"
        Me.tableCell24.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell24.StylePriority.UseFont = False
        Me.tableCell24.StylePriority.UsePadding = False
        Me.tableCell24.StylePriority.UseTextAlignment = False
        Me.tableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell24.Weight = 2.4323854759990731R
        '
        'XrCheckBox1
        '
        Me.XrCheckBox1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("CheckState", Nothing, "frncheckup.glasses")})
        Me.XrCheckBox1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCheckBox1.Name = "XrCheckBox1"
        Me.XrCheckBox1.SizeF = New System.Drawing.SizeF(236.4584!, 23.0!)
        Me.XrCheckBox1.Text = "With glasses or contact lens ( สวมแว่นหรือใส่คอนแทคเลนส์ )"
        '
        'tableCell17
        '
        Me.tableCell17.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.bloodpressure")})
        Me.tableCell17.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell17.Name = "tableCell17"
        Me.tableCell17.StylePriority.UseFont = False
        Me.tableCell17.StylePriority.UseTextAlignment = False
        Me.tableCell17.Text = "118/78"
        Me.tableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell17.Weight = 0.3179235996466514R
        '
        'tableCell6
        '
        Me.tableCell6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell6.Name = "tableCell6"
        Me.tableCell6.StylePriority.UseBackColor = False
        Me.tableCell6.StylePriority.UseBorderColor = False
        Me.tableCell6.Weight = 0.059418317314414729R
        '
        'tableCell36
        '
        Me.tableCell36.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell36.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell36.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell36.Name = "tableCell36"
        Me.tableCell36.StylePriority.UseBackColor = False
        Me.tableCell36.StylePriority.UseBorderColor = False
        Me.tableCell36.StylePriority.UseFont = False
        Me.tableCell36.StylePriority.UseTextAlignment = False
        Me.tableCell36.Text = "Eyes examination by Ophthalmologist (ตรวจต้อหินและตรวจตาโดยจักษุแพทย์)"
        Me.tableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell36.Weight = 3.0R
        '
        'tableCell39
        '
        Me.tableCell39.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell39.Name = "tableCell39"
        Me.tableCell39.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell39.StylePriority.UseFont = False
        Me.tableCell39.StylePriority.UsePadding = False
        Me.tableCell39.StylePriority.UseTextAlignment = False
        Me.tableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell39.Weight = 0.20382487086482354R
        '
        'txtBloodPressure
        '
        Me.txtBloodPressure.Description = "Parameter1"
        Me.txtBloodPressure.Name = "txtBloodPressure"
        Me.txtBloodPressure.Type = GetType(Integer)
        Me.txtBloodPressure.ValueInfo = "0"
        '
        'tableRow16
        '
        Me.tableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell56, Me.tableCell57, Me.tableCell58})
        Me.tableRow16.Name = "tableRow16"
        Me.tableRow16.Weight = 1.0R
        '
        'tableCell56
        '
        Me.tableCell56.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell56.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell56.Name = "tableCell56"
        Me.tableCell56.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell56.StylePriority.UseBackColor = False
        Me.tableCell56.StylePriority.UseFont = False
        Me.tableCell56.StylePriority.UsePadding = False
        Me.tableCell56.StylePriority.UseTextAlignment = False
        Me.tableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell56.Weight = 0.059418343208931468R
        '
        'tableCell57
        '
        Me.tableCell57.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell57.Name = "tableCell57"
        Me.tableCell57.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell57.StylePriority.UseFont = False
        Me.tableCell57.StylePriority.UsePadding = False
        Me.tableCell57.StylePriority.UseTextAlignment = False
        Me.tableCell57.Text = "( Eye )"
        Me.tableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell57.Weight = 0.50819623792493673R
        '
        'tableCell58
        '
        Me.tableCell58.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell58.Name = "tableCell58"
        Me.tableCell58.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell58.StylePriority.UseFont = False
        Me.tableCell58.StylePriority.UsePadding = False
        Me.tableCell58.StylePriority.UseTextAlignment = False
        Me.tableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell58.Weight = 2.432385418866132R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell5.ForeColor = System.Drawing.Color.DimGray
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.XrTableCell5.StylePriority.UseFont = False
        Me.XrTableCell5.StylePriority.UseForeColor = False
        Me.XrTableCell5.StylePriority.UsePadding = False
        Me.XrTableCell5.Text = "BODY MASS"
        Me.XrTableCell5.Weight = 0.81939594126368787R
        '
        'XrTableCell22
        '
        Me.XrTableCell22.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell22.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell22.ForeColor = System.Drawing.Color.Black
        Me.XrTableCell22.Name = "XrTableCell22"
        Me.XrTableCell22.StylePriority.UseBorderColor = False
        Me.XrTableCell22.StylePriority.UseFont = False
        Me.XrTableCell22.StylePriority.UseForeColor = False
        Me.XrTableCell22.StylePriority.UseTextAlignment = False
        Me.XrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrTableCell22.Weight = 2.9405816713994994R
        '
        'tableCell11
        '
        Me.tableCell11.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell11.ForeColor = System.Drawing.Color.Gray
        Me.tableCell11.Name = "tableCell11"
        Me.tableCell11.StylePriority.UseFont = False
        Me.tableCell11.StylePriority.UseForeColor = False
        Me.tableCell11.Weight = 0.33010635594966115R
        '
        'tableCell61
        '
        Me.tableCell61.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell61.Name = "tableCell61"
        Me.tableCell61.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell61.StylePriority.UseFont = False
        Me.tableCell61.StylePriority.UsePadding = False
        Me.tableCell61.StylePriority.UseTextAlignment = False
        Me.tableCell61.Text = "ตรวจฟัน"
        Me.tableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell61.Weight = 0.50819623792493673R
        '
        'tableCell34
        '
        Me.tableCell34.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.eyeR")})
        Me.tableCell34.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell34.Name = "tableCell34"
        Me.tableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell34.StylePriority.UseFont = False
        Me.tableCell34.StylePriority.UsePadding = False
        Me.tableCell34.StylePriority.UseTextAlignment = False
        Me.tableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell34.Weight = 2.4323854759990726R
        '
        'tableRow17
        '
        Me.tableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell60, Me.tableCell61, Me.tableCell62})
        Me.tableRow17.Name = "tableRow17"
        Me.tableRow17.Weight = 1.0R
        '
        'tableCell60
        '
        Me.tableCell60.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell60.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell60.Name = "tableCell60"
        Me.tableCell60.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell60.StylePriority.UseBackColor = False
        Me.tableCell60.StylePriority.UseFont = False
        Me.tableCell60.StylePriority.UsePadding = False
        Me.tableCell60.StylePriority.UseTextAlignment = False
        Me.tableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell60.Weight = 0.059418343208931468R
        '
        'tableCell62
        '
        Me.tableCell62.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.tooth_result")})
        Me.tableCell62.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell62.Name = "tableCell62"
        Me.tableCell62.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell62.StylePriority.UseFont = False
        Me.tableCell62.StylePriority.UsePadding = False
        Me.tableCell62.StylePriority.UseTextAlignment = False
        Me.tableCell62.Text = "tableCell62"
        Me.tableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell62.Weight = 2.432385418866132R
        '
        'tableCell33
        '
        Me.tableCell33.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell33.Name = "tableCell33"
        Me.tableCell33.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell33.StylePriority.UseFont = False
        Me.tableCell33.StylePriority.UsePadding = False
        Me.tableCell33.StylePriority.UseTextAlignment = False
        Me.tableCell33.Text = "ขวา (R)"
        Me.tableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell33.Weight = 0.30437133582168885R
        '
        'tableRow12
        '
        Me.tableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell50})
        Me.tableRow12.Name = "tableRow12"
        Me.tableRow12.Weight = 1.0R
        '
        'tableCell50
        '
        Me.tableCell50.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.eyeLR_result")})
        Me.tableCell50.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell50.Name = "tableCell50"
        Me.tableCell50.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell50.StylePriority.UseFont = False
        Me.tableCell50.StylePriority.UsePadding = False
        Me.tableCell50.StylePriority.UseTextAlignment = False
        Me.tableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell50.Weight = 3.0R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell3.ForeColor = System.Drawing.Color.Gray
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.StylePriority.UseBorderColor = False
        Me.XrTableCell3.StylePriority.UseFont = False
        Me.XrTableCell3.StylePriority.UseForeColor = False
        Me.XrTableCell3.StylePriority.UseTextAlignment = False
        Me.XrTableCell3.Text = "CM."
        Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.XrTableCell3.Weight = 0.26050910660728388R
        '
        'tableRow6
        '
        Me.tableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell36})
        Me.tableRow6.Name = "tableRow6"
        Me.tableRow6.Weight = 1.0R
        '
        'tableCell59
        '
        Me.tableCell59.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell59.Name = "tableCell59"
        Me.tableCell59.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell59.StylePriority.UseFont = False
        Me.tableCell59.StylePriority.UsePadding = False
        Me.tableCell59.StylePriority.UseTextAlignment = False
        Me.tableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell59.Weight = 2.432385418866132R
        '
        'tableCell16
        '
        Me.tableCell16.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell16.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell16.Name = "tableCell16"
        Me.tableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell16.StylePriority.UseFont = False
        Me.tableCell16.StylePriority.UseForeColor = False
        Me.tableCell16.StylePriority.UsePadding = False
        Me.tableCell16.Text = "BLOOD PRESSURE"
        Me.tableCell16.Weight = 0.819395894047417R
        '
        'tableRow9
        '
        Me.tableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell32, Me.tableCell39, Me.tableCell33, Me.tableCell34})
        Me.tableRow9.Name = "tableRow9"
        Me.tableRow9.Weight = 1.0R
        '
        'tableCell32
        '
        Me.tableCell32.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell32.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell32.Name = "tableCell32"
        Me.tableCell32.StylePriority.UseBackColor = False
        Me.tableCell32.StylePriority.UseBorderColor = False
        Me.tableCell32.Weight = 0.059418317314414729R
        '
        'tableCell9
        '
        Me.tableCell9.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell9.ForeColor = System.Drawing.Color.Gray
        Me.tableCell9.Name = "tableCell9"
        Me.tableCell9.StylePriority.UseFont = False
        Me.tableCell9.StylePriority.UseForeColor = False
        Me.tableCell9.Weight = 0.6668900007505999R
        '
        'tableRow10
        '
        Me.tableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell35, Me.tableCell40, Me.tableCell37, Me.tableCell38})
        Me.tableRow10.Name = "tableRow10"
        Me.tableRow10.Weight = 1.0R
        '
        'tableCell35
        '
        Me.tableCell35.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell35.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell35.Name = "tableCell35"
        Me.tableCell35.StylePriority.UseBackColor = False
        Me.tableCell35.StylePriority.UseBorderColor = False
        Me.tableCell35.Weight = 0.059418317314414729R
        '
        'tableCell40
        '
        Me.tableCell40.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell40.Name = "tableCell40"
        Me.tableCell40.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell40.StylePriority.UseFont = False
        Me.tableCell40.StylePriority.UsePadding = False
        Me.tableCell40.StylePriority.UseTextAlignment = False
        Me.tableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell40.Weight = 0.20382487086482354R
        '
        'tableCell37
        '
        Me.tableCell37.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell37.Name = "tableCell37"
        Me.tableCell37.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell37.StylePriority.UseFont = False
        Me.tableCell37.StylePriority.UsePadding = False
        Me.tableCell37.StylePriority.UseTextAlignment = False
        Me.tableCell37.Text = "ซ้าย (L)"
        Me.tableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell37.Weight = 0.3043713358216889R
        '
        'tableCell38
        '
        Me.tableCell38.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.eyeL")})
        Me.tableCell38.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell38.Name = "tableCell38"
        Me.tableCell38.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell38.StylePriority.UseFont = False
        Me.tableCell38.StylePriority.UsePadding = False
        Me.tableCell38.StylePriority.UseTextAlignment = False
        Me.tableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell38.Weight = 2.4323854759990726R
        '
        'XrTableRow4
        '
        Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell11, Me.XrTableCell12})
        Me.XrTableRow4.Name = "XrTableRow4"
        Me.XrTableRow4.Weight = 1.0R
        '
        'XrTableCell10
        '
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.Weight = 1.0R
        '
        'XrTableCell11
        '
        Me.XrTableCell11.Name = "XrTableCell11"
        Me.XrTableCell11.Weight = 1.0R
        '
        'XrTableCell12
        '
        Me.XrTableCell12.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell12.Name = "XrTableCell12"
        Me.XrTableCell12.StylePriority.UseFont = False
        Me.XrTableCell12.Weight = 1.0R
        '
        'tableCell13
        '
        Me.tableCell13.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tableCell13.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell13.ForeColor = System.Drawing.Color.Gray
        Me.tableCell13.Name = "tableCell13"
        Me.tableCell13.StylePriority.UseBackColor = False
        Me.tableCell13.StylePriority.UseFont = False
        Me.tableCell13.StylePriority.UseForeColor = False
        Me.tableCell13.Weight = 0.065755268329852568R
        '
        'tableCell2
        '
        Me.tableCell2.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell2.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell2.Name = "tableCell2"
        Me.tableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell2.StylePriority.UseFont = False
        Me.tableCell2.StylePriority.UseForeColor = False
        Me.tableCell2.StylePriority.UsePadding = False
        Me.tableCell2.Text = "RESTING HEART RATE"
        Me.tableCell2.Weight = 0.81939594126368787R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.StylePriority.UseFont = False
        Me.XrTableCell6.StylePriority.UseTextAlignment = False
        Me.XrTableCell6.Text = "18.5 to 27.3"
        Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell6.Weight = 0.70915996898298617R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.height")})
        Me.XrTableCell2.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.StylePriority.UseFont = False
        Me.XrTableCell2.StylePriority.UseTextAlignment = False
        Me.XrTableCell2.Text = "175"
        Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell2.Weight = 0.35746422897644459R
        '
        'tableCell3
        '
        Me.tableCell3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.pulserate")})
        Me.tableCell3.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell3.Name = "tableCell3"
        Me.tableCell3.StylePriority.UseFont = False
        Me.tableCell3.StylePriority.UseTextAlignment = False
        Me.tableCell3.Text = "76 "
        Me.tableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell3.Weight = 0.31792359661606318R
        '
        'txtHeight
        '
        Me.txtHeight.Description = "Parameter1"
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Type = GetType(Integer)
        Me.txtHeight.ValueInfo = "0"
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2, Me.XrPageInfo1, Me.XrLabel3})
        Me.TopMargin.HeightF = 47.00001!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrTableCell17
        '
        Me.XrTableCell17.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell17.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell17.Name = "XrTableCell17"
        Me.XrTableCell17.StylePriority.UseBackColor = False
        Me.XrTableCell17.StylePriority.UseBorderColor = False
        Me.XrTableCell17.Weight = 0.059418328600500649R
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell8, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell14, Me.XrTableCell15, Me.XrTableCell9})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 1.0R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.BackColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell7.BorderColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.StylePriority.UseBackColor = False
        Me.XrTableCell7.StylePriority.UseBorderColor = False
        Me.XrTableCell7.Weight = 0.059418328600500649R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell8.ForeColor = System.Drawing.Color.DimGray
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.XrTableCell8.StylePriority.UseFont = False
        Me.XrTableCell8.StylePriority.UseForeColor = False
        Me.XrTableCell8.StylePriority.UsePadding = False
        Me.XrTableCell8.Text = "HEIGHT"
        Me.XrTableCell8.Weight = 0.32260839600822888R
        '
        'XrTableCell14
        '
        Me.XrTableCell14.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell14.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell14.ForeColor = System.Drawing.SystemColors.GrayText
        Me.XrTableCell14.Name = "XrTableCell14"
        Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.XrTableCell14.StylePriority.UseBorderColor = False
        Me.XrTableCell14.StylePriority.UseFont = False
        Me.XrTableCell14.StylePriority.UseForeColor = False
        Me.XrTableCell14.StylePriority.UsePadding = False
        Me.XrTableCell14.StylePriority.UseTextAlignment = False
        Me.XrTableCell14.Text = "WEIGHT"
        Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrTableCell14.Weight = 0.33311000677345737R
        '
        'XrTableCell15
        '
        Me.XrTableCell15.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.weight")})
        Me.XrTableCell15.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell15.ForeColor = System.Drawing.Color.Black
        Me.XrTableCell15.Name = "XrTableCell15"
        Me.XrTableCell15.StylePriority.UseBorderColor = False
        Me.XrTableCell15.StylePriority.UseFont = False
        Me.XrTableCell15.StylePriority.UseForeColor = False
        Me.XrTableCell15.StylePriority.UseTextAlignment = False
        Me.XrTableCell15.Text = "100"
        Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.XrTableCell15.Weight = 0.35502956819074166R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.XrTableCell9.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell9.ForeColor = System.Drawing.Color.DimGray
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.StylePriority.UseBorderColor = False
        Me.XrTableCell9.StylePriority.UseFont = False
        Me.XrTableCell9.StylePriority.UseForeColor = False
        Me.XrTableCell9.StylePriority.UseTextAlignment = False
        Me.XrTableCell9.Text = "KG."
        Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.XrTableCell9.Weight = 1.3118603648433429R
        '
        'tableRow1
        '
        Me.tableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell1, Me.tableCell2, Me.tableCell3, Me.tableCell12, Me.tableCell14, Me.tableCell4, Me.tableCell5})
        Me.tableRow1.Name = "tableRow1"
        Me.tableRow1.Weight = 1.0R
        '
        'tableCell1
        '
        Me.tableCell1.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell1.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell1.Name = "tableCell1"
        Me.tableCell1.StylePriority.UseBackColor = False
        Me.tableCell1.StylePriority.UseBorderColor = False
        Me.tableCell1.Weight = 0.059418317314414729R
        '
        'tableCell14
        '
        Me.tableCell14.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tableCell14.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell14.ForeColor = System.Drawing.Color.Gray
        Me.tableCell14.Name = "tableCell14"
        Me.tableCell14.StylePriority.UseBackColor = False
        Me.tableCell14.StylePriority.UseFont = False
        Me.tableCell14.StylePriority.UseForeColor = False
        Me.tableCell14.Weight = 0.06575544890722701R
        '
        'tableCell4
        '
        Me.tableCell4.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell4.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell4.Name = "tableCell4"
        Me.tableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell4.StylePriority.UseFont = False
        Me.tableCell4.StylePriority.UseForeColor = False
        Me.tableCell4.StylePriority.UsePadding = False
        Me.tableCell4.Text = "HEALTHY RANGE"
        Me.tableCell4.Weight = 0.6982405515433342R
        '
        'tableCell5
        '
        Me.tableCell5.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.tableCell5.Name = "tableCell5"
        Me.tableCell5.StylePriority.UseFont = False
        Me.tableCell5.StylePriority.UseTextAlignment = False
        Me.tableCell5.Text = "60 to 100 bpm "
        Me.tableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell5.Weight = 0.70915994641081437R
        '
        'tableCell51
        '
        Me.tableCell51.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell51.Name = "tableCell51"
        Me.tableCell51.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell51.StylePriority.UseFont = False
        Me.tableCell51.StylePriority.UsePadding = False
        Me.tableCell51.StylePriority.UseTextAlignment = False
        Me.tableCell51.Text = "Color Blindess"
        Me.tableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell51.Weight = 0.50819623792493673R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.BackColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell1.BorderColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell1.BorderWidth = 2.0!
        Me.XrTableCell1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell1.Multiline = True
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.RowSpan = 5
        Me.XrTableCell1.StyleName = "XrControlStyle1"
        Me.XrTableCell1.StylePriority.UseBackColor = False
        Me.XrTableCell1.StylePriority.UseBorderColor = False
        Me.XrTableCell1.StylePriority.UseBorders = False
        Me.XrTableCell1.StylePriority.UseBorderWidth = False
        Me.XrTableCell1.StylePriority.UseFont = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "การตรวจร่างกาย (Physical Examination)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell1.Weight = 3.0R
        '
        'txtEyeL
        '
        Me.txtEyeL.Description = "Parameter1"
        Me.txtEyeL.Name = "txtEyeL"
        Me.txtEyeL.Type = GetType(Integer)
        Me.txtEyeL.ValueInfo = "0"
        '
        'tableCell18
        '
        Me.tableCell18.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell18.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell18.Name = "tableCell18"
        Me.tableCell18.StylePriority.UseFont = False
        Me.tableCell18.StylePriority.UseForeColor = False
        Me.tableCell18.StylePriority.UseTextAlignment = False
        Me.tableCell18.Text = "mmHG"
        Me.tableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.tableCell18.Weight = 0.33010637438567475R
        '
        'tableRow14
        '
        Me.tableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell49, Me.tableCell51, Me.tableCell48})
        Me.tableRow14.Name = "tableRow14"
        Me.tableRow14.Weight = 1.0R
        '
        'tableCell49
        '
        Me.tableCell49.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell49.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell49.Name = "tableCell49"
        Me.tableCell49.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell49.StylePriority.UseBackColor = False
        Me.tableCell49.StylePriority.UseFont = False
        Me.tableCell49.StylePriority.UsePadding = False
        Me.tableCell49.StylePriority.UseTextAlignment = False
        Me.tableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell49.Weight = 0.059418343208931468R
        '
        'tableCell48
        '
        Me.tableCell48.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.blindness_result")})
        Me.tableCell48.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell48.Name = "tableCell48"
        Me.tableCell48.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell48.StylePriority.UseFont = False
        Me.tableCell48.StylePriority.UsePadding = False
        Me.tableCell48.StylePriority.UseTextAlignment = False
        Me.tableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell48.Weight = 2.432385418866132R
        '
        'txtWeight
        '
        Me.txtWeight.Description = "Parameter1"
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Type = GetType(Integer)
        Me.txtWeight.ValueInfo = "0"
        '
        'BottomMargin
        '
        Me.BottomMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1})
        Me.BottomMargin.HeightF = 34.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.CalculatedField1")})
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(295.5517!, 23.0!)
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.hn")})
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'tableRow4
        '
        Me.tableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell22, Me.tableCell28})
        Me.tableRow4.Name = "tableRow4"
        Me.tableRow4.Weight = 1.0R
        '
        'tableCell22
        '
        Me.tableCell22.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell22.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell22.Name = "tableCell22"
        Me.tableCell22.StylePriority.UseBackColor = False
        Me.tableCell22.StylePriority.UseBorderColor = False
        Me.tableCell22.Weight = 0.059418317314414729R
        '
        'tableCell28
        '
        Me.tableCell28.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.tableCell28.Name = "tableCell28"
        Me.tableCell28.StylePriority.UseFont = False
        Me.tableCell28.StylePriority.UseTextAlignment = False
        Me.tableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell28.Weight = 2.9405816826855853R
        '
        'tableCell15
        '
        Me.tableCell15.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell15.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell15.Name = "tableCell15"
        Me.tableCell15.StylePriority.UseBackColor = False
        Me.tableCell15.StylePriority.UseBorderColor = False
        Me.tableCell15.Weight = 0.059418317314414729R
        '
        'XrTableCell16
        '
        Me.XrTableCell16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.bmi")})
        Me.XrTableCell16.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.XrTableCell16.Name = "XrTableCell16"
        Me.XrTableCell16.StylePriority.UseFont = False
        Me.XrTableCell16.StylePriority.UseTextAlignment = False
        Me.XrTableCell16.Text = "26.3"
        Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell16.Weight = 0.31792359661606318R
        '
        'tableCell54
        '
        Me.tableCell54.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell54.Name = "tableCell54"
        Me.tableCell54.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell54.StylePriority.UseFont = False
        Me.tableCell54.StylePriority.UsePadding = False
        Me.tableCell54.StylePriority.UseTextAlignment = False
        Me.tableCell54.Text = "( ตาบอดสี )"
        Me.tableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell54.Weight = 0.50819623792493673R
        '
        'XrTableRow5
        '
        Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell17, Me.XrTableCell22})
        Me.XrTableRow5.Name = "XrTableRow5"
        Me.XrTableRow5.Weight = 1.0R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.XrTableCell18.StylePriority.UseFont = False
        Me.XrTableCell18.StylePriority.UsePadding = False
        Me.XrTableCell18.StylePriority.UseTextAlignment = False
        Me.XrTableCell18.Text = "Vision"
        Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrTableCell18.Weight = 0.50819620668651244R
        '
        'tableCell55
        '
        Me.tableCell55.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell55.Name = "tableCell55"
        Me.tableCell55.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell55.StylePriority.UseFont = False
        Me.tableCell55.StylePriority.UsePadding = False
        Me.tableCell55.StylePriority.UseTextAlignment = False
        Me.tableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell55.Weight = 2.432385418866132R
        '
        'XrTable1
        '
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow5, Me.XrTableRow2, Me.tableRow11, Me.tableRow1, Me.tableRow2, Me.tableRow3, Me.tableRow4, Me.tableRow6, Me.tableRow7, Me.tableRow8, Me.tableRow12, Me.tableRow9, Me.tableRow10, Me.tableRow14, Me.tableRow15, Me.tableRow13, Me.tableRow16, Me.tableRow17, Me.tableRow5})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(518.0!, 550.0!)
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell16, Me.tableCell11, Me.tableCell13, Me.XrTableCell13, Me.XrTableCell6})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.BackColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell4.BorderColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.StylePriority.UseBackColor = False
        Me.XrTableCell4.StylePriority.UseBorderColor = False
        Me.XrTableCell4.Weight = 0.059418317314414729R
        '
        'tableRow2
        '
        Me.tableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell6, Me.tableCell7, Me.tableCell8, Me.tableCell9, Me.tableCell10})
        Me.tableRow2.Name = "tableRow2"
        Me.tableRow2.Weight = 1.0R
        '
        'tableCell7
        '
        Me.tableCell7.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell7.ForeColor = System.Drawing.Color.Gray
        Me.tableCell7.Name = "tableCell7"
        Me.tableCell7.StylePriority.UseFont = False
        Me.tableCell7.StylePriority.UseForeColor = False
        Me.tableCell7.Weight = 0.680072515885843R
        '
        'tableCell8
        '
        Me.tableCell8.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell8.Name = "tableCell8"
        Me.tableCell8.StylePriority.UseFont = False
        Me.tableCell8.StylePriority.UseTextAlignment = False
        Me.tableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell8.Weight = 0.593619105856684R
        '
        'tableCell10
        '
        Me.tableCell10.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell10.Name = "tableCell10"
        Me.tableCell10.StylePriority.UseFont = False
        Me.tableCell10.StylePriority.UseTextAlignment = False
        Me.tableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell10.Weight = 1.0000000601924581R
        '
        'tableRow3
        '
        Me.tableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell15, Me.tableCell16, Me.tableCell17, Me.tableCell18, Me.tableCell21, Me.tableCell20, Me.tableCell19})
        Me.tableRow3.Name = "tableRow3"
        Me.tableRow3.Weight = 1.0R
        '
        'tableCell21
        '
        Me.tableCell21.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tableCell21.Font = New System.Drawing.Font("Times New Roman", 14.25!)
        Me.tableCell21.Name = "tableCell21"
        Me.tableCell21.StylePriority.UseBackColor = False
        Me.tableCell21.StylePriority.UseFont = False
        Me.tableCell21.StylePriority.UseTextAlignment = False
        Me.tableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell21.Weight = 0.065755019636355827R
        '
        'tableCell20
        '
        Me.tableCell20.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell20.ForeColor = System.Drawing.Color.DimGray
        Me.tableCell20.Name = "tableCell20"
        Me.tableCell20.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100.0!)
        Me.tableCell20.StylePriority.UseFont = False
        Me.tableCell20.StylePriority.UseForeColor = False
        Me.tableCell20.StylePriority.UsePadding = False
        Me.tableCell20.StylePriority.UseTextAlignment = False
        Me.tableCell20.Text = "HEALTHY RANGE"
        Me.tableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell20.Weight = 0.69824081069911226R
        '
        'tableCell19
        '
        Me.tableCell19.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.tableCell19.Name = "tableCell19"
        Me.tableCell19.StylePriority.UseFont = False
        Me.tableCell19.StylePriority.UseTextAlignment = False
        Me.tableCell19.Text = "< 120/80 mmHg"
        Me.tableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell19.Weight = 0.70915998427037386R
        '
        'tableRow7
        '
        Me.tableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell23, Me.XrTableCell18, Me.tableCell26})
        Me.tableRow7.Name = "tableRow7"
        Me.tableRow7.Weight = 1.0R
        '
        'tableCell23
        '
        Me.tableCell23.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell23.BorderColor = System.Drawing.SystemColors.Highlight
        Me.tableCell23.Name = "tableCell23"
        Me.tableCell23.StylePriority.UseBackColor = False
        Me.tableCell23.StylePriority.UseBorderColor = False
        Me.tableCell23.Weight = 0.059418317314414729R
        '
        'tableRow15
        '
        Me.tableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell53, Me.tableCell54, Me.tableCell55})
        Me.tableRow15.Name = "tableRow15"
        Me.tableRow15.Weight = 1.0R
        '
        'tableRow5
        '
        Me.tableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell29, Me.tableCell31, Me.tableCell59})
        Me.tableRow5.Name = "tableRow5"
        Me.tableRow5.Weight = 1.0R
        '
        'tableCell29
        '
        Me.tableCell29.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell29.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tableCell29.Name = "tableCell29"
        Me.tableCell29.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell29.StylePriority.UseBackColor = False
        Me.tableCell29.StylePriority.UseFont = False
        Me.tableCell29.StylePriority.UsePadding = False
        Me.tableCell29.StylePriority.UseTextAlignment = False
        Me.tableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.tableCell29.Weight = 0.059418343208931468R
        '
        'txtHeartRate
        '
        Me.txtHeartRate.Description = "Parameter1"
        Me.txtHeartRate.Name = "txtHeartRate"
        Me.txtHeartRate.Type = GetType(Integer)
        Me.txtHeartRate.ValueInfo = "0"
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'txtEyeR
        '
        Me.txtEyeR.Description = "Parameter1"
        Me.txtEyeR.Name = "txtEyeR"
        Me.txtEyeR.Type = GetType(Integer)
        Me.txtEyeR.ValueInfo = "0"
        '
        'txtBmi
        '
        Me.txtBmi.Description = "Parameter1"
        Me.txtBmi.Name = "txtBmi"
        Me.txtBmi.Type = GetType(Integer)
        Me.txtBmi.ValueInfo = "0"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.HeightF = 557.759!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'txtColorBlind
        '
        Me.txtColorBlind.Description = "Parameter1"
        Me.txtColorBlind.Name = "txtColorBlind"
        Me.txtColorBlind.Type = GetType(Integer)
        Me.txtColorBlind.ValueInfo = "0"
        '
        'DtCheckUP1
        '
        Me.DtCheckUP1.DataSetName = "dtCheckUP"
        Me.DtCheckUP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CalculatedField1
        '
        Me.CalculatedField1.DataMember = "frncheckup"
        Me.CalculatedField1.DisplayName = "ชื่อนามสกุล"
        Me.CalculatedField1.Expression = "[stprename]+' '+[name] +' '+ [lastname]"
        Me.CalculatedField1.Name = "CalculatedField1"
        '
        'XrLabel3
        '
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0!, 24.00001!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(151.7419!, 23.0!)
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Medical Examination"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(151.7419!, 24.00001!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(266.2581!, 23.0!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(418.0!, 24.00001!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'rptCheckUpA5_1
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField1})
        Me.DataMember = "frncheckup"
        Me.DataSource = Me.DtCheckUP1
        Me.Margins = New System.Drawing.Printing.Margins(32, 32, 47, 34)
        Me.PageHeight = 827
        Me.PageWidth = 583
        Me.PaperKind = System.Drawing.Printing.PaperKind.A5
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.txtHeight, Me.txtWeight, Me.txtBmi, Me.txtHeartRate, Me.txtBloodPressure, Me.txtEyeR, Me.txtEyeL, Me.txtColorBlind})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "15.1"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents tableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrCheckBox2 As DevExpress.XtraReports.UI.XRCheckBox
    Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell31 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell25 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell52 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell46 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell41 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell45 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell53 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell24 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrCheckBox1 As DevExpress.XtraReports.UI.XRCheckBox
    Friend WithEvents tableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell36 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell39 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents txtBloodPressure As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents tableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell56 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell57 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell58 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell61 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell60 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell62 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell50 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell59 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell40 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell37 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents txtHeight As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents XrTableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell51 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents txtEyeL As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents tableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents txtWeight As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents tableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell28 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell54 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell55 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell21 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell23 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell29 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents txtHeartRate As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents txtEyeR As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents txtBmi As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents txtColorBlind As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents DtCheckUP1 As dtCheckUP
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents CalculatedField1 As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
End Class
