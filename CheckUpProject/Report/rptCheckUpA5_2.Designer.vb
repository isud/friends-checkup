﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rptCheckUpA5_2
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tableCell76 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell92 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell108 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell81 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell40 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell31 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell36 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell22 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell23 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell17 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell32 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell21 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell37 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell33 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell24 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell25 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell27 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell39 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell28 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell29 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell49 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell48 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell50 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell41 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell43 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell44 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell45 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell46 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell47 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow15 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell77 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell78 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell79 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell80 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell82 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell83 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow14 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell68 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell69 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell70 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell71 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell72 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell73 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell74 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell75 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell60 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell61 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell62 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell63 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell64 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell65 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell66 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell67 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell52 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell53 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell54 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell55 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell56 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell57 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell58 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell59 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow17 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell51 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell105 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow18 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell99 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow16 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell91 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell97 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow19 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell35 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell84 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell85 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell87 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell88 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell89 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow21 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell103 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell104 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell106 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell109 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell110 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableRow20 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.tableCell93 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell94 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.tableCell101 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DtCheckUP1 = New CheckUpProject.dtCheckUP()
        Me.CalculatedField1 = New DevExpress.XtraReports.UI.CalculatedField()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'tableCell76
        '
        Me.tableCell76.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell76.ForeColor = System.Drawing.Color.Gray
        Me.tableCell76.Name = "tableCell76"
        Me.tableCell76.StylePriority.UseBackColor = False
        Me.tableCell76.StylePriority.UseForeColor = False
        Me.tableCell76.Weight = 0.068529753757240991R
        '
        'tableCell92
        '
        Me.tableCell92.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell92.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tableCell92.ForeColor = System.Drawing.Color.Black
        Me.tableCell92.Name = "tableCell92"
        Me.tableCell92.StylePriority.UseBackColor = False
        Me.tableCell92.StylePriority.UseFont = False
        Me.tableCell92.StylePriority.UseForeColor = False
        Me.tableCell92.StylePriority.UseTextAlignment = False
        Me.tableCell92.Text = "Stool Examination การตรวจอุจจาระ"
        Me.tableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell92.Weight = 3.0R
        '
        'tableCell108
        '
        Me.tableCell108.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell108.ForeColor = System.Drawing.Color.Gray
        Me.tableCell108.Name = "tableCell108"
        Me.tableCell108.StylePriority.UseBackColor = False
        Me.tableCell108.StylePriority.UseForeColor = False
        Me.tableCell108.Weight = 0.075605499852681057R
        '
        'tableCell81
        '
        Me.tableCell81.ForeColor = System.Drawing.Color.Gray
        Me.tableCell81.Name = "tableCell81"
        Me.tableCell81.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell81.StylePriority.UseForeColor = False
        Me.tableCell81.StylePriority.UsePadding = False
        Me.tableCell81.Text = "pH"
        Me.tableCell81.Weight = 0.55487357072193333R
        '
        'tableCell40
        '
        Me.tableCell40.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell40.ForeColor = System.Drawing.Color.Gray
        Me.tableCell40.Name = "tableCell40"
        Me.tableCell40.StylePriority.UseBackColor = False
        Me.tableCell40.StylePriority.UseForeColor = False
        Me.tableCell40.Weight = 0.068529753757240991R
        '
        'tableRow3
        '
        Me.tableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell31, Me.tableCell10, Me.tableCell20, Me.tableCell11, Me.tableCell36, Me.tableCell12, Me.tableCell15, Me.tableCell13})
        Me.tableRow3.Name = "tableRow3"
        Me.tableRow3.Weight = 1.0R
        '
        'tableCell31
        '
        Me.tableCell31.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell31.ForeColor = System.Drawing.Color.Gray
        Me.tableCell31.Name = "tableCell31"
        Me.tableCell31.StylePriority.UseBackColor = False
        Me.tableCell31.StylePriority.UseForeColor = False
        Me.tableCell31.Weight = 0.068529730914741044R
        '
        'tableCell10
        '
        Me.tableCell10.ForeColor = System.Drawing.Color.Gray
        Me.tableCell10.Name = "tableCell10"
        Me.tableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell10.StylePriority.UseForeColor = False
        Me.tableCell10.StylePriority.UsePadding = False
        Me.tableCell10.Text = "WBC"
        Me.tableCell10.Weight = 0.53296179601014015R
        '
        'tableCell20
        '
        Me.tableCell20.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.wbc")})
        Me.tableCell20.Name = "tableCell20"
        Me.tableCell20.Weight = 0.29597659210245231R
        '
        'tableCell11
        '
        Me.tableCell11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell11.Name = "tableCell11"
        Me.tableCell11.StylePriority.UseForeColor = False
        Me.tableCell11.StylePriority.UseTextAlignment = False
        Me.tableCell11.Text = "10^3 cells/mm^3"
        Me.tableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell11.Weight = 0.60591386537921865R
        '
        'tableCell36
        '
        Me.tableCell36.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell36.ForeColor = System.Drawing.Color.Gray
        Me.tableCell36.Name = "tableCell36"
        Me.tableCell36.StylePriority.UseBackColor = False
        Me.tableCell36.StylePriority.UseForeColor = False
        Me.tableCell36.Text = " "
        Me.tableCell36.Weight = 0.075605850104347533R
        '
        'tableCell12
        '
        Me.tableCell12.ForeColor = System.Drawing.Color.Gray
        Me.tableCell12.Name = "tableCell12"
        Me.tableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell12.StylePriority.UseForeColor = False
        Me.tableCell12.StylePriority.UsePadding = False
        Me.tableCell12.Text = "Lymphocytes"
        Me.tableCell12.Weight = 0.55487357072193322R
        '
        'tableCell15
        '
        Me.tableCell15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Lymphocytes")})
        Me.tableCell15.Name = "tableCell15"
        Me.tableCell15.Weight = 0.32121469166249345R
        '
        'tableCell13
        '
        Me.tableCell13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell13.Name = "tableCell13"
        Me.tableCell13.StylePriority.UseForeColor = False
        Me.tableCell13.StylePriority.UseTextAlignment = False
        Me.tableCell13.Text = "%"
        Me.tableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell13.Weight = 0.54492390310467342R
        '
        'XrTable1
        '
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3, Me.XrTableRow2, Me.tableRow5, Me.tableRow4, Me.tableRow3, Me.tableRow2, Me.tableRow1, Me.tableRow6, Me.tableRow9, Me.tableRow11, Me.tableRow8, Me.tableRow15, Me.tableRow14, Me.tableRow13, Me.tableRow12, Me.tableRow17, Me.tableRow18, Me.tableRow16, Me.tableRow19, Me.tableRow7, Me.tableRow21, Me.tableRow20})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(501.0!, 550.0!)
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 1.0R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.BackColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.StylePriority.UseBackColor = False
        Me.XrTableCell7.StylePriority.UseFont = False
        Me.XrTableCell7.StylePriority.UseTextAlignment = False
        Me.XrTableCell7.Text = "Blood Examination ตรวจเลือด"
        Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell7.Weight = 3.0R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.tableCell1, Me.tableCell14, Me.tableCell22, Me.tableCell23, Me.XrTableCell6})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.BackColor = System.Drawing.SystemColors.Highlight
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.StylePriority.UseBackColor = False
        Me.XrTableCell4.Weight = 0.068529723300574386R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.ForeColor = System.Drawing.Color.Gray
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.XrTableCell5.StylePriority.UseForeColor = False
        Me.XrTableCell5.StylePriority.UsePadding = False
        Me.XrTableCell5.Text = "Blood Group ( กลุ่มเลือด )"
        Me.XrTableCell5.Weight = 0.93147021578609235R
        '
        'tableCell1
        '
        Me.tableCell1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.bloodGroup")})
        Me.tableCell1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell1.Multiline = True
        Me.tableCell1.Name = "tableCell1"
        Me.tableCell1.StylePriority.UseFont = False
        Me.tableCell1.StylePriority.UseTextAlignment = False
        Me.tableCell1.Text = "A" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.tableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.tableCell1.Weight = 0.50338217476071878R
        '
        'tableCell14
        '
        Me.tableCell14.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell14.Name = "tableCell14"
        Me.tableCell14.StylePriority.UseBackColor = False
        Me.tableCell14.Weight = 0.075605840586639328R
        '
        'tableCell22
        '
        Me.tableCell22.ForeColor = System.Drawing.Color.Gray
        Me.tableCell22.Name = "tableCell22"
        Me.tableCell22.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell22.StylePriority.UseForeColor = False
        Me.tableCell22.StylePriority.UsePadding = False
        Me.tableCell22.Text = "Rhesus ( เลือด Rh )"
        Me.tableCell22.Weight = 0.79638639987702819R
        '
        'tableCell23
        '
        Me.tableCell23.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.bloodRh")})
        Me.tableCell23.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold)
        Me.tableCell23.Name = "tableCell23"
        Me.tableCell23.StylePriority.UseFont = False
        Me.tableCell23.StylePriority.UseTextAlignment = False
        Me.tableCell23.Text = "Negative"
        Me.tableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        Me.tableCell23.Weight = 0.56474534731777593R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Weight = 0.059880298371171259R
        '
        'tableRow5
        '
        Me.tableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell19})
        Me.tableRow5.Name = "tableRow5"
        Me.tableRow5.Weight = 1.0R
        '
        'tableCell19
        '
        Me.tableCell19.Name = "tableCell19"
        Me.tableCell19.Weight = 3.0R
        '
        'tableRow4
        '
        Me.tableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell17})
        Me.tableRow4.Name = "tableRow4"
        Me.tableRow4.Weight = 1.0R
        '
        'tableCell17
        '
        Me.tableCell17.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tableCell17.Name = "tableCell17"
        Me.tableCell17.StylePriority.UseBackColor = False
        Me.tableCell17.StylePriority.UseFont = False
        Me.tableCell17.StylePriority.UseTextAlignment = False
        Me.tableCell17.Text = "CBC ความเข้มข้นของเลือดและเม็ดเลือด"
        Me.tableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell17.Weight = 3.0R
        '
        'tableRow2
        '
        Me.tableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell32, Me.tableCell6, Me.tableCell21, Me.tableCell7, Me.tableCell37, Me.tableCell8, Me.tableCell16, Me.tableCell9})
        Me.tableRow2.Name = "tableRow2"
        Me.tableRow2.Weight = 1.0R
        '
        'tableCell32
        '
        Me.tableCell32.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell32.ForeColor = System.Drawing.Color.Gray
        Me.tableCell32.Name = "tableCell32"
        Me.tableCell32.StylePriority.UseBackColor = False
        Me.tableCell32.StylePriority.UseForeColor = False
        Me.tableCell32.Weight = 0.068529776599741R
        '
        'tableCell6
        '
        Me.tableCell6.ForeColor = System.Drawing.Color.Gray
        Me.tableCell6.Name = "tableCell6"
        Me.tableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell6.StylePriority.UseForeColor = False
        Me.tableCell6.StylePriority.UsePadding = False
        Me.tableCell6.Text = "Hct"
        Me.tableCell6.Weight = 0.53296170464014014R
        '
        'tableCell21
        '
        Me.tableCell21.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Hct")})
        Me.tableCell21.Name = "tableCell21"
        Me.tableCell21.Weight = 0.29597677484245227R
        '
        'tableCell7
        '
        Me.tableCell7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell7.Multiline = True
        Me.tableCell7.Name = "tableCell7"
        Me.tableCell7.StylePriority.UseForeColor = False
        Me.tableCell7.StylePriority.UseTextAlignment = False
        Me.tableCell7.Text = "%" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.tableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell7.Weight = 0.60591386537921865R
        '
        'tableCell37
        '
        Me.tableCell37.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell37.ForeColor = System.Drawing.Color.Gray
        Me.tableCell37.Name = "tableCell37"
        Me.tableCell37.StylePriority.UseBackColor = False
        Me.tableCell37.StylePriority.UseForeColor = False
        Me.tableCell37.Text = " "
        Me.tableCell37.Weight = 0.075605667364347734R
        '
        'tableCell8
        '
        Me.tableCell8.ForeColor = System.Drawing.Color.Gray
        Me.tableCell8.Name = "tableCell8"
        Me.tableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell8.StylePriority.UseForeColor = False
        Me.tableCell8.StylePriority.UsePadding = False
        Me.tableCell8.Text = "Monocytes"
        Me.tableCell8.Weight = 0.55487357072193333R
        '
        'tableCell16
        '
        Me.tableCell16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Monocytes")})
        Me.tableCell16.Name = "tableCell16"
        Me.tableCell16.Weight = 0.32121501145749343R
        '
        'tableCell9
        '
        Me.tableCell9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell9.Name = "tableCell9"
        Me.tableCell9.StylePriority.UseForeColor = False
        Me.tableCell9.StylePriority.UseTextAlignment = False
        Me.tableCell9.Text = "%"
        Me.tableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell9.Weight = 0.54492362899467339R
        '
        'tableRow1
        '
        Me.tableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell33, Me.tableCell2, Me.tableCell24, Me.tableCell3, Me.tableCell38, Me.tableCell4, Me.tableCell18, Me.tableCell5})
        Me.tableRow1.Name = "tableRow1"
        Me.tableRow1.Weight = 1.0R
        '
        'tableCell33
        '
        Me.tableCell33.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell33.ForeColor = System.Drawing.Color.Gray
        Me.tableCell33.Name = "tableCell33"
        Me.tableCell33.StylePriority.UseBackColor = False
        Me.tableCell33.StylePriority.UseForeColor = False
        Me.tableCell33.Weight = 0.068529708072241041R
        '
        'tableCell2
        '
        Me.tableCell2.ForeColor = System.Drawing.Color.Gray
        Me.tableCell2.Name = "tableCell2"
        Me.tableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell2.StylePriority.UseForeColor = False
        Me.tableCell2.StylePriority.UsePadding = False
        Me.tableCell2.Text = "Hb"
        Me.tableCell2.Weight = 0.53296191022264006R
        '
        'tableCell24
        '
        Me.tableCell24.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Hgb")})
        Me.tableCell24.Name = "tableCell24"
        Me.tableCell24.Weight = 0.29597668347245232R
        '
        'tableCell3
        '
        Me.tableCell3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell3.Name = "tableCell3"
        Me.tableCell3.StylePriority.UseForeColor = False
        Me.tableCell3.StylePriority.UseTextAlignment = False
        Me.tableCell3.Text = "gm%"
        Me.tableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell3.Weight = 0.60591377400921864R
        '
        'tableCell38
        '
        Me.tableCell38.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell38.ForeColor = System.Drawing.Color.Gray
        Me.tableCell38.Name = "tableCell38"
        Me.tableCell38.StylePriority.UseBackColor = False
        Me.tableCell38.StylePriority.UseForeColor = False
        Me.tableCell38.Text = " "
        Me.tableCell38.Weight = 0.075605667364347734R
        '
        'tableCell4
        '
        Me.tableCell4.ForeColor = System.Drawing.Color.Gray
        Me.tableCell4.Name = "tableCell4"
        Me.tableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell4.StylePriority.UseForeColor = False
        Me.tableCell4.StylePriority.UsePadding = False
        Me.tableCell4.Text = "Eosinophils"
        Me.tableCell4.Weight = 0.55487357072193333R
        '
        'tableCell18
        '
        Me.tableCell18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Eosionophils")})
        Me.tableCell18.Name = "tableCell18"
        Me.tableCell18.Weight = 0.32121505714249338R
        '
        'tableCell5
        '
        Me.tableCell5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell5.Name = "tableCell5"
        Me.tableCell5.StylePriority.UseForeColor = False
        Me.tableCell5.StylePriority.UseTextAlignment = False
        Me.tableCell5.Text = "%"
        Me.tableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell5.Weight = 0.54492362899467339R
        '
        'tableRow6
        '
        Me.tableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell34, Me.tableCell25, Me.tableCell26, Me.tableCell27, Me.tableCell39, Me.tableCell28, Me.tableCell29, Me.tableCell30})
        Me.tableRow6.Name = "tableRow6"
        Me.tableRow6.Weight = 1.0R
        '
        'tableCell34
        '
        Me.tableCell34.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell34.ForeColor = System.Drawing.Color.Gray
        Me.tableCell34.Name = "tableCell34"
        Me.tableCell34.StylePriority.UseBackColor = False
        Me.tableCell34.StylePriority.UseForeColor = False
        Me.tableCell34.Weight = 0.068529753757240991R
        '
        'tableCell25
        '
        Me.tableCell25.ForeColor = System.Drawing.Color.Gray
        Me.tableCell25.Name = "tableCell25"
        Me.tableCell25.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell25.StylePriority.UseForeColor = False
        Me.tableCell25.StylePriority.UsePadding = False
        Me.tableCell25.Text = "Bashophils "
        Me.tableCell25.Weight = 0.53296186453764R
        '
        'tableCell26
        '
        Me.tableCell26.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Bashophils")})
        Me.tableCell26.Name = "tableCell26"
        Me.tableCell26.Weight = 0.29597668347245232R
        '
        'tableCell27
        '
        Me.tableCell27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell27.Name = "tableCell27"
        Me.tableCell27.StylePriority.UseForeColor = False
        Me.tableCell27.StylePriority.UseTextAlignment = False
        Me.tableCell27.Text = "gm%"
        Me.tableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell27.Weight = 0.60591377400921864R
        '
        'tableCell39
        '
        Me.tableCell39.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell39.ForeColor = System.Drawing.Color.Gray
        Me.tableCell39.Name = "tableCell39"
        Me.tableCell39.StylePriority.UseBackColor = False
        Me.tableCell39.StylePriority.UseForeColor = False
        Me.tableCell39.Text = " "
        Me.tableCell39.Weight = 0.075605667364347734R
        '
        'tableCell28
        '
        Me.tableCell28.ForeColor = System.Drawing.Color.Gray
        Me.tableCell28.Name = "tableCell28"
        Me.tableCell28.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell28.StylePriority.UseForeColor = False
        Me.tableCell28.StylePriority.UsePadding = False
        Me.tableCell28.Text = "Neurophils "
        Me.tableCell28.Weight = 0.55487357072193333R
        '
        'tableCell29
        '
        Me.tableCell29.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.Neutrophils")})
        Me.tableCell29.Name = "tableCell29"
        Me.tableCell29.Weight = 0.32121505714249338R
        '
        'tableCell30
        '
        Me.tableCell30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell30.Name = "tableCell30"
        Me.tableCell30.StylePriority.UseForeColor = False
        Me.tableCell30.StylePriority.UseTextAlignment = False
        Me.tableCell30.Text = "%"
        Me.tableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell30.Weight = 0.54492362899467339R
        '
        'tableRow9
        '
        Me.tableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell49, Me.tableCell48})
        Me.tableRow9.Name = "tableRow9"
        Me.tableRow9.Weight = 1.0R
        '
        'tableCell49
        '
        Me.tableCell49.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell49.ForeColor = System.Drawing.Color.Gray
        Me.tableCell49.Name = "tableCell49"
        Me.tableCell49.StylePriority.UseBackColor = False
        Me.tableCell49.StylePriority.UseForeColor = False
        Me.tableCell49.Weight = 0.068529784213907652R
        '
        'tableCell48
        '
        Me.tableCell48.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.tableCell48.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.blood_result")})
        Me.tableCell48.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell48.ForeColor = System.Drawing.Color.Black
        Me.tableCell48.Name = "tableCell48"
        Me.tableCell48.StylePriority.UseBackColor = False
        Me.tableCell48.StylePriority.UseFont = False
        Me.tableCell48.StylePriority.UseForeColor = False
        Me.tableCell48.Weight = 2.9314702157860921R
        '
        'tableRow11
        '
        Me.tableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell50})
        Me.tableRow11.Name = "tableRow11"
        Me.tableRow11.Weight = 1.0R
        '
        'tableCell50
        '
        Me.tableCell50.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell50.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tableCell50.ForeColor = System.Drawing.Color.Black
        Me.tableCell50.Name = "tableCell50"
        Me.tableCell50.StylePriority.UseBackColor = False
        Me.tableCell50.StylePriority.UseFont = False
        Me.tableCell50.StylePriority.UseForeColor = False
        Me.tableCell50.StylePriority.UseTextAlignment = False
        Me.tableCell50.Text = "Urine Analysis  การตรวจปัสสาวะ"
        Me.tableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell50.Weight = 3.0R
        '
        'tableRow8
        '
        Me.tableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell40, Me.tableCell41, Me.tableCell42, Me.tableCell43, Me.tableCell44, Me.tableCell45, Me.tableCell46, Me.tableCell47})
        Me.tableRow8.Name = "tableRow8"
        Me.tableRow8.Weight = 1.0R
        '
        'tableCell41
        '
        Me.tableCell41.ForeColor = System.Drawing.Color.Gray
        Me.tableCell41.Name = "tableCell41"
        Me.tableCell41.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell41.StylePriority.UseForeColor = False
        Me.tableCell41.StylePriority.UsePadding = False
        Me.tableCell41.Text = "Color"
        Me.tableCell41.Weight = 0.53296186453764R
        '
        'tableCell42
        '
        Me.tableCell42.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_color")})
        Me.tableCell42.Name = "tableCell42"
        Me.tableCell42.Weight = 0.29597668347245232R
        '
        'tableCell43
        '
        Me.tableCell43.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell43.Name = "tableCell43"
        Me.tableCell43.StylePriority.UseForeColor = False
        Me.tableCell43.StylePriority.UseTextAlignment = False
        Me.tableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell43.Weight = 0.60591377400921864R
        '
        'tableCell44
        '
        Me.tableCell44.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell44.ForeColor = System.Drawing.Color.Gray
        Me.tableCell44.Name = "tableCell44"
        Me.tableCell44.StylePriority.UseBackColor = False
        Me.tableCell44.StylePriority.UseForeColor = False
        Me.tableCell44.Weight = 0.075605667364347734R
        '
        'tableCell45
        '
        Me.tableCell45.ForeColor = System.Drawing.Color.Gray
        Me.tableCell45.Name = "tableCell45"
        Me.tableCell45.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell45.StylePriority.UseForeColor = False
        Me.tableCell45.StylePriority.UsePadding = False
        Me.tableCell45.Text = "Appearance"
        Me.tableCell45.Weight = 0.55487357072193333R
        '
        'tableCell46
        '
        Me.tableCell46.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_appearance")})
        Me.tableCell46.Name = "tableCell46"
        Me.tableCell46.Weight = 0.32121505714249338R
        '
        'tableCell47
        '
        Me.tableCell47.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell47.Name = "tableCell47"
        Me.tableCell47.StylePriority.UseForeColor = False
        Me.tableCell47.StylePriority.UseTextAlignment = False
        Me.tableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell47.Weight = 0.54492362899467339R
        '
        'tableRow15
        '
        Me.tableRow15.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell76, Me.tableCell77, Me.tableCell78, Me.tableCell79, Me.tableCell80, Me.tableCell81, Me.tableCell82, Me.tableCell83})
        Me.tableRow15.Name = "tableRow15"
        Me.tableRow15.Weight = 1.0R
        '
        'tableCell77
        '
        Me.tableCell77.ForeColor = System.Drawing.Color.Gray
        Me.tableCell77.Name = "tableCell77"
        Me.tableCell77.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell77.StylePriority.UseForeColor = False
        Me.tableCell77.StylePriority.UsePadding = False
        Me.tableCell77.Text = "SpGr"
        Me.tableCell77.Weight = 0.53296186453764R
        '
        'tableCell78
        '
        Me.tableCell78.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_spgr")})
        Me.tableCell78.Name = "tableCell78"
        Me.tableCell78.Weight = 0.29597668347245232R
        '
        'tableCell79
        '
        Me.tableCell79.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell79.Name = "tableCell79"
        Me.tableCell79.StylePriority.UseForeColor = False
        Me.tableCell79.StylePriority.UseTextAlignment = False
        Me.tableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell79.Weight = 0.60591377400921864R
        '
        'tableCell80
        '
        Me.tableCell80.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell80.ForeColor = System.Drawing.Color.Gray
        Me.tableCell80.Name = "tableCell80"
        Me.tableCell80.StylePriority.UseBackColor = False
        Me.tableCell80.StylePriority.UseForeColor = False
        Me.tableCell80.Weight = 0.075605667364347734R
        '
        'tableCell82
        '
        Me.tableCell82.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_ph")})
        Me.tableCell82.Name = "tableCell82"
        Me.tableCell82.Weight = 0.32121505714249338R
        '
        'tableCell83
        '
        Me.tableCell83.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell83.Name = "tableCell83"
        Me.tableCell83.StylePriority.UseForeColor = False
        Me.tableCell83.StylePriority.UseTextAlignment = False
        Me.tableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell83.Weight = 0.54492362899467339R
        '
        'tableRow14
        '
        Me.tableRow14.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell68, Me.tableCell69, Me.tableCell70, Me.tableCell71, Me.tableCell72, Me.tableCell73, Me.tableCell74, Me.tableCell75})
        Me.tableRow14.Name = "tableRow14"
        Me.tableRow14.Weight = 1.0R
        '
        'tableCell68
        '
        Me.tableCell68.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell68.ForeColor = System.Drawing.Color.Gray
        Me.tableCell68.Name = "tableCell68"
        Me.tableCell68.StylePriority.UseBackColor = False
        Me.tableCell68.StylePriority.UseForeColor = False
        Me.tableCell68.Weight = 0.068529753757240991R
        '
        'tableCell69
        '
        Me.tableCell69.ForeColor = System.Drawing.Color.Gray
        Me.tableCell69.Name = "tableCell69"
        Me.tableCell69.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell69.StylePriority.UseForeColor = False
        Me.tableCell69.StylePriority.UsePadding = False
        Me.tableCell69.Text = "Albumin"
        Me.tableCell69.Weight = 0.53296186453764R
        '
        'tableCell70
        '
        Me.tableCell70.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_albumin")})
        Me.tableCell70.Name = "tableCell70"
        Me.tableCell70.Weight = 0.29597668347245232R
        '
        'tableCell71
        '
        Me.tableCell71.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell71.Name = "tableCell71"
        Me.tableCell71.StylePriority.UseForeColor = False
        Me.tableCell71.StylePriority.UseTextAlignment = False
        Me.tableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell71.Weight = 0.60591377400921864R
        '
        'tableCell72
        '
        Me.tableCell72.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell72.ForeColor = System.Drawing.Color.Gray
        Me.tableCell72.Name = "tableCell72"
        Me.tableCell72.StylePriority.UseBackColor = False
        Me.tableCell72.StylePriority.UseForeColor = False
        Me.tableCell72.Weight = 0.075605667364347734R
        '
        'tableCell73
        '
        Me.tableCell73.ForeColor = System.Drawing.Color.Gray
        Me.tableCell73.Name = "tableCell73"
        Me.tableCell73.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell73.StylePriority.UseForeColor = False
        Me.tableCell73.StylePriority.UsePadding = False
        Me.tableCell73.Text = "Glucose"
        Me.tableCell73.Weight = 0.55487357072193333R
        '
        'tableCell74
        '
        Me.tableCell74.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_glucose")})
        Me.tableCell74.Name = "tableCell74"
        Me.tableCell74.Weight = 0.32121505714249338R
        '
        'tableCell75
        '
        Me.tableCell75.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell75.Name = "tableCell75"
        Me.tableCell75.StylePriority.UseForeColor = False
        Me.tableCell75.StylePriority.UseTextAlignment = False
        Me.tableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell75.Weight = 0.54492362899467339R
        '
        'tableRow13
        '
        Me.tableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell60, Me.tableCell61, Me.tableCell62, Me.tableCell63, Me.tableCell64, Me.tableCell65, Me.tableCell66, Me.tableCell67})
        Me.tableRow13.Name = "tableRow13"
        Me.tableRow13.Weight = 1.0R
        '
        'tableCell60
        '
        Me.tableCell60.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell60.ForeColor = System.Drawing.Color.Gray
        Me.tableCell60.Name = "tableCell60"
        Me.tableCell60.StylePriority.UseBackColor = False
        Me.tableCell60.StylePriority.UseForeColor = False
        Me.tableCell60.Weight = 0.068529753757240991R
        '
        'tableCell61
        '
        Me.tableCell61.ForeColor = System.Drawing.Color.Gray
        Me.tableCell61.Name = "tableCell61"
        Me.tableCell61.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell61.StylePriority.UseForeColor = False
        Me.tableCell61.StylePriority.UsePadding = False
        Me.tableCell61.Text = "Blood"
        Me.tableCell61.Weight = 0.53296186453764R
        '
        'tableCell62
        '
        Me.tableCell62.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_blood")})
        Me.tableCell62.Name = "tableCell62"
        Me.tableCell62.Weight = 0.29597668347245232R
        '
        'tableCell63
        '
        Me.tableCell63.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell63.Name = "tableCell63"
        Me.tableCell63.StylePriority.UseForeColor = False
        Me.tableCell63.StylePriority.UseTextAlignment = False
        Me.tableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell63.Weight = 0.60591377400921864R
        '
        'tableCell64
        '
        Me.tableCell64.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell64.ForeColor = System.Drawing.Color.Gray
        Me.tableCell64.Name = "tableCell64"
        Me.tableCell64.StylePriority.UseBackColor = False
        Me.tableCell64.StylePriority.UseForeColor = False
        Me.tableCell64.Weight = 0.075605667364347734R
        '
        'tableCell65
        '
        Me.tableCell65.ForeColor = System.Drawing.Color.Gray
        Me.tableCell65.Name = "tableCell65"
        Me.tableCell65.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell65.StylePriority.UseForeColor = False
        Me.tableCell65.StylePriority.UsePadding = False
        Me.tableCell65.Text = "RBC"
        Me.tableCell65.Weight = 0.55487357072193333R
        '
        'tableCell66
        '
        Me.tableCell66.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_rbc")})
        Me.tableCell66.Name = "tableCell66"
        Me.tableCell66.Weight = 0.32121505714249338R
        '
        'tableCell67
        '
        Me.tableCell67.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell67.Name = "tableCell67"
        Me.tableCell67.StylePriority.UseForeColor = False
        Me.tableCell67.StylePriority.UseTextAlignment = False
        Me.tableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell67.Weight = 0.54492362899467339R
        '
        'tableRow12
        '
        Me.tableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell52, Me.tableCell53, Me.tableCell54, Me.tableCell55, Me.tableCell56, Me.tableCell57, Me.tableCell58, Me.tableCell59})
        Me.tableRow12.Name = "tableRow12"
        Me.tableRow12.Weight = 1.0R
        '
        'tableCell52
        '
        Me.tableCell52.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell52.ForeColor = System.Drawing.Color.Gray
        Me.tableCell52.Name = "tableCell52"
        Me.tableCell52.StylePriority.UseBackColor = False
        Me.tableCell52.StylePriority.UseForeColor = False
        Me.tableCell52.Weight = 0.068529753757240991R
        '
        'tableCell53
        '
        Me.tableCell53.ForeColor = System.Drawing.Color.Gray
        Me.tableCell53.Name = "tableCell53"
        Me.tableCell53.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell53.StylePriority.UseForeColor = False
        Me.tableCell53.StylePriority.UsePadding = False
        Me.tableCell53.Text = "Epi"
        Me.tableCell53.Weight = 0.53296186453764R
        '
        'tableCell54
        '
        Me.tableCell54.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_epi")})
        Me.tableCell54.Name = "tableCell54"
        Me.tableCell54.Weight = 0.29597668347245232R
        '
        'tableCell55
        '
        Me.tableCell55.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell55.Name = "tableCell55"
        Me.tableCell55.StylePriority.UseForeColor = False
        Me.tableCell55.StylePriority.UseTextAlignment = False
        Me.tableCell55.Text = "Cells/HP"
        Me.tableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell55.Weight = 0.60591377400921864R
        '
        'tableCell56
        '
        Me.tableCell56.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell56.ForeColor = System.Drawing.Color.Gray
        Me.tableCell56.Name = "tableCell56"
        Me.tableCell56.StylePriority.UseBackColor = False
        Me.tableCell56.StylePriority.UseForeColor = False
        Me.tableCell56.Weight = 0.075605667364347734R
        '
        'tableCell57
        '
        Me.tableCell57.ForeColor = System.Drawing.Color.Gray
        Me.tableCell57.Name = "tableCell57"
        Me.tableCell57.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell57.StylePriority.UseForeColor = False
        Me.tableCell57.StylePriority.UsePadding = False
        Me.tableCell57.Text = "WBC"
        Me.tableCell57.Weight = 0.55487357072193333R
        '
        'tableCell58
        '
        Me.tableCell58.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_wbc")})
        Me.tableCell58.Name = "tableCell58"
        Me.tableCell58.Weight = 0.32121505714249338R
        '
        'tableCell59
        '
        Me.tableCell59.ForeColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.tableCell59.Name = "tableCell59"
        Me.tableCell59.StylePriority.UseForeColor = False
        Me.tableCell59.StylePriority.UseTextAlignment = False
        Me.tableCell59.Text = "Cells/HP"
        Me.tableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.tableCell59.Weight = 0.54492362899467339R
        '
        'tableRow17
        '
        Me.tableRow17.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell51, Me.tableCell105})
        Me.tableRow17.Name = "tableRow17"
        Me.tableRow17.Weight = 1.0R
        '
        'tableCell51
        '
        Me.tableCell51.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell51.Name = "tableCell51"
        Me.tableCell51.StylePriority.UseBackColor = False
        Me.tableCell51.Weight = 0.068529780039530364R
        '
        'tableCell105
        '
        Me.tableCell105.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_result_remark")})
        Me.tableCell105.Name = "tableCell105"
        Me.tableCell105.Weight = 2.9314702199604694R
        '
        'tableRow18
        '
        Me.tableRow18.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell99})
        Me.tableRow18.Name = "tableRow18"
        Me.tableRow18.Weight = 1.0R
        '
        'tableCell99
        '
        Me.tableCell99.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell99.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tableCell99.Name = "tableCell99"
        Me.tableCell99.StylePriority.UseBackColor = False
        Me.tableCell99.StylePriority.UseFont = False
        Me.tableCell99.StylePriority.UseTextAlignment = False
        Me.tableCell99.Text = "Amphetamine Urine ตรวจหาสารแอมเฟตามีนในปัสสาวะ:"
        Me.tableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.tableCell99.Weight = 3.0R
        '
        'tableRow16
        '
        Me.tableRow16.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell91, Me.tableCell97})
        Me.tableRow16.Name = "tableRow16"
        Me.tableRow16.Weight = 1.0R
        '
        'tableCell91
        '
        Me.tableCell91.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell91.ForeColor = System.Drawing.Color.Gray
        Me.tableCell91.Name = "tableCell91"
        Me.tableCell91.StylePriority.UseBackColor = False
        Me.tableCell91.StylePriority.UseForeColor = False
        Me.tableCell91.Weight = 0.068529753757240991R
        '
        'tableCell97
        '
        Me.tableCell97.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckupblood.urine_amphetamine")})
        Me.tableCell97.Name = "tableCell97"
        Me.tableCell97.Weight = 2.9314702462427591R
        '
        'tableRow19
        '
        Me.tableRow19.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell92})
        Me.tableRow19.Name = "tableRow19"
        Me.tableRow19.Weight = 1.0R
        '
        'tableRow7
        '
        Me.tableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell35, Me.tableCell84, Me.tableCell85, Me.tableCell87, Me.tableCell88, Me.tableCell89})
        Me.tableRow7.Name = "tableRow7"
        Me.tableRow7.Weight = 1.0R
        '
        'tableCell35
        '
        Me.tableCell35.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell35.ForeColor = System.Drawing.Color.Gray
        Me.tableCell35.Name = "tableCell35"
        Me.tableCell35.StylePriority.UseBackColor = False
        Me.tableCell35.StylePriority.UseForeColor = False
        Me.tableCell35.Weight = 0.068529753757240991R
        '
        'tableCell84
        '
        Me.tableCell84.ForeColor = System.Drawing.Color.Gray
        Me.tableCell84.Name = "tableCell84"
        Me.tableCell84.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell84.StylePriority.UseForeColor = False
        Me.tableCell84.StylePriority.UsePadding = False
        Me.tableCell84.Text = "WBC"
        Me.tableCell84.Weight = 0.53296186453764R
        '
        'tableCell85
        '
        Me.tableCell85.Name = "tableCell85"
        Me.tableCell85.Weight = 0.90189025951333779R
        '
        'tableCell87
        '
        Me.tableCell87.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell87.ForeColor = System.Drawing.Color.Gray
        Me.tableCell87.Name = "tableCell87"
        Me.tableCell87.StylePriority.UseBackColor = False
        Me.tableCell87.StylePriority.UseForeColor = False
        Me.tableCell87.Weight = 0.075605865332680877R
        '
        'tableCell88
        '
        Me.tableCell88.ForeColor = System.Drawing.Color.Gray
        Me.tableCell88.Name = "tableCell88"
        Me.tableCell88.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell88.StylePriority.UseForeColor = False
        Me.tableCell88.StylePriority.UsePadding = False
        Me.tableCell88.Text = "RBC"
        Me.tableCell88.Weight = 0.55487357072193333R
        '
        'tableCell89
        '
        Me.tableCell89.Name = "tableCell89"
        Me.tableCell89.Weight = 0.86613868613716671R
        '
        'tableRow21
        '
        Me.tableRow21.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell103, Me.tableCell104, Me.tableCell106, Me.tableCell108, Me.tableCell109, Me.tableCell110})
        Me.tableRow21.Name = "tableRow21"
        Me.tableRow21.Weight = 1.0R
        '
        'tableCell103
        '
        Me.tableCell103.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell103.ForeColor = System.Drawing.Color.Gray
        Me.tableCell103.Name = "tableCell103"
        Me.tableCell103.StylePriority.UseBackColor = False
        Me.tableCell103.StylePriority.UseForeColor = False
        Me.tableCell103.Weight = 0.068529753757240991R
        '
        'tableCell104
        '
        Me.tableCell104.ForeColor = System.Drawing.Color.Gray
        Me.tableCell104.Name = "tableCell104"
        Me.tableCell104.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell104.StylePriority.UseForeColor = False
        Me.tableCell104.StylePriority.UsePadding = False
        Me.tableCell104.Text = "Ova Parasites"
        Me.tableCell104.Weight = 0.53296172748264015R
        '
        'tableCell106
        '
        Me.tableCell106.Name = "tableCell106"
        Me.tableCell106.Weight = 0.90189076204833751R
        '
        'tableCell109
        '
        Me.tableCell109.ForeColor = System.Drawing.Color.Gray
        Me.tableCell109.Name = "tableCell109"
        Me.tableCell109.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell109.StylePriority.UseForeColor = False
        Me.tableCell109.StylePriority.UsePadding = False
        Me.tableCell109.Text = "Occult Blood"
        Me.tableCell109.Weight = 0.55487357072193333R
        '
        'tableCell110
        '
        Me.tableCell110.Name = "tableCell110"
        Me.tableCell110.Weight = 0.86613868613716671R
        '
        'tableRow20
        '
        Me.tableRow20.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.tableCell93, Me.tableCell94, Me.tableCell101})
        Me.tableRow20.Name = "tableRow20"
        Me.tableRow20.Weight = 1.0R
        '
        'tableCell93
        '
        Me.tableCell93.BackColor = System.Drawing.SystemColors.Highlight
        Me.tableCell93.ForeColor = System.Drawing.Color.Gray
        Me.tableCell93.Name = "tableCell93"
        Me.tableCell93.StylePriority.UseBackColor = False
        Me.tableCell93.StylePriority.UseForeColor = False
        Me.tableCell93.Weight = 0.068529753757240991R
        '
        'tableCell94
        '
        Me.tableCell94.ForeColor = System.Drawing.Color.Gray
        Me.tableCell94.Name = "tableCell94"
        Me.tableCell94.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100.0!)
        Me.tableCell94.StylePriority.UseForeColor = False
        Me.tableCell94.StylePriority.UsePadding = False
        Me.tableCell94.Text = "Stool Culture ตรวจอุจจาระเพาะเชื้อ"
        Me.tableCell94.Weight = 1.4348520174526445R
        '
        'tableCell101
        '
        Me.tableCell101.Name = "tableCell101"
        Me.tableCell101.Weight = 1.4966182287901142R
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.HeightF = 577.2136!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrPageInfo1, Me.XrPageInfo2})
        Me.TopMargin.HeightF = 47.17262!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1})
        Me.BottomMargin.HeightF = 46.16732!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DtCheckUP1
        '
        Me.DtCheckUP1.DataSetName = "dtCheckUP"
        Me.DtCheckUP1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CalculatedField1
        '
        Me.CalculatedField1.DataMember = "frncheckup"
        Me.CalculatedField1.DisplayName = "ชื่อนามสกุล"
        Me.CalculatedField1.Expression = "[stprename]+' '+[name] +' '+ [lastname]"
        Me.CalculatedField1.Name = "CalculatedField1"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.CalculatedField1")})
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(100.4491!, 0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(276.0417!, 23.0!)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "frncheckup.hn")})
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel3
        '
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0!, 24.17262!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(151.7419!, 23.0!)
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Medical Examination"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(151.7419!, 24.17262!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(256.7165!, 23.0!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(408.4583!, 24.17262!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'rptCheckUpA5_2
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField1})
        Me.DataMember = "frncheckupblood"
        Me.DataSource = Me.DtCheckUP1
        Me.Margins = New System.Drawing.Printing.Margins(42, 40, 47, 46)
        Me.PageHeight = 827
        Me.PageWidth = 583
        Me.PaperKind = System.Drawing.Printing.PaperKind.A5
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "15.1"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtCheckUP1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents tableCell76 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell92 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell108 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell81 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell40 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell31 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell36 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell22 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell23 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell17 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell32 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell21 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell37 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell33 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell24 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell25 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell27 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell39 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell28 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell29 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell49 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell48 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell50 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell41 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell43 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell44 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell45 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell46 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell47 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow15 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell77 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell78 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell79 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell80 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell82 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell83 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow14 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell68 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell69 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell70 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell71 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell72 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell73 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell74 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell75 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell60 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell61 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell62 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell63 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell64 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell65 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell66 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell67 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell52 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell53 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell54 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell55 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell56 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell57 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell58 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell59 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow17 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell51 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell105 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow18 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell99 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow16 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell91 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell97 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow19 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell35 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell84 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell85 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell87 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell88 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell89 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow21 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell103 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell104 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell106 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell109 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell110 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableRow20 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents tableCell93 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell94 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents tableCell101 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DtCheckUP1 As dtCheckUP
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents CalculatedField1 As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
End Class
