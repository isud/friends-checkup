﻿Public Class setting

    Private Sub setting_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtDBPASSWORD.Text = My.Settings.DBPASSWORD
        txtDBPORT.Text = My.Settings.DBPORT
        txtDBSCHEMA.Text = My.Settings.DBSCHEMA
        txtDBUSER.Text = My.Settings.DBUSER
        txtIPDB.Text = My.Settings.IPDB
    End Sub

    Private Sub btnEditSetting_Click(sender As Object, e As EventArgs) Handles btnEditSetting.Click
        Try
            My.Settings.DBPASSWORD = txtDBPASSWORD.Text
            My.Settings.DBPORT = txtDBPORT.Text
            My.Settings.DBSCHEMA = txtDBSCHEMA.Text
            My.Settings.DBUSER = txtDBUSER.Text
            My.Settings.IPDB = txtIPDB.Text

            MsgBox("แก้ไขการตั้งค่าเรียบร้อยแล้ว", MsgBoxStyle.Information)

            Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
End Class